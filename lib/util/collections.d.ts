export interface IUniqueItem {
    id: string;
}
export declare class QueueOfUniqueItems<T extends IUniqueItem> {
    private map;
    private list;
    constructor();
    addAll(items: T[]): void;
    add(item: T): void;
    removeItemWithId(itemId: string): void;
    hasItemWithId(itemId: string): boolean;
    getItemWithId(itemId: string): T;
    readonly items: T[];
}
export declare class FIFOList<T> {
    private maxNumOfItems;
    private _items;
    constructor(maxNumOfItems: number);
    addAsFirst(newItem: T): void;
    addAsLast(newItem: T): void;
    readonly items: T[];
}
