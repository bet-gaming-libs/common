export declare function sleep(forMilliseconds: number): Promise<void>;
export interface ITimerListener {
    onTimerTick(): void;
}
export declare class CustomTimer {
    private intervalInMilliseconds;
    private listener;
    private intervalId;
    constructor(intervalInMilliseconds: number, listener: ITimerListener);
    start(): void;
    private tick;
    stop(): void;
    private readonly isRunning;
}
