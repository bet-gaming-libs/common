import { IError, CustomPromise } from './promise';
export interface IResponse<T> {
    error?: IError;
    result?: T;
}
export declare type ResponseHandler<T> = (response: IResponse<T>) => void;
export declare class ResponseManager<T> {
    private promise;
    static newHandler<T>(promise: CustomPromise<T>): ResponseHandler<T>;
    private constructor();
    handler: (response: IResponse<T>) => void;
}
export declare class ResponseDispatcher<T> {
    private handler;
    static create<T>(handler: ResponseHandler<T>): ResponseDispatcher<T>;
    private constructor();
    dispatchResult(result: T): void;
    dispatchError(code: string, data?: any): void;
}
