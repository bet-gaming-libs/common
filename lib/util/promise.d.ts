export interface IError {
    code: string;
    data?: any;
}
export declare type ResultHandler<T> = (result: T) => void;
export declare type ErrorHandler = (error: IError) => void;
export interface IPromise<T> {
    then(resultHandler: ResultHandler<T>): IPromise<T>;
    catch(errorHandler: ErrorHandler): IPromise<T>;
}
export declare class CustomPromise<T> implements IPromise<T> {
    private resultHandler;
    private errorHandler;
    constructor();
    then(resultHandler: ResultHandler<T>): this;
    catch(errorHandler: ErrorHandler): this;
    done(result: T): void;
    fail(error: IError): void;
}
