export declare function chooseRandomElement<T>(all: T[]): T;
export declare function getRandomNumberBetwen0AndMax(max: number): number;
export declare function getRandomNumberBetwen1AndMax(max: number): number;
export declare function getRandomNumberWithinRange(min: number, max: number): number;
