export declare function getRandomUniqueElements<T>(array: T[], numOfElements: number): T[];
export declare function removeItemFromArray<T extends number | string>(item: T, array: T[]): void;
export declare function shuffleArray<T>(items: T[]): T[];
export declare function splitArrayIntoTwo<T>(items: T[], numOfItemsInFirstPart: number): T[][];
export declare function splitArrayIntoMany<T>(source: T[], maxNumOfItemsPerPart: number): T[][];
