export declare function roundDownToNumber(amount: number, precision: number): number;
export declare function roundDownToString(amount: number, precision: number): string;
export declare function roundStringDownToString(amount: string, precision: number): string;
