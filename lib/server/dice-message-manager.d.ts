import { SocketMessageManager } from './message-manager';
import { ISocket } from './interfaces';
export declare abstract class DiceMessageManager extends SocketMessageManager {
    constructor(socket: ISocket, messageReceiver: any);
    protected sendDiceMessage(messageName: string, ...args: any[]): void;
}
