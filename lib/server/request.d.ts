import { CustomPromise, IPromise } from '../util/promise';
import { IEmitter } from './interfaces';
export declare function makeRequest<T>(messagePrefix: string, messageName: string, ars: any, socket: IEmitter, promise?: CustomPromise<T>): IPromise<T>;
