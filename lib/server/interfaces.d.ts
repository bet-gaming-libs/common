export interface IEmitter {
    emit(event: string, ...args: any[]): any;
}
/*** @doNotMinify */
export interface ISocket extends IEmitter {
    on(event: string, fn: Function): ISocket;
}
export interface IServerConfig {
    host: string;
    port: number;
    ssl: boolean;
}
