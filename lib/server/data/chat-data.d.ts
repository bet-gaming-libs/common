export declare const chatLoginMessagePrefix = "My Chat Public Key is: ";
export interface IChatAuthenticationData {
    isEasyAccount: boolean;
    chatPublicKey: string;
    eosAccountName: string;
    easyAccountId: string;
    signature: string;
    isSignedDataHased: boolean;
    nonce: string;
}
export interface IChatMessageData {
    chatPublicKey: string;
    message: string;
    nonce: string;
    signature: string;
}
export interface IValidatedChatMessage {
    from: string;
    message: string;
    time: number;
}
