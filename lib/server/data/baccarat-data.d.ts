import { ICardData } from "./card-data";
import { IResolvedBet } from "./common";
export interface IHandData {
    cards: ICardData[];
    totalValue: number;
}
export interface IBaccaratGameResultDataForMultiPlayer {
    gameId: string;
    winner: number;
    hands: IHandData[];
}
export interface IBaccaratGameResultDataForSinglePlayer {
    winner: number;
    hands: IHandData[];
}
export interface IBaccaratGameStartData {
    currentGameId: string;
    gameStartTime: number;
    currentServerTime: number;
}
export interface IBaccaratBetDataForSinglePlayer {
    id: string;
    bettor: string;
    referral: string;
    referral_data: string;
    bet_amt: string;
    player_bet_amt: string;
    banker_bet_amt: string;
    tie_bet_amt: string;
    playerpair_bet_amt: string;
    bankerpair_bet_amt: string;
    seed: string;
}
export interface IResolvedBaccaratBet extends IResolvedBet {
    bet: IBaccaratBetDataForSinglePlayer;
    result: IBaccaratGameResultDataForSinglePlayer;
}
