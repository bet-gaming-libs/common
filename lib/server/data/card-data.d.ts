export declare enum CardSuit {
    Hearts = 0,
    Diamonds = 1,
    Spades = 2,
    Clubs = 3
}
export interface ICardData {
    suit: CardSuit;
    rankName: string;
}
export declare class CardRankNumber {
    static ACE: number;
    static TEN: number;
    static JACK: number;
    static QUEEN: number;
    static KING: number;
}
export declare class CardRankName {
    static ACE: string;
    static JACK: string;
    static QUEEN: string;
    static KING: string;
}
export declare function numberToSuit(value: number): CardSuit;
export declare function nameOfSuit(value: CardSuit): string;
export declare function nameOfRank(value: number): string;
