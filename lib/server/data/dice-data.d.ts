import { IResolvedBet } from "./common";
export interface IResolvedDiceBet extends IResolvedBet {
    isRollOver: boolean;
    number: number;
    roll: number;
}
export interface IDiceGameData {
    recentBets: IResolvedDiceBet[];
}
