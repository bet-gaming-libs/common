import { IResolvedBet } from "./common";
export interface IResolvedHiloBet extends IResolvedBet {
    betType: string;
    startCard: number;
    nextCard: number;
}
