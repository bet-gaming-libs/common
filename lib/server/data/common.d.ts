export interface IResolvedBet {
    id: string;
    isEasyAccount: boolean;
    bettor: string;
    amount: string;
    resolvedMilliseconds: number;
    referrer: string;
    jackpotSpin: string;
    stakeWeight: string;
}
