import { IResolvedBet } from "./common";
export interface IBlackjackInitialCards {
    dealerCards: number[];
    playerCards: number[];
}
export interface IInitialBlackjackBetInfo {
    amountAsInteger: number;
    token: {
        name: string;
        precision: number;
    };
}
export interface IBlackjackGameState {
    id: string;
    initialBet: IInitialBlackjackBetInfo;
    initialCards: IBlackjackInitialCards;
    isDealerBlackjack: boolean;
    commands: string[];
    additionalPlayerCards: number[];
    additionalDealerCards: number[];
}
export interface IBlackjackHandResult {
    sum: number;
    isBlackjack: boolean;
}
export interface IBlackjackPlayerHandResult extends IBlackjackHandResult {
    totalWagered: number;
    payout: number;
}
export interface IResolvedBlackjackBet extends IResolvedBet {
    dealerHandResult: IBlackjackHandResult;
    playerResults: IBlackjackHandResult[];
    totalWagered: string;
    totalPayout: string;
}
