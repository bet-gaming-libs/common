import { IResolvedBet } from "./common";
export interface IActiveCrashBet {
    id: string;
    bettor: string;
    referral: string;
    referral_data: string;
    bet_amt: string;
    max_win: string;
    adv_tap_out_times100: number;
    seed: string;
    signing_key: string;
}
export interface IResolvedCrashBet extends IResolvedBet {
}
export interface ITapOutRequestData {
    betId: string;
    signature: string;
}
export interface ITappedOutMessageData {
    betId: string;
    value: number;
}
export interface ICrashResultData {
    cashOutPoint: number;
    /** isInsant = value == 0 **/
    crashPoint: number;
    isMaxLiabilitiesReached: boolean;
    isEndedEarly: boolean;
}
export interface ICrashMessageData {
    bet: IResolvedCrashBet;
    result: ICrashResultData;
}
export interface ICrashGameStateData {
    currentGameId: number;
    isMoving: boolean;
    secondsBeforeStart: number;
    currentPoint: number;
}
