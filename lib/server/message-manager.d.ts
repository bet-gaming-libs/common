import { IPromise } from '../util/promise';
import { ISocket, IEmitter } from './interfaces';
export declare class MessageManager {
    protected emitter: IEmitter;
    protected messagePrefix: string;
    constructor(emitter: IEmitter, messagePrefix: string);
    sendMessage(messageName: string, ...args: any[]): string[];
}
export declare class SocketMessageManager extends MessageManager {
    private socket;
    private messageReceiver;
    constructor(socket: ISocket, messagePrefix: string, messageReceiver: any);
    private setupMessageListener;
    protected makeRequest<T>(messageName: string, ...args: any[]): IPromise<T>;
}
