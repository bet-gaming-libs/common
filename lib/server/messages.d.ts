export declare class DiceMessage {
    static PREFIX: string;
    static GET_REFERRER: string;
    static SAVE_REFERRER: string;
    static SAVE_REFERRER_FOR_WAX: string;
    static RESOLVE_BET: string;
    static SYNC_GAME_DATA: string;
    static SYNC_BANKROLL: string;
    static NEW_RESOLVED_BET: string;
}
export declare class ChatMessage {
    static PREFIX: string;
    static AUTHENTICATE: string;
    static GET_RECENT_MESSAGES: string;
    static CHAT: string;
}
export declare class BaccaratMessage {
    static PREFIX: string;
    static BET_RESULT: string;
}
export declare class CrashMessage {
    static PREFIX: string;
    static TAP_OUT: string;
    static START_GAME: string;
    static TAPPED_OUT: string;
    static CRASH: string;
    static RESOLVE_BET: string;
    static RESOLVE_BET_RESULT: string;
}
export declare class HiloMessage {
    static PREFIX: string;
    static RESOLVE_BET_RESULT: string;
}
export declare class BlackjackMessage {
    static PREFIX: string;
    static GAME_COMMAND: string;
    static RESOLVE_BET_RESULT: string;
}
export declare class BlackjackGameCommand {
    static RESUME_GAME: string;
    static START_GAME: string;
    static YES_TO_INSURANCE: string;
    static NO_TO_INSURANCE: string;
    static PEEK_FOR_BLACKJACK: string;
    static SPLIT: string;
    static HIT: string;
    static DOUBLE_DOWN: string;
    static STAND: string;
    static GET_DEALERS_CARDS: string;
}
export declare function dispatchMessage(args: any, client: any): void;
