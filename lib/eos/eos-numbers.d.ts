import { AssetMath } from './int-math';
export interface IEOSAssetAmount {
    symbol: string;
    contract: string;
    integer: number;
    quantity: string;
}
export declare class EOSAssetAmount extends AssetMath {
    readonly symbol: string;
    readonly contract: string;
    constructor(precision: number, symbol: string, contract: string, decimalValue?: string);
    readonly quantity: string;
}
