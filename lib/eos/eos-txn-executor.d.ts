export interface IEosTransactionListener {
    onEosTransactionConfirmed(txnId: string): any;
}
export declare type TxnConfirmedChecker = (txnId: string) => Promise<boolean>;
