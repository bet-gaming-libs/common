import { ITableRowQueryParameters } from "./eos-data";
export declare function getBetIdFromTransactionId(txnId: string): string;
export declare function executeTransactionUntilSuccessful(eos: any, actions: ITransactionAction[]): Promise<string>;
export interface IActionAuthorization {
    actor: string;
    permission: string;
}
export interface ITransactionAction {
    account: string;
    name: string;
    authorization: IActionAuthorization[];
    data: any;
}
export declare function executeTransaction(eos: any, actions: ITransactionAction[]): Promise<string>;
export declare function getAllTableRows<T>(eos: any, code: string, scope: string, table: string, table_key: string): Promise<T[]>;
export declare function getTokenBalance(eos: any, tokenContractAccount: string, tokenHolderAccount: string, tokenSymbol: string, table?: string): Promise<string>;
export declare function getGlobalVariable(eos: any, smartContractAccount: string, variableId: number): Promise<string>;
export declare function getAccountIdForActiveEasyAccountBet(eos: any, easyAccountContract: string, betId: string): Promise<string>;
export declare function getUniqueTableRow<T>(eos: any, code: string, scope: string, table: string, rowId: string): Promise<T[]>;
export declare function getTableRows<T>(eos: any, parameters: ITableRowQueryParameters): Promise<T[]>;
export interface ITableRowQueryResult<T> {
    rows: T[];
    more: boolean;
}
export declare function getTableRowsResult<T>(eos: any, parameters: ITableRowQueryParameters): Promise<ITableRowQueryResult<T>>;
