export interface IEosConfigParameters {
    httpEndpoint: string;
    chainId: string;
    keyProvider: string;
    verbose: boolean;
}
export interface ITableRowQueryParameters {
    json?: boolean;
    code: string;
    scope: string;
    table: string;
    limit?: number;
    table_key?: string;
    key_type?: string;
    index_position?: string;
    lower_bound?: number | string;
    upper_bound?: number | string;
}
export interface IGlobalVariableRow {
    id: number;
    val: number;
}
export interface ITokenBalanceRow {
    balance: string;
}
export interface ITransactionResult {
    transaction_id: string;
}
export interface IEasyAccountCurrencyBalanceRow {
    quantity: string;
    amt_wagered: string;
}
export interface IEasyAccountBetBalanceRow {
    id: string;
    quantity: string;
    claimed_divs: string[];
}
export interface IEasyAccountInfoRow {
    id: string;
    key: string;
    nonce: number;
    email_backup: number;
}
export interface IEasyAccountActiveBet {
    key_id: string;
}
export interface IEosActionData {
    account: string;
    name: string;
    data: any;
}
export interface IEosActionAuthorization {
    actor: string;
    permission: string;
}
export interface IEosTransactionAction extends IEosActionData {
    authorization: IEosActionAuthorization[];
}
export interface ISignatureInfo {
    nonce: string;
    signature: string;
    isSignedDataHashed: boolean;
}
