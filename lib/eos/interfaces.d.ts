export interface IEosTokenTransferData {
    from: string;
    to: string;
    quantity: string;
    memo: string;
}
