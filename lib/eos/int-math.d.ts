export declare class AssetMath {
    private _integer;
    readonly precision: number;
    private _coin_name;
    constructor(precision: number, coin_name: string);
    initWithInteger(intVal: string | number): void;
    initWithDecimal(intDec: string | number): void;
    decToInt(valDecimal: string | number): number;
    addInt(valInteger: string | number): this;
    addDec(valDecimal: string | number): this;
    subInt(valInteger: string | number): this;
    subDec(valDecimal: string | number): this;
    divInt(valInteger: string | number): this;
    divDec(valDecimal: string | number): this;
    multInt(valInteger: string | number): this;
    multDec(valDecimal: string | number): this;
    readonly assetString: string;
    readonly decimal: string;
    toDecimalString(): string;
    readonly integer: number;
    readonly coinName: string;
}
