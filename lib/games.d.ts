export declare enum GameId {
    DICE = 1,
    CRASH = 2,
    BACCARAT = 3,
    HILO = 4,
    BLACKJACK = 5
}
