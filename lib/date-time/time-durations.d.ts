import { ITimeDuration } from './interfaces';
export declare class Milliseconds {
    readonly milliseconds: number;
    constructor(milliseconds: number);
    add(other: ITimeDuration): ITimeDuration;
    subtract(other: ITimeDuration): ITimeDuration;
}
export declare class Seconds extends Milliseconds implements ITimeDuration {
    constructor(seconds: number);
}
export declare class Minutes extends Milliseconds implements ITimeDuration {
    constructor(minutes: number);
}
export declare class Hours extends Minutes implements ITimeDuration {
    constructor(hours: number);
}
