export interface ITimeDuration {
    milliseconds: number;
}
