import { ITimeDuration } from './interfaces';
export declare class TimeFrame {
    readonly start: Date;
    readonly end: Date;
    constructor(start: Date, duration: ITimeDuration);
    toString(): string;
}
