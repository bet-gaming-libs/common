export declare function NOW(): string;
export declare function toDateTimeFormat(date: Date): string;
export declare function getRandomDateWithinRange(start: Date, end: Date): Date;
export declare function getStartOfToday(): Date;
export declare function getEndOfToday(): Date;
export declare enum Month {
    JAN = 0,
    FEB = 1,
    MAR = 2,
    APR = 3,
    MAY = 4,
    JUN = 5,
    JUL = 6,
    AUG = 7,
    SEP = 8,
    OCT = 9,
    NOV = 10,
    DEC = 11
}
