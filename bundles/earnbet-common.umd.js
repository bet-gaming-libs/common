(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('big.js')) :
    typeof define === 'function' && define.amd ? define('earnbet-common', ['exports', 'big.js'], factory) :
    (global = global || self, factory(global['earnbet-common'] = {}, global.Big));
}(this, (function (exports, Big) { 'use strict';

    Big = Big && Big.hasOwnProperty('default') ? Big['default'] : Big;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/array.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     * @param {?} array
     * @param {?} numOfElements
     * @return {?}
     */
    function getRandomUniqueElements(array, numOfElements) {
        /** @type {?} */
        var elements = [];
        /** @type {?} */
        var source = array.slice();
        for (var i = 0; i < numOfElements; i++) {
            /** @type {?} */
            var index = getRandomIndex(source);
            elements.push(source[index]);
            source.splice(index, 1);
        }
        return elements;
    }
    /**
     * @param {?} array
     * @return {?}
     */
    function getRandomIndex(array) {
        return Math.round(Math.random() * (array.length - 1));
    }
    /**
     * @template T
     * @param {?} item
     * @param {?} array
     * @return {?}
     */
    function removeItemFromArray(item, array) {
        for (var i = 0; i < array.length; i++) {
            if (item == array[i]) {
                array.splice(i, 1);
                return;
            }
        }
    }
    /**
     * @template T
     * @param {?} items
     * @return {?}
     */
    function shuffleArray(items) {
        /** @type {?} */
        var clone = items.slice();
        clone.sort((/**
         * @return {?}
         */
        function () { return 0.5 - Math.random(); }));
        return clone;
    }
    /**
     * @template T
     * @param {?} items
     * @param {?} numOfItemsInFirstPart
     * @return {?}
     */
    function splitArrayIntoTwo(items, numOfItemsInFirstPart) {
        /** @type {?} */
        var part1 = items.slice(0, numOfItemsInFirstPart);
        /** @type {?} */
        var part2 = items.slice(numOfItemsInFirstPart);
        return [
            part1,
            part2
        ];
    }
    /**
     * @template T
     * @param {?} source
     * @param {?} maxNumOfItemsPerPart
     * @return {?}
     */
    function splitArrayIntoMany(source, maxNumOfItemsPerPart) {
        /** @type {?} */
        var items = source.slice();
        /** @type {?} */
        var parts = [];
        while (items.length > 0) {
            /** @type {?} */
            var part = items.splice(0, items.length > maxNumOfItemsPerPart ?
                maxNumOfItemsPerPart :
                items.length);
            parts.push(part);
        }
        return parts;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/promise.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IError() { }
    if (false) {
        /** @type {?} */
        IError.prototype.code;
        /** @type {?|undefined} */
        IError.prototype.data;
    }
    /**
     * @record
     * @template T
     */
    function IPromise() { }
    if (false) {
        /**
         * @param {?} resultHandler
         * @return {?}
         */
        IPromise.prototype.then = function (resultHandler) { };
        /**
         * @param {?} errorHandler
         * @return {?}
         */
        IPromise.prototype.catch = function (errorHandler) { };
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    CustomPromise = /** @class */ (function () {
        function CustomPromise() {
        }
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} resultHandler
         * @return {THIS}
         */
        CustomPromise.prototype.then = /**
         * @template THIS
         * @this {THIS}
         * @param {?} resultHandler
         * @return {THIS}
         */
        function (resultHandler) {
            (/** @type {?} */ (this)).resultHandler = resultHandler;
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} errorHandler
         * @return {THIS}
         */
        CustomPromise.prototype.catch = /**
         * @template THIS
         * @this {THIS}
         * @param {?} errorHandler
         * @return {THIS}
         */
        function (errorHandler) {
            (/** @type {?} */ (this)).errorHandler = errorHandler;
            return (/** @type {?} */ (this));
        };
        /**
         * @param {?} result
         * @return {?}
         */
        CustomPromise.prototype.done = /**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            if (this.resultHandler)
                this.resultHandler(result);
        };
        /**
         * @param {?} error
         * @return {?}
         */
        CustomPromise.prototype.fail = /**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            if (this.errorHandler)
                this.errorHandler(error);
        };
        return CustomPromise;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CustomPromise.prototype.resultHandler;
        /**
         * @type {?}
         * @private
         */
        CustomPromise.prototype.errorHandler;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/url.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} name
     * @return {?}
     */
    function getUrlParameterByName(name) {
        /** @type {?} */
        var url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        /** @type {?} */
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
        /** @type {?} */
        var results = regex.exec(url);
        if (!results)
            return null;
        if (!results[2])
            return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/timer.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} forMilliseconds
     * @return {?}
     */
    function sleep(forMilliseconds) {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) { return setTimeout(resolve, forMilliseconds); }));
    }
    /**
     * @record
     */
    function ITimerListener() { }
    if (false) {
        /**
         * @return {?}
         */
        ITimerListener.prototype.onTimerTick = function () { };
    }
    var CustomTimer = /** @class */ (function () {
        function CustomTimer(intervalInMilliseconds, listener) {
            var _this = this;
            this.intervalInMilliseconds = intervalInMilliseconds;
            this.listener = listener;
            this.intervalId = undefined;
            this.tick = (/**
             * @return {?}
             */
            function () {
                _this.listener.onTimerTick();
            });
        }
        /**
         * @return {?}
         */
        CustomTimer.prototype.start = /**
         * @return {?}
         */
        function () {
            if (this.isRunning) {
                return;
            }
            this.intervalId = setInterval(this.tick, this.intervalInMilliseconds);
        };
        /**
         * @return {?}
         */
        CustomTimer.prototype.stop = /**
         * @return {?}
         */
        function () {
            if (!this.isRunning) {
                return;
            }
            clearInterval(this.intervalId);
            this.intervalId = undefined;
        };
        Object.defineProperty(CustomTimer.prototype, "isRunning", {
            get: /**
             * @private
             * @return {?}
             */
            function () {
                return this.intervalId != undefined;
            },
            enumerable: true,
            configurable: true
        });
        return CustomTimer;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        CustomTimer.prototype.intervalId;
        /**
         * @type {?}
         * @private
         */
        CustomTimer.prototype.tick;
        /**
         * @type {?}
         * @private
         */
        CustomTimer.prototype.intervalInMilliseconds;
        /**
         * @type {?}
         * @private
         */
        CustomTimer.prototype.listener;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/collections.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IUniqueItem() { }
    if (false) {
        /** @type {?} */
        IUniqueItem.prototype.id;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    QueueOfUniqueItems = /** @class */ (function () {
        function QueueOfUniqueItems() {
            this.map = {};
            this.list = [];
        }
        /**
         * @param {?} items
         * @return {?}
         */
        QueueOfUniqueItems.prototype.addAll = /**
         * @param {?} items
         * @return {?}
         */
        function (items) {
            var e_1, _a;
            try {
                for (var items_1 = __values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                    var item = items_1_1.value;
                    this.add(item);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        };
        /**
         * @param {?} item
         * @return {?}
         */
        QueueOfUniqueItems.prototype.add = /**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (!this.hasItemWithId(item.id)) {
                this.map[item.id] = item;
                this.list.push(item);
            }
        };
        /**
         * @param {?} itemId
         * @return {?}
         */
        QueueOfUniqueItems.prototype.removeItemWithId = /**
         * @param {?} itemId
         * @return {?}
         */
        function (itemId) {
            delete this.map[itemId];
            for (var i = 0; i < this.list.length; i++) {
                /** @type {?} */
                var item = this.list[i];
                if (itemId == item.id) {
                    this.list.splice(i, 1);
                    break;
                }
            }
        };
        /**
         * @param {?} itemId
         * @return {?}
         */
        QueueOfUniqueItems.prototype.hasItemWithId = /**
         * @param {?} itemId
         * @return {?}
         */
        function (itemId) {
            return this.map[itemId] != undefined;
        };
        /**
         * @param {?} itemId
         * @return {?}
         */
        QueueOfUniqueItems.prototype.getItemWithId = /**
         * @param {?} itemId
         * @return {?}
         */
        function (itemId) {
            return this.map[itemId];
        };
        Object.defineProperty(QueueOfUniqueItems.prototype, "items", {
            get: /**
             * @return {?}
             */
            function () {
                return this.list.slice();
            },
            enumerable: true,
            configurable: true
        });
        return QueueOfUniqueItems;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        QueueOfUniqueItems.prototype.map;
        /**
         * @type {?}
         * @private
         */
        QueueOfUniqueItems.prototype.list;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    FIFOList = /** @class */ (function () {
        function FIFOList(maxNumOfItems) {
            this.maxNumOfItems = maxNumOfItems;
            this._items = [];
        }
        /**
         * @param {?} newItem
         * @return {?}
         */
        FIFOList.prototype.addAsFirst = /**
         * @param {?} newItem
         * @return {?}
         */
        function (newItem) {
            /** @type {?} */
            var end = this.items.slice(0, this.maxNumOfItems - 1);
            this._items = [newItem].concat(end);
        };
        /**
         * @param {?} newItem
         * @return {?}
         */
        FIFOList.prototype.addAsLast = /**
         * @param {?} newItem
         * @return {?}
         */
        function (newItem) {
            /** @type {?} */
            var start = [];
            if (this.items.length < this.maxNumOfItems) {
                start = this.items.slice();
            }
            else {
                start = this.items.slice(1, this.maxNumOfItems);
            }
            /** @type {?} */
            var merged = start.concat([newItem]);
            this._items = merged;
        };
        Object.defineProperty(FIFOList.prototype, "items", {
            get: /**
             * @return {?}
             */
            function () {
                return this._items;
            },
            enumerable: true,
            configurable: true
        });
        return FIFOList;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        FIFOList.prototype._items;
        /**
         * @type {?}
         * @private
         */
        FIFOList.prototype.maxNumOfItems;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/response.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     * @template T
     */
    function IResponse() { }
    if (false) {
        /** @type {?|undefined} */
        IResponse.prototype.error;
        /** @type {?|undefined} */
        IResponse.prototype.result;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    ResponseManager = /** @class */ (function () {
        // instance
        function ResponseManager(promise) {
            var _this = this;
            this.promise = promise;
            this.handler = (/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                if (response.error)
                    _this.promise.fail(response.error);
                if (response.result)
                    _this.promise.done(response.result);
            });
        }
        // class
        // class
        /**
         * @template T
         * @param {?} promise
         * @return {?}
         */
        ResponseManager.newHandler = 
        // class
        /**
         * @template T
         * @param {?} promise
         * @return {?}
         */
        function (promise) {
            return new ResponseManager(promise).handler;
        };
        return ResponseManager;
    }());
    if (false) {
        /** @type {?} */
        ResponseManager.prototype.handler;
        /**
         * @type {?}
         * @private
         */
        ResponseManager.prototype.promise;
    }
    /**
     * @template T
     */
    var   /**
     * @template T
     */
    ResponseDispatcher = /** @class */ (function () {
        // instance
        function ResponseDispatcher(handler) {
            this.handler = handler;
        }
        // class
        // class
        /**
         * @template T
         * @param {?} handler
         * @return {?}
         */
        ResponseDispatcher.create = 
        // class
        /**
         * @template T
         * @param {?} handler
         * @return {?}
         */
        function (handler) {
            return new ResponseDispatcher(handler);
        };
        /**
         * @param {?} result
         * @return {?}
         */
        ResponseDispatcher.prototype.dispatchResult = /**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            this.handler({
                result: result
            });
        };
        /**
         * @param {?} code
         * @param {?=} data
         * @return {?}
         */
        ResponseDispatcher.prototype.dispatchError = /**
         * @param {?} code
         * @param {?=} data
         * @return {?}
         */
        function (code, data) {
            this.handler({
                error: {
                    code: code,
                    data: data
                }
            });
        };
        return ResponseDispatcher;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        ResponseDispatcher.prototype.handler;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/log.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} message
     * @return {?}
     */
    function debugMessage(message) {
        /** @type {?} */
        var date = new Date();
        console.log((date.getMonth() + 1) + '-' +
            date.getDate() + ' ' +
            date.getHours() + ':' +
            date.getMinutes() + ':' +
            date.getSeconds() + '.' +
            date.getMilliseconds()
            + ' - ' +
            message);
    }
    /**
     * @param {?=} message
     * @param {...?} optionalParams
     * @return {?}
     */
    function logWithTime(message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        /** @type {?} */
        var date = new Date();
        /** @type {?} */
        var dateString = (date.getMonth() + 1) + '-' +
            date.getDate() + ' ' +
            date.getHours() + ':' +
            date.getMinutes() + ':' +
            date.getSeconds() + '.' +
            date.getMilliseconds();
        console.log.apply(console, __spread([dateString
            //+ ' - ' + message
            ,
            message], optionalParams));
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/string-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} sString
     * @return {?}
     */
    function trimString(sString) {
        while (sString.substring(0, 1) == ' ') {
            sString = sString.substring(1, sString.length);
        }
        while (sString.substring(sString.length - 1, sString.length) == ' ') {
            sString = sString.substring(0, sString.length - 1);
        }
        return sString;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/math-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundDownToNumber(amount, precision) {
        return Number(roundDownToString(amount, precision));
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundDownToString(amount, precision) {
        /** @type {?} */
        var amount_str = String(amount.toFixed(precision + 1));
        return roundStringDownToString(amount_str, precision);
    }
    /**
     * @param {?} amount
     * @param {?} precision
     * @return {?}
     */
    function roundStringDownToString(amount, precision) {
        /** @type {?} */
        var parts = amount.split('.');
        /** @type {?} */
        var isWholeNumber = parts.length == 1;
        /** @type {?} */
        var rounded = Number(parts[0] + '.' +
            (isWholeNumber ?
                '0' :
                parts[1].substr(0, precision)));
        /** @type {?} */
        var result = rounded.toFixed(precision);
        return result;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/util/random.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     * @param {?} all
     * @return {?}
     */
    function chooseRandomElement(all) {
        /** @type {?} */
        var randomIndex = Math.floor(Math.random() *
            all.length);
        return all[randomIndex];
    }
    /**
     * @param {?} max
     * @return {?}
     */
    function getRandomNumberBetwen0AndMax(max) {
        return getRandomNumberWithinRange(0, max);
    }
    /**
     * @param {?} max
     * @return {?}
     */
    function getRandomNumberBetwen1AndMax(max) {
        return getRandomNumberWithinRange(1, max);
    }
    /**
     * @param {?} min
     * @param {?} max
     * @return {?}
     */
    function getRandomNumberWithinRange(min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/date-time/index.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var months = [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
    ];
    /**
     * @return {?}
     */
    function NOW() {
        return toDateTimeFormat(new Date());
    }
    /**
     * @param {?} date
     * @return {?}
     */
    function toDateTimeFormat(date) {
        /** @type {?} */
        var parts = date.toString().split(' ');
        /** @type {?} */
        var monthName = parts[1];
        /** @type {?} */
        var monthIndex = months.indexOf(monthName);
        if (monthIndex < 0) {
            throw new Error('Month NOT Found: ' + monthName);
        }
        /** @type {?} */
        var monthNum = monthIndex + 1;
        /** @type {?} */
        var month = (monthNum < 10 ?
            '0' :
            '') + monthNum;
        /** @type {?} */
        var day = parts[2];
        /** @type {?} */
        var year = parts[3];
        /** @type {?} */
        var time = parts[4];
        return year + '-' + month + '-' + day + ' ' + time;
    }
    /**
     * @param {?} start
     * @param {?} end
     * @return {?}
     */
    function getRandomDateWithinRange(start, end) {
        /** @type {?} */
        var startTime = start.getTime();
        /** @type {?} */
        var duration = end.getTime() - startTime;
        return new Date(startTime +
            (Math.random() * duration));
    }
    /**
     * @return {?}
     */
    function getStartOfToday() {
        /** @type {?} */
        var startOfDate = new Date();
        startOfDate.setHours(0, 0, 0, 0);
        return startOfDate;
    }
    /**
     * @return {?}
     */
    function getEndOfToday() {
        /** @type {?} */
        var endOfDay = new Date();
        endOfDay.setHours(23, 59, 59, 999);
        return endOfDay;
    }
    /** @enum {number} */
    var Month = {
        JAN: 0,
        FEB: 1,
        MAR: 2,
        APR: 3,
        MAY: 4,
        JUN: 5,
        JUL: 6,
        AUG: 7,
        SEP: 8,
        OCT: 9,
        NOV: 10,
        DEC: 11,
    };
    Month[Month.JAN] = 'JAN';
    Month[Month.FEB] = 'FEB';
    Month[Month.MAR] = 'MAR';
    Month[Month.APR] = 'APR';
    Month[Month.MAY] = 'MAY';
    Month[Month.JUN] = 'JUN';
    Month[Month.JUL] = 'JUL';
    Month[Month.AUG] = 'AUG';
    Month[Month.SEP] = 'SEP';
    Month[Month.OCT] = 'OCT';
    Month[Month.NOV] = 'NOV';
    Month[Month.DEC] = 'DEC';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/date-time/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function ITimeDuration() { }
    if (false) {
        /** @type {?} */
        ITimeDuration.prototype.milliseconds;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/date-time/time-durations.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var SECOND_IN_MS = 1000;
    /** @type {?} */
    var MINUTE_IN_MS = 60000;
    /** @type {?} */
    var DAY_IN_MS = 86400000;
    var Milliseconds = /** @class */ (function () {
        function Milliseconds(milliseconds) {
            this.milliseconds = milliseconds;
        }
        /**
         * @param {?} other
         * @return {?}
         */
        Milliseconds.prototype.add = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return new Milliseconds(this.milliseconds +
                other.milliseconds);
        };
        /**
         * @param {?} other
         * @return {?}
         */
        Milliseconds.prototype.subtract = /**
         * @param {?} other
         * @return {?}
         */
        function (other) {
            return new Milliseconds(this.milliseconds -
                other.milliseconds);
        };
        return Milliseconds;
    }());
    if (false) {
        /** @type {?} */
        Milliseconds.prototype.milliseconds;
    }
    var Seconds = /** @class */ (function (_super) {
        __extends(Seconds, _super);
        function Seconds(seconds) {
            return _super.call(this, seconds * SECOND_IN_MS) || this;
        }
        return Seconds;
    }(Milliseconds));
    var Minutes = /** @class */ (function (_super) {
        __extends(Minutes, _super);
        function Minutes(minutes) {
            return _super.call(this, minutes * MINUTE_IN_MS) || this;
        }
        return Minutes;
    }(Milliseconds));
    var Hours = /** @class */ (function (_super) {
        __extends(Hours, _super);
        function Hours(hours) {
            return _super.call(this, hours * 60) || this;
        }
        return Hours;
    }(Minutes));

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/date-time/time-frame.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var TimeFrame = /** @class */ (function () {
        function TimeFrame(start, duration) {
            this.start = start;
            this.end = new Date(start.getTime() + duration.milliseconds);
        }
        /**
         * @return {?}
         */
        TimeFrame.prototype.toString = /**
         * @return {?}
         */
        function () {
            return this.start.toString() + ' TO ' +
                this.end.toString();
        };
        return TimeFrame;
    }());
    if (false) {
        /** @type {?} */
        TimeFrame.prototype.end;
        /** @type {?} */
        TimeFrame.prototype.start;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/games.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var GameId = {
        DICE: 1,
        CRASH: 2,
        BACCARAT: 3,
        HILO: 4,
        BLACKJACK: 5,
    };
    GameId[GameId.DICE] = 'DICE';
    GameId[GameId.CRASH] = 'CRASH';
    GameId[GameId.BACCARAT] = 'BACCARAT';
    GameId[GameId.HILO] = 'HILO';
    GameId[GameId.BLACKJACK] = 'BLACKJACK';

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEosConfigParameters() { }
    if (false) {
        /** @type {?} */
        IEosConfigParameters.prototype.httpEndpoint;
        /** @type {?} */
        IEosConfigParameters.prototype.chainId;
        /** @type {?} */
        IEosConfigParameters.prototype.keyProvider;
        /** @type {?} */
        IEosConfigParameters.prototype.verbose;
    }
    /**
     * @record
     */
    function ITableRowQueryParameters() { }
    if (false) {
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.json;
        /** @type {?} */
        ITableRowQueryParameters.prototype.code;
        /** @type {?} */
        ITableRowQueryParameters.prototype.scope;
        /** @type {?} */
        ITableRowQueryParameters.prototype.table;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.limit;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.table_key;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.key_type;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.index_position;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.lower_bound;
        /** @type {?|undefined} */
        ITableRowQueryParameters.prototype.upper_bound;
    }
    /**
     * @record
     */
    function IGlobalVariableRow() { }
    if (false) {
        /** @type {?} */
        IGlobalVariableRow.prototype.id;
        /** @type {?} */
        IGlobalVariableRow.prototype.val;
    }
    /**
     * @record
     */
    function ITokenBalanceRow() { }
    if (false) {
        /** @type {?} */
        ITokenBalanceRow.prototype.balance;
    }
    /**
     * @record
     */
    function ITransactionResult() { }
    if (false) {
        /** @type {?} */
        ITransactionResult.prototype.transaction_id;
    }
    /**
     * @record
     */
    function IEasyAccountCurrencyBalanceRow() { }
    if (false) {
        /** @type {?} */
        IEasyAccountCurrencyBalanceRow.prototype.quantity;
        /** @type {?} */
        IEasyAccountCurrencyBalanceRow.prototype.amt_wagered;
    }
    /**
     * @record
     */
    function IEasyAccountBetBalanceRow() { }
    if (false) {
        /** @type {?} */
        IEasyAccountBetBalanceRow.prototype.id;
        /** @type {?} */
        IEasyAccountBetBalanceRow.prototype.quantity;
        /** @type {?} */
        IEasyAccountBetBalanceRow.prototype.claimed_divs;
    }
    /**
     * @record
     */
    function IEasyAccountInfoRow() { }
    if (false) {
        /** @type {?} */
        IEasyAccountInfoRow.prototype.id;
        /** @type {?} */
        IEasyAccountInfoRow.prototype.key;
        /** @type {?} */
        IEasyAccountInfoRow.prototype.nonce;
        /** @type {?} */
        IEasyAccountInfoRow.prototype.email_backup;
    }
    /**
     * @record
     */
    function IEasyAccountActiveBet() { }
    if (false) {
        /** @type {?} */
        IEasyAccountActiveBet.prototype.key_id;
    }
    /**
     * @record
     */
    function IEosActionData() { }
    if (false) {
        /** @type {?} */
        IEosActionData.prototype.account;
        /** @type {?} */
        IEosActionData.prototype.name;
        /** @type {?} */
        IEosActionData.prototype.data;
    }
    /**
     * @record
     */
    function IEosActionAuthorization() { }
    if (false) {
        /** @type {?} */
        IEosActionAuthorization.prototype.actor;
        /** @type {?} */
        IEosActionAuthorization.prototype.permission;
    }
    /**
     * @record
     */
    function IEosTransactionAction() { }
    if (false) {
        /** @type {?} */
        IEosTransactionAction.prototype.authorization;
    }
    /**
     * @record
     */
    function ISignatureInfo() { }
    if (false) {
        /** @type {?} */
        ISignatureInfo.prototype.nonce;
        /** @type {?} */
        ISignatureInfo.prototype.signature;
        /** @type {?} */
        ISignatureInfo.prototype.isSignedDataHashed;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/int-math.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var AssetMath = /** @class */ (function () {
        function AssetMath(precision, coin_name) {
            this._integer = 0;
            this.precision = precision;
            this._coin_name = coin_name;
        }
        /**
         * @param {?} intVal
         * @return {?}
         */
        AssetMath.prototype.initWithInteger = /**
         * @param {?} intVal
         * @return {?}
         */
        function (intVal) {
            Big.RM = 0;
            this._integer = Number(new Big(intVal).toFixed(0));
        };
        /**
         * @param {?} intDec
         * @return {?}
         */
        AssetMath.prototype.initWithDecimal = /**
         * @param {?} intDec
         * @return {?}
         */
        function (intDec) {
            Big.RM = 0;
            this._integer = Number(new Big(intDec).times("1e" + this.precision).toFixed(0));
        };
        /**
         * @param {?} valDecimal
         * @return {?}
         */
        AssetMath.prototype.decToInt = /**
         * @param {?} valDecimal
         * @return {?}
         */
        function (valDecimal) {
            Big.RM = 0;
            return Number(new Big(valDecimal).times("1e" + this.precision).toFixed(0));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        AssetMath.prototype.addInt = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        function (valInteger) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).plus(valInteger));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        AssetMath.prototype.addDec = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        function (valDecimal) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).add((/** @type {?} */ (this)).decToInt(valDecimal)));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        AssetMath.prototype.subInt = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        function (valInteger) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus(valInteger));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        AssetMath.prototype.subDec = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        function (valDecimal) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus((/** @type {?} */ (this)).decToInt(valDecimal)));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        AssetMath.prototype.divInt = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        function (valInteger) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div(valInteger));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        AssetMath.prototype.divDec = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        function (valDecimal) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div((/** @type {?} */ (this)).decToInt(valDecimal)));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        AssetMath.prototype.multInt = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valInteger
         * @return {THIS}
         */
        function (valInteger) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times(valInteger));
            return (/** @type {?} */ (this));
        };
        /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        AssetMath.prototype.multDec = /**
         * @template THIS
         * @this {THIS}
         * @param {?} valDecimal
         * @return {THIS}
         */
        function (valDecimal) {
            Big.RM = 0;
            (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times((/** @type {?} */ (this)).decToInt(valDecimal)));
            return (/** @type {?} */ (this));
        };
        Object.defineProperty(AssetMath.prototype, "assetString", {
            get: /**
             * @return {?}
             */
            function () {
                return this.toDecimalString() + " " + this.coinName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AssetMath.prototype, "decimal", {
            get: /**
             * @return {?}
             */
            function () {
                return this.toDecimalString();
            },
            enumerable: true,
            configurable: true
        });
        /**
         * @return {?}
         */
        AssetMath.prototype.toDecimalString = /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var decimal = new Big(this._integer).times("1e-" + this.precision);
            return decimal.toFixed(this.precision);
        };
        Object.defineProperty(AssetMath.prototype, "integer", {
            get: /**
             * @return {?}
             */
            function () {
                return this._integer;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AssetMath.prototype, "coinName", {
            get: /**
             * @return {?}
             */
            function () {
                return this._coin_name;
            },
            enumerable: true,
            configurable: true
        });
        return AssetMath;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        AssetMath.prototype._integer;
        /** @type {?} */
        AssetMath.prototype.precision;
        /**
         * @type {?}
         * @private
         */
        AssetMath.prototype._coin_name;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-numbers.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEOSAssetAmount() { }
    if (false) {
        /** @type {?} */
        IEOSAssetAmount.prototype.symbol;
        /** @type {?} */
        IEOSAssetAmount.prototype.contract;
        /** @type {?} */
        IEOSAssetAmount.prototype.integer;
        /** @type {?} */
        IEOSAssetAmount.prototype.quantity;
    }
    var EOSAssetAmount = /** @class */ (function (_super) {
        __extends(EOSAssetAmount, _super);
        function EOSAssetAmount(precision, symbol, contract, decimalValue) {
            if (decimalValue === void 0) { decimalValue = '0'; }
            var _this = _super.call(this, precision, symbol) || this;
            _this.symbol = symbol;
            _this.contract = contract;
            _this.initWithDecimal(decimalValue);
            return _this;
        }
        Object.defineProperty(EOSAssetAmount.prototype, "quantity", {
            get: /**
             * @return {?}
             */
            function () {
                return this.assetString;
            },
            enumerable: true,
            configurable: true
        });
        return EOSAssetAmount;
    }(AssetMath));
    if (false) {
        /** @type {?} */
        EOSAssetAmount.prototype.symbol;
        /** @type {?} */
        EOSAssetAmount.prototype.contract;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-util.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var bigInt = require('big-integer');
    /**
     * @param {?} txnId
     * @return {?}
     */
    function getBetIdFromTransactionId(txnId) {
        /** @type {?} */
        var shortStr = txnId.substring(0, 16);
        return bigInt(shortStr, 16).toString();
    }
    /**
     * @param {?} eos
     * @param {?} actions
     * @return {?}
     * @this {*}
     */
    function executeTransactionUntilSuccessful(eos, actions) {
        return __awaiter(this, void 0, void 0, function () {
            var txnId, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, executeTransaction(eos, actions)];
                    case 1:
                        txnId = _a.sent();
                        if (txnId != undefined) {
                            return [2 /*return*/, txnId];
                        }
                        else {
                            return [2 /*return*/, executeTransactionUntilSuccessful(eos, actions)];
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.error(error_1);
                        return [2 /*return*/, executeTransactionUntilSuccessful(eos, actions)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    }
    /**
     * @record
     */
    function IActionAuthorization() { }
    if (false) {
        /** @type {?} */
        IActionAuthorization.prototype.actor;
        /** @type {?} */
        IActionAuthorization.prototype.permission;
    }
    /**
     * @record
     */
    function ITransactionAction() { }
    if (false) {
        /** @type {?} */
        ITransactionAction.prototype.account;
        /** @type {?} */
        ITransactionAction.prototype.name;
        /** @type {?} */
        ITransactionAction.prototype.authorization;
        /** @type {?} */
        ITransactionAction.prototype.data;
    }
    /**
     * @param {?} eos
     * @param {?} actions
     * @return {?}
     * @this {*}
     */
    function executeTransaction(eos, actions) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, eos.transaction({
                                actions: actions
                            })];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result.transaction_id];
                    case 2:
                        error_2 = _a.sent();
                        //error = JSON.parse(error);
                        //console.log( error );
                        throw error_2;
                    case 3: return [2 /*return*/];
                }
            });
        });
    }
    /**
     * @template T
     * @param {?} eos
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} table_key
     * @return {?}
     * @this {*}
     */
    function getAllTableRows(eos, code, scope, table, table_key) {
        return __awaiter(this, void 0, void 0, function () {
            var allRows, limit, lower_bound, result, numOfRows, lastRow, indexOfLastRow;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        allRows = [];
                        limit = -1;
                        lower_bound = '0';
                        _a.label = 1;
                    case 1:
                        if (!true) return [3 /*break*/, 3];
                        return [4 /*yield*/, getTableRowsResult(eos, {
                                code: code, scope: scope, table: table,
                                limit: limit, lower_bound: lower_bound
                            })];
                    case 2:
                        result = _a.sent();
                        if (result == undefined ||
                            result.rows.length == 0) {
                            return [3 /*break*/, 3];
                        }
                        // add new rows
                        allRows = allRows.concat(result.rows);
                        //const indexOfFirstRow = bigInt( result.rows[0][table_key] );
                        numOfRows = result.rows.length;
                        lastRow = result.rows[numOfRows - 1];
                        indexOfLastRow = bigInt(lastRow[table_key]);
                        //console.log('Index of First Row: '+indexOfFirstRow.toString())
                        //console.log('Index of Last Row: '+indexOfLastRow.toString())
                        if (result.more === false) {
                            return [3 /*break*/, 3];
                        }
                        lower_bound = indexOfLastRow.add(1).toString();
                        return [3 /*break*/, 1];
                    case 3: return [2 /*return*/, allRows];
                }
            });
        });
    }
    /*
    export function getEosBalance(eos,accountName:string):Promise<string>
    {
        return getTokenBalance(
            eos,
            'eosio.token',
            accountName,
            'EOS'
        );
    }

    export function getBtcBalance(eos,accountName:string):Promise<string>
    {
        return getTokenBalance(
            eos,
            'eosbettokens',
            accountName,
            'BTC'
        );
    }

    export function getBetBalance(eos,accountName:string):Promise<string>
    {
        return getTokenBalance(
            eos,
            'betdividends',
            accountName,
            'BET'
        );
    }
    */
    /**
     * @param {?} eos
     * @param {?} tokenContractAccount
     * @param {?} tokenHolderAccount
     * @param {?} tokenSymbol
     * @param {?=} table
     * @return {?}
     * @this {*}
     */
    function getTokenBalance(eos, tokenContractAccount, tokenHolderAccount, tokenSymbol, table) {
        if (table === void 0) { table = 'accounts'; }
        return __awaiter(this, void 0, void 0, function () {
            var rows, rows_1, rows_1_1, row, parts;
            var e_1, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, getTableRows(eos, {
                            code: tokenContractAccount,
                            scope: tokenHolderAccount,
                            table: table
                        })];
                    case 1:
                        rows = _b.sent();
                        try {
                            for (rows_1 = __values(rows), rows_1_1 = rows_1.next(); !rows_1_1.done; rows_1_1 = rows_1.next()) {
                                row = rows_1_1.value;
                                parts = row.balance.split(' ');
                                if (parts[1] == tokenSymbol) {
                                    return [2 /*return*/, parts[0]];
                                }
                            }
                        }
                        catch (e_1_1) { e_1 = { error: e_1_1 }; }
                        finally {
                            try {
                                if (rows_1_1 && !rows_1_1.done && (_a = rows_1.return)) _a.call(rows_1);
                            }
                            finally { if (e_1) throw e_1.error; }
                        }
                        return [2 /*return*/, '0'];
                }
            });
        });
    }
    /**
     * @record
     */
    function IGlobalVarRow() { }
    if (false) {
        /** @type {?} */
        IGlobalVarRow.prototype.id;
        /** @type {?} */
        IGlobalVarRow.prototype.val;
    }
    /**
     * @param {?} eos
     * @param {?} smartContractAccount
     * @param {?} variableId
     * @return {?}
     * @this {*}
     */
    function getGlobalVariable(eos, smartContractAccount, variableId) {
        return __awaiter(this, void 0, void 0, function () {
            var rows, rows_2, rows_2_1, row;
            var e_2, _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0: return [4 /*yield*/, getTableRows(eos, {
                            code: smartContractAccount,
                            scope: smartContractAccount,
                            table: 'globalvars'
                        })];
                    case 1:
                        rows = _b.sent();
                        if (rows.length === 0) {
                            throw new Error("Contract not INIT");
                        }
                        try {
                            for (rows_2 = __values(rows), rows_2_1 = rows_2.next(); !rows_2_1.done; rows_2_1 = rows_2.next()) {
                                row = rows_2_1.value;
                                if (variableId == row.id) {
                                    return [2 /*return*/, row.val];
                                }
                            }
                        }
                        catch (e_2_1) { e_2 = { error: e_2_1 }; }
                        finally {
                            try {
                                if (rows_2_1 && !rows_2_1.done && (_a = rows_2.return)) _a.call(rows_2);
                            }
                            finally { if (e_2) throw e_2.error; }
                        }
                        return [2 /*return*/, undefined];
                }
            });
        });
    }
    /**
     * @param {?} eos
     * @param {?} easyAccountContract
     * @param {?} betId
     * @return {?}
     * @this {*}
     */
    function getAccountIdForActiveEasyAccountBet(eos, easyAccountContract, betId) {
        return __awaiter(this, void 0, void 0, function () {
            var rows;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getUniqueTableRow(eos, easyAccountContract, easyAccountContract, 'activebets', betId)];
                    case 1:
                        rows = _a.sent();
                        return [2 /*return*/, rows && rows.length > 0 ?
                                rows[0].key_id :
                                undefined];
                }
            });
        });
    }
    /**
     * @template T
     * @param {?} eos
     * @param {?} code
     * @param {?} scope
     * @param {?} table
     * @param {?} rowId
     * @return {?}
     */
    function getUniqueTableRow(eos, code, scope, table, rowId) {
        return getTableRows(eos, {
            code: code,
            scope: scope,
            table: table,
            lower_bound: rowId,
            upper_bound: rowId
        });
    }
    /**
     * @template T
     * @param {?} eos
     * @param {?} parameters
     * @return {?}
     * @this {*}
     */
    function getTableRows(eos, parameters) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getTableRowsResult(eos, parameters)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, result ?
                                result.rows :
                                undefined];
                }
            });
        });
    }
    /**
     * @record
     * @template T
     */
    function ITableRowQueryResult() { }
    if (false) {
        /** @type {?} */
        ITableRowQueryResult.prototype.rows;
        /** @type {?} */
        ITableRowQueryResult.prototype.more;
    }
    /**
     * @template T
     * @param {?} eos
     * @param {?} parameters
     * @return {?}
     * @this {*}
     */
    function getTableRowsResult(eos, parameters) {
        return __awaiter(this, void 0, void 0, function () {
            var result, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        parameters.json = true;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, eos.getTableRows(parameters)];
                    case 2:
                        result = _a.sent();
                        return [2 /*return*/, result];
                    case 3:
                        error_3 = _a.sent();
                        console.log('Cound not get table rows');
                        console.log(error_3);
                        return [2 /*return*/, undefined];
                    case 4: return [2 /*return*/];
                }
            });
        });
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/eos-txn-executor.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEosTransactionListener() { }
    if (false) {
        /**
         * @param {?} txnId
         * @return {?}
         */
        IEosTransactionListener.prototype.onEosTransactionConfirmed = function (txnId) { };
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/eos/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEosTokenTransferData() { }
    if (false) {
        /** @type {?} */
        IEosTokenTransferData.prototype.from;
        /** @type {?} */
        IEosTokenTransferData.prototype.to;
        /** @type {?} */
        IEosTokenTransferData.prototype.quantity;
        /** @type {?} */
        IEosTokenTransferData.prototype.memo;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/common.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IResolvedBet() { }
    if (false) {
        /** @type {?} */
        IResolvedBet.prototype.id;
        /** @type {?} */
        IResolvedBet.prototype.isEasyAccount;
        /** @type {?} */
        IResolvedBet.prototype.bettor;
        /** @type {?} */
        IResolvedBet.prototype.amount;
        /** @type {?} */
        IResolvedBet.prototype.resolvedMilliseconds;
        /** @type {?} */
        IResolvedBet.prototype.referrer;
        /** @type {?} */
        IResolvedBet.prototype.jackpotSpin;
        /** @type {?} */
        IResolvedBet.prototype.stakeWeight;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/dice-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IResolvedDiceBet() { }
    if (false) {
        /** @type {?} */
        IResolvedDiceBet.prototype.isRollOver;
        /** @type {?} */
        IResolvedDiceBet.prototype.number;
        /** @type {?} */
        IResolvedDiceBet.prototype.roll;
    }
    /**
     * @record
     */
    function IDiceGameData() { }
    if (false) {
        /** @type {?} */
        IDiceGameData.prototype.recentBets;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/chat-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var chatLoginMessagePrefix = 'My Chat Public Key is: ';
    /**
     * @record
     */
    function IChatAuthenticationData() { }
    if (false) {
        /** @type {?} */
        IChatAuthenticationData.prototype.isEasyAccount;
        /** @type {?} */
        IChatAuthenticationData.prototype.chatPublicKey;
        /** @type {?} */
        IChatAuthenticationData.prototype.eosAccountName;
        /** @type {?} */
        IChatAuthenticationData.prototype.easyAccountId;
        /** @type {?} */
        IChatAuthenticationData.prototype.signature;
        /** @type {?} */
        IChatAuthenticationData.prototype.isSignedDataHased;
        /** @type {?} */
        IChatAuthenticationData.prototype.nonce;
    }
    /**
     * @record
     */
    function IChatMessageData() { }
    if (false) {
        /** @type {?} */
        IChatMessageData.prototype.chatPublicKey;
        /** @type {?} */
        IChatMessageData.prototype.message;
        /** @type {?} */
        IChatMessageData.prototype.nonce;
        /** @type {?} */
        IChatMessageData.prototype.signature;
    }
    /**
     * @record
     */
    function IValidatedChatMessage() { }
    if (false) {
        /** @type {?} */
        IValidatedChatMessage.prototype.from;
        /** @type {?} */
        IValidatedChatMessage.prototype.message;
        /** @type {?} */
        IValidatedChatMessage.prototype.time;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/card-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @enum {number} */
    var CardSuit = {
        Hearts: 0,
        Diamonds: 1,
        Spades: 2,
        Clubs: 3,
    };
    CardSuit[CardSuit.Hearts] = 'Hearts';
    CardSuit[CardSuit.Diamonds] = 'Diamonds';
    CardSuit[CardSuit.Spades] = 'Spades';
    CardSuit[CardSuit.Clubs] = 'Clubs';
    /**
     * @record
     */
    function ICardData() { }
    if (false) {
        /** @type {?} */
        ICardData.prototype.suit;
        /** @type {?} */
        ICardData.prototype.rankName;
    }
    var CardRankNumber = /** @class */ (function () {
        function CardRankNumber() {
        }
        CardRankNumber.ACE = 1;
        CardRankNumber.TEN = 10;
        CardRankNumber.JACK = 11;
        CardRankNumber.QUEEN = 12;
        CardRankNumber.KING = 0;
        return CardRankNumber;
    }());
    if (false) {
        /** @type {?} */
        CardRankNumber.ACE;
        /** @type {?} */
        CardRankNumber.TEN;
        /** @type {?} */
        CardRankNumber.JACK;
        /** @type {?} */
        CardRankNumber.QUEEN;
        /** @type {?} */
        CardRankNumber.KING;
    }
    var CardRankName = /** @class */ (function () {
        function CardRankName() {
        }
        CardRankName.ACE = 'Ace';
        CardRankName.JACK = 'Jack';
        CardRankName.QUEEN = 'Queen';
        CardRankName.KING = 'King';
        return CardRankName;
    }());
    if (false) {
        /** @type {?} */
        CardRankName.ACE;
        /** @type {?} */
        CardRankName.JACK;
        /** @type {?} */
        CardRankName.QUEEN;
        /** @type {?} */
        CardRankName.KING;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function numberToSuit(value) {
        switch (value) {
            case 0:
                return CardSuit.Hearts;
            case 1:
                return CardSuit.Diamonds;
            case 2:
                return CardSuit.Spades;
            case 3:
                return CardSuit.Clubs;
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function nameOfSuit(value) {
        switch (value) {
            case CardSuit.Hearts:
                return "Hearts";
            case CardSuit.Diamonds:
                return "Diamonds";
            case CardSuit.Spades:
                return "Spades";
            case CardSuit.Clubs:
                return "Clubs";
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    function nameOfRank(value) {
        switch (value) {
            case CardRankNumber.ACE:
                return CardRankName.ACE.substr(0, 1);
            case CardRankNumber.JACK:
                return CardRankName.JACK.substr(0, 1);
            case CardRankNumber.QUEEN:
                return CardRankName.QUEEN.substr(0, 1);
            case CardRankNumber.KING:
                return CardRankName.KING.substr(0, 1);
            default:
                return value.toString();
        }
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/baccarat-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IHandData() { }
    if (false) {
        /** @type {?} */
        IHandData.prototype.cards;
        /** @type {?} */
        IHandData.prototype.totalValue;
    }
    /**
     * @record
     */
    function IBaccaratGameResultDataForMultiPlayer() { }
    if (false) {
        /** @type {?} */
        IBaccaratGameResultDataForMultiPlayer.prototype.gameId;
        /** @type {?} */
        IBaccaratGameResultDataForMultiPlayer.prototype.winner;
        /** @type {?} */
        IBaccaratGameResultDataForMultiPlayer.prototype.hands;
    }
    /**
     * @record
     */
    function IBaccaratGameResultDataForSinglePlayer() { }
    if (false) {
        /** @type {?} */
        IBaccaratGameResultDataForSinglePlayer.prototype.winner;
        /** @type {?} */
        IBaccaratGameResultDataForSinglePlayer.prototype.hands;
    }
    /**
     * @record
     */
    function IBaccaratGameStartData() { }
    if (false) {
        /** @type {?} */
        IBaccaratGameStartData.prototype.currentGameId;
        /** @type {?} */
        IBaccaratGameStartData.prototype.gameStartTime;
        /** @type {?} */
        IBaccaratGameStartData.prototype.currentServerTime;
    }
    /**
     * @record
     */
    function IBaccaratBetDataForSinglePlayer() { }
    if (false) {
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.id;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.bettor;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.referral;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.referral_data;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.player_bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.banker_bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.tie_bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.playerpair_bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.bankerpair_bet_amt;
        /** @type {?} */
        IBaccaratBetDataForSinglePlayer.prototype.seed;
    }
    /**
     * @record
     */
    function IResolvedBaccaratBet() { }
    if (false) {
        /** @type {?} */
        IResolvedBaccaratBet.prototype.bet;
        /** @type {?} */
        IResolvedBaccaratBet.prototype.result;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/crash-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IActiveCrashBet() { }
    if (false) {
        /** @type {?} */
        IActiveCrashBet.prototype.id;
        /** @type {?} */
        IActiveCrashBet.prototype.bettor;
        /** @type {?} */
        IActiveCrashBet.prototype.referral;
        /** @type {?} */
        IActiveCrashBet.prototype.referral_data;
        /** @type {?} */
        IActiveCrashBet.prototype.bet_amt;
        /** @type {?} */
        IActiveCrashBet.prototype.max_win;
        /** @type {?} */
        IActiveCrashBet.prototype.adv_tap_out_times100;
        /** @type {?} */
        IActiveCrashBet.prototype.seed;
        /** @type {?} */
        IActiveCrashBet.prototype.signing_key;
    }
    /**
     * @record
     */
    function IResolvedCrashBet() { }
    /**
     * @record
     */
    function ITapOutRequestData() { }
    if (false) {
        /** @type {?} */
        ITapOutRequestData.prototype.betId;
        /** @type {?} */
        ITapOutRequestData.prototype.signature;
    }
    /**
     * @record
     */
    function ITappedOutMessageData() { }
    if (false) {
        /** @type {?} */
        ITappedOutMessageData.prototype.betId;
        /** @type {?} */
        ITappedOutMessageData.prototype.value;
    }
    /**
     * @record
     */
    function ICrashResultData() { }
    if (false) {
        /** @type {?} */
        ICrashResultData.prototype.cashOutPoint;
        /**
         * isInsant = value == 0 *
         * @type {?}
         */
        ICrashResultData.prototype.crashPoint;
        /** @type {?} */
        ICrashResultData.prototype.isMaxLiabilitiesReached;
        /** @type {?} */
        ICrashResultData.prototype.isEndedEarly;
    }
    /**
     * @record
     */
    function ICrashMessageData() { }
    if (false) {
        /** @type {?} */
        ICrashMessageData.prototype.bet;
        /** @type {?} */
        ICrashMessageData.prototype.result;
    }
    /**
     * @record
     */
    function ICrashGameStateData() { }
    if (false) {
        /** @type {?} */
        ICrashGameStateData.prototype.currentGameId;
        /** @type {?} */
        ICrashGameStateData.prototype.isMoving;
        /** @type {?} */
        ICrashGameStateData.prototype.secondsBeforeStart;
        /** @type {?} */
        ICrashGameStateData.prototype.currentPoint;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/hilo-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IResolvedHiloBet() { }
    if (false) {
        /** @type {?} */
        IResolvedHiloBet.prototype.betType;
        /** @type {?} */
        IResolvedHiloBet.prototype.startCard;
        /** @type {?} */
        IResolvedHiloBet.prototype.nextCard;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/data/blackjack-data.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IBlackjackInitialCards() { }
    if (false) {
        /** @type {?} */
        IBlackjackInitialCards.prototype.dealerCards;
        /** @type {?} */
        IBlackjackInitialCards.prototype.playerCards;
    }
    /**
     * @record
     */
    function IInitialBlackjackBetInfo() { }
    if (false) {
        /** @type {?} */
        IInitialBlackjackBetInfo.prototype.amountAsInteger;
        /** @type {?} */
        IInitialBlackjackBetInfo.prototype.token;
    }
    /**
     * @record
     */
    function IBlackjackGameState() { }
    if (false) {
        /** @type {?} */
        IBlackjackGameState.prototype.id;
        /** @type {?} */
        IBlackjackGameState.prototype.initialBet;
        /** @type {?} */
        IBlackjackGameState.prototype.initialCards;
        /** @type {?} */
        IBlackjackGameState.prototype.isDealerBlackjack;
        /** @type {?} */
        IBlackjackGameState.prototype.commands;
        /** @type {?} */
        IBlackjackGameState.prototype.additionalPlayerCards;
        /** @type {?} */
        IBlackjackGameState.prototype.additionalDealerCards;
    }
    /**
     * @record
     */
    function IBlackjackHandResult() { }
    if (false) {
        /** @type {?} */
        IBlackjackHandResult.prototype.sum;
        /** @type {?} */
        IBlackjackHandResult.prototype.isBlackjack;
    }
    /**
     * @record
     */
    function IBlackjackPlayerHandResult() { }
    if (false) {
        /** @type {?} */
        IBlackjackPlayerHandResult.prototype.totalWagered;
        /** @type {?} */
        IBlackjackPlayerHandResult.prototype.payout;
    }
    /**
     * @record
     */
    function IResolvedBlackjackBet() { }
    if (false) {
        /** @type {?} */
        IResolvedBlackjackBet.prototype.dealerHandResult;
        /** @type {?} */
        IResolvedBlackjackBet.prototype.playerResults;
        /** @type {?} */
        IResolvedBlackjackBet.prototype.totalWagered;
        /** @type {?} */
        IResolvedBlackjackBet.prototype.totalPayout;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/messages.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var DiceMessage = /** @class */ (function () {
        function DiceMessage() {
        }
        DiceMessage.PREFIX = 'DiceMessage';
        /* Client to Server */
        DiceMessage.GET_REFERRER = 'GET_REFERRER';
        DiceMessage.SAVE_REFERRER = 'SAVE_REFERRER';
        DiceMessage.SAVE_REFERRER_FOR_WAX = 'SAVE_REFERRER_FOR_WAX';
        DiceMessage.RESOLVE_BET = 'RESOLVE_BET';
        /* Server to Client */
        DiceMessage.SYNC_GAME_DATA = 'SYNC_GAME_DATA';
        DiceMessage.SYNC_BANKROLL = 'SYNC_BANKROLL';
        DiceMessage.NEW_RESOLVED_BET = 'NEW_RESOLVED_BET';
        return DiceMessage;
    }());
    if (false) {
        /** @type {?} */
        DiceMessage.PREFIX;
        /** @type {?} */
        DiceMessage.GET_REFERRER;
        /** @type {?} */
        DiceMessage.SAVE_REFERRER;
        /** @type {?} */
        DiceMessage.SAVE_REFERRER_FOR_WAX;
        /** @type {?} */
        DiceMessage.RESOLVE_BET;
        /** @type {?} */
        DiceMessage.SYNC_GAME_DATA;
        /** @type {?} */
        DiceMessage.SYNC_BANKROLL;
        /** @type {?} */
        DiceMessage.NEW_RESOLVED_BET;
    }
    var ChatMessage = /** @class */ (function () {
        function ChatMessage() {
        }
        ChatMessage.PREFIX = 'ChatMessage';
        /* Client to Server */
        ChatMessage.AUTHENTICATE = 'AUTHENTICATE';
        ChatMessage.GET_RECENT_MESSAGES = 'GET_RECENT_MESSAGES';
        ChatMessage.CHAT = 'CHAT';
        return ChatMessage;
    }());
    if (false) {
        /** @type {?} */
        ChatMessage.PREFIX;
        /** @type {?} */
        ChatMessage.AUTHENTICATE;
        /** @type {?} */
        ChatMessage.GET_RECENT_MESSAGES;
        /** @type {?} */
        ChatMessage.CHAT;
    }
    var BaccaratMessage = /** @class */ (function () {
        function BaccaratMessage() {
        }
        BaccaratMessage.PREFIX = 'BaccaratMessage';
        BaccaratMessage.BET_RESULT = "BET_RESULT";
        return BaccaratMessage;
    }());
    if (false) {
        /** @type {?} */
        BaccaratMessage.PREFIX;
        /** @type {?} */
        BaccaratMessage.BET_RESULT;
    }
    var CrashMessage = /** @class */ (function () {
        function CrashMessage() {
        }
        CrashMessage.PREFIX = 'CrashMessage';
        /* Server to Client */
        /* MULTI-PLAYER */
        /*
            static START_BETTING = 'START_BETTING';
            static NEW_BETS = 'NEW_BETS';
        
            static SYNC_STATE = 'SYNC_STATE';
            */
        /* */
        /* Client to Server */
        CrashMessage.TAP_OUT = 'TAP_OUT';
        CrashMessage.START_GAME = 'START_GAME';
        CrashMessage.TAPPED_OUT = 'TAPPED_OUT';
        CrashMessage.CRASH = 'CRASH';
        CrashMessage.RESOLVE_BET = 'RESOLVE_BET';
        CrashMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
        return CrashMessage;
    }());
    if (false) {
        /** @type {?} */
        CrashMessage.PREFIX;
        /** @type {?} */
        CrashMessage.TAP_OUT;
        /** @type {?} */
        CrashMessage.START_GAME;
        /** @type {?} */
        CrashMessage.TAPPED_OUT;
        /** @type {?} */
        CrashMessage.CRASH;
        /** @type {?} */
        CrashMessage.RESOLVE_BET;
        /** @type {?} */
        CrashMessage.RESOLVE_BET_RESULT;
    }
    var HiloMessage = /** @class */ (function () {
        function HiloMessage() {
        }
        HiloMessage.PREFIX = 'HiloMessage';
        HiloMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
        return HiloMessage;
    }());
    if (false) {
        /** @type {?} */
        HiloMessage.PREFIX;
        /** @type {?} */
        HiloMessage.RESOLVE_BET_RESULT;
    }
    var BlackjackMessage = /** @class */ (function () {
        function BlackjackMessage() {
        }
        BlackjackMessage.PREFIX = 'BlackjackMessage';
        BlackjackMessage.GAME_COMMAND = 'GAME_COMMAND';
        BlackjackMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
        return BlackjackMessage;
    }());
    if (false) {
        /** @type {?} */
        BlackjackMessage.PREFIX;
        /** @type {?} */
        BlackjackMessage.GAME_COMMAND;
        /** @type {?} */
        BlackjackMessage.RESOLVE_BET_RESULT;
    }
    var BlackjackGameCommand = /** @class */ (function () {
        function BlackjackGameCommand() {
        }
        BlackjackGameCommand.RESUME_GAME = 'RESUME_GAME';
        BlackjackGameCommand.START_GAME = 'START_GAME';
        BlackjackGameCommand.YES_TO_INSURANCE = 'YES_TO_INSURANCE';
        BlackjackGameCommand.NO_TO_INSURANCE = 'NO_TO_INSURANCE';
        BlackjackGameCommand.PEEK_FOR_BLACKJACK = 'PEEK_FOR_BLACKJACK';
        BlackjackGameCommand.SPLIT = 'SPLIT';
        BlackjackGameCommand.HIT = 'HIT';
        BlackjackGameCommand.DOUBLE_DOWN = 'DOUBLE_DOWN';
        BlackjackGameCommand.STAND = 'STAND';
        BlackjackGameCommand.GET_DEALERS_CARDS = 'GET_DEALERS_CARDS';
        return BlackjackGameCommand;
    }());
    if (false) {
        /** @type {?} */
        BlackjackGameCommand.RESUME_GAME;
        /** @type {?} */
        BlackjackGameCommand.START_GAME;
        /** @type {?} */
        BlackjackGameCommand.YES_TO_INSURANCE;
        /** @type {?} */
        BlackjackGameCommand.NO_TO_INSURANCE;
        /** @type {?} */
        BlackjackGameCommand.PEEK_FOR_BLACKJACK;
        /** @type {?} */
        BlackjackGameCommand.SPLIT;
        /** @type {?} */
        BlackjackGameCommand.HIT;
        /** @type {?} */
        BlackjackGameCommand.DOUBLE_DOWN;
        /** @type {?} */
        BlackjackGameCommand.STAND;
        /** @type {?} */
        BlackjackGameCommand.GET_DEALERS_CARDS;
    }
    /**
     * @param {?} string
     * @return {?}
     */
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    }
    /**
     * @param {?} message
     * @return {?}
     */
    function getMethodNameForMessage(message) {
        /** @type {?} */
        var parts = message.split('_');
        for (var i = 0; i < parts.length; i++) {
            parts[i] = capitalizeFirstLetter(parts[i]);
        }
        return 'on' + parts.join('');
    }
    /**
     * @param {?} args
     * @param {?} client
     * @return {?}
     */
    function dispatchMessage(args, client) {
        /** @type {?} */
        var message = args[0];
        /** @type {?} */
        var methodName = getMethodNameForMessage(message);
        //console.log(methodName);
        //console.log(args);
        /** @type {?} */
        var argsArray = [];
        for (var i = 1; i < args.length; i++) {
            argsArray.push(args[i]);
        }
        /** @type {?} */
        var method = client[methodName];
        if (method == undefined) {
            console.log(methodName + ' DOES NOT EXIST!');
            return;
        }
        method.apply(client, argsArray);
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/request.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @template T
     * @param {?} messagePrefix
     * @param {?} messageName
     * @param {?} ars
     * @param {?} socket
     * @param {?=} promise
     * @return {?}
     */
    function makeRequest(messagePrefix, messageName, ars, socket, promise) {
        if (promise === void 0) { promise = new CustomPromise(); }
        /** @type {?} */
        var args = [messagePrefix, messageName];
        for (var i = 0; i < ars.length; i++) {
            args.push(ars[i]);
        }
        args.push(ResponseManager.newHandler(promise));
        socket.emit.apply(socket, args);
        return promise;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/message-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var MessageManager = /** @class */ (function () {
        function MessageManager(emitter, messagePrefix) {
            this.emitter = emitter;
            this.messagePrefix = messagePrefix;
        }
        /**
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        MessageManager.prototype.sendMessage = /**
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        function (messageName) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            /** @type {?} */
            var start = [this.messagePrefix, messageName];
            /** @type {?} */
            var argsArray = start.concat(args);
            this.emitter.emit.apply(this.emitter, argsArray);
            return argsArray;
        };
        return MessageManager;
    }());
    if (false) {
        /**
         * @type {?}
         * @protected
         */
        MessageManager.prototype.emitter;
        /**
         * @type {?}
         * @protected
         */
        MessageManager.prototype.messagePrefix;
    }
    var SocketMessageManager = /** @class */ (function (_super) {
        __extends(SocketMessageManager, _super);
        function SocketMessageManager(socket, messagePrefix, messageReceiver) {
            var _this = _super.call(this, socket, messagePrefix) || this;
            _this.socket = socket;
            _this.messageReceiver = messageReceiver;
            _this.setupMessageListener();
            return _this;
        }
        /**
         * @private
         * @return {?}
         */
        SocketMessageManager.prototype.setupMessageListener = /**
         * @private
         * @return {?}
         */
        function () {
            // listen to all messages with message prefix
            /** @type {?} */
            var receiver = this.messageReceiver;
            this.socket.on(this.messagePrefix, (/**
             * @return {?}
             */
            function () {
                dispatchMessage(arguments, receiver);
            }));
        };
        /**
         * @protected
         * @template T
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        SocketMessageManager.prototype.makeRequest = /**
         * @protected
         * @template T
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        function (messageName) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            return makeRequest(this.messagePrefix, messageName, args, this.socket);
        };
        return SocketMessageManager;
    }(MessageManager));
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SocketMessageManager.prototype.socket;
        /**
         * @type {?}
         * @private
         */
        SocketMessageManager.prototype.messageReceiver;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/interfaces.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function IEmitter() { }
    if (false) {
        /**
         * @param {?} event
         * @param {...?} args
         * @return {?}
         */
        IEmitter.prototype.emit = function (event, args) { };
    }
    /**
     * \@doNotMinify
     * @record
     */
    function ISocket() { }
    if (false) {
        /**
         * @param {?} event
         * @param {?} fn
         * @return {?}
         */
        ISocket.prototype.on = function (event, fn) { };
    }
    /**
     * @record
     */
    function IServerConfig() { }
    if (false) {
        /** @type {?} */
        IServerConfig.prototype.host;
        /** @type {?} */
        IServerConfig.prototype.port;
        /** @type {?} */
        IServerConfig.prototype.ssl;
    }

    /**
     * @fileoverview added by tsickle
     * Generated from: lib/server/dice-message-manager.ts
     * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @abstract
     */
    var   /**
     * @abstract
     */
    DiceMessageManager = /** @class */ (function (_super) {
        __extends(DiceMessageManager, _super);
        function DiceMessageManager(socket, messageReceiver) {
            return _super.call(this, socket, DiceMessage.PREFIX, messageReceiver) || this;
        }
        /**
         * @protected
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        DiceMessageManager.prototype.sendDiceMessage = /**
         * @protected
         * @param {?} messageName
         * @param {...?} args
         * @return {?}
         */
        function (messageName) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            /** @type {?} */
            var start = [messageName];
            this.sendMessage.apply(this, start.concat(args));
        };
        return DiceMessageManager;
    }(SocketMessageManager));

    exports.AssetMath = AssetMath;
    exports.BaccaratMessage = BaccaratMessage;
    exports.BlackjackGameCommand = BlackjackGameCommand;
    exports.BlackjackMessage = BlackjackMessage;
    exports.CardRankName = CardRankName;
    exports.CardRankNumber = CardRankNumber;
    exports.CardSuit = CardSuit;
    exports.ChatMessage = ChatMessage;
    exports.CrashMessage = CrashMessage;
    exports.CustomPromise = CustomPromise;
    exports.CustomTimer = CustomTimer;
    exports.DiceMessage = DiceMessage;
    exports.DiceMessageManager = DiceMessageManager;
    exports.EOSAssetAmount = EOSAssetAmount;
    exports.FIFOList = FIFOList;
    exports.GameId = GameId;
    exports.HiloMessage = HiloMessage;
    exports.Hours = Hours;
    exports.MessageManager = MessageManager;
    exports.Milliseconds = Milliseconds;
    exports.Minutes = Minutes;
    exports.Month = Month;
    exports.NOW = NOW;
    exports.QueueOfUniqueItems = QueueOfUniqueItems;
    exports.ResponseDispatcher = ResponseDispatcher;
    exports.ResponseManager = ResponseManager;
    exports.Seconds = Seconds;
    exports.SocketMessageManager = SocketMessageManager;
    exports.TimeFrame = TimeFrame;
    exports.chatLoginMessagePrefix = chatLoginMessagePrefix;
    exports.chooseRandomElement = chooseRandomElement;
    exports.debugMessage = debugMessage;
    exports.dispatchMessage = dispatchMessage;
    exports.executeTransaction = executeTransaction;
    exports.executeTransactionUntilSuccessful = executeTransactionUntilSuccessful;
    exports.getAccountIdForActiveEasyAccountBet = getAccountIdForActiveEasyAccountBet;
    exports.getAllTableRows = getAllTableRows;
    exports.getBetIdFromTransactionId = getBetIdFromTransactionId;
    exports.getEndOfToday = getEndOfToday;
    exports.getGlobalVariable = getGlobalVariable;
    exports.getRandomDateWithinRange = getRandomDateWithinRange;
    exports.getRandomNumberBetwen0AndMax = getRandomNumberBetwen0AndMax;
    exports.getRandomNumberBetwen1AndMax = getRandomNumberBetwen1AndMax;
    exports.getRandomNumberWithinRange = getRandomNumberWithinRange;
    exports.getRandomUniqueElements = getRandomUniqueElements;
    exports.getStartOfToday = getStartOfToday;
    exports.getTableRows = getTableRows;
    exports.getTableRowsResult = getTableRowsResult;
    exports.getTokenBalance = getTokenBalance;
    exports.getUniqueTableRow = getUniqueTableRow;
    exports.getUrlParameterByName = getUrlParameterByName;
    exports.logWithTime = logWithTime;
    exports.nameOfRank = nameOfRank;
    exports.nameOfSuit = nameOfSuit;
    exports.numberToSuit = numberToSuit;
    exports.removeItemFromArray = removeItemFromArray;
    exports.roundDownToNumber = roundDownToNumber;
    exports.roundDownToString = roundDownToString;
    exports.roundStringDownToString = roundStringDownToString;
    exports.shuffleArray = shuffleArray;
    exports.sleep = sleep;
    exports.splitArrayIntoMany = splitArrayIntoMany;
    exports.splitArrayIntoTwo = splitArrayIntoTwo;
    exports.toDateTimeFormat = toDateTimeFormat;
    exports.trimString = trimString;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=earnbet-common.umd.js.map
