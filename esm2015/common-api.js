/**
 * @fileoverview added by tsickle
 * Generated from: common-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export { getRandomUniqueElements, removeItemFromArray, shuffleArray, splitArrayIntoTwo, splitArrayIntoMany } from './lib/util/array';
export { CustomPromise } from './lib/util/promise';
export { getUrlParameterByName } from './lib/util/url';
export { sleep, CustomTimer } from './lib/util/timer';
export { QueueOfUniqueItems, FIFOList } from './lib/util/collections';
export { ResponseManager, ResponseDispatcher } from './lib/util/response';
export { debugMessage, logWithTime } from './lib/util/log';
export {} from './lib/util/collections';
export { trimString } from './lib/util/string-util';
export { roundDownToNumber, roundDownToString, roundStringDownToString } from './lib/util/math-util';
export { chooseRandomElement, getRandomNumberBetwen0AndMax, getRandomNumberBetwen1AndMax, getRandomNumberWithinRange } from './lib/util/random';
export { NOW, toDateTimeFormat, getRandomDateWithinRange, getStartOfToday, getEndOfToday, Month } from './lib/date-time/index';
export {} from './lib/date-time/interfaces';
export { Milliseconds, Seconds, Minutes, Hours } from './lib/date-time/time-durations';
export { TimeFrame } from './lib/date-time/time-frame';
export { GameId } from './lib/games';
export {} from './lib/eos/eos-data';
export { EOSAssetAmount } from './lib/eos/eos-numbers';
export { AssetMath } from './lib/eos/int-math';
export { getBetIdFromTransactionId, executeTransactionUntilSuccessful, executeTransaction, getAllTableRows, getTokenBalance, getGlobalVariable, getAccountIdForActiveEasyAccountBet, getUniqueTableRow, getTableRows, getTableRowsResult } from './lib/eos/eos-util';
export {} from './lib/eos/eos-txn-executor';
export {} from './lib/eos/interfaces';
export {} from './lib/server/data/common';
export {} from './lib/server/data/dice-data';
export { chatLoginMessagePrefix } from './lib/server/data/chat-data';
export { numberToSuit, nameOfSuit, nameOfRank, CardSuit, CardRankNumber, CardRankName } from './lib/server/data/card-data';
export {} from './lib/server/data/baccarat-data';
export {} from './lib/server/data/crash-data';
export {} from './lib/server/data/hilo-data';
export {} from './lib/server/data/blackjack-data';
export { dispatchMessage, DiceMessage, ChatMessage, BaccaratMessage, CrashMessage, HiloMessage, BlackjackMessage, BlackjackGameCommand } from './lib/server/messages';
export { MessageManager, SocketMessageManager } from './lib/server/message-manager';
export {} from './lib/server/interfaces';
export { DiceMessageManager } from './lib/server/dice-message-manager';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsiY29tbW9uLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLGtIQUFjLGtCQUFrQixDQUFDO0FBQ2pDLDhCQUFjLG9CQUFvQixDQUFDO0FBQ25DLHNDQUFjLGdCQUFnQixDQUFDO0FBQy9CLG1DQUFjLGtCQUFrQixDQUFDO0FBQ2pDLDZDQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLG9EQUFjLHFCQUFxQixDQUFDO0FBQ3BDLDBDQUFjLGdCQUFnQixDQUFDO0FBQy9CLGVBQWMsd0JBQXdCLENBQUM7QUFDdkMsMkJBQWMsd0JBQXdCLENBQUM7QUFDdkMsOEVBQWMsc0JBQXNCLENBQUM7QUFDckMsNEhBQWMsbUJBQW1CLENBQUM7QUFFbEMsdUdBQWMsdUJBQXVCLENBQUM7QUFDdEMsZUFBYyw0QkFBNEIsQ0FBQztBQUMzQyxzREFBYyxnQ0FBZ0MsQ0FBQztBQUMvQywwQkFBYyw0QkFBNEIsQ0FBQztBQUUzQyx1QkFBYyxhQUFhLENBQUM7QUFFNUIsZUFBYyxvQkFBb0IsQ0FBQztBQUNuQywrQkFBYyx1QkFBdUIsQ0FBQztBQUN0QywwQkFBYyxvQkFBb0IsQ0FBQztBQUNuQyxnUEFBYyxvQkFBb0IsQ0FBQztBQUNuQyxlQUFjLDRCQUE0QixDQUFDO0FBQzNDLGVBQWMsc0JBQXNCLENBQUM7QUFFckMsZUFBYywwQkFBMEIsQ0FBQztBQUN6QyxlQUFjLDZCQUE2QixDQUFDO0FBQzVDLHVDQUFjLDZCQUE2QixDQUFDO0FBQzVDLDZGQUFjLDZCQUE2QixDQUFDO0FBQzVDLGVBQWMsaUNBQWlDLENBQUM7QUFDaEQsZUFBYyw4QkFBOEIsQ0FBQztBQUM3QyxlQUFjLDZCQUE2QixDQUFDO0FBQzVDLGVBQWMsa0NBQWtDLENBQUM7QUFFakQsOElBQWMsdUJBQXVCLENBQUM7QUFDdEMscURBQWMsOEJBQThCLENBQUM7QUFDN0MsZUFBYyx5QkFBeUIsQ0FBQztBQUN4QyxtQ0FBYyxtQ0FBbUMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvYXJyYXknO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvdXRpbC9wcm9taXNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvdXJsJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvdGltZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvdXRpbC9jb2xsZWN0aW9ucyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi91dGlsL3Jlc3BvbnNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvbG9nJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3V0aWwvY29sbGVjdGlvbnMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvdXRpbC9zdHJpbmctdXRpbCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi91dGlsL21hdGgtdXRpbCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi91dGlsL3JhbmRvbSc7XG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2RhdGUtdGltZS9pbmRleCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9kYXRlLXRpbWUvaW50ZXJmYWNlcyc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9kYXRlLXRpbWUvdGltZS1kdXJhdGlvbnMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZGF0ZS10aW1lL3RpbWUtZnJhbWUnO1xuXG5leHBvcnQgKiBmcm9tICcuL2xpYi9nYW1lcyc7XG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL2Vvcy9lb3MtZGF0YSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9lb3MvZW9zLW51bWJlcnMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZW9zL2ludC1tYXRoJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL2Vvcy9lb3MtdXRpbCc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9lb3MvZW9zLXR4bi1leGVjdXRvcic7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9lb3MvaW50ZXJmYWNlcyc7XG5cbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9kYXRhL2NvbW1vbic7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2ZXIvZGF0YS9kaWNlLWRhdGEnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmVyL2RhdGEvY2hhdC1kYXRhJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9kYXRhL2NhcmQtZGF0YSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9zZXJ2ZXIvZGF0YS9iYWNjYXJhdC1kYXRhJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9kYXRhL2NyYXNoLWRhdGEnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmVyL2RhdGEvaGlsby1kYXRhJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9kYXRhL2JsYWNramFjay1kYXRhJztcblxuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmVyL21lc3NhZ2VzJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZlci9tZXNzYWdlLW1hbmFnZXInO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmVyL2ludGVyZmFjZXMnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmVyL2RpY2UtbWVzc2FnZS1tYW5hZ2VyJztcbiJdfQ==