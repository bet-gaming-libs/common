/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/time-durations.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SECOND_IN_MS = 1000;
/** @type {?} */
const MINUTE_IN_MS = 60000;
/** @type {?} */
const DAY_IN_MS = 86400000;
export class Milliseconds {
    /**
     * @param {?} milliseconds
     */
    constructor(milliseconds) {
        this.milliseconds = milliseconds;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    add(other) {
        return new Milliseconds(this.milliseconds +
            other.milliseconds);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    subtract(other) {
        return new Milliseconds(this.milliseconds -
            other.milliseconds);
    }
}
if (false) {
    /** @type {?} */
    Milliseconds.prototype.milliseconds;
}
export class Seconds extends Milliseconds {
    /**
     * @param {?} seconds
     */
    constructor(seconds) {
        super(seconds * SECOND_IN_MS);
    }
}
export class Minutes extends Milliseconds {
    /**
     * @param {?} minutes
     */
    constructor(minutes) {
        super(minutes * MINUTE_IN_MS);
    }
}
export class Hours extends Minutes {
    /**
     * @param {?} hours
     */
    constructor(hours) {
        super(hours * 60);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1kdXJhdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9kYXRlLXRpbWUvdGltZS1kdXJhdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O01BSU0sWUFBWSxHQUFHLElBQUk7O01BQ25CLFlBQVksR0FBRyxLQUFLOztNQUNwQixTQUFTLEdBQUcsUUFBUTtBQUkxQixNQUFNLE9BQU8sWUFBWTs7OztJQUVyQixZQUNhLFlBQW1CO1FBQW5CLGlCQUFZLEdBQVosWUFBWSxDQUFPO0lBRWhDLENBQUM7Ozs7O0lBRUQsR0FBRyxDQUFDLEtBQW1CO1FBQ25CLE9BQU8sSUFBSSxZQUFZLENBQ25CLElBQUksQ0FBQyxZQUFZO1lBQ2pCLEtBQUssQ0FBQyxZQUFZLENBQ3JCLENBQUM7SUFDTixDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFtQjtRQUN4QixPQUFPLElBQUksWUFBWSxDQUNuQixJQUFJLENBQUMsWUFBWTtZQUNqQixLQUFLLENBQUMsWUFBWSxDQUNyQixDQUFDO0lBQ04sQ0FBQztDQUNKOzs7SUFqQk8sb0NBQTRCOztBQW1CcEMsTUFBTSxPQUFPLE9BQVEsU0FBUSxZQUFZOzs7O0lBRXJDLFlBQVksT0FBYztRQUN0QixLQUFLLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Q0FDSjtBQUVELE1BQU0sT0FBTyxPQUFRLFNBQVEsWUFBWTs7OztJQUVyQyxZQUFZLE9BQWM7UUFDdEIsS0FBSyxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsQ0FBQztJQUNsQyxDQUFDO0NBQ0o7QUFFRCxNQUFNLE9BQU8sS0FBTSxTQUFRLE9BQU87Ozs7SUFFOUIsWUFBWSxLQUFZO1FBQ3BCLEtBQUssQ0FBQyxLQUFLLEdBQUMsRUFBRSxDQUFDLENBQUM7SUFDcEIsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtJVGltZUR1cmF0aW9ufSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cblxuY29uc3QgU0VDT05EX0lOX01TID0gMTAwMDtcbmNvbnN0IE1JTlVURV9JTl9NUyA9IDYwMDAwO1xuY29uc3QgREFZX0lOX01TID0gODY0MDAwMDA7XG5cblxuXG5leHBvcnQgY2xhc3MgTWlsbGlzZWNvbmRzXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IG1pbGxpc2Vjb25kczpudW1iZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBhZGQob3RoZXI6SVRpbWVEdXJhdGlvbik6SVRpbWVEdXJhdGlvbiB7XG4gICAgICAgIHJldHVybiBuZXcgTWlsbGlzZWNvbmRzKFxuICAgICAgICAgICAgdGhpcy5taWxsaXNlY29uZHMgK1xuICAgICAgICAgICAgb3RoZXIubWlsbGlzZWNvbmRzXG4gICAgICAgICk7XG4gICAgfVxuICAgIFxuICAgIHN1YnRyYWN0KG90aGVyOklUaW1lRHVyYXRpb24pOklUaW1lRHVyYXRpb24ge1xuICAgICAgICByZXR1cm4gbmV3IE1pbGxpc2Vjb25kcyhcbiAgICAgICAgICAgIHRoaXMubWlsbGlzZWNvbmRzIC1cbiAgICAgICAgICAgIG90aGVyLm1pbGxpc2Vjb25kc1xuICAgICAgICApO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFNlY29uZHMgZXh0ZW5kcyBNaWxsaXNlY29uZHMgaW1wbGVtZW50cyBJVGltZUR1cmF0aW9uXG57XG4gICAgY29uc3RydWN0b3Ioc2Vjb25kczpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoc2Vjb25kcyAqIFNFQ09ORF9JTl9NUyk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTWludXRlcyBleHRlbmRzIE1pbGxpc2Vjb25kcyBpbXBsZW1lbnRzIElUaW1lRHVyYXRpb25cbntcbiAgICBjb25zdHJ1Y3RvcihtaW51dGVzOm51bWJlcikge1xuICAgICAgICBzdXBlcihtaW51dGVzICogTUlOVVRFX0lOX01TKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBIb3VycyBleHRlbmRzIE1pbnV0ZXMgaW1wbGVtZW50cyBJVGltZUR1cmF0aW9uXG57XG4gICAgY29uc3RydWN0b3IoaG91cnM6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKGhvdXJzKjYwKTtcbiAgICB9XG59Il19