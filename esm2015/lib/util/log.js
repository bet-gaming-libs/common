/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/log.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} message
 * @return {?}
 */
export function debugMessage(message) {
    /** @type {?} */
    const date = new Date();
    console.log((date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds()
        + ' - ' +
        message);
}
/**
 * @param {?=} message
 * @param {...?} optionalParams
 * @return {?}
 */
export function logWithTime(message, ...optionalParams) {
    /** @type {?} */
    const date = new Date();
    /** @type {?} */
    const dateString = (date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds();
    console.log(dateString
    //+ ' - ' + message
    , message, ...optionalParams);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvdXRpbC9sb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLFlBQVksQ0FBQyxPQUFjOztVQUNqQyxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUU7SUFHdkIsT0FBTyxDQUFDLEdBQUcsQ0FDUCxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHO1FBQzNCLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxHQUFHO1FBRXBCLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxHQUFHO1FBQ3JCLElBQUksQ0FBQyxVQUFVLEVBQUUsR0FBRyxHQUFHO1FBQ3ZCLElBQUksQ0FBQyxVQUFVLEVBQUUsR0FBRyxHQUFHO1FBQ3ZCLElBQUksQ0FBQyxlQUFlLEVBQUU7VUFFcEIsS0FBSztRQUNQLE9BQU8sQ0FDVixDQUFDO0FBQ04sQ0FBQzs7Ozs7O0FBRUQsTUFBTSxVQUFVLFdBQVcsQ0FBQyxPQUFZLEVBQUMsR0FBRyxjQUFvQjs7VUFDdEQsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFOztVQUVqQixVQUFVLEdBQ1osQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRztRQUMzQixJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRztRQUVwQixJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRztRQUNyQixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFO0lBRzFCLE9BQU8sQ0FBQyxHQUFHLENBQ1AsVUFBVTtJQUNWLG1CQUFtQjtNQUVuQixPQUFPLEVBQ1AsR0FBRyxjQUFjLENBQ3BCLENBQUM7QUFDTixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIGRlYnVnTWVzc2FnZShtZXNzYWdlOnN0cmluZykge1xuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpO1xuXG5cbiAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgKGRhdGUuZ2V0TW9udGgoKSArIDEpICsgJy0nICtcbiAgICAgICAgZGF0ZS5nZXREYXRlKCkgKyAnICcgK1xuICAgICAgICBcbiAgICAgICAgZGF0ZS5nZXRIb3VycygpICsgJzonICtcbiAgICAgICAgZGF0ZS5nZXRNaW51dGVzKCkgKyAnOicgK1xuICAgICAgICBkYXRlLmdldFNlY29uZHMoKSArICcuJyArXG4gICAgICAgIGRhdGUuZ2V0TWlsbGlzZWNvbmRzKClcbiAgICAgICAgXG4gICAgICAgICsgJyAtICcgK1xuICAgICAgICBtZXNzYWdlXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGxvZ1dpdGhUaW1lKG1lc3NhZ2U/OmFueSwuLi5vcHRpb25hbFBhcmFtczphbnlbXSkge1xuICAgIGNvbnN0IGRhdGUgPSBuZXcgRGF0ZSgpO1xuXG4gICAgY29uc3QgZGF0ZVN0cmluZyA9IFxuICAgICAgICAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLScgK1xuICAgICAgICBkYXRlLmdldERhdGUoKSArICcgJyArXG4gICAgICAgIFxuICAgICAgICBkYXRlLmdldEhvdXJzKCkgKyAnOicgK1xuICAgICAgICBkYXRlLmdldE1pbnV0ZXMoKSArICc6JyArXG4gICAgICAgIGRhdGUuZ2V0U2Vjb25kcygpICsgJy4nICtcbiAgICAgICAgZGF0ZS5nZXRNaWxsaXNlY29uZHMoKTtcblxuXG4gICAgY29uc29sZS5sb2coXG4gICAgICAgIGRhdGVTdHJpbmdcbiAgICAgICAgLy8rICcgLSAnICsgbWVzc2FnZVxuICAgICAgICAsXG4gICAgICAgIG1lc3NhZ2UsXG4gICAgICAgIC4uLm9wdGlvbmFsUGFyYW1zXG4gICAgKTtcbn0iXX0=