/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/timer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} forMilliseconds
 * @return {?}
 */
export function sleep(forMilliseconds) {
    return new Promise((/**
     * @param {?} resolve
     * @return {?}
     */
    resolve => setTimeout(resolve, forMilliseconds)));
}
/**
 * @record
 */
export function ITimerListener() { }
if (false) {
    /**
     * @return {?}
     */
    ITimerListener.prototype.onTimerTick = function () { };
}
export class CustomTimer {
    /**
     * @param {?} intervalInMilliseconds
     * @param {?} listener
     */
    constructor(intervalInMilliseconds, listener) {
        this.intervalInMilliseconds = intervalInMilliseconds;
        this.listener = listener;
        this.intervalId = undefined;
        this.tick = (/**
         * @return {?}
         */
        () => {
            this.listener.onTimerTick();
        });
    }
    /**
     * @return {?}
     */
    start() {
        if (this.isRunning) {
            return;
        }
        this.intervalId = setInterval(this.tick, this.intervalInMilliseconds);
    }
    /**
     * @return {?}
     */
    stop() {
        if (!this.isRunning) {
            return;
        }
        clearInterval(this.intervalId);
        this.intervalId = undefined;
    }
    /**
     * @private
     * @return {?}
     */
    get isRunning() {
        return this.intervalId != undefined;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalId;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.tick;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalInMilliseconds;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.listener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL3RpbWVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxLQUFLLENBQUMsZUFBc0I7SUFDeEMsT0FBTyxJQUFJLE9BQU87Ozs7SUFBTyxPQUFPLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsZUFBZSxDQUFDLEVBQUMsQ0FBQztBQUM5RSxDQUFDOzs7O0FBRUQsb0NBR0M7Ozs7O0lBREcsdURBQW1COztBQUd2QixNQUFNLE9BQU8sV0FBVzs7Ozs7SUFJcEIsWUFDWSxzQkFBNkIsRUFDN0IsUUFBdUI7UUFEdkIsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUFPO1FBQzdCLGFBQVEsR0FBUixRQUFRLENBQWU7UUFKM0IsZUFBVSxHQUFHLFNBQVMsQ0FBQztRQWtCdkIsU0FBSTs7O1FBQUcsR0FBRyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDaEMsQ0FBQyxFQUFBO0lBZEQsQ0FBQzs7OztJQUVELEtBQUs7UUFDRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQ3pCLElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxDQUFDLHNCQUFzQixDQUM5QixDQUFDO0lBQ04sQ0FBQzs7OztJQUtELElBQUk7UUFDQSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNqQixPQUFPO1NBQ1Y7UUFFRCxhQUFhLENBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FDbEIsQ0FBQztRQUVGLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsSUFBWSxTQUFTO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUM7SUFDeEMsQ0FBQztDQUNKOzs7Ozs7SUFyQ0csaUNBQStCOzs7OztJQWtCL0IsMkJBRUM7Ozs7O0lBakJHLDZDQUFxQzs7Ozs7SUFDckMsK0JBQStCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIHNsZWVwKGZvck1pbGxpc2Vjb25kczpudW1iZXIpIHtcbiAgICByZXR1cm4gbmV3IFByb21pc2U8dm9pZD4ocmVzb2x2ZSA9PiBzZXRUaW1lb3V0KHJlc29sdmUsIGZvck1pbGxpc2Vjb25kcykpO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUaW1lckxpc3RlbmVyXG57XG4gICAgb25UaW1lclRpY2soKTp2b2lkO1xufVxuXG5leHBvcnQgY2xhc3MgQ3VzdG9tVGltZXJcbntcbiAgICBwcml2YXRlIGludGVydmFsSWQgPSB1bmRlZmluZWQ7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBpbnRlcnZhbEluTWlsbGlzZWNvbmRzOm51bWJlcixcbiAgICAgICAgcHJpdmF0ZSBsaXN0ZW5lcjpJVGltZXJMaXN0ZW5lclxuICAgICkge1xuICAgIH1cblxuICAgIHN0YXJ0KCkge1xuICAgICAgICBpZiAodGhpcy5pc1J1bm5pbmcpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuaW50ZXJ2YWxJZCA9IHNldEludGVydmFsKFxuICAgICAgICAgICAgdGhpcy50aWNrLFxuICAgICAgICAgICAgdGhpcy5pbnRlcnZhbEluTWlsbGlzZWNvbmRzXG4gICAgICAgICk7XG4gICAgfVxuICAgIHByaXZhdGUgdGljayA9ICgpID0+IHtcbiAgICAgICAgdGhpcy5saXN0ZW5lci5vblRpbWVyVGljaygpO1xuICAgIH1cblxuICAgIHN0b3AoKSB7XG4gICAgICAgIGlmICghdGhpcy5pc1J1bm5pbmcpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGNsZWFySW50ZXJ2YWwoXG4gICAgICAgICAgICB0aGlzLmludGVydmFsSWRcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLmludGVydmFsSWQgPSB1bmRlZmluZWQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgaXNSdW5uaW5nKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5pbnRlcnZhbElkICE9IHVuZGVmaW5lZDtcbiAgICB9XG59Il19