/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/array.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} array
 * @param {?} numOfElements
 * @return {?}
 */
export function getRandomUniqueElements(array, numOfElements) {
    /** @type {?} */
    var elements = [];
    /** @type {?} */
    var source = array.slice();
    for (var i = 0; i < numOfElements; i++) {
        /** @type {?} */
        var index = getRandomIndex(source);
        elements.push(source[index]);
        source.splice(index, 1);
    }
    return elements;
}
/**
 * @param {?} array
 * @return {?}
 */
function getRandomIndex(array) {
    return Math.round(Math.random() * (array.length - 1));
}
/**
 * @template T
 * @param {?} item
 * @param {?} array
 * @return {?}
 */
export function removeItemFromArray(item, array) {
    for (var i = 0; i < array.length; i++) {
        if (item == array[i]) {
            array.splice(i, 1);
            return;
        }
    }
}
/**
 * @template T
 * @param {?} items
 * @return {?}
 */
export function shuffleArray(items) {
    /** @type {?} */
    const clone = items.slice();
    clone.sort((/**
     * @return {?}
     */
    () => 0.5 - Math.random()));
    return clone;
}
/**
 * @template T
 * @param {?} items
 * @param {?} numOfItemsInFirstPart
 * @return {?}
 */
export function splitArrayIntoTwo(items, numOfItemsInFirstPart) {
    /** @type {?} */
    const part1 = items.slice(0, numOfItemsInFirstPart);
    /** @type {?} */
    const part2 = items.slice(numOfItemsInFirstPart);
    return [
        part1,
        part2
    ];
}
/**
 * @template T
 * @param {?} source
 * @param {?} maxNumOfItemsPerPart
 * @return {?}
 */
export function splitArrayIntoMany(source, maxNumOfItemsPerPart) {
    /** @type {?} */
    const items = source.slice();
    /** @type {?} */
    const parts = [];
    while (items.length > 0) {
        /** @type {?} */
        const part = items.splice(0, items.length > maxNumOfItemsPerPart ?
            maxNumOfItemsPerPart :
            items.length);
        parts.push(part);
    }
    return parts;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXJyYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL2FycmF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLHVCQUF1QixDQUFJLEtBQVMsRUFBQyxhQUFvQjs7UUFFakUsUUFBUSxHQUFPLEVBQUU7O1FBRWpCLE1BQU0sR0FBTyxLQUFLLENBQUMsS0FBSyxFQUFFO0lBRTlCLEtBQUssSUFBSSxDQUFDLEdBQVEsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O1lBQ3JDLEtBQUssR0FBVSxjQUFjLENBQUMsTUFBTSxDQUFDO1FBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDLENBQUM7S0FDMUI7SUFFRCxPQUFPLFFBQVEsQ0FBQztBQUNwQixDQUFDOzs7OztBQUVELFNBQVMsY0FBYyxDQUFDLEtBQVc7SUFFL0IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUUsQ0FBQztBQUM1RCxDQUFDOzs7Ozs7O0FBRUQsTUFBTSxVQUFVLG1CQUFtQixDQUEwQixJQUFNLEVBQUMsS0FBUztJQUN6RSxLQUFLLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUNqQyxJQUFJLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDbEIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7WUFFbEIsT0FBTztTQUNWO0tBQ0o7QUFDTCxDQUFDOzs7Ozs7QUFJRCxNQUFNLFVBQVUsWUFBWSxDQUFJLEtBQVM7O1VBQy9CLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFO0lBRTNCLEtBQUssQ0FBQyxJQUFJOzs7SUFBRSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUM7SUFFeEMsT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQzs7Ozs7OztBQUVELE1BQU0sVUFBVSxpQkFBaUIsQ0FBSSxLQUFTLEVBQUMscUJBQTRCOztVQUNqRSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUMscUJBQXFCLENBQUM7O1VBQzVDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLHFCQUFxQixDQUFDO0lBRWhELE9BQU87UUFDSCxLQUFLO1FBQ0wsS0FBSztLQUNSLENBQUM7QUFDTixDQUFDOzs7Ozs7O0FBRUQsTUFBTSxVQUFVLGtCQUFrQixDQUFJLE1BQVUsRUFBQyxvQkFBMkI7O1VBQ2xFLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFOztVQUV0QixLQUFLLEdBQVMsRUFBRTtJQUV0QixPQUFPLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztjQUNmLElBQUksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUNyQixDQUFDLEVBQ0QsS0FBSyxDQUFDLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ2pDLG9CQUFvQixDQUFDLENBQUM7WUFDdEIsS0FBSyxDQUFDLE1BQU0sQ0FDbkI7UUFFRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0tBQ3BCO0lBRUQsT0FBTyxLQUFLLENBQUM7QUFDakIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBnZXRSYW5kb21VbmlxdWVFbGVtZW50czxUPihhcnJheTpUW10sbnVtT2ZFbGVtZW50czpudW1iZXIpOlRbXVxue1xuICAgIHZhciBlbGVtZW50czpUW10gPSBbXTtcbiAgICBcbiAgICB2YXIgc291cmNlOlRbXSA9IGFycmF5LnNsaWNlKCk7XG4gICAgXG4gICAgZm9yICh2YXIgaTpudW1iZXI9MDsgaSA8IG51bU9mRWxlbWVudHM7IGkrKykge1xuICAgICAgICB2YXIgaW5kZXg6bnVtYmVyID0gZ2V0UmFuZG9tSW5kZXgoc291cmNlKTtcbiAgICAgICAgZWxlbWVudHMucHVzaChzb3VyY2VbaW5kZXhdKTtcbiAgICAgICAgc291cmNlLnNwbGljZShpbmRleCwxKTtcbiAgICB9XG4gICAgXG4gICAgcmV0dXJuIGVsZW1lbnRzO1xufVxuXG5mdW5jdGlvbiBnZXRSYW5kb21JbmRleChhcnJheTphbnlbXSk6bnVtYmVyXG57XG4gICAgcmV0dXJuIE1hdGgucm91bmQoIE1hdGgucmFuZG9tKCkgKiAoYXJyYXkubGVuZ3RoIC0gMSkgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHJlbW92ZUl0ZW1Gcm9tQXJyYXk8VCBleHRlbmRzIG51bWJlcnxzdHJpbmc+KGl0ZW06VCxhcnJheTpUW10pIHtcbiAgICBmb3IgKHZhciBpPTA7IGkgPCBhcnJheS5sZW5ndGg7IGkrKykge1xuICAgICAgICBpZiAoaXRlbSA9PSBhcnJheVtpXSkge1xuICAgICAgICAgICAgYXJyYXkuc3BsaWNlKGksMSk7XG5cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH1cbn1cblxuXG5cbmV4cG9ydCBmdW5jdGlvbiBzaHVmZmxlQXJyYXk8VD4oaXRlbXM6VFtdKTpUW10ge1xuICAgIGNvbnN0IGNsb25lID0gaXRlbXMuc2xpY2UoKTtcblxuICAgIGNsb25lLnNvcnQoICgpID0+IDAuNSAtIE1hdGgucmFuZG9tKCkgKTtcblxuICAgIHJldHVybiBjbG9uZTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNwbGl0QXJyYXlJbnRvVHdvPFQ+KGl0ZW1zOlRbXSxudW1PZkl0ZW1zSW5GaXJzdFBhcnQ6bnVtYmVyKTpUW11bXSB7XG4gICAgY29uc3QgcGFydDEgPSBpdGVtcy5zbGljZSgwLG51bU9mSXRlbXNJbkZpcnN0UGFydCk7XG4gICAgY29uc3QgcGFydDIgPSBpdGVtcy5zbGljZShudW1PZkl0ZW1zSW5GaXJzdFBhcnQpO1xuXG4gICAgcmV0dXJuIFtcbiAgICAgICAgcGFydDEsXG4gICAgICAgIHBhcnQyXG4gICAgXTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIHNwbGl0QXJyYXlJbnRvTWFueTxUPihzb3VyY2U6VFtdLG1heE51bU9mSXRlbXNQZXJQYXJ0Om51bWJlcik6VFtdW10ge1xuICAgIGNvbnN0IGl0ZW1zID0gc291cmNlLnNsaWNlKCk7XG5cbiAgICBjb25zdCBwYXJ0czpUW11bXSA9IFtdO1xuXG4gICAgd2hpbGUgKGl0ZW1zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgY29uc3QgcGFydCA9IGl0ZW1zLnNwbGljZShcbiAgICAgICAgICAgIDAsXG4gICAgICAgICAgICBpdGVtcy5sZW5ndGggPiBtYXhOdW1PZkl0ZW1zUGVyUGFydCA/XG4gICAgICAgICAgICAgICAgbWF4TnVtT2ZJdGVtc1BlclBhcnQgOlxuICAgICAgICAgICAgICAgIGl0ZW1zLmxlbmd0aFxuICAgICAgICApO1xuICAgICAgICBcbiAgICAgICAgcGFydHMucHVzaChwYXJ0KTtcbiAgICB9XG5cbiAgICByZXR1cm4gcGFydHM7XG59Il19