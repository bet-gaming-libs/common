/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/promise.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IError() { }
if (false) {
    /** @type {?} */
    IError.prototype.code;
    /** @type {?|undefined} */
    IError.prototype.data;
}
/**
 * @record
 * @template T
 */
export function IPromise() { }
if (false) {
    /**
     * @param {?} resultHandler
     * @return {?}
     */
    IPromise.prototype.then = function (resultHandler) { };
    /**
     * @param {?} errorHandler
     * @return {?}
     */
    IPromise.prototype.catch = function (errorHandler) { };
}
/**
 * @template T
 */
export class CustomPromise {
    constructor() { }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} resultHandler
     * @return {THIS}
     */
    then(resultHandler) {
        (/** @type {?} */ (this)).resultHandler = resultHandler;
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} errorHandler
     * @return {THIS}
     */
    catch(errorHandler) {
        (/** @type {?} */ (this)).errorHandler = errorHandler;
        return (/** @type {?} */ (this));
    }
    /**
     * @param {?} result
     * @return {?}
     */
    done(result) {
        if (this.resultHandler)
            this.resultHandler(result);
    }
    /**
     * @param {?} error
     * @return {?}
     */
    fail(error) {
        if (this.errorHandler)
            this.errorHandler(error);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.resultHandler;
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.errorHandler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvbWlzZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3V0aWwvcHJvbWlzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLDRCQUlDOzs7SUFGRyxzQkFBWTs7SUFDWixzQkFBTTs7Ozs7O0FBTVYsOEJBSUM7Ozs7OztJQUZHLHVEQUFpRDs7Ozs7SUFDakQsdURBQTZDOzs7OztBQUdqRCxNQUFNLE9BQU8sYUFBYTtJQUt0QixnQkFBZSxDQUFDOzs7Ozs7O0lBRVQsSUFBSSxDQUFDLGFBQThCO1FBRXRDLG1CQUFBLElBQUksRUFBQSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDbkMsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBQ00sS0FBSyxDQUFDLFlBQXlCO1FBRWxDLG1CQUFBLElBQUksRUFBQSxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUM7UUFDakMsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7OztJQUVNLElBQUksQ0FBQyxNQUFRO1FBRWhCLElBQUksSUFBSSxDQUFDLGFBQWE7WUFDbEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7OztJQUNNLElBQUksQ0FBQyxLQUFZO1FBRXBCLElBQUksSUFBSSxDQUFDLFlBQVk7WUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDO0NBQ0o7Ozs7OztJQTFCRyxzQ0FBdUM7Ozs7O0lBQ3ZDLHFDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgSUVycm9yXG57XG4gICAgY29kZTpzdHJpbmc7XG4gICAgZGF0YT87XG59XG5cbmV4cG9ydCB0eXBlIFJlc3VsdEhhbmRsZXI8VD4gPSAocmVzdWx0OlQpID0+IHZvaWQ7XG5leHBvcnQgdHlwZSBFcnJvckhhbmRsZXIgPSAoZXJyb3I6SUVycm9yKSA9PiB2b2lkO1xuXG5leHBvcnQgaW50ZXJmYWNlIElQcm9taXNlPFQ+XG57XG4gICAgdGhlbihyZXN1bHRIYW5kbGVyOlJlc3VsdEhhbmRsZXI8VD4pOklQcm9taXNlPFQ+O1xuICAgIGNhdGNoKGVycm9ySGFuZGxlcjpFcnJvckhhbmRsZXIpOklQcm9taXNlPFQ+O1xufVxuXG5leHBvcnQgY2xhc3MgQ3VzdG9tUHJvbWlzZTxUPiBpbXBsZW1lbnRzIElQcm9taXNlPFQ+XG57XG4gICAgcHJpdmF0ZSByZXN1bHRIYW5kbGVyOlJlc3VsdEhhbmRsZXI8VD47XG4gICAgcHJpdmF0ZSBlcnJvckhhbmRsZXI6RXJyb3JIYW5kbGVyO1xuXG4gICAgY29uc3RydWN0b3IoKSB7fVxuXG4gICAgcHVibGljIHRoZW4ocmVzdWx0SGFuZGxlcjpSZXN1bHRIYW5kbGVyPFQ+KVxuICAgIHtcbiAgICAgICAgdGhpcy5yZXN1bHRIYW5kbGVyID0gcmVzdWx0SGFuZGxlcjtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIHB1YmxpYyBjYXRjaChlcnJvckhhbmRsZXI6RXJyb3JIYW5kbGVyKVxuICAgIHtcbiAgICAgICAgdGhpcy5lcnJvckhhbmRsZXIgPSBlcnJvckhhbmRsZXI7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIHB1YmxpYyBkb25lKHJlc3VsdDpUKVxuICAgIHtcbiAgICAgICAgaWYgKHRoaXMucmVzdWx0SGFuZGxlcilcbiAgICAgICAgICAgIHRoaXMucmVzdWx0SGFuZGxlcihyZXN1bHQpO1xuICAgIH1cbiAgICBwdWJsaWMgZmFpbChlcnJvcjpJRXJyb3IpXG4gICAge1xuICAgICAgICBpZiAodGhpcy5lcnJvckhhbmRsZXIpXG4gICAgICAgICAgICB0aGlzLmVycm9ySGFuZGxlcihlcnJvcik7XG4gICAgfVxufSJdfQ==