/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/string-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} sString
 * @return {?}
 */
export function trimString(sString) {
    while (sString.substring(0, 1) == ' ') {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length - 1, sString.length) == ' ') {
        sString = sString.substring(0, sString.length - 1);
    }
    return sString;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyaW5nLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL3N0cmluZy11dGlsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxVQUFVLENBQUMsT0FBYztJQUNyQyxPQUFPLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsRUFDdkM7UUFDQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0tBQy9DO0lBRUQsT0FBTyxPQUFPLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLEVBQ2pFO1FBQ0MsT0FBTyxHQUFHLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUMsQ0FBQyxDQUFDLENBQUM7S0FDaEQ7SUFFRCxPQUFPLE9BQU8sQ0FBQztBQUNoQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGZ1bmN0aW9uIHRyaW1TdHJpbmcoc1N0cmluZzpzdHJpbmcpIHtcbiAgICB3aGlsZSAoc1N0cmluZy5zdWJzdHJpbmcoMCwxKSA9PSAnICcpXG5cdHtcblx0XHRzU3RyaW5nID0gc1N0cmluZy5zdWJzdHJpbmcoMSwgc1N0cmluZy5sZW5ndGgpO1xuXHR9XG5cblx0d2hpbGUgKHNTdHJpbmcuc3Vic3RyaW5nKHNTdHJpbmcubGVuZ3RoLTEsIHNTdHJpbmcubGVuZ3RoKSA9PSAnICcpXG5cdHtcblx0XHRzU3RyaW5nID0gc1N0cmluZy5zdWJzdHJpbmcoMCxzU3RyaW5nLmxlbmd0aC0xKTtcblx0fVxuXG5cdHJldHVybiBzU3RyaW5nO1xufSJdfQ==