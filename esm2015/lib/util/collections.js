/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/collections.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IUniqueItem() { }
if (false) {
    /** @type {?} */
    IUniqueItem.prototype.id;
}
/**
 * @template T
 */
export class QueueOfUniqueItems {
    constructor() {
        this.map = {};
        this.list = [];
    }
    /**
     * @param {?} items
     * @return {?}
     */
    addAll(items) {
        for (var item of items) {
            this.add(item);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    add(item) {
        if (!this.hasItemWithId(item.id)) {
            this.map[item.id] = item;
            this.list.push(item);
        }
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    removeItemWithId(itemId) {
        delete this.map[itemId];
        for (var i = 0; i < this.list.length; i++) {
            /** @type {?} */
            const item = this.list[i];
            if (itemId == item.id) {
                this.list.splice(i, 1);
                break;
            }
        }
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    hasItemWithId(itemId) {
        return this.map[itemId] != undefined;
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    getItemWithId(itemId) {
        return this.map[itemId];
    }
    /**
     * @return {?}
     */
    get items() {
        return this.list.slice();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.map;
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.list;
}
/**
 * @template T
 */
export class FIFOList {
    /**
     * @param {?} maxNumOfItems
     */
    constructor(maxNumOfItems) {
        this.maxNumOfItems = maxNumOfItems;
        this._items = [];
    }
    /**
     * @param {?} newItem
     * @return {?}
     */
    addAsFirst(newItem) {
        /** @type {?} */
        const end = this.items.slice(0, this.maxNumOfItems - 1);
        this._items = [newItem].concat(end);
    }
    /**
     * @param {?} newItem
     * @return {?}
     */
    addAsLast(newItem) {
        /** @type {?} */
        let start = [];
        if (this.items.length < this.maxNumOfItems) {
            start = this.items.slice();
        }
        else {
            start = this.items.slice(1, this.maxNumOfItems);
        }
        /** @type {?} */
        const merged = start.concat([newItem]);
        this._items = merged;
    }
    /**
     * @return {?}
     */
    get items() {
        return this._items;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype._items;
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype.maxNumOfItems;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL2NvbGxlY3Rpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsaUNBR0M7OztJQURHLHlCQUFVOzs7OztBQUdkLE1BQU0sT0FBTyxrQkFBa0I7SUFLM0I7UUFIUSxRQUFHLEdBQXdCLEVBQUUsQ0FBQztRQUM5QixTQUFJLEdBQU8sRUFBRSxDQUFDO0lBR3RCLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLEtBQVM7UUFDWixLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssRUFBRTtZQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ2xCO0lBQ0wsQ0FBQzs7Ozs7SUFDRCxHQUFHLENBQUMsSUFBTTtRQUNOLElBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRztZQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7WUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7SUFDTCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLE1BQWE7UUFDMUIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXhCLEtBQUssSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7a0JBQy9CLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUV6QixJQUFJLE1BQU0sSUFBSSxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3RCLE1BQU07YUFDVDtTQUNKO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsTUFBYTtRQUN2QixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksU0FBUyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLE1BQWE7UUFDdkIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxJQUFJLEtBQUs7UUFDTCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDN0IsQ0FBQztDQUNKOzs7Ozs7SUEzQ0csaUNBQXNDOzs7OztJQUN0QyxrQ0FBc0I7Ozs7O0FBNkMxQixNQUFNLE9BQU8sUUFBUTs7OztJQUlqQixZQUFvQixhQUFvQjtRQUFwQixrQkFBYSxHQUFiLGFBQWEsQ0FBTztRQUZoQyxXQUFNLEdBQU8sRUFBRSxDQUFDO0lBR3hCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQVM7O2NBQ1YsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUN4QixDQUFDLEVBQ0QsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQ3pCO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7OztJQUNELFNBQVMsQ0FBQyxPQUFTOztZQUNYLEtBQUssR0FBTyxFQUFFO1FBRWxCLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN4QyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUM5QjthQUFNO1lBQ0gsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUNwQixDQUFDLEVBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FDckIsQ0FBQztTQUNMOztjQUVLLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDO0NBQ0o7Ozs7OztJQWhDRywwQkFBd0I7Ozs7O0lBRVosaUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJVW5pcXVlSXRlbVxue1xuICAgIGlkOnN0cmluZztcbn1cblxuZXhwb3J0IGNsYXNzIFF1ZXVlT2ZVbmlxdWVJdGVtczxUIGV4dGVuZHMgSVVuaXF1ZUl0ZW0+XG57XG4gICAgcHJpdmF0ZSBtYXA6e1tpdGVtSWQ6c3RyaW5nXTogVH0gPSB7fTtcbiAgICBwcml2YXRlIGxpc3Q6VFtdID0gW107XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICB9XG5cbiAgICBhZGRBbGwoaXRlbXM6VFtdKSB7XG4gICAgICAgIGZvciAodmFyIGl0ZW0gb2YgaXRlbXMpIHtcbiAgICAgICAgICAgIHRoaXMuYWRkKGl0ZW0pO1xuICAgICAgICB9XG4gICAgfVxuICAgIGFkZChpdGVtOlQpIHtcbiAgICAgICAgaWYgKCAhdGhpcy5oYXNJdGVtV2l0aElkKGl0ZW0uaWQpICkge1xuICAgICAgICAgICAgdGhpcy5tYXBbaXRlbS5pZF0gPSBpdGVtO1xuICAgICAgICAgICAgXG4gICAgICAgICAgICB0aGlzLmxpc3QucHVzaChpdGVtKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlbW92ZUl0ZW1XaXRoSWQoaXRlbUlkOnN0cmluZykge1xuICAgICAgICBkZWxldGUgdGhpcy5tYXBbaXRlbUlkXTtcblxuICAgICAgICBmb3IgKHZhciBpPTA7IGkgPCB0aGlzLmxpc3QubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmxpc3RbaV07XG5cbiAgICAgICAgICAgIGlmIChpdGVtSWQgPT0gaXRlbS5pZCkge1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdC5zcGxpY2UoaSwxKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGhhc0l0ZW1XaXRoSWQoaXRlbUlkOnN0cmluZyk6Ym9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLm1hcFtpdGVtSWRdICE9IHVuZGVmaW5lZDtcbiAgICB9XG5cbiAgICBnZXRJdGVtV2l0aElkKGl0ZW1JZDpzdHJpbmcpOlQge1xuICAgICAgICByZXR1cm4gdGhpcy5tYXBbaXRlbUlkXTtcbiAgICB9XG5cbiAgICBnZXQgaXRlbXMoKTpUW10ge1xuICAgICAgICByZXR1cm4gdGhpcy5saXN0LnNsaWNlKCk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBjbGFzcyBGSUZPTGlzdDxUPlxue1xuICAgIHByaXZhdGUgX2l0ZW1zOlRbXSA9IFtdO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBtYXhOdW1PZkl0ZW1zOm51bWJlcikge1xuICAgIH1cblxuICAgIGFkZEFzRmlyc3QobmV3SXRlbTpUKSB7XG4gICAgICAgIGNvbnN0IGVuZCA9IHRoaXMuaXRlbXMuc2xpY2UoXG4gICAgICAgICAgICAwLFxuICAgICAgICAgICAgdGhpcy5tYXhOdW1PZkl0ZW1zIC0gMVxuICAgICAgICApO1xuICAgICAgICB0aGlzLl9pdGVtcyA9IFtuZXdJdGVtXS5jb25jYXQoZW5kKTtcbiAgICB9XG4gICAgYWRkQXNMYXN0KG5ld0l0ZW06VCkge1xuICAgICAgICBsZXQgc3RhcnQ6VFtdID0gW107XG5cbiAgICAgICAgaWYgKHRoaXMuaXRlbXMubGVuZ3RoIDwgdGhpcy5tYXhOdW1PZkl0ZW1zKSB7XG4gICAgICAgICAgICBzdGFydCA9IHRoaXMuaXRlbXMuc2xpY2UoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gdGhpcy5pdGVtcy5zbGljZShcbiAgICAgICAgICAgICAgICAxLFxuICAgICAgICAgICAgICAgIHRoaXMubWF4TnVtT2ZJdGVtc1xuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IG1lcmdlZCA9IHN0YXJ0LmNvbmNhdChbbmV3SXRlbV0pO1xuXG4gICAgICAgIHRoaXMuX2l0ZW1zID0gbWVyZ2VkO1xuICAgIH1cblxuICAgIGdldCBpdGVtcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2l0ZW1zO1xuICAgIH1cbn0iXX0=