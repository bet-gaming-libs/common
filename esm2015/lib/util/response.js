/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/response.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template T
 */
export function IResponse() { }
if (false) {
    /** @type {?|undefined} */
    IResponse.prototype.error;
    /** @type {?|undefined} */
    IResponse.prototype.result;
}
/**
 * @template T
 */
export class ResponseManager {
    // instance
    /**
     * @private
     * @param {?} promise
     */
    constructor(promise) {
        this.promise = promise;
        this.handler = (/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            if (response.error)
                this.promise.fail(response.error);
            if (response.result)
                this.promise.done(response.result);
        });
    }
    // class
    /**
     * @template T
     * @param {?} promise
     * @return {?}
     */
    static newHandler(promise) {
        return new ResponseManager(promise).handler;
    }
}
if (false) {
    /** @type {?} */
    ResponseManager.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    ResponseManager.prototype.promise;
}
/**
 * @template T
 */
export class ResponseDispatcher {
    // instance
    /**
     * @private
     * @param {?} handler
     */
    constructor(handler) {
        this.handler = handler;
    }
    // class
    /**
     * @template T
     * @param {?} handler
     * @return {?}
     */
    static create(handler) {
        return new ResponseDispatcher(handler);
    }
    /**
     * @param {?} result
     * @return {?}
     */
    dispatchResult(result) {
        this.handler({
            result
        });
    }
    /**
     * @param {?} code
     * @param {?=} data
     * @return {?}
     */
    dispatchError(code, data) {
        this.handler({
            error: {
                code,
                data
            }
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ResponseDispatcher.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL3Jlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUdBLCtCQUlDOzs7SUFGRywwQkFBYzs7SUFDZCwyQkFBVTs7Ozs7QUFLZCxNQUFNLE9BQU8sZUFBZTs7Ozs7O0lBVXhCLFlBQTRCLE9BQXdCO1FBQXhCLFlBQU8sR0FBUCxPQUFPLENBQWlCO1FBRzdDLFlBQU87Ozs7UUFBRyxDQUFDLFFBQXFCLEVBQUUsRUFBRTtZQUV2QyxJQUFJLFFBQVEsQ0FBQyxLQUFLO2dCQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUV0QyxJQUFJLFFBQVEsQ0FBQyxNQUFNO2dCQUNmLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUMzQyxDQUFDLEVBQUE7SUFURCxDQUFDOzs7Ozs7O0lBUkQsTUFBTSxDQUFDLFVBQVUsQ0FBSSxPQUF3QjtRQUV6QyxPQUFPLElBQUksZUFBZSxDQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUNuRCxDQUFDO0NBZUo7OztJQVJHLGtDQU9DOzs7OztJQVZtQixrQ0FBZ0M7Ozs7O0FBYXhELE1BQU0sT0FBTyxrQkFBa0I7Ozs7OztJQVUzQixZQUE0QixPQUEwQjtRQUExQixZQUFPLEdBQVAsT0FBTyxDQUFtQjtJQUN0RCxDQUFDOzs7Ozs7O0lBUkQsTUFBTSxDQUFDLE1BQU0sQ0FBSSxPQUEwQjtRQUV2QyxPQUFPLElBQUksa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFPTSxjQUFjLENBQUMsTUFBUTtRQUUxQixJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1QsTUFBTTtTQUNULENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVNLGFBQWEsQ0FBQyxJQUFXLEVBQUMsSUFBSztRQUVsQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ1QsS0FBSyxFQUFFO2dCQUNILElBQUk7Z0JBQ0osSUFBSTthQUNQO1NBQ0osQ0FBQyxDQUFDO0lBQ1AsQ0FBQztDQUNKOzs7Ozs7SUFuQnVCLHFDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SUVycm9yLEN1c3RvbVByb21pc2V9IGZyb20gJy4vcHJvbWlzZSc7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJUmVzcG9uc2U8VD5cbntcbiAgICBlcnJvcj86SUVycm9yO1xuICAgIHJlc3VsdD86VDtcbn1cblxuZXhwb3J0IHR5cGUgUmVzcG9uc2VIYW5kbGVyPFQ+ID0gKHJlc3BvbnNlOklSZXNwb25zZTxUPikgPT4gdm9pZDtcblxuZXhwb3J0IGNsYXNzIFJlc3BvbnNlTWFuYWdlcjxUPlxue1xuICAgIC8vIGNsYXNzXG4gICAgc3RhdGljIG5ld0hhbmRsZXI8VD4ocHJvbWlzZTpDdXN0b21Qcm9taXNlPFQ+KTpSZXNwb25zZUhhbmRsZXI8VD5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUmVzcG9uc2VNYW5hZ2VyPFQ+KHByb21pc2UpLmhhbmRsZXI7XG4gICAgfVxuXG5cbiAgICAvLyBpbnN0YW5jZVxuICAgIHByaXZhdGUgY29uc3RydWN0b3IocHJpdmF0ZSBwcm9taXNlOkN1c3RvbVByb21pc2U8VD4pIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgaGFuZGxlciA9IChyZXNwb25zZTpJUmVzcG9uc2U8VD4pID0+XG4gICAge1xuICAgICAgICBpZiAocmVzcG9uc2UuZXJyb3IpXG4gICAgICAgICAgICB0aGlzLnByb21pc2UuZmFpbChyZXNwb25zZS5lcnJvcik7XG5cbiAgICAgICAgaWYgKHJlc3BvbnNlLnJlc3VsdClcbiAgICAgICAgICAgIHRoaXMucHJvbWlzZS5kb25lKHJlc3BvbnNlLnJlc3VsdCk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgUmVzcG9uc2VEaXNwYXRjaGVyPFQ+XG57XG4gICAgLy8gY2xhc3NcbiAgICBzdGF0aWMgY3JlYXRlPFQ+KGhhbmRsZXI6UmVzcG9uc2VIYW5kbGVyPFQ+KTpSZXNwb25zZURpc3BhdGNoZXI8VD5cbiAgICB7XG4gICAgICAgIHJldHVybiBuZXcgUmVzcG9uc2VEaXNwYXRjaGVyKGhhbmRsZXIpO1xuICAgIH1cbiAgICBcbiAgICBcbiAgICAvLyBpbnN0YW5jZVxuICAgIHByaXZhdGUgY29uc3RydWN0b3IocHJpdmF0ZSBoYW5kbGVyOlJlc3BvbnNlSGFuZGxlcjxUPikge1xuICAgIH1cblxuICAgIHB1YmxpYyBkaXNwYXRjaFJlc3VsdChyZXN1bHQ6VClcbiAgICB7XG4gICAgICAgIHRoaXMuaGFuZGxlcih7XG4gICAgICAgICAgICByZXN1bHRcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIGRpc3BhdGNoRXJyb3IoY29kZTpzdHJpbmcsZGF0YT8pXG4gICAge1xuICAgICAgICB0aGlzLmhhbmRsZXIoe1xuICAgICAgICAgICAgZXJyb3I6IHtcbiAgICAgICAgICAgICAgICBjb2RlLFxuICAgICAgICAgICAgICAgIGRhdGFcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxufSJdfQ==