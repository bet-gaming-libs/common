/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/random.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} all
 * @return {?}
 */
export function chooseRandomElement(all) {
    /** @type {?} */
    const randomIndex = Math.floor(Math.random() *
        all.length);
    return all[randomIndex];
}
/**
 * @param {?} max
 * @return {?}
 */
export function getRandomNumberBetwen0AndMax(max) {
    return getRandomNumberWithinRange(0, max);
}
/**
 * @param {?} max
 * @return {?}
 */
export function getRandomNumberBetwen1AndMax(max) {
    return getRandomNumberWithinRange(1, max);
}
/**
 * @param {?} min
 * @param {?} max
 * @return {?}
 */
export function getRandomNumberWithinRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFuZG9tLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvdXRpbC9yYW5kb20udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxtQkFBbUIsQ0FBSSxHQUFPOztVQUNwQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDMUIsSUFBSSxDQUFDLE1BQU0sRUFBRTtRQUNiLEdBQUcsQ0FBQyxNQUFNLENBQ2I7SUFFRCxPQUFPLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztBQUM1QixDQUFDOzs7OztBQUVELE1BQU0sVUFBVSw0QkFBNEIsQ0FBQyxHQUFVO0lBQ25ELE9BQU8sMEJBQTBCLENBQUMsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxDQUFDO0FBQzdDLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLDRCQUE0QixDQUFDLEdBQVU7SUFDbkQsT0FBTywwQkFBMEIsQ0FBQyxDQUFDLEVBQUMsR0FBRyxDQUFDLENBQUM7QUFDN0MsQ0FBQzs7Ozs7O0FBR0QsTUFBTSxVQUFVLDBCQUEwQixDQUFDLEdBQVcsRUFBRSxHQUFXO0lBQy9ELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLENBQUMsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO0FBQzdELENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gY2hvb3NlUmFuZG9tRWxlbWVudDxUPihhbGw6VFtdKSB7XG4gICAgY29uc3QgcmFuZG9tSW5kZXggPSBNYXRoLmZsb29yKFxuICAgICAgICBNYXRoLnJhbmRvbSgpICpcbiAgICAgICAgYWxsLmxlbmd0aFxuICAgICk7XG5cbiAgICByZXR1cm4gYWxsW3JhbmRvbUluZGV4XTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFJhbmRvbU51bWJlckJldHdlbjBBbmRNYXgobWF4Om51bWJlcik6bnVtYmVyIHtcbiAgICByZXR1cm4gZ2V0UmFuZG9tTnVtYmVyV2l0aGluUmFuZ2UoMCxtYXgpO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0UmFuZG9tTnVtYmVyQmV0d2VuMUFuZE1heChtYXg6bnVtYmVyKTpudW1iZXIge1xuICAgIHJldHVybiBnZXRSYW5kb21OdW1iZXJXaXRoaW5SYW5nZSgxLG1heCk7XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFJhbmRvbU51bWJlcldpdGhpblJhbmdlKG1pbjogbnVtYmVyLCBtYXg6IG51bWJlcik6IG51bWJlciB7XG4gICAgcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIChtYXggLSBtaW4gKyAxKSArIG1pbik7XG59XG5cbi8qXG5mdW5jdGlvbiBnZXRSYW5kb21OdW1iZXJCZXR3ZWVuMEFuZDFMZXNzVGhhbk1heChtYXg6IG51bWJlcikge1xuICAgIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiBtYXgpO1xufVxuKi8iXX0=