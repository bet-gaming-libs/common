/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
const bigInt = require('big-integer');
/**
 * @param {?} txnId
 * @return {?}
 */
export function getBetIdFromTransactionId(txnId) {
    /** @type {?} */
    var shortStr = txnId.substring(0, 16);
    return bigInt(shortStr, 16).toString();
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
export function executeTransactionUntilSuccessful(eos, actions) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        try {
            /** @type {?} */
            const txnId = yield executeTransaction(eos, actions);
            if (txnId != undefined) {
                return txnId;
            }
            else {
                return executeTransactionUntilSuccessful(eos, actions);
            }
        }
        catch (error) {
            console.error(error);
            return executeTransactionUntilSuccessful(eos, actions);
        }
    });
}
/**
 * @record
 */
export function IActionAuthorization() { }
if (false) {
    /** @type {?} */
    IActionAuthorization.prototype.actor;
    /** @type {?} */
    IActionAuthorization.prototype.permission;
}
/**
 * @record
 */
export function ITransactionAction() { }
if (false) {
    /** @type {?} */
    ITransactionAction.prototype.account;
    /** @type {?} */
    ITransactionAction.prototype.name;
    /** @type {?} */
    ITransactionAction.prototype.authorization;
    /** @type {?} */
    ITransactionAction.prototype.data;
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
export function executeTransaction(eos, actions) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        try {
            /** @type {?} */
            const result = yield eos.transaction({
                actions
            });
            return result.transaction_id;
        }
        catch (error) {
            //error = JSON.parse(error);
            //console.log( error );
            throw error;
            //return undefined;
        }
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} table_key
 * @return {?}
 * @this {*}
 */
export function getAllTableRows(eos, code, scope, table, table_key) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        var allRows = [];
        /** @type {?} */
        const limit = -1;
        /** @type {?} */
        var lower_bound = '0';
        while (true) {
            /** @type {?} */
            const result = yield getTableRowsResult(eos, {
                code, scope, table,
                limit, lower_bound
            });
            if (result == undefined ||
                result.rows.length == 0) {
                break;
            }
            // add new rows
            allRows = allRows.concat(result.rows);
            //const indexOfFirstRow = bigInt( result.rows[0][table_key] );
            /** @type {?} */
            const numOfRows = result.rows.length;
            /** @type {?} */
            const lastRow = result.rows[numOfRows - 1];
            /** @type {?} */
            const indexOfLastRow = bigInt(lastRow[table_key]);
            //console.log('Index of First Row: '+indexOfFirstRow.toString())
            //console.log('Index of Last Row: '+indexOfLastRow.toString())
            if (result.more === false) {
                break;
            }
            lower_bound = indexOfLastRow.add(1).toString();
        }
        return allRows;
    });
}
/*
export function getEosBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosio.token',
        accountName,
        'EOS'
    );
}

export function getBtcBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosbettokens',
        accountName,
        'BTC'
    );
}

export function getBetBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'betdividends',
        accountName,
        'BET'
    );
}
*/
/**
 * @param {?} eos
 * @param {?} tokenContractAccount
 * @param {?} tokenHolderAccount
 * @param {?} tokenSymbol
 * @param {?=} table
 * @return {?}
 * @this {*}
 */
export function getTokenBalance(eos, tokenContractAccount, tokenHolderAccount, tokenSymbol, table = 'accounts') {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getTableRows(eos, {
            code: tokenContractAccount,
            scope: tokenHolderAccount,
            table
        });
        for (var row of rows) {
            /** @type {?} */
            var parts = row.balance.split(' ');
            if (parts[1] == tokenSymbol) {
                return parts[0];
            }
        }
        return '0';
    });
}
/**
 * @record
 */
function IGlobalVarRow() { }
if (false) {
    /** @type {?} */
    IGlobalVarRow.prototype.id;
    /** @type {?} */
    IGlobalVarRow.prototype.val;
}
/**
 * @param {?} eos
 * @param {?} smartContractAccount
 * @param {?} variableId
 * @return {?}
 * @this {*}
 */
export function getGlobalVariable(eos, smartContractAccount, variableId) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getTableRows(eos, {
            code: smartContractAccount,
            scope: smartContractAccount,
            table: 'globalvars'
        });
        if (rows.length === 0) {
            throw new Error("Contract not INIT");
        }
        for (var row of rows) {
            if (variableId == row.id) {
                return row.val;
            }
        }
        return undefined;
    });
}
/**
 * @param {?} eos
 * @param {?} easyAccountContract
 * @param {?} betId
 * @return {?}
 * @this {*}
 */
export function getAccountIdForActiveEasyAccountBet(eos, easyAccountContract, betId) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getUniqueTableRow(eos, easyAccountContract, easyAccountContract, 'activebets', betId);
        return rows && rows.length > 0 ?
            rows[0].key_id :
            undefined;
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} rowId
 * @return {?}
 */
export function getUniqueTableRow(eos, code, scope, table, rowId) {
    return getTableRows(eos, {
        code,
        scope,
        table,
        lower_bound: rowId,
        upper_bound: rowId
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
export function getTableRows(eos, parameters) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield getTableRowsResult(eos, parameters);
        return result ?
            result.rows :
            undefined;
    });
}
/**
 * @record
 * @template T
 */
export function ITableRowQueryResult() { }
if (false) {
    /** @type {?} */
    ITableRowQueryResult.prototype.rows;
    /** @type {?} */
    ITableRowQueryResult.prototype.more;
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
export function getTableRowsResult(eos, parameters) {
    return tslib_1.__awaiter(this, void 0, void 0, function* () {
        parameters.json = true;
        try {
            /** @type {?} */
            const result = yield eos.getTableRows(parameters);
            return result;
        }
        catch (error) {
            console.log('Cound not get table rows');
            console.log(error);
            return undefined;
        }
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztNQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDOzs7OztBQVFyQyxNQUFNLFVBQVUseUJBQXlCLENBQUMsS0FBWTs7UUFDOUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUVyQyxPQUFPLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7QUFDM0MsQ0FBQzs7Ozs7OztBQUdELE1BQU0sVUFBZ0IsaUNBQWlDLENBQ25ELEdBQUcsRUFDSCxPQUE0Qjs7UUFFNUIsSUFBSTs7a0JBQ00sS0FBSyxHQUFHLE1BQU0sa0JBQWtCLENBQUMsR0FBRyxFQUFDLE9BQU8sQ0FBQztZQUVuRCxJQUFJLEtBQUssSUFBSSxTQUFTLEVBQUU7Z0JBQ3BCLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO2lCQUFNO2dCQUNILE9BQU8saUNBQWlDLENBQUMsR0FBRyxFQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3pEO1NBQ0o7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFckIsT0FBTyxpQ0FBaUMsQ0FBQyxHQUFHLEVBQUMsT0FBTyxDQUFDLENBQUM7U0FDekQ7SUFDTCxDQUFDO0NBQUE7Ozs7QUFHRCwwQ0FHQzs7O0lBRkcscUNBQWM7O0lBQ2QsMENBQW1COzs7OztBQUd2Qix3Q0FLQzs7O0lBSkcscUNBQWdCOztJQUNoQixrQ0FBYTs7SUFDYiwyQ0FBc0M7O0lBQ3RDLGtDQUFVOzs7Ozs7OztBQUdkLE1BQU0sVUFBZ0Isa0JBQWtCLENBQ3BDLEdBQUcsRUFDSCxPQUE2Qjs7UUFFN0IsSUFBSTs7a0JBQ00sTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLFdBQVcsQ0FBQztnQkFDakMsT0FBTzthQUNWLENBQUM7WUFFRixPQUFPLE1BQU0sQ0FBQyxjQUFjLENBQUM7U0FDaEM7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLDRCQUE0QjtZQUM1Qix1QkFBdUI7WUFFdkIsTUFBTSxLQUFLLENBQUM7WUFFWixtQkFBbUI7U0FDdEI7SUFDTCxDQUFDO0NBQUE7Ozs7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFnQixlQUFlLENBQ2pDLEdBQUcsRUFDSCxJQUFXLEVBQ1gsS0FBWSxFQUNaLEtBQVksRUFDWixTQUFnQjs7O1lBRVosT0FBTyxHQUFPLEVBQUU7O2NBRWQsS0FBSyxHQUFHLENBQUMsQ0FBQzs7WUFDWixXQUFXLEdBQUcsR0FBRztRQUVyQixPQUFPLElBQUksRUFBRTs7a0JBQ0gsTUFBTSxHQUFHLE1BQU0sa0JBQWtCLENBQ25DLEdBQUcsRUFDSDtnQkFDSSxJQUFJLEVBQUMsS0FBSyxFQUFDLEtBQUs7Z0JBQ2hCLEtBQUssRUFBQyxXQUFXO2FBQ3BCLENBQ0o7WUFFRCxJQUNJLE1BQU0sSUFBSSxTQUFTO2dCQUNuQixNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQ3pCO2dCQUNFLE1BQU07YUFDVDtZQUdELGVBQWU7WUFDZixPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7OztrQkFLaEMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTTs7a0JBQzlCLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBQyxDQUFDLENBQUM7O2tCQUVsQyxjQUFjLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUdqRCxnRUFBZ0U7WUFDaEUsOERBQThEO1lBRzlELElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxLQUFLLEVBQUU7Z0JBQ3ZCLE1BQU07YUFDVDtZQUdELFdBQVcsR0FBRyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2xEO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9DRCxNQUFNLFVBQWdCLGVBQWUsQ0FDakMsR0FBRyxFQUNILG9CQUEyQixFQUMzQixrQkFBeUIsRUFDekIsV0FBa0IsRUFDbEIsS0FBSyxHQUFHLFVBQVU7OztjQUVaLElBQUksR0FBRyxNQUFNLFlBQVksQ0FDM0IsR0FBRyxFQUNIO1lBQ0ksSUFBSSxFQUFFLG9CQUFvQjtZQUMxQixLQUFLLEVBQUUsa0JBQWtCO1lBQ3pCLEtBQUs7U0FDUixDQUNKO1FBRUQsS0FBSyxJQUFJLEdBQUcsSUFBSSxJQUFJLEVBQUU7O2dCQUNkLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7WUFFbEMsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksV0FBVyxFQUFFO2dCQUN6QixPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUNuQjtTQUNKO1FBRUQsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDO0NBQUE7Ozs7QUFHRCw0QkFHQzs7O0lBRkcsMkJBQVc7O0lBQ1gsNEJBQVk7Ozs7Ozs7OztBQUdoQixNQUFNLFVBQWdCLGlCQUFpQixDQUNuQyxHQUFHLEVBQ0gsb0JBQTJCLEVBQzNCLFVBQWtCOzs7Y0FFWixJQUFJLEdBQUcsTUFBTSxZQUFZLENBQzNCLEdBQUcsRUFDSDtZQUNJLElBQUksRUFBRSxvQkFBb0I7WUFDMUIsS0FBSyxFQUFFLG9CQUFvQjtZQUMzQixLQUFLLEVBQUUsWUFBWTtTQUN0QixDQUNKO1FBR0QsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUNuQixNQUFNLElBQUksS0FBSyxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDeEM7UUFHRCxLQUFLLElBQUksR0FBRyxJQUFJLElBQUksRUFBRTtZQUNsQixJQUFJLFVBQVUsSUFBSSxHQUFHLENBQUMsRUFBRSxFQUFFO2dCQUN0QixPQUFPLEdBQUcsQ0FBQyxHQUFHLENBQUM7YUFDbEI7U0FDSjtRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7Q0FBQTs7Ozs7Ozs7QUFHRCxNQUFNLFVBQWdCLG1DQUFtQyxDQUNyRCxHQUFHLEVBQ0gsbUJBQTBCLEVBQzFCLEtBQVk7OztjQUVOLElBQUksR0FBRyxNQUFNLGlCQUFpQixDQUNoQyxHQUFHLEVBQ0gsbUJBQW1CLEVBQUMsbUJBQW1CLEVBQ3ZDLFlBQVksRUFDWixLQUFLLENBQ1I7UUFFRCxPQUFPLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQixTQUFTLENBQUM7SUFDdEIsQ0FBQztDQUFBOzs7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFVLGlCQUFpQixDQUM3QixHQUFHLEVBQ0gsSUFBVyxFQUFDLEtBQVksRUFBQyxLQUFZLEVBQ3JDLEtBQVk7SUFFWixPQUFPLFlBQVksQ0FDZixHQUFHLEVBQ0g7UUFDSSxJQUFJO1FBQ0osS0FBSztRQUNMLEtBQUs7UUFFTCxXQUFXLEVBQUUsS0FBSztRQUNsQixXQUFXLEVBQUUsS0FBSztLQUNyQixDQUNKLENBQUM7QUFDTixDQUFDOzs7Ozs7OztBQUdELE1BQU0sVUFBZ0IsWUFBWSxDQUM5QixHQUFHLEVBQ0gsVUFBb0M7OztjQUU5QixNQUFNLEdBQUcsTUFBTSxrQkFBa0IsQ0FDbkMsR0FBRyxFQUNILFVBQVUsQ0FDYjtRQUVELE9BQU8sTUFBTSxDQUFDLENBQUM7WUFDUCxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDYixTQUFTLENBQUM7SUFDdEIsQ0FBQztDQUFBOzs7OztBQUVELDBDQUlDOzs7SUFGRyxvQ0FBUzs7SUFDVCxvQ0FBYTs7Ozs7Ozs7O0FBR2pCLE1BQU0sVUFBZ0Isa0JBQWtCLENBQ3BDLEdBQUcsRUFDSCxVQUFtQzs7UUFFbkMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFFdkIsSUFBSTs7a0JBQ00sTUFBTSxHQUFHLE1BQU0sR0FBRyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUM7WUFDakQsT0FBTyxNQUFNLENBQUM7U0FDakI7UUFBQyxPQUFPLEtBQUssRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBRW5CLE9BQU8sU0FBUyxDQUFDO1NBQ3BCO0lBQ0wsQ0FBQztDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgYmlnSW50ID0gcmVxdWlyZSgnYmlnLWludGVnZXInKTtcblxuXG5pbXBvcnQgeyBJVGFibGVSb3dRdWVyeVBhcmFtZXRlcnMsIElUb2tlbkJhbGFuY2VSb3csIElFYXN5QWNjb3VudEFjdGl2ZUJldCB9IFxuICAgIGZyb20gXCIuL2Vvcy1kYXRhXCI7XG5cblxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0QmV0SWRGcm9tVHJhbnNhY3Rpb25JZCh0eG5JZDpzdHJpbmcpOnN0cmluZyB7XG4gICAgdmFyIHNob3J0U3RyID0gdHhuSWQuc3Vic3RyaW5nKDAsIDE2KTtcbiAgICBcbiAgICByZXR1cm4gYmlnSW50KHNob3J0U3RyLCAxNikudG9TdHJpbmcoKTtcbn1cblxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZXhlY3V0ZVRyYW5zYWN0aW9uVW50aWxTdWNjZXNzZnVsKFxuICAgIGVvcyxcbiAgICBhY3Rpb25zOklUcmFuc2FjdGlvbkFjdGlvbltdXG4pOlByb21pc2U8c3RyaW5nPiB7XG4gICAgdHJ5IHtcbiAgICAgICAgY29uc3QgdHhuSWQgPSBhd2FpdCBleGVjdXRlVHJhbnNhY3Rpb24oZW9zLGFjdGlvbnMpO1xuXG4gICAgICAgIGlmICh0eG5JZCAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB0eG5JZDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBleGVjdXRlVHJhbnNhY3Rpb25VbnRpbFN1Y2Nlc3NmdWwoZW9zLGFjdGlvbnMpO1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICAgICAgcmV0dXJuIGV4ZWN1dGVUcmFuc2FjdGlvblVudGlsU3VjY2Vzc2Z1bChlb3MsYWN0aW9ucyk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUFjdGlvbkF1dGhvcml6YXRpb24ge1xuICAgIGFjdG9yOiBzdHJpbmc7XG4gICAgcGVybWlzc2lvbjogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUcmFuc2FjdGlvbkFjdGlvbiB7XG4gICAgYWNjb3VudDogc3RyaW5nO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBhdXRob3JpemF0aW9uOiBJQWN0aW9uQXV0aG9yaXphdGlvbltdO1xuICAgIGRhdGE6IGFueTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGV4ZWN1dGVUcmFuc2FjdGlvbihcbiAgICBlb3MsXG4gICAgYWN0aW9uczogSVRyYW5zYWN0aW9uQWN0aW9uW11cbik6UHJvbWlzZTxzdHJpbmc+IHtcbiAgICB0cnkge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBlb3MudHJhbnNhY3Rpb24oe1xuICAgICAgICAgICAgYWN0aW9uc1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0LnRyYW5zYWN0aW9uX2lkO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIC8vZXJyb3IgPSBKU09OLnBhcnNlKGVycm9yKTtcbiAgICAgICAgLy9jb25zb2xlLmxvZyggZXJyb3IgKTtcbiAgICAgICAgXG4gICAgICAgIHRocm93IGVycm9yO1xuXG4gICAgICAgIC8vcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRBbGxUYWJsZVJvd3M8VD4oXG4gICAgZW9zLFxuICAgIGNvZGU6c3RyaW5nLFxuICAgIHNjb3BlOnN0cmluZyxcbiAgICB0YWJsZTpzdHJpbmcsXG4gICAgdGFibGVfa2V5OnN0cmluZyxcbikge1xuICAgIHZhciBhbGxSb3dzOlRbXSA9IFtdO1xuXG4gICAgY29uc3QgbGltaXQgPSAtMTtcbiAgICB2YXIgbG93ZXJfYm91bmQgPSAnMCc7XG5cbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBnZXRUYWJsZVJvd3NSZXN1bHQ8VD4oXG4gICAgICAgICAgICBlb3MsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgY29kZSxzY29wZSx0YWJsZSxcbiAgICAgICAgICAgICAgICBsaW1pdCxsb3dlcl9ib3VuZFxuICAgICAgICAgICAgfVxuICAgICAgICApO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHJlc3VsdCA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgIHJlc3VsdC5yb3dzLmxlbmd0aCA9PSAwXG4gICAgICAgICkge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8vIGFkZCBuZXcgcm93c1xuICAgICAgICBhbGxSb3dzID0gYWxsUm93cy5jb25jYXQocmVzdWx0LnJvd3MpO1xuXG5cbiAgICAgICAgLy9jb25zdCBpbmRleE9mRmlyc3RSb3cgPSBiaWdJbnQoIHJlc3VsdC5yb3dzWzBdW3RhYmxlX2tleV0gKTtcblxuICAgICAgICBjb25zdCBudW1PZlJvd3MgPSByZXN1bHQucm93cy5sZW5ndGg7XG4gICAgICAgIGNvbnN0IGxhc3RSb3cgPSByZXN1bHQucm93c1tudW1PZlJvd3MtMV07XG5cbiAgICAgICAgY29uc3QgaW5kZXhPZkxhc3RSb3cgPSBiaWdJbnQobGFzdFJvd1t0YWJsZV9rZXldKTtcblxuICAgICAgICBcbiAgICAgICAgLy9jb25zb2xlLmxvZygnSW5kZXggb2YgRmlyc3QgUm93OiAnK2luZGV4T2ZGaXJzdFJvdy50b1N0cmluZygpKVxuICAgICAgICAvL2NvbnNvbGUubG9nKCdJbmRleCBvZiBMYXN0IFJvdzogJytpbmRleE9mTGFzdFJvdy50b1N0cmluZygpKVxuXG5cbiAgICAgICAgaWYgKHJlc3VsdC5tb3JlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGxvd2VyX2JvdW5kID0gaW5kZXhPZkxhc3RSb3cuYWRkKDEpLnRvU3RyaW5nKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFsbFJvd3M7XG59XG5cblxuLypcbmV4cG9ydCBmdW5jdGlvbiBnZXRFb3NCYWxhbmNlKGVvcyxhY2NvdW50TmFtZTpzdHJpbmcpOlByb21pc2U8c3RyaW5nPlxue1xuICAgIHJldHVybiBnZXRUb2tlbkJhbGFuY2UoXG4gICAgICAgIGVvcyxcbiAgICAgICAgJ2Vvc2lvLnRva2VuJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdFT1MnXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEJ0Y0JhbGFuY2UoZW9zLGFjY291bnROYW1lOnN0cmluZyk6UHJvbWlzZTxzdHJpbmc+XG57XG4gICAgcmV0dXJuIGdldFRva2VuQmFsYW5jZShcbiAgICAgICAgZW9zLFxuICAgICAgICAnZW9zYmV0dG9rZW5zJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdCVEMnXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEJldEJhbGFuY2UoZW9zLGFjY291bnROYW1lOnN0cmluZyk6UHJvbWlzZTxzdHJpbmc+XG57XG4gICAgcmV0dXJuIGdldFRva2VuQmFsYW5jZShcbiAgICAgICAgZW9zLFxuICAgICAgICAnYmV0ZGl2aWRlbmRzJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdCRVQnXG4gICAgKTtcbn1cbiovXG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRva2VuQmFsYW5jZShcbiAgICBlb3MsXG4gICAgdG9rZW5Db250cmFjdEFjY291bnQ6c3RyaW5nLFxuICAgIHRva2VuSG9sZGVyQWNjb3VudDpzdHJpbmcsXG4gICAgdG9rZW5TeW1ib2w6c3RyaW5nLFxuICAgIHRhYmxlID0gJ2FjY291bnRzJ1xuKTpQcm9taXNlPHN0cmluZz4ge1xuICAgIGNvbnN0IHJvd3MgPSBhd2FpdCBnZXRUYWJsZVJvd3M8SVRva2VuQmFsYW5jZVJvdz4oXG4gICAgICAgIGVvcyxcbiAgICAgICAge1xuICAgICAgICAgICAgY29kZTogdG9rZW5Db250cmFjdEFjY291bnQsXG4gICAgICAgICAgICBzY29wZTogdG9rZW5Ib2xkZXJBY2NvdW50LFxuICAgICAgICAgICAgdGFibGVcbiAgICAgICAgfVxuICAgICk7XG5cbiAgICBmb3IgKHZhciByb3cgb2Ygcm93cykge1xuICAgICAgICB2YXIgcGFydHMgPSByb3cuYmFsYW5jZS5zcGxpdCgnICcpO1xuXG4gICAgICAgIGlmIChwYXJ0c1sxXSA9PSB0b2tlblN5bWJvbCkge1xuICAgICAgICAgICAgcmV0dXJuIHBhcnRzWzBdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuICcwJztcbn1cblxuXG5pbnRlcmZhY2UgSUdsb2JhbFZhclJvdyB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICB2YWw6IHN0cmluZztcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEdsb2JhbFZhcmlhYmxlKFxuICAgIGVvcyxcbiAgICBzbWFydENvbnRyYWN0QWNjb3VudDpzdHJpbmcsXG4gICAgdmFyaWFibGVJZDogbnVtYmVyXG4pIHtcbiAgICBjb25zdCByb3dzID0gYXdhaXQgZ2V0VGFibGVSb3dzPElHbG9iYWxWYXJSb3c+KFxuICAgICAgICBlb3MsXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvZGU6IHNtYXJ0Q29udHJhY3RBY2NvdW50LFxuICAgICAgICAgICAgc2NvcGU6IHNtYXJ0Q29udHJhY3RBY2NvdW50LFxuICAgICAgICAgICAgdGFibGU6ICdnbG9iYWx2YXJzJ1xuICAgICAgICB9XG4gICAgKTtcblxuICAgIFxuICAgIGlmIChyb3dzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDb250cmFjdCBub3QgSU5JVFwiKTtcbiAgICB9XG5cblxuICAgIGZvciAodmFyIHJvdyBvZiByb3dzKSB7XG4gICAgICAgIGlmICh2YXJpYWJsZUlkID09IHJvdy5pZCkge1xuICAgICAgICAgICAgcmV0dXJuIHJvdy52YWw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xufVxuXG4gXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0QWNjb3VudElkRm9yQWN0aXZlRWFzeUFjY291bnRCZXQoXG4gICAgZW9zLFxuICAgIGVhc3lBY2NvdW50Q29udHJhY3Q6c3RyaW5nLFxuICAgIGJldElkOnN0cmluZ1xuKTpQcm9taXNlPHN0cmluZz4ge1xuICAgIGNvbnN0IHJvd3MgPSBhd2FpdCBnZXRVbmlxdWVUYWJsZVJvdzxJRWFzeUFjY291bnRBY3RpdmVCZXQ+KFxuICAgICAgICBlb3MsXG4gICAgICAgIGVhc3lBY2NvdW50Q29udHJhY3QsZWFzeUFjY291bnRDb250cmFjdCxcbiAgICAgICAgJ2FjdGl2ZWJldHMnLFxuICAgICAgICBiZXRJZFxuICAgICk7XG5cbiAgICByZXR1cm4gcm93cyAmJiByb3dzLmxlbmd0aCA+IDAgP1xuICAgICAgICAgICAgcm93c1swXS5rZXlfaWQgOlxuICAgICAgICAgICAgdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VW5pcXVlVGFibGVSb3c8VD4oXG4gICAgZW9zLFxuICAgIGNvZGU6c3RyaW5nLHNjb3BlOnN0cmluZyx0YWJsZTpzdHJpbmcsXG4gICAgcm93SWQ6c3RyaW5nXG4pIHtcbiAgICByZXR1cm4gZ2V0VGFibGVSb3dzPFQ+KFxuICAgICAgICBlb3MsXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvZGUsXG4gICAgICAgICAgICBzY29wZSxcbiAgICAgICAgICAgIHRhYmxlLFxuXG4gICAgICAgICAgICBsb3dlcl9ib3VuZDogcm93SWQsXG4gICAgICAgICAgICB1cHBlcl9ib3VuZDogcm93SWRcbiAgICAgICAgfVxuICAgICk7XG59XG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRhYmxlUm93czxUPihcbiAgICBlb3MsXG4gICAgcGFyYW1ldGVyczogSVRhYmxlUm93UXVlcnlQYXJhbWV0ZXJzXG4pOiBQcm9taXNlPFRbXT4ge1xuICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGdldFRhYmxlUm93c1Jlc3VsdDxUPihcbiAgICAgICAgZW9zLFxuICAgICAgICBwYXJhbWV0ZXJzXG4gICAgKTtcblxuICAgIHJldHVybiByZXN1bHQgP1xuICAgICAgICAgICAgcmVzdWx0LnJvd3MgOlxuICAgICAgICAgICAgdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUYWJsZVJvd1F1ZXJ5UmVzdWx0PFQ+XG57XG4gICAgcm93czpUW107XG4gICAgbW9yZTpib29sZWFuO1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0VGFibGVSb3dzUmVzdWx0PFQ+KFxuICAgIGVvcyxcbiAgICBwYXJhbWV0ZXJzOklUYWJsZVJvd1F1ZXJ5UGFyYW1ldGVyc1xuKTpQcm9taXNlPElUYWJsZVJvd1F1ZXJ5UmVzdWx0PFQ+PiB7XG4gICAgcGFyYW1ldGVycy5qc29uID0gdHJ1ZTtcblxuICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGVvcy5nZXRUYWJsZVJvd3MocGFyYW1ldGVycyk7XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NvdW5kIG5vdCBnZXQgdGFibGUgcm93cycpO1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG5cbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG59Il19