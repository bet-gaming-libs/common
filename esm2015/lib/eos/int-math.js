/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/int-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import Big from 'big.js';
export class AssetMath {
    /**
     * @param {?} precision
     * @param {?} coin_name
     */
    constructor(precision, coin_name) {
        this._integer = 0;
        this.precision = precision;
        this._coin_name = coin_name;
    }
    /**
     * @param {?} intVal
     * @return {?}
     */
    initWithInteger(intVal) {
        Big.RM = 0;
        this._integer = Number(new Big(intVal).toFixed(0));
    }
    /**
     * @param {?} intDec
     * @return {?}
     */
    initWithDecimal(intDec) {
        Big.RM = 0;
        this._integer = Number(new Big(intDec).times(`1e${this.precision}`).toFixed(0));
    }
    /**
     * @param {?} valDecimal
     * @return {?}
     */
    decToInt(valDecimal) {
        Big.RM = 0;
        return Number(new Big(valDecimal).times(`1e${this.precision}`).toFixed(0));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    addInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).plus(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    addDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).add((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    subInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    subDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    divInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    divDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    multInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    multDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @return {?}
     */
    get assetString() {
        return `${this.toDecimalString()} ${this.coinName}`;
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.toDecimalString();
    }
    /**
     * @return {?}
     */
    toDecimalString() {
        /** @type {?} */
        let decimal = new Big(this._integer).times(`1e-${this.precision}`);
        return decimal.toFixed(this.precision);
    }
    /**
     * @return {?}
     */
    get integer() {
        return this._integer;
    }
    /**
     * @return {?}
     */
    get coinName() {
        return this._coin_name;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._integer;
    /** @type {?} */
    AssetMath.prototype.precision;
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._coin_name;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50LW1hdGguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvaW50LW1hdGgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEdBQUcsTUFBTSxRQUFRLENBQUM7QUFFekIsTUFBTSxPQUFPLFNBQVM7Ozs7O0lBS2xCLFlBQVksU0FBaUIsRUFBRSxTQUFpQjtRQUM1QyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQztRQUNsQixJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQztJQUNoQyxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxNQUF1QjtRQUNuQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7O0lBQ0QsZUFBZSxDQUFDLE1BQXVCO1FBQ25DLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ1gsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsVUFBMkI7UUFDaEMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDWCxPQUFPLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvRSxDQUFDOzs7Ozs7O0lBRUQsTUFBTSxDQUFDLFVBQTJCO1FBQzlCLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNoRSxPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFDRCxNQUFNLENBQUMsVUFBMkI7UUFDOUIsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlFLE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUVELE1BQU0sQ0FBQyxVQUEyQjtRQUM5QixHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVYLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDakUsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBQ0QsTUFBTSxDQUFDLFVBQTJCO1FBQzlCLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRixPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFFRCxNQUFNLENBQUMsVUFBMkI7UUFDOUIsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQy9ELE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUNELE1BQU0sQ0FBQyxVQUEyQjtRQUM5QixHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVYLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUUsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBRUQsT0FBTyxDQUFDLFVBQTJCO1FBQy9CLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNqRSxPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFFRCxPQUFPLENBQUMsVUFBMkI7UUFDL0IsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hGLE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFBO0lBQ3ZELENBQUM7Ozs7SUFDRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7O0lBRUQsZUFBZTs7WUFDUCxPQUFPLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUNsRSxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxJQUFJLE9BQU87UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7OztJQUNELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDO0NBQ0o7Ozs7OztJQS9GRyw2QkFBeUI7O0lBQ3pCLDhCQUEyQjs7Ozs7SUFDM0IsK0JBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IEJpZyBmcm9tICdiaWcuanMnO1xuXG5leHBvcnQgY2xhc3MgQXNzZXRNYXRoIHtcbiAgICBwcml2YXRlIF9pbnRlZ2VyOiBudW1iZXI7XG4gICAgcmVhZG9ubHkgcHJlY2lzaW9uOiBudW1iZXI7XG4gICAgcHJpdmF0ZSBfY29pbl9uYW1lOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3RvcihwcmVjaXNpb246IG51bWJlciwgY29pbl9uYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgdGhpcy5faW50ZWdlciA9IDA7XG4gICAgICAgIHRoaXMucHJlY2lzaW9uID0gcHJlY2lzaW9uO1xuICAgICAgICB0aGlzLl9jb2luX25hbWUgPSBjb2luX25hbWU7XG4gICAgfVxuXG4gICAgaW5pdFdpdGhJbnRlZ2VyKGludFZhbDogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyhpbnRWYWwpLnRvRml4ZWQoMCkpO1xuICAgIH1cbiAgICBpbml0V2l0aERlY2ltYWwoaW50RGVjOiBzdHJpbmcgfCBudW1iZXIpIHtcbiAgICAgICAgQmlnLlJNID0gMDtcbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKGludERlYykudGltZXMoYDFlJHt0aGlzLnByZWNpc2lvbn1gKS50b0ZpeGVkKDApKTtcbiAgICB9XG5cbiAgICBkZWNUb0ludCh2YWxEZWNpbWFsOiBzdHJpbmcgfCBudW1iZXIpOiBudW1iZXIge1xuICAgICAgICBCaWcuUk0gPSAwO1xuICAgICAgICByZXR1cm4gTnVtYmVyKG5ldyBCaWcodmFsRGVjaW1hbCkudGltZXMoYDFlJHt0aGlzLnByZWNpc2lvbn1gKS50b0ZpeGVkKDApKTtcbiAgICB9XG5cbiAgICBhZGRJbnQodmFsSW50ZWdlcjogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLnBsdXModmFsSW50ZWdlcikpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgYWRkRGVjKHZhbERlY2ltYWw6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS5hZGQodGhpcy5kZWNUb0ludCh2YWxEZWNpbWFsKSkpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBzdWJJbnQodmFsSW50ZWdlcjogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLm1pbnVzKHZhbEludGVnZXIpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIHN1YkRlYyh2YWxEZWNpbWFsOiBzdHJpbmcgfCBudW1iZXIpIHtcbiAgICAgICAgQmlnLlJNID0gMDtcblxuICAgICAgICB0aGlzLl9pbnRlZ2VyID0gTnVtYmVyKG5ldyBCaWcodGhpcy5faW50ZWdlcikubWludXModGhpcy5kZWNUb0ludCh2YWxEZWNpbWFsKSkpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBkaXZJbnQodmFsSW50ZWdlcjogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLmRpdih2YWxJbnRlZ2VyKSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgICBkaXZEZWModmFsRGVjaW1hbDogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLmRpdih0aGlzLmRlY1RvSW50KHZhbERlY2ltYWwpKSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIG11bHRJbnQodmFsSW50ZWdlcjogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLnRpbWVzKHZhbEludGVnZXIpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgbXVsdERlYyh2YWxEZWNpbWFsOiBzdHJpbmcgfCBudW1iZXIpIHtcbiAgICAgICAgQmlnLlJNID0gMDtcblxuICAgICAgICB0aGlzLl9pbnRlZ2VyID0gTnVtYmVyKG5ldyBCaWcodGhpcy5faW50ZWdlcikudGltZXModGhpcy5kZWNUb0ludCh2YWxEZWNpbWFsKSkpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBnZXQgYXNzZXRTdHJpbmcoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIGAke3RoaXMudG9EZWNpbWFsU3RyaW5nKCl9ICR7dGhpcy5jb2luTmFtZX1gXG4gICAgfVxuICAgIGdldCBkZWNpbWFsKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLnRvRGVjaW1hbFN0cmluZygpO1xuICAgIH1cblxuICAgIHRvRGVjaW1hbFN0cmluZygpOiBzdHJpbmcge1xuICAgICAgICBsZXQgZGVjaW1hbCA9IG5ldyBCaWcodGhpcy5faW50ZWdlcikudGltZXMoYDFlLSR7dGhpcy5wcmVjaXNpb259YCk7XG4gICAgICAgIHJldHVybiBkZWNpbWFsLnRvRml4ZWQodGhpcy5wcmVjaXNpb24pO1xuICAgIH1cblxuICAgIGdldCBpbnRlZ2VyKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9pbnRlZ2VyO1xuICAgIH1cbiAgICBnZXQgY29pbk5hbWUoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvaW5fbmFtZTtcbiAgICB9XG59XG5cblxuIl19