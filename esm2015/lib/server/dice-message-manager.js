/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/dice-message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { DiceMessage } from './messages';
import { SocketMessageManager } from './message-manager';
/**
 * @abstract
 */
export class DiceMessageManager extends SocketMessageManager {
    /**
     * @param {?} socket
     * @param {?} messageReceiver
     */
    constructor(socket, messageReceiver) {
        super(socket, DiceMessage.PREFIX, messageReceiver);
    }
    /**
     * @protected
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    sendDiceMessage(messageName, ...args) {
        /** @type {?} */
        var start = [messageName];
        this.sendMessage.apply(this, start.concat(args));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGljZS1tZXNzYWdlLW1hbmFnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2ZXIvZGljZS1tZXNzYWdlLW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7O0FBSXpELE1BQU0sT0FBZ0Isa0JBQW1CLFNBQVEsb0JBQW9COzs7OztJQUVqRSxZQUFZLE1BQWMsRUFBQyxlQUFtQjtRQUMxQyxLQUFLLENBQUMsTUFBTSxFQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUMsZUFBZSxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7OztJQUVTLGVBQWUsQ0FBQyxXQUFrQixFQUFFLEdBQUcsSUFBVTs7WUFDbkQsS0FBSyxHQUFHLENBQUMsV0FBVyxDQUFDO1FBRXpCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUNsQixJQUFJLEVBQ0osS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FDckIsQ0FBQztJQUNOLENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpY2VNZXNzYWdlIH0gZnJvbSAnLi9tZXNzYWdlcyc7XG5pbXBvcnQgeyBTb2NrZXRNZXNzYWdlTWFuYWdlciB9IGZyb20gJy4vbWVzc2FnZS1tYW5hZ2VyJztcbmltcG9ydCB7IElTb2NrZXQgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEaWNlTWVzc2FnZU1hbmFnZXIgZXh0ZW5kcyBTb2NrZXRNZXNzYWdlTWFuYWdlclxue1xuICAgIGNvbnN0cnVjdG9yKHNvY2tldDpJU29ja2V0LG1lc3NhZ2VSZWNlaXZlcjphbnkpIHtcbiAgICAgICAgc3VwZXIoc29ja2V0LERpY2VNZXNzYWdlLlBSRUZJWCxtZXNzYWdlUmVjZWl2ZXIpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBzZW5kRGljZU1lc3NhZ2UobWVzc2FnZU5hbWU6c3RyaW5nLCAuLi5hcmdzOmFueVtdKSB7XG4gICAgICAgIHZhciBzdGFydCA9IFttZXNzYWdlTmFtZV07XG5cbiAgICAgICAgdGhpcy5zZW5kTWVzc2FnZS5hcHBseShcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBzdGFydC5jb25jYXQoYXJncylcbiAgICAgICAgKTtcbiAgICB9XG59Il19