/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { makeRequest } from './request';
import { dispatchMessage } from './messages';
export class MessageManager {
    /**
     * @param {?} emitter
     * @param {?} messagePrefix
     */
    constructor(emitter, messagePrefix) {
        this.emitter = emitter;
        this.messagePrefix = messagePrefix;
    }
    /**
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    sendMessage(messageName, ...args) {
        /** @type {?} */
        const start = [this.messagePrefix, messageName];
        /** @type {?} */
        const argsArray = start.concat(args);
        this.emitter.emit.apply(this.emitter, argsArray);
        return argsArray;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.emitter;
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.messagePrefix;
}
export class SocketMessageManager extends MessageManager {
    /**
     * @param {?} socket
     * @param {?} messagePrefix
     * @param {?} messageReceiver
     */
    constructor(socket, messagePrefix, messageReceiver) {
        super(socket, messagePrefix);
        this.socket = socket;
        this.messageReceiver = messageReceiver;
        this.setupMessageListener();
    }
    /**
     * @private
     * @return {?}
     */
    setupMessageListener() {
        // listen to all messages with message prefix
        /** @type {?} */
        const receiver = this.messageReceiver;
        this.socket.on(this.messagePrefix, (/**
         * @return {?}
         */
        function () {
            dispatchMessage(arguments, receiver);
        }));
    }
    /**
     * @protected
     * @template T
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    makeRequest(messageName, ...args) {
        return makeRequest(this.messagePrefix, messageName, args, this.socket);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.socket;
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.messageReceiver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL21lc3NhZ2UtbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUNBLE9BQU8sRUFBQyxXQUFXLEVBQUMsTUFBTSxXQUFXLENBQUM7QUFFdEMsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLFlBQVksQ0FBQztBQUczQyxNQUFNLE9BQU8sY0FBYzs7Ozs7SUFFdkIsWUFDYyxPQUFnQixFQUNoQixhQUFvQjtRQURwQixZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQ2hCLGtCQUFhLEdBQWIsYUFBYSxDQUFPO0lBRWxDLENBQUM7Ozs7OztJQUVNLFdBQVcsQ0FBQyxXQUFrQixFQUFFLEdBQUcsSUFBVTs7Y0FFMUMsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBQyxXQUFXLENBQUM7O2NBQ3hDLFNBQVMsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztRQUVwQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQ25CLElBQUksQ0FBQyxPQUFPLEVBQ1osU0FBUyxDQUNaLENBQUM7UUFFRixPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0NBQ0o7Ozs7OztJQWpCTyxpQ0FBMEI7Ozs7O0lBQzFCLHVDQUE4Qjs7QUFrQnRDLE1BQU0sT0FBTyxvQkFBcUIsU0FBUSxjQUFjOzs7Ozs7SUFFcEQsWUFDWSxNQUFjLEVBQ3RCLGFBQW9CLEVBQ1osZUFBbUI7UUFFM0IsS0FBSyxDQUFDLE1BQU0sRUFBQyxhQUFhLENBQUMsQ0FBQztRQUpwQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBRWQsb0JBQWUsR0FBZixlQUFlLENBQUk7UUFJM0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFFTyxvQkFBb0I7OztjQUVsQixRQUFRLEdBQUcsSUFBSSxDQUFDLGVBQWU7UUFFckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQ1YsSUFBSSxDQUFDLGFBQWE7OztRQUNsQjtZQUNJLGVBQWUsQ0FBQyxTQUFTLEVBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEMsQ0FBQyxFQUNKLENBQUM7SUFDTixDQUFDOzs7Ozs7OztJQUVTLFdBQVcsQ0FDakIsV0FBa0IsRUFDbEIsR0FBRyxJQUFVO1FBR2IsT0FBTyxXQUFXLENBQ2QsSUFBSSxDQUFDLGFBQWEsRUFDbEIsV0FBVyxFQUNYLElBQUksRUFDSixJQUFJLENBQUMsTUFBTSxDQUNkLENBQUM7SUFDTixDQUFDO0NBQ0o7Ozs7OztJQWpDTyxzQ0FBc0I7Ozs7O0lBRXRCLCtDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElQcm9taXNlIH0gZnJvbSAnLi4vdXRpbC9wcm9taXNlJztcbmltcG9ydCB7bWFrZVJlcXVlc3R9IGZyb20gJy4vcmVxdWVzdCc7XG5pbXBvcnQgeyBJU29ja2V0LCBJRW1pdHRlciB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5pbXBvcnQge2Rpc3BhdGNoTWVzc2FnZX0gZnJvbSAnLi9tZXNzYWdlcyc7XG5cblxuZXhwb3J0IGNsYXNzIE1lc3NhZ2VNYW5hZ2VyXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByb3RlY3RlZCBlbWl0dGVyOklFbWl0dGVyLFxuICAgICAgICBwcm90ZWN0ZWQgbWVzc2FnZVByZWZpeDpzdHJpbmdcbiAgICApIHsgICAgXG4gICAgfVxuXG4gICAgcHVibGljIHNlbmRNZXNzYWdlKG1lc3NhZ2VOYW1lOnN0cmluZywgLi4uYXJnczphbnlbXSlcbiAgICB7XG4gICAgICAgIGNvbnN0IHN0YXJ0ID0gW3RoaXMubWVzc2FnZVByZWZpeCxtZXNzYWdlTmFtZV07XG4gICAgICAgIGNvbnN0IGFyZ3NBcnJheSA9IHN0YXJ0LmNvbmNhdChhcmdzKTtcblxuICAgICAgICB0aGlzLmVtaXR0ZXIuZW1pdC5hcHBseShcbiAgICAgICAgICAgIHRoaXMuZW1pdHRlcixcbiAgICAgICAgICAgIGFyZ3NBcnJheVxuICAgICAgICApO1xuXG4gICAgICAgIHJldHVybiBhcmdzQXJyYXk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgU29ja2V0TWVzc2FnZU1hbmFnZXIgZXh0ZW5kcyBNZXNzYWdlTWFuYWdlclxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHNvY2tldDpJU29ja2V0LFxuICAgICAgICBtZXNzYWdlUHJlZml4OnN0cmluZyxcbiAgICAgICAgcHJpdmF0ZSBtZXNzYWdlUmVjZWl2ZXI6YW55XG4gICAgKSB7XG4gICAgICAgIHN1cGVyKHNvY2tldCxtZXNzYWdlUHJlZml4KTtcblxuICAgICAgICB0aGlzLnNldHVwTWVzc2FnZUxpc3RlbmVyKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXR1cE1lc3NhZ2VMaXN0ZW5lcigpIHtcbiAgICAgICAgLy8gbGlzdGVuIHRvIGFsbCBtZXNzYWdlcyB3aXRoIG1lc3NhZ2UgcHJlZml4XG4gICAgICAgIGNvbnN0IHJlY2VpdmVyID0gdGhpcy5tZXNzYWdlUmVjZWl2ZXI7XG5cbiAgICAgICAgdGhpcy5zb2NrZXQub24oXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VQcmVmaXgsXG4gICAgICAgICAgICBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICBkaXNwYXRjaE1lc3NhZ2UoYXJndW1lbnRzLHJlY2VpdmVyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgbWFrZVJlcXVlc3Q8VD4oXG4gICAgICAgIG1lc3NhZ2VOYW1lOnN0cmluZyxcbiAgICAgICAgLi4uYXJnczphbnlbXVxuICAgICk6SVByb21pc2U8VD5cbiAgICB7XG4gICAgICAgIHJldHVybiBtYWtlUmVxdWVzdDxUPihcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZVByZWZpeCxcbiAgICAgICAgICAgIG1lc3NhZ2VOYW1lLFxuICAgICAgICAgICAgYXJncyxcbiAgICAgICAgICAgIHRoaXMuc29ja2V0XG4gICAgICAgICk7XG4gICAgfVxufSJdfQ==