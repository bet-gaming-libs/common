/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/request.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { CustomPromise } from '../util/promise';
import { ResponseManager } from "../util/response";
/**
 * @template T
 * @param {?} messagePrefix
 * @param {?} messageName
 * @param {?} ars
 * @param {?} socket
 * @param {?=} promise
 * @return {?}
 */
export function makeRequest(messagePrefix, messageName, ars, socket, promise = new CustomPromise()) {
    /** @type {?} */
    var args = [messagePrefix, messageName];
    for (var i = 0; i < ars.length; i++) {
        args.push(ars[i]);
    }
    args.push(ResponseManager.newHandler(promise));
    socket.emit.apply(socket, args);
    return promise;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWVzdC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9yZXF1ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFDLGFBQWEsRUFBVSxNQUFNLGlCQUFpQixDQUFDO0FBRXZELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQzs7Ozs7Ozs7OztBQUtuRCxNQUFNLFVBQVUsV0FBVyxDQUN2QixhQUFvQixFQUNwQixXQUFrQixFQUNsQixHQUFPLEVBQ1AsTUFBZSxFQUVmLFVBQTJCLElBQUksYUFBYSxFQUFLOztRQUc3QyxJQUFJLEdBQVMsQ0FBQyxhQUFhLEVBQUMsV0FBVyxDQUFDO0lBQzVDLEtBQUssSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1FBQy9CLElBQUksQ0FBQyxJQUFJLENBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFFLENBQUM7S0FDdkI7SUFFRCxJQUFJLENBQUMsSUFBSSxDQUFFLGVBQWUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUUsQ0FBQztJQUVqRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FDYixNQUFNLEVBQ04sSUFBSSxDQUNQLENBQUM7SUFFRixPQUFPLE9BQU8sQ0FBQztBQUNuQixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDdXN0b21Qcm9taXNlLElQcm9taXNlfSBmcm9tICcuLi91dGlsL3Byb21pc2UnO1xuXG5pbXBvcnQgeyBSZXNwb25zZU1hbmFnZXIgfSBmcm9tIFwiLi4vdXRpbC9yZXNwb25zZVwiO1xuXG5pbXBvcnQgeyBJRW1pdHRlciB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGZ1bmN0aW9uIG1ha2VSZXF1ZXN0PFQ+KFxuICAgIG1lc3NhZ2VQcmVmaXg6c3RyaW5nLFxuICAgIG1lc3NhZ2VOYW1lOnN0cmluZyxcbiAgICBhcnM6YW55LFxuICAgIHNvY2tldDpJRW1pdHRlcixcbiAgICBcbiAgICBwcm9taXNlOkN1c3RvbVByb21pc2U8VD4gPSBuZXcgQ3VzdG9tUHJvbWlzZTxUPigpXG4pOklQcm9taXNlPFQ+XG57XG4gICAgdmFyIGFyZ3M6YW55W10gPSBbbWVzc2FnZVByZWZpeCxtZXNzYWdlTmFtZV07XG4gICAgZm9yICh2YXIgaT0wOyBpIDwgYXJzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGFyZ3MucHVzaCggYXJzW2ldICk7XG4gICAgfVxuXG4gICAgYXJncy5wdXNoKCBSZXNwb25zZU1hbmFnZXIubmV3SGFuZGxlcihwcm9taXNlKSApO1xuXG4gICAgc29ja2V0LmVtaXQuYXBwbHkoXG4gICAgICAgIHNvY2tldCxcbiAgICAgICAgYXJnc1xuICAgICk7XG5cbiAgICByZXR1cm4gcHJvbWlzZTtcbn0iXX0=