/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/crash-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IActiveCrashBet() { }
if (false) {
    /** @type {?} */
    IActiveCrashBet.prototype.id;
    /** @type {?} */
    IActiveCrashBet.prototype.bettor;
    /** @type {?} */
    IActiveCrashBet.prototype.referral;
    /** @type {?} */
    IActiveCrashBet.prototype.referral_data;
    /** @type {?} */
    IActiveCrashBet.prototype.bet_amt;
    /** @type {?} */
    IActiveCrashBet.prototype.max_win;
    /** @type {?} */
    IActiveCrashBet.prototype.adv_tap_out_times100;
    /** @type {?} */
    IActiveCrashBet.prototype.seed;
    /** @type {?} */
    IActiveCrashBet.prototype.signing_key;
}
/**
 * @record
 */
export function IResolvedCrashBet() { }
/**
 * @record
 */
export function ITapOutRequestData() { }
if (false) {
    /** @type {?} */
    ITapOutRequestData.prototype.betId;
    /** @type {?} */
    ITapOutRequestData.prototype.signature;
}
/**
 * @record
 */
export function ITappedOutMessageData() { }
if (false) {
    /** @type {?} */
    ITappedOutMessageData.prototype.betId;
    /** @type {?} */
    ITappedOutMessageData.prototype.value;
}
/**
 * @record
 */
export function ICrashResultData() { }
if (false) {
    /** @type {?} */
    ICrashResultData.prototype.cashOutPoint;
    /**
     * isInsant = value == 0 *
     * @type {?}
     */
    ICrashResultData.prototype.crashPoint;
    /** @type {?} */
    ICrashResultData.prototype.isMaxLiabilitiesReached;
    /** @type {?} */
    ICrashResultData.prototype.isEndedEarly;
}
/**
 * @record
 */
export function ICrashMessageData() { }
if (false) {
    /** @type {?} */
    ICrashMessageData.prototype.bet;
    /** @type {?} */
    ICrashMessageData.prototype.result;
}
/**
 * @record
 */
export function ICrashGameStateData() { }
if (false) {
    /** @type {?} */
    ICrashGameStateData.prototype.currentGameId;
    /** @type {?} */
    ICrashGameStateData.prototype.isMoving;
    /** @type {?} */
    ICrashGameStateData.prototype.secondsBeforeStart;
    /** @type {?} */
    ICrashGameStateData.prototype.currentPoint;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY3Jhc2gtZGF0YS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9kYXRhL2NyYXNoLWRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSxxQ0FXQzs7O0lBVEcsNkJBQVU7O0lBQ1YsaUNBQWM7O0lBQ2QsbUNBQWdCOztJQUNoQix3Q0FBcUI7O0lBQ3JCLGtDQUFlOztJQUNmLGtDQUFlOztJQUNmLCtDQUE0Qjs7SUFDNUIsK0JBQVk7O0lBQ1osc0NBQW1COzs7OztBQUd2Qix1Q0FHQzs7OztBQUVELHdDQUtDOzs7SUFIRyxtQ0FBYTs7SUFFYix1Q0FBaUI7Ozs7O0FBR3JCLDJDQUlDOzs7SUFGRyxzQ0FBYTs7SUFDYixzQ0FBYTs7Ozs7QUFHakIsc0NBT0M7OztJQUxHLHdDQUFvQjs7Ozs7SUFFcEIsc0NBQWtCOztJQUNsQixtREFBZ0M7O0lBQ2hDLHdDQUFxQjs7Ozs7QUFHekIsdUNBSUM7OztJQUZHLGdDQUFzQjs7SUFDdEIsbUNBQXdCOzs7OztBQUk1Qix5Q0FNQzs7O0lBSkcsNENBQXFCOztJQUNyQix1Q0FBaUI7O0lBQ2pCLGlEQUEwQjs7SUFDMUIsMkNBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSVJlc29sdmVkQmV0IH0gZnJvbSBcIi4vY29tbW9uXCI7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQWN0aXZlQ3Jhc2hCZXRcbntcbiAgICBpZDpzdHJpbmc7XG4gICAgYmV0dG9yOnN0cmluZztcbiAgICByZWZlcnJhbDpzdHJpbmc7XG4gICAgcmVmZXJyYWxfZGF0YTpzdHJpbmc7XG4gICAgYmV0X2FtdDpzdHJpbmc7XG4gICAgbWF4X3dpbjpzdHJpbmc7XG4gICAgYWR2X3RhcF9vdXRfdGltZXMxMDA6bnVtYmVyO1xuICAgIHNlZWQ6c3RyaW5nO1xuICAgIHNpZ25pbmdfa2V5OnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJUmVzb2x2ZWRDcmFzaEJldCBleHRlbmRzIElSZXNvbHZlZEJldFxue1xuXG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVRhcE91dFJlcXVlc3REYXRhXG57XG4gICAgYmV0SWQ6c3RyaW5nO1xuICAgIC8vaW5BZHZhbmNlQXRWYWx1ZTpzdHJpbmc7XG4gICAgc2lnbmF0dXJlOnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJVGFwcGVkT3V0TWVzc2FnZURhdGFcbntcbiAgICBiZXRJZDpzdHJpbmc7XG4gICAgdmFsdWU6bnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElDcmFzaFJlc3VsdERhdGFcbntcbiAgICBjYXNoT3V0UG9pbnQ6bnVtYmVyO1xuICAgIC8qKiBpc0luc2FudCA9IHZhbHVlID09IDAgKiovXG4gICAgY3Jhc2hQb2ludDpudW1iZXI7XG4gICAgaXNNYXhMaWFiaWxpdGllc1JlYWNoZWQ6Ym9vbGVhbjtcbiAgICBpc0VuZGVkRWFybHk6Ym9vbGVhbjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQ3Jhc2hNZXNzYWdlRGF0YVxue1xuICAgIGJldDpJUmVzb2x2ZWRDcmFzaEJldDtcbiAgICByZXN1bHQ6SUNyYXNoUmVzdWx0RGF0YTtcbn1cblxuXG5leHBvcnQgaW50ZXJmYWNlIElDcmFzaEdhbWVTdGF0ZURhdGFcbntcbiAgICBjdXJyZW50R2FtZUlkOm51bWJlcjtcbiAgICBpc01vdmluZzpib29sZWFuO1xuICAgIHNlY29uZHNCZWZvcmVTdGFydDpudW1iZXI7XG4gICAgY3VycmVudFBvaW50Om51bWJlcjtcbn0iXX0=