/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/baccarat-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IHandData() { }
if (false) {
    /** @type {?} */
    IHandData.prototype.cards;
    /** @type {?} */
    IHandData.prototype.totalValue;
}
/**
 * @record
 */
export function IBaccaratGameResultDataForMultiPlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.gameId;
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.winner;
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.hands;
}
/**
 * @record
 */
export function IBaccaratGameResultDataForSinglePlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratGameResultDataForSinglePlayer.prototype.winner;
    /** @type {?} */
    IBaccaratGameResultDataForSinglePlayer.prototype.hands;
}
/**
 * @record
 */
export function IBaccaratGameStartData() { }
if (false) {
    /** @type {?} */
    IBaccaratGameStartData.prototype.currentGameId;
    /** @type {?} */
    IBaccaratGameStartData.prototype.gameStartTime;
    /** @type {?} */
    IBaccaratGameStartData.prototype.currentServerTime;
}
/**
 * @record
 */
export function IBaccaratBetDataForSinglePlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.id;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bettor;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.referral;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.referral_data;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.player_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.banker_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.tie_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.playerpair_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bankerpair_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.seed;
}
/**
 * @record
 */
export function IResolvedBaccaratBet() { }
if (false) {
    /** @type {?} */
    IResolvedBaccaratBet.prototype.bet;
    /** @type {?} */
    IResolvedBaccaratBet.prototype.result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFjY2FyYXQtZGF0YS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9kYXRhL2JhY2NhcmF0LWRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFJQSwrQkFJQzs7O0lBRkcsMEJBQWtCOztJQUNsQiwrQkFBa0I7Ozs7O0FBR3RCLDJEQUtDOzs7SUFIRyx1REFBYzs7SUFDZCx1REFBYzs7SUFDZCxzREFBa0I7Ozs7O0FBRXRCLDREQUlDOzs7SUFGRyx3REFBYzs7SUFDZCx1REFBa0I7Ozs7O0FBR3RCLDRDQUtDOzs7SUFIRywrQ0FBcUI7O0lBQ3JCLCtDQUFxQjs7SUFDckIsbURBQXlCOzs7OztBQWlCN0IscURBYUM7OztJQVhHLDZDQUFVOztJQUNWLGlEQUFjOztJQUNkLG1EQUFnQjs7SUFDaEIsd0RBQXFCOztJQUNyQixrREFBZTs7SUFDZix5REFBc0I7O0lBQ3RCLHlEQUFzQjs7SUFDdEIsc0RBQW1COztJQUNuQiw2REFBMEI7O0lBQzFCLDZEQUEwQjs7SUFDMUIsK0NBQVk7Ozs7O0FBSWhCLDBDQUlDOzs7SUFGRyxtQ0FBb0M7O0lBQ3BDLHNDQUE4QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IElDYXJkRGF0YSB9IGZyb20gXCIuL2NhcmQtZGF0YVwiO1xuaW1wb3J0IHsgSVJlc29sdmVkQmV0IH0gZnJvbSBcIi4vY29tbW9uXCI7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJSGFuZERhdGFcbntcbiAgICBjYXJkczpJQ2FyZERhdGFbXTtcbiAgICB0b3RhbFZhbHVlOm51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQmFjY2FyYXRHYW1lUmVzdWx0RGF0YUZvck11bHRpUGxheWVyXG57XG4gICAgZ2FtZUlkOnN0cmluZztcbiAgICB3aW5uZXI6bnVtYmVyO1xuICAgIGhhbmRzOklIYW5kRGF0YVtdO1xufVxuZXhwb3J0IGludGVyZmFjZSBJQmFjY2FyYXRHYW1lUmVzdWx0RGF0YUZvclNpbmdsZVBsYXllclxue1xuICAgIHdpbm5lcjpudW1iZXI7XG4gICAgaGFuZHM6SUhhbmREYXRhW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJhY2NhcmF0R2FtZVN0YXJ0RGF0YVxue1xuICAgIGN1cnJlbnRHYW1lSWQ6c3RyaW5nO1xuICAgIGdhbWVTdGFydFRpbWU6bnVtYmVyO1xuICAgIGN1cnJlbnRTZXJ2ZXJUaW1lOm51bWJlcjtcbn1cblxuLypcbmV4cG9ydCBpbnRlcmZhY2UgSUJhY2NhcmF0QmV0RGF0YUZvck11bHRpUGxheWVyXG57XG4gICAgYmV0X2lkOiBzdHJpbmc7XG4gICAgcmVxdWVzdGVkX2dhbWVfaWQ6IHN0cmluZztcbiAgICBiZXR0b3I6IHN0cmluZztcbiAgICByZWZlcnJhbDogc3RyaW5nO1xuICAgIHJlZmVycmFsX2RhdGE6IG51bWJlcjtcbiAgICBiZXRfdHlwZTogbnVtYmVyO1xuICAgIGJldF9hbXQ6IG51bWJlcjtcbiAgICBiZXRfdGltZTogRGF0ZTtcbn1cbiovXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJhY2NhcmF0QmV0RGF0YUZvclNpbmdsZVBsYXllclxue1xuICAgIGlkOnN0cmluZztcbiAgICBiZXR0b3I6c3RyaW5nO1xuICAgIHJlZmVycmFsOnN0cmluZztcbiAgICByZWZlcnJhbF9kYXRhOnN0cmluZztcbiAgICBiZXRfYW10OnN0cmluZztcbiAgICBwbGF5ZXJfYmV0X2FtdDpzdHJpbmc7XG4gICAgYmFua2VyX2JldF9hbXQ6c3RyaW5nO1xuICAgIHRpZV9iZXRfYW10OnN0cmluZztcbiAgICBwbGF5ZXJwYWlyX2JldF9hbXQ6c3RyaW5nO1xuICAgIGJhbmtlcnBhaXJfYmV0X2FtdDpzdHJpbmc7XG4gICAgc2VlZDpzdHJpbmc7XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJUmVzb2x2ZWRCYWNjYXJhdEJldCBleHRlbmRzIElSZXNvbHZlZEJldFxue1xuICAgIGJldDpJQmFjY2FyYXRCZXREYXRhRm9yU2luZ2xlUGxheWVyO1xuICAgIHJlc3VsdDpJQmFjY2FyYXRHYW1lUmVzdWx0RGF0YUZvclNpbmdsZVBsYXllcjtcbn1cblxuXG5cbi8qXG5leHBvcnQgaW50ZXJmYWNlIElCYWNjYXJhdEdhbWVTeW5jRGF0YVxue1xuICAgIGdhbWVTdGFydDpJQmFjY2FyYXRHYW1lU3RhcnREYXRhO1xuICAgIGxhc3RHYW1lOklCYWNjYXJhdEdhbWVSZXN1bHREYXRhO1xuICAgIHdpbnM6bnVtYmVyW107XG4gICAgYmV0czpJQmFjY2FyYXRCZXREYXRhW107XG59XG4qLyJdfQ==