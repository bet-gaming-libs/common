/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/card-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const CardSuit = {
    Hearts: 0,
    Diamonds: 1,
    Spades: 2,
    Clubs: 3,
};
export { CardSuit };
CardSuit[CardSuit.Hearts] = 'Hearts';
CardSuit[CardSuit.Diamonds] = 'Diamonds';
CardSuit[CardSuit.Spades] = 'Spades';
CardSuit[CardSuit.Clubs] = 'Clubs';
/**
 * @record
 */
export function ICardData() { }
if (false) {
    /** @type {?} */
    ICardData.prototype.suit;
    /** @type {?} */
    ICardData.prototype.rankName;
}
export class CardRankNumber {
}
CardRankNumber.ACE = 1;
CardRankNumber.TEN = 10;
CardRankNumber.JACK = 11;
CardRankNumber.QUEEN = 12;
CardRankNumber.KING = 0;
if (false) {
    /** @type {?} */
    CardRankNumber.ACE;
    /** @type {?} */
    CardRankNumber.TEN;
    /** @type {?} */
    CardRankNumber.JACK;
    /** @type {?} */
    CardRankNumber.QUEEN;
    /** @type {?} */
    CardRankNumber.KING;
}
export class CardRankName {
}
CardRankName.ACE = 'Ace';
CardRankName.JACK = 'Jack';
CardRankName.QUEEN = 'Queen';
CardRankName.KING = 'King';
if (false) {
    /** @type {?} */
    CardRankName.ACE;
    /** @type {?} */
    CardRankName.JACK;
    /** @type {?} */
    CardRankName.QUEEN;
    /** @type {?} */
    CardRankName.KING;
}
/**
 * @param {?} value
 * @return {?}
 */
export function numberToSuit(value) {
    switch (value) {
        case 0:
            return CardSuit.Hearts;
        case 1:
            return CardSuit.Diamonds;
        case 2:
            return CardSuit.Spades;
        case 3:
            return CardSuit.Clubs;
    }
}
/**
 * @param {?} value
 * @return {?}
 */
export function nameOfSuit(value) {
    switch (value) {
        case CardSuit.Hearts:
            return "Hearts";
        case CardSuit.Diamonds:
            return "Diamonds";
        case CardSuit.Spades:
            return "Spades";
        case CardSuit.Clubs:
            return "Clubs";
    }
}
/**
 * @param {?} value
 * @return {?}
 */
export function nameOfRank(value) {
    switch (value) {
        case CardRankNumber.ACE:
            return CardRankName.ACE.substr(0, 1);
        case CardRankNumber.JACK:
            return CardRankName.JACK.substr(0, 1);
        case CardRankNumber.QUEEN:
            return CardRankName.QUEEN.substr(0, 1);
        case CardRankNumber.KING:
            return CardRankName.KING.substr(0, 1);
        default:
            return value.toString();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvY2FyZC1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE1BQVksUUFBUTtJQUVoQixNQUFNLEdBQUE7SUFDTixRQUFRLEdBQUE7SUFDUixNQUFNLEdBQUE7SUFDTixLQUFLLEdBQUE7RUFDUjs7Ozs7Ozs7O0FBRUQsK0JBSUM7OztJQUZHLHlCQUFjOztJQUNkLDZCQUFnQjs7QUFHcEIsTUFBTSxPQUFPLGNBQWM7O0FBRWhCLGtCQUFHLEdBQUssQ0FBQyxDQUFDO0FBQ1Ysa0JBQUcsR0FBSyxFQUFFLENBQUM7QUFDWCxtQkFBSSxHQUFJLEVBQUUsQ0FBQztBQUNYLG9CQUFLLEdBQUcsRUFBRSxDQUFDO0FBQ1gsbUJBQUksR0FBSSxDQUFDLENBQUM7OztJQUpqQixtQkFBaUI7O0lBQ2pCLG1CQUFrQjs7SUFDbEIsb0JBQWtCOztJQUNsQixxQkFBa0I7O0lBQ2xCLG9CQUFpQjs7QUFHckIsTUFBTSxPQUFPLFlBQVk7O0FBRWQsZ0JBQUcsR0FBSyxLQUFLLENBQUM7QUFDZCxpQkFBSSxHQUFJLE1BQU0sQ0FBQztBQUNmLGtCQUFLLEdBQUcsT0FBTyxDQUFDO0FBQ2hCLGlCQUFJLEdBQUksTUFBTSxDQUFDOzs7SUFIdEIsaUJBQXFCOztJQUNyQixrQkFBc0I7O0lBQ3RCLG1CQUF1Qjs7SUFDdkIsa0JBQXNCOzs7Ozs7QUFHMUIsTUFBTSxVQUFVLFlBQVksQ0FBQyxLQUFZO0lBQ3JDLFFBQVEsS0FBSyxFQUFFO1FBQ1gsS0FBSyxDQUFDO1lBQ0YsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDO1FBRTNCLEtBQUssQ0FBQztZQUNGLE9BQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQztRQUU3QixLQUFLLENBQUM7WUFDRixPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFFM0IsS0FBSyxDQUFDO1lBQ0YsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDO0tBQzdCO0FBQ0wsQ0FBQzs7Ozs7QUFFRCxNQUFNLFVBQVUsVUFBVSxDQUFDLEtBQWM7SUFDckMsUUFBUSxLQUFLLEVBQUU7UUFDWCxLQUFLLFFBQVEsQ0FBQyxNQUFNO1lBQ2hCLE9BQU8sUUFBUSxDQUFDO1FBRXBCLEtBQUssUUFBUSxDQUFDLFFBQVE7WUFDbEIsT0FBTyxVQUFVLENBQUM7UUFFdEIsS0FBSyxRQUFRLENBQUMsTUFBTTtZQUNoQixPQUFPLFFBQVEsQ0FBQztRQUVwQixLQUFLLFFBQVEsQ0FBQyxLQUFLO1lBQ2YsT0FBTyxPQUFPLENBQUM7S0FDdEI7QUFDTCxDQUFDOzs7OztBQUVELE1BQU0sVUFBVSxVQUFVLENBQUMsS0FBWTtJQUNuQyxRQUFRLEtBQUssRUFBRTtRQUNYLEtBQUssY0FBYyxDQUFDLEdBQUc7WUFDbkIsT0FBTyxZQUFZLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7UUFFeEMsS0FBSyxjQUFjLENBQUMsSUFBSTtZQUNwQixPQUFPLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztRQUV6QyxLQUFLLGNBQWMsQ0FBQyxLQUFLO1lBQ3JCLE9BQU8sWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBRTFDLEtBQUssY0FBYyxDQUFDLElBQUk7WUFDcEIsT0FBTyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7UUFHekM7WUFDSSxPQUFPLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztLQUMvQjtBQUNMLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBDYXJkU3VpdFxue1xuICAgIEhlYXJ0cyxcbiAgICBEaWFtb25kcyxcbiAgICBTcGFkZXMsXG4gICAgQ2x1YnNcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQ2FyZERhdGFcbntcbiAgICBzdWl0OkNhcmRTdWl0O1xuICAgIHJhbmtOYW1lOnN0cmluZztcbn1cblxuZXhwb3J0IGNsYXNzIENhcmRSYW5rTnVtYmVyXG57XG4gICAgc3RhdGljIEFDRSAgID0gMTtcbiAgICBzdGF0aWMgVEVOICAgPSAxMDtcbiAgICBzdGF0aWMgSkFDSyAgPSAxMTtcbiAgICBzdGF0aWMgUVVFRU4gPSAxMjtcbiAgICBzdGF0aWMgS0lORyAgPSAwO1xufVxuXG5leHBvcnQgY2xhc3MgQ2FyZFJhbmtOYW1lXG57XG4gICAgc3RhdGljIEFDRSAgID0gJ0FjZSc7XG4gICAgc3RhdGljIEpBQ0sgID0gJ0phY2snO1xuICAgIHN0YXRpYyBRVUVFTiA9ICdRdWVlbic7XG4gICAgc3RhdGljIEtJTkcgID0gJ0tpbmcnO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbnVtYmVyVG9TdWl0KHZhbHVlOm51bWJlcik6Q2FyZFN1aXQge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgY2FzZSAwOlxuICAgICAgICAgICAgcmV0dXJuIENhcmRTdWl0LkhlYXJ0cztcblxuICAgICAgICBjYXNlIDE6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFN1aXQuRGlhbW9uZHM7XG5cbiAgICAgICAgY2FzZSAyOlxuICAgICAgICAgICAgcmV0dXJuIENhcmRTdWl0LlNwYWRlcztcblxuICAgICAgICBjYXNlIDM6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFN1aXQuQ2x1YnM7XG4gICAgfVxufVxuXG5leHBvcnQgZnVuY3Rpb24gbmFtZU9mU3VpdCh2YWx1ZTpDYXJkU3VpdCk6c3RyaW5nIHtcbiAgICBzd2l0Y2ggKHZhbHVlKSB7XG4gICAgICAgIGNhc2UgQ2FyZFN1aXQuSGVhcnRzOlxuICAgICAgICAgICAgcmV0dXJuIFwiSGVhcnRzXCI7XG5cbiAgICAgICAgY2FzZSBDYXJkU3VpdC5EaWFtb25kczpcbiAgICAgICAgICAgIHJldHVybiBcIkRpYW1vbmRzXCI7XG5cbiAgICAgICAgY2FzZSBDYXJkU3VpdC5TcGFkZXM6XG4gICAgICAgICAgICByZXR1cm4gXCJTcGFkZXNcIjtcblxuICAgICAgICBjYXNlIENhcmRTdWl0LkNsdWJzOlxuICAgICAgICAgICAgcmV0dXJuIFwiQ2x1YnNcIjtcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBuYW1lT2ZSYW5rKHZhbHVlOm51bWJlcik6c3RyaW5nIHtcbiAgICBzd2l0Y2ggKHZhbHVlKSB7XG4gICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuQUNFOlxuICAgICAgICAgICAgcmV0dXJuIENhcmRSYW5rTmFtZS5BQ0Uuc3Vic3RyKDAsMSk7XG5cbiAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5KQUNLOlxuICAgICAgICAgICAgcmV0dXJuIENhcmRSYW5rTmFtZS5KQUNLLnN1YnN0cigwLDEpO1xuXG4gICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuUVVFRU46XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFJhbmtOYW1lLlFVRUVOLnN1YnN0cigwLDEpO1xuXG4gICAgICAgIGNhc2UgQ2FyZFJhbmtOdW1iZXIuS0lORzpcbiAgICAgICAgICAgIHJldHVybiBDYXJkUmFua05hbWUuS0lORy5zdWJzdHIoMCwxKTtcblxuXG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKTtcbiAgICB9XG59Il19