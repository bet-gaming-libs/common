/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/blackjack-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IBlackjackInitialCards() { }
if (false) {
    /** @type {?} */
    IBlackjackInitialCards.prototype.dealerCards;
    /** @type {?} */
    IBlackjackInitialCards.prototype.playerCards;
}
/**
 * @record
 */
export function IInitialBlackjackBetInfo() { }
if (false) {
    /** @type {?} */
    IInitialBlackjackBetInfo.prototype.amountAsInteger;
    /** @type {?} */
    IInitialBlackjackBetInfo.prototype.token;
}
/**
 * @record
 */
export function IBlackjackGameState() { }
if (false) {
    /** @type {?} */
    IBlackjackGameState.prototype.id;
    /** @type {?} */
    IBlackjackGameState.prototype.initialBet;
    /** @type {?} */
    IBlackjackGameState.prototype.initialCards;
    /** @type {?} */
    IBlackjackGameState.prototype.isDealerBlackjack;
    /** @type {?} */
    IBlackjackGameState.prototype.commands;
    /** @type {?} */
    IBlackjackGameState.prototype.additionalPlayerCards;
    /** @type {?} */
    IBlackjackGameState.prototype.additionalDealerCards;
}
/**
 * @record
 */
export function IBlackjackHandResult() { }
if (false) {
    /** @type {?} */
    IBlackjackHandResult.prototype.sum;
    /** @type {?} */
    IBlackjackHandResult.prototype.isBlackjack;
}
/**
 * @record
 */
export function IBlackjackPlayerHandResult() { }
if (false) {
    /** @type {?} */
    IBlackjackPlayerHandResult.prototype.totalWagered;
    /** @type {?} */
    IBlackjackPlayerHandResult.prototype.payout;
}
/**
 * @record
 */
export function IResolvedBlackjackBet() { }
if (false) {
    /** @type {?} */
    IResolvedBlackjackBet.prototype.dealerHandResult;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.playerResults;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.totalWagered;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.totalPayout;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmxhY2tqYWNrLWRhdGEuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2ZXIvZGF0YS9ibGFja2phY2stZGF0YS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUVBLDRDQUlDOzs7SUFGRyw2Q0FBcUI7O0lBQ3JCLDZDQUFxQjs7Ozs7QUFJekIsOENBT0M7OztJQUxHLG1EQUF1Qjs7SUFDdkIseUNBR0M7Ozs7O0FBR0wseUNBU0M7OztJQVBHLGlDQUFVOztJQUNWLHlDQUFvQzs7SUFDcEMsMkNBQW9DOztJQUNwQyxnREFBMEI7O0lBQzFCLHVDQUFrQjs7SUFDbEIsb0RBQStCOztJQUMvQixvREFBK0I7Ozs7O0FBSW5DLDBDQUlDOzs7SUFGRyxtQ0FBVzs7SUFDWCwyQ0FBb0I7Ozs7O0FBR3hCLGdEQUlDOzs7SUFGRyxrREFBb0I7O0lBQ3BCLDRDQUFjOzs7OztBQUdsQiwyQ0FPQzs7O0lBTEcsaURBQXNDOztJQUN0Qyw4Q0FBcUM7O0lBRXJDLDZDQUFvQjs7SUFDcEIsNENBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSVJlc29sdmVkQmV0IH0gZnJvbSBcIi4vY29tbW9uXCI7XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJsYWNramFja0luaXRpYWxDYXJkc1xue1xuICAgIGRlYWxlckNhcmRzOm51bWJlcltdO1xuICAgIHBsYXllckNhcmRzOm51bWJlcltdO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUluaXRpYWxCbGFja2phY2tCZXRJbmZvXG57XG4gICAgYW1vdW50QXNJbnRlZ2VyOm51bWJlcjtcbiAgICB0b2tlbjoge1xuICAgICAgICBuYW1lOnN0cmluZztcbiAgICAgICAgcHJlY2lzaW9uOm51bWJlcjtcbiAgICB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUJsYWNramFja0dhbWVTdGF0ZVxue1xuICAgIGlkOnN0cmluZztcbiAgICBpbml0aWFsQmV0OklJbml0aWFsQmxhY2tqYWNrQmV0SW5mbztcbiAgICBpbml0aWFsQ2FyZHM6SUJsYWNramFja0luaXRpYWxDYXJkcztcbiAgICBpc0RlYWxlckJsYWNramFjazpib29sZWFuO1xuICAgIGNvbW1hbmRzOnN0cmluZ1tdO1xuICAgIGFkZGl0aW9uYWxQbGF5ZXJDYXJkczpudW1iZXJbXTtcbiAgICBhZGRpdGlvbmFsRGVhbGVyQ2FyZHM6bnVtYmVyW107XG59XG5cblxuZXhwb3J0IGludGVyZmFjZSBJQmxhY2tqYWNrSGFuZFJlc3VsdFxue1xuICAgIHN1bTpudW1iZXI7XG4gICAgaXNCbGFja2phY2s6Ym9vbGVhbjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJQmxhY2tqYWNrUGxheWVySGFuZFJlc3VsdCBleHRlbmRzIElCbGFja2phY2tIYW5kUmVzdWx0XG57XG4gICAgdG90YWxXYWdlcmVkOm51bWJlcjtcbiAgICBwYXlvdXQ6bnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElSZXNvbHZlZEJsYWNramFja0JldCBleHRlbmRzIElSZXNvbHZlZEJldFxue1xuICAgIGRlYWxlckhhbmRSZXN1bHQ6SUJsYWNramFja0hhbmRSZXN1bHQ7XG4gICAgcGxheWVyUmVzdWx0czpJQmxhY2tqYWNrSGFuZFJlc3VsdFtdO1xuICAgIFxuICAgIHRvdGFsV2FnZXJlZDpzdHJpbmc7XG4gICAgdG90YWxQYXlvdXQ6c3RyaW5nO1xufSJdfQ==