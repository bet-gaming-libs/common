import Big from 'big.js';
import { __awaiter } from 'tslib';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/array.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} array
 * @param {?} numOfElements
 * @return {?}
 */
function getRandomUniqueElements(array, numOfElements) {
    /** @type {?} */
    var elements = [];
    /** @type {?} */
    var source = array.slice();
    for (var i = 0; i < numOfElements; i++) {
        /** @type {?} */
        var index = getRandomIndex(source);
        elements.push(source[index]);
        source.splice(index, 1);
    }
    return elements;
}
/**
 * @param {?} array
 * @return {?}
 */
function getRandomIndex(array) {
    return Math.round(Math.random() * (array.length - 1));
}
/**
 * @template T
 * @param {?} item
 * @param {?} array
 * @return {?}
 */
function removeItemFromArray(item, array) {
    for (var i = 0; i < array.length; i++) {
        if (item == array[i]) {
            array.splice(i, 1);
            return;
        }
    }
}
/**
 * @template T
 * @param {?} items
 * @return {?}
 */
function shuffleArray(items) {
    /** @type {?} */
    const clone = items.slice();
    clone.sort((/**
     * @return {?}
     */
    () => 0.5 - Math.random()));
    return clone;
}
/**
 * @template T
 * @param {?} items
 * @param {?} numOfItemsInFirstPart
 * @return {?}
 */
function splitArrayIntoTwo(items, numOfItemsInFirstPart) {
    /** @type {?} */
    const part1 = items.slice(0, numOfItemsInFirstPart);
    /** @type {?} */
    const part2 = items.slice(numOfItemsInFirstPart);
    return [
        part1,
        part2
    ];
}
/**
 * @template T
 * @param {?} source
 * @param {?} maxNumOfItemsPerPart
 * @return {?}
 */
function splitArrayIntoMany(source, maxNumOfItemsPerPart) {
    /** @type {?} */
    const items = source.slice();
    /** @type {?} */
    const parts = [];
    while (items.length > 0) {
        /** @type {?} */
        const part = items.splice(0, items.length > maxNumOfItemsPerPart ?
            maxNumOfItemsPerPart :
            items.length);
        parts.push(part);
    }
    return parts;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/promise.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IError() { }
if (false) {
    /** @type {?} */
    IError.prototype.code;
    /** @type {?|undefined} */
    IError.prototype.data;
}
/**
 * @record
 * @template T
 */
function IPromise() { }
if (false) {
    /**
     * @param {?} resultHandler
     * @return {?}
     */
    IPromise.prototype.then = function (resultHandler) { };
    /**
     * @param {?} errorHandler
     * @return {?}
     */
    IPromise.prototype.catch = function (errorHandler) { };
}
/**
 * @template T
 */
class CustomPromise {
    constructor() { }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} resultHandler
     * @return {THIS}
     */
    then(resultHandler) {
        (/** @type {?} */ (this)).resultHandler = resultHandler;
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} errorHandler
     * @return {THIS}
     */
    catch(errorHandler) {
        (/** @type {?} */ (this)).errorHandler = errorHandler;
        return (/** @type {?} */ (this));
    }
    /**
     * @param {?} result
     * @return {?}
     */
    done(result) {
        if (this.resultHandler)
            this.resultHandler(result);
    }
    /**
     * @param {?} error
     * @return {?}
     */
    fail(error) {
        if (this.errorHandler)
            this.errorHandler(error);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.resultHandler;
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.errorHandler;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/url.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} name
 * @return {?}
 */
function getUrlParameterByName(name) {
    /** @type {?} */
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    /** @type {?} */
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
    /** @type {?} */
    var results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/timer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} forMilliseconds
 * @return {?}
 */
function sleep(forMilliseconds) {
    return new Promise((/**
     * @param {?} resolve
     * @return {?}
     */
    resolve => setTimeout(resolve, forMilliseconds)));
}
/**
 * @record
 */
function ITimerListener() { }
if (false) {
    /**
     * @return {?}
     */
    ITimerListener.prototype.onTimerTick = function () { };
}
class CustomTimer {
    /**
     * @param {?} intervalInMilliseconds
     * @param {?} listener
     */
    constructor(intervalInMilliseconds, listener) {
        this.intervalInMilliseconds = intervalInMilliseconds;
        this.listener = listener;
        this.intervalId = undefined;
        this.tick = (/**
         * @return {?}
         */
        () => {
            this.listener.onTimerTick();
        });
    }
    /**
     * @return {?}
     */
    start() {
        if (this.isRunning) {
            return;
        }
        this.intervalId = setInterval(this.tick, this.intervalInMilliseconds);
    }
    /**
     * @return {?}
     */
    stop() {
        if (!this.isRunning) {
            return;
        }
        clearInterval(this.intervalId);
        this.intervalId = undefined;
    }
    /**
     * @private
     * @return {?}
     */
    get isRunning() {
        return this.intervalId != undefined;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalId;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.tick;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalInMilliseconds;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.listener;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/collections.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IUniqueItem() { }
if (false) {
    /** @type {?} */
    IUniqueItem.prototype.id;
}
/**
 * @template T
 */
class QueueOfUniqueItems {
    constructor() {
        this.map = {};
        this.list = [];
    }
    /**
     * @param {?} items
     * @return {?}
     */
    addAll(items) {
        for (var item of items) {
            this.add(item);
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    add(item) {
        if (!this.hasItemWithId(item.id)) {
            this.map[item.id] = item;
            this.list.push(item);
        }
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    removeItemWithId(itemId) {
        delete this.map[itemId];
        for (var i = 0; i < this.list.length; i++) {
            /** @type {?} */
            const item = this.list[i];
            if (itemId == item.id) {
                this.list.splice(i, 1);
                break;
            }
        }
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    hasItemWithId(itemId) {
        return this.map[itemId] != undefined;
    }
    /**
     * @param {?} itemId
     * @return {?}
     */
    getItemWithId(itemId) {
        return this.map[itemId];
    }
    /**
     * @return {?}
     */
    get items() {
        return this.list.slice();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.map;
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.list;
}
/**
 * @template T
 */
class FIFOList {
    /**
     * @param {?} maxNumOfItems
     */
    constructor(maxNumOfItems) {
        this.maxNumOfItems = maxNumOfItems;
        this._items = [];
    }
    /**
     * @param {?} newItem
     * @return {?}
     */
    addAsFirst(newItem) {
        /** @type {?} */
        const end = this.items.slice(0, this.maxNumOfItems - 1);
        this._items = [newItem].concat(end);
    }
    /**
     * @param {?} newItem
     * @return {?}
     */
    addAsLast(newItem) {
        /** @type {?} */
        let start = [];
        if (this.items.length < this.maxNumOfItems) {
            start = this.items.slice();
        }
        else {
            start = this.items.slice(1, this.maxNumOfItems);
        }
        /** @type {?} */
        const merged = start.concat([newItem]);
        this._items = merged;
    }
    /**
     * @return {?}
     */
    get items() {
        return this._items;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype._items;
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype.maxNumOfItems;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/response.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template T
 */
function IResponse() { }
if (false) {
    /** @type {?|undefined} */
    IResponse.prototype.error;
    /** @type {?|undefined} */
    IResponse.prototype.result;
}
/**
 * @template T
 */
class ResponseManager {
    // instance
    /**
     * @private
     * @param {?} promise
     */
    constructor(promise) {
        this.promise = promise;
        this.handler = (/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            if (response.error)
                this.promise.fail(response.error);
            if (response.result)
                this.promise.done(response.result);
        });
    }
    // class
    /**
     * @template T
     * @param {?} promise
     * @return {?}
     */
    static newHandler(promise) {
        return new ResponseManager(promise).handler;
    }
}
if (false) {
    /** @type {?} */
    ResponseManager.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    ResponseManager.prototype.promise;
}
/**
 * @template T
 */
class ResponseDispatcher {
    // instance
    /**
     * @private
     * @param {?} handler
     */
    constructor(handler) {
        this.handler = handler;
    }
    // class
    /**
     * @template T
     * @param {?} handler
     * @return {?}
     */
    static create(handler) {
        return new ResponseDispatcher(handler);
    }
    /**
     * @param {?} result
     * @return {?}
     */
    dispatchResult(result) {
        this.handler({
            result
        });
    }
    /**
     * @param {?} code
     * @param {?=} data
     * @return {?}
     */
    dispatchError(code, data) {
        this.handler({
            error: {
                code,
                data
            }
        });
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ResponseDispatcher.prototype.handler;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/log.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} message
 * @return {?}
 */
function debugMessage(message) {
    /** @type {?} */
    const date = new Date();
    console.log((date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds()
        + ' - ' +
        message);
}
/**
 * @param {?=} message
 * @param {...?} optionalParams
 * @return {?}
 */
function logWithTime(message, ...optionalParams) {
    /** @type {?} */
    const date = new Date();
    /** @type {?} */
    const dateString = (date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds();
    console.log(dateString
    //+ ' - ' + message
    , message, ...optionalParams);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/string-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} sString
 * @return {?}
 */
function trimString(sString) {
    while (sString.substring(0, 1) == ' ') {
        sString = sString.substring(1, sString.length);
    }
    while (sString.substring(sString.length - 1, sString.length) == ' ') {
        sString = sString.substring(0, sString.length - 1);
    }
    return sString;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/math-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundDownToNumber(amount, precision) {
    return Number(roundDownToString(amount, precision));
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundDownToString(amount, precision) {
    /** @type {?} */
    const amount_str = String(amount.toFixed(precision + 1));
    return roundStringDownToString(amount_str, precision);
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
function roundStringDownToString(amount, precision) {
    /** @type {?} */
    const parts = amount.split('.');
    /** @type {?} */
    const isWholeNumber = parts.length == 1;
    /** @type {?} */
    const rounded = Number(parts[0] + '.' +
        (isWholeNumber ?
            '0' :
            parts[1].substr(0, precision)));
    /** @type {?} */
    const result = rounded.toFixed(precision);
    return result;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/random.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} all
 * @return {?}
 */
function chooseRandomElement(all) {
    /** @type {?} */
    const randomIndex = Math.floor(Math.random() *
        all.length);
    return all[randomIndex];
}
/**
 * @param {?} max
 * @return {?}
 */
function getRandomNumberBetwen0AndMax(max) {
    return getRandomNumberWithinRange(0, max);
}
/**
 * @param {?} max
 * @return {?}
 */
function getRandomNumberBetwen1AndMax(max) {
    return getRandomNumberWithinRange(1, max);
}
/**
 * @param {?} min
 * @param {?} max
 * @return {?}
 */
function getRandomNumberWithinRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const months = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];
/**
 * @return {?}
 */
function NOW() {
    return toDateTimeFormat(new Date());
}
/**
 * @param {?} date
 * @return {?}
 */
function toDateTimeFormat(date) {
    /** @type {?} */
    const parts = date.toString().split(' ');
    /** @type {?} */
    const monthName = parts[1];
    /** @type {?} */
    const monthIndex = months.indexOf(monthName);
    if (monthIndex < 0) {
        throw new Error('Month NOT Found: ' + monthName);
    }
    /** @type {?} */
    let monthNum = monthIndex + 1;
    /** @type {?} */
    const month = (monthNum < 10 ?
        '0' :
        '') + monthNum;
    /** @type {?} */
    const day = parts[2];
    /** @type {?} */
    const year = parts[3];
    /** @type {?} */
    const time = parts[4];
    return year + '-' + month + '-' + day + ' ' + time;
}
/**
 * @param {?} start
 * @param {?} end
 * @return {?}
 */
function getRandomDateWithinRange(start, end) {
    /** @type {?} */
    const startTime = start.getTime();
    /** @type {?} */
    const duration = end.getTime() - startTime;
    return new Date(startTime +
        (Math.random() * duration));
}
/**
 * @return {?}
 */
function getStartOfToday() {
    /** @type {?} */
    const startOfDate = new Date();
    startOfDate.setHours(0, 0, 0, 0);
    return startOfDate;
}
/**
 * @return {?}
 */
function getEndOfToday() {
    /** @type {?} */
    const endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);
    return endOfDay;
}
/** @enum {number} */
const Month = {
    JAN: 0,
    FEB: 1,
    MAR: 2,
    APR: 3,
    MAY: 4,
    JUN: 5,
    JUL: 6,
    AUG: 7,
    SEP: 8,
    OCT: 9,
    NOV: 10,
    DEC: 11,
};
Month[Month.JAN] = 'JAN';
Month[Month.FEB] = 'FEB';
Month[Month.MAR] = 'MAR';
Month[Month.APR] = 'APR';
Month[Month.MAY] = 'MAY';
Month[Month.JUN] = 'JUN';
Month[Month.JUL] = 'JUL';
Month[Month.AUG] = 'AUG';
Month[Month.SEP] = 'SEP';
Month[Month.OCT] = 'OCT';
Month[Month.NOV] = 'NOV';
Month[Month.DEC] = 'DEC';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function ITimeDuration() { }
if (false) {
    /** @type {?} */
    ITimeDuration.prototype.milliseconds;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/time-durations.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const SECOND_IN_MS = 1000;
/** @type {?} */
const MINUTE_IN_MS = 60000;
/** @type {?} */
const DAY_IN_MS = 86400000;
class Milliseconds {
    /**
     * @param {?} milliseconds
     */
    constructor(milliseconds) {
        this.milliseconds = milliseconds;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    add(other) {
        return new Milliseconds(this.milliseconds +
            other.milliseconds);
    }
    /**
     * @param {?} other
     * @return {?}
     */
    subtract(other) {
        return new Milliseconds(this.milliseconds -
            other.milliseconds);
    }
}
if (false) {
    /** @type {?} */
    Milliseconds.prototype.milliseconds;
}
class Seconds extends Milliseconds {
    /**
     * @param {?} seconds
     */
    constructor(seconds) {
        super(seconds * SECOND_IN_MS);
    }
}
class Minutes extends Milliseconds {
    /**
     * @param {?} minutes
     */
    constructor(minutes) {
        super(minutes * MINUTE_IN_MS);
    }
}
class Hours extends Minutes {
    /**
     * @param {?} hours
     */
    constructor(hours) {
        super(hours * 60);
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/time-frame.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class TimeFrame {
    /**
     * @param {?} start
     * @param {?} duration
     */
    constructor(start, duration) {
        this.start = start;
        this.end = new Date(start.getTime() + duration.milliseconds);
    }
    /**
     * @return {?}
     */
    toString() {
        return this.start.toString() + ' TO ' +
            this.end.toString();
    }
}
if (false) {
    /** @type {?} */
    TimeFrame.prototype.end;
    /** @type {?} */
    TimeFrame.prototype.start;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/games.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const GameId = {
    DICE: 1,
    CRASH: 2,
    BACCARAT: 3,
    HILO: 4,
    BLACKJACK: 5,
};
GameId[GameId.DICE] = 'DICE';
GameId[GameId.CRASH] = 'CRASH';
GameId[GameId.BACCARAT] = 'BACCARAT';
GameId[GameId.HILO] = 'HILO';
GameId[GameId.BLACKJACK] = 'BLACKJACK';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEosConfigParameters() { }
if (false) {
    /** @type {?} */
    IEosConfigParameters.prototype.httpEndpoint;
    /** @type {?} */
    IEosConfigParameters.prototype.chainId;
    /** @type {?} */
    IEosConfigParameters.prototype.keyProvider;
    /** @type {?} */
    IEosConfigParameters.prototype.verbose;
}
/**
 * @record
 */
function ITableRowQueryParameters() { }
if (false) {
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.json;
    /** @type {?} */
    ITableRowQueryParameters.prototype.code;
    /** @type {?} */
    ITableRowQueryParameters.prototype.scope;
    /** @type {?} */
    ITableRowQueryParameters.prototype.table;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.limit;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.table_key;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.key_type;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.index_position;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.lower_bound;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.upper_bound;
}
/**
 * @record
 */
function IGlobalVariableRow() { }
if (false) {
    /** @type {?} */
    IGlobalVariableRow.prototype.id;
    /** @type {?} */
    IGlobalVariableRow.prototype.val;
}
/**
 * @record
 */
function ITokenBalanceRow() { }
if (false) {
    /** @type {?} */
    ITokenBalanceRow.prototype.balance;
}
/**
 * @record
 */
function ITransactionResult() { }
if (false) {
    /** @type {?} */
    ITransactionResult.prototype.transaction_id;
}
/**
 * @record
 */
function IEasyAccountCurrencyBalanceRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountCurrencyBalanceRow.prototype.quantity;
    /** @type {?} */
    IEasyAccountCurrencyBalanceRow.prototype.amt_wagered;
}
/**
 * @record
 */
function IEasyAccountBetBalanceRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.id;
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.quantity;
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.claimed_divs;
}
/**
 * @record
 */
function IEasyAccountInfoRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountInfoRow.prototype.id;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.key;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.nonce;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.email_backup;
}
/**
 * @record
 */
function IEasyAccountActiveBet() { }
if (false) {
    /** @type {?} */
    IEasyAccountActiveBet.prototype.key_id;
}
/**
 * @record
 */
function IEosActionData() { }
if (false) {
    /** @type {?} */
    IEosActionData.prototype.account;
    /** @type {?} */
    IEosActionData.prototype.name;
    /** @type {?} */
    IEosActionData.prototype.data;
}
/**
 * @record
 */
function IEosActionAuthorization() { }
if (false) {
    /** @type {?} */
    IEosActionAuthorization.prototype.actor;
    /** @type {?} */
    IEosActionAuthorization.prototype.permission;
}
/**
 * @record
 */
function IEosTransactionAction() { }
if (false) {
    /** @type {?} */
    IEosTransactionAction.prototype.authorization;
}
/**
 * @record
 */
function ISignatureInfo() { }
if (false) {
    /** @type {?} */
    ISignatureInfo.prototype.nonce;
    /** @type {?} */
    ISignatureInfo.prototype.signature;
    /** @type {?} */
    ISignatureInfo.prototype.isSignedDataHashed;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/int-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class AssetMath {
    /**
     * @param {?} precision
     * @param {?} coin_name
     */
    constructor(precision, coin_name) {
        this._integer = 0;
        this.precision = precision;
        this._coin_name = coin_name;
    }
    /**
     * @param {?} intVal
     * @return {?}
     */
    initWithInteger(intVal) {
        Big.RM = 0;
        this._integer = Number(new Big(intVal).toFixed(0));
    }
    /**
     * @param {?} intDec
     * @return {?}
     */
    initWithDecimal(intDec) {
        Big.RM = 0;
        this._integer = Number(new Big(intDec).times(`1e${this.precision}`).toFixed(0));
    }
    /**
     * @param {?} valDecimal
     * @return {?}
     */
    decToInt(valDecimal) {
        Big.RM = 0;
        return Number(new Big(valDecimal).times(`1e${this.precision}`).toFixed(0));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    addInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).plus(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    addDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).add((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    subInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    subDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    divInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    divDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    multInt(valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times(valInteger));
        return (/** @type {?} */ (this));
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    multDec(valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    }
    /**
     * @return {?}
     */
    get assetString() {
        return `${this.toDecimalString()} ${this.coinName}`;
    }
    /**
     * @return {?}
     */
    get decimal() {
        return this.toDecimalString();
    }
    /**
     * @return {?}
     */
    toDecimalString() {
        /** @type {?} */
        let decimal = new Big(this._integer).times(`1e-${this.precision}`);
        return decimal.toFixed(this.precision);
    }
    /**
     * @return {?}
     */
    get integer() {
        return this._integer;
    }
    /**
     * @return {?}
     */
    get coinName() {
        return this._coin_name;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._integer;
    /** @type {?} */
    AssetMath.prototype.precision;
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._coin_name;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEOSAssetAmount() { }
if (false) {
    /** @type {?} */
    IEOSAssetAmount.prototype.symbol;
    /** @type {?} */
    IEOSAssetAmount.prototype.contract;
    /** @type {?} */
    IEOSAssetAmount.prototype.integer;
    /** @type {?} */
    IEOSAssetAmount.prototype.quantity;
}
class EOSAssetAmount extends AssetMath {
    /**
     * @param {?} precision
     * @param {?} symbol
     * @param {?} contract
     * @param {?=} decimalValue
     */
    constructor(precision, symbol, contract, decimalValue = '0') {
        super(precision, symbol);
        this.symbol = symbol;
        this.contract = contract;
        this.initWithDecimal(decimalValue);
    }
    /**
     * @return {?}
     */
    get quantity() {
        return this.assetString;
    }
}
if (false) {
    /** @type {?} */
    EOSAssetAmount.prototype.symbol;
    /** @type {?} */
    EOSAssetAmount.prototype.contract;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const bigInt = require('big-integer');
/**
 * @param {?} txnId
 * @return {?}
 */
function getBetIdFromTransactionId(txnId) {
    /** @type {?} */
    var shortStr = txnId.substring(0, 16);
    return bigInt(shortStr, 16).toString();
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
function executeTransactionUntilSuccessful(eos, actions) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            /** @type {?} */
            const txnId = yield executeTransaction(eos, actions);
            if (txnId != undefined) {
                return txnId;
            }
            else {
                return executeTransactionUntilSuccessful(eos, actions);
            }
        }
        catch (error) {
            console.error(error);
            return executeTransactionUntilSuccessful(eos, actions);
        }
    });
}
/**
 * @record
 */
function IActionAuthorization() { }
if (false) {
    /** @type {?} */
    IActionAuthorization.prototype.actor;
    /** @type {?} */
    IActionAuthorization.prototype.permission;
}
/**
 * @record
 */
function ITransactionAction() { }
if (false) {
    /** @type {?} */
    ITransactionAction.prototype.account;
    /** @type {?} */
    ITransactionAction.prototype.name;
    /** @type {?} */
    ITransactionAction.prototype.authorization;
    /** @type {?} */
    ITransactionAction.prototype.data;
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
function executeTransaction(eos, actions) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            /** @type {?} */
            const result = yield eos.transaction({
                actions
            });
            return result.transaction_id;
        }
        catch (error) {
            //error = JSON.parse(error);
            //console.log( error );
            throw error;
            //return undefined;
        }
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} table_key
 * @return {?}
 * @this {*}
 */
function getAllTableRows(eos, code, scope, table, table_key) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        var allRows = [];
        /** @type {?} */
        const limit = -1;
        /** @type {?} */
        var lower_bound = '0';
        while (true) {
            /** @type {?} */
            const result = yield getTableRowsResult(eos, {
                code, scope, table,
                limit, lower_bound
            });
            if (result == undefined ||
                result.rows.length == 0) {
                break;
            }
            // add new rows
            allRows = allRows.concat(result.rows);
            //const indexOfFirstRow = bigInt( result.rows[0][table_key] );
            /** @type {?} */
            const numOfRows = result.rows.length;
            /** @type {?} */
            const lastRow = result.rows[numOfRows - 1];
            /** @type {?} */
            const indexOfLastRow = bigInt(lastRow[table_key]);
            //console.log('Index of First Row: '+indexOfFirstRow.toString())
            //console.log('Index of Last Row: '+indexOfLastRow.toString())
            if (result.more === false) {
                break;
            }
            lower_bound = indexOfLastRow.add(1).toString();
        }
        return allRows;
    });
}
/*
export function getEosBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosio.token',
        accountName,
        'EOS'
    );
}

export function getBtcBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosbettokens',
        accountName,
        'BTC'
    );
}

export function getBetBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'betdividends',
        accountName,
        'BET'
    );
}
*/
/**
 * @param {?} eos
 * @param {?} tokenContractAccount
 * @param {?} tokenHolderAccount
 * @param {?} tokenSymbol
 * @param {?=} table
 * @return {?}
 * @this {*}
 */
function getTokenBalance(eos, tokenContractAccount, tokenHolderAccount, tokenSymbol, table = 'accounts') {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getTableRows(eos, {
            code: tokenContractAccount,
            scope: tokenHolderAccount,
            table
        });
        for (var row of rows) {
            /** @type {?} */
            var parts = row.balance.split(' ');
            if (parts[1] == tokenSymbol) {
                return parts[0];
            }
        }
        return '0';
    });
}
/**
 * @record
 */
function IGlobalVarRow() { }
if (false) {
    /** @type {?} */
    IGlobalVarRow.prototype.id;
    /** @type {?} */
    IGlobalVarRow.prototype.val;
}
/**
 * @param {?} eos
 * @param {?} smartContractAccount
 * @param {?} variableId
 * @return {?}
 * @this {*}
 */
function getGlobalVariable(eos, smartContractAccount, variableId) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getTableRows(eos, {
            code: smartContractAccount,
            scope: smartContractAccount,
            table: 'globalvars'
        });
        if (rows.length === 0) {
            throw new Error("Contract not INIT");
        }
        for (var row of rows) {
            if (variableId == row.id) {
                return row.val;
            }
        }
        return undefined;
    });
}
/**
 * @param {?} eos
 * @param {?} easyAccountContract
 * @param {?} betId
 * @return {?}
 * @this {*}
 */
function getAccountIdForActiveEasyAccountBet(eos, easyAccountContract, betId) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const rows = yield getUniqueTableRow(eos, easyAccountContract, easyAccountContract, 'activebets', betId);
        return rows && rows.length > 0 ?
            rows[0].key_id :
            undefined;
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} rowId
 * @return {?}
 */
function getUniqueTableRow(eos, code, scope, table, rowId) {
    return getTableRows(eos, {
        code,
        scope,
        table,
        lower_bound: rowId,
        upper_bound: rowId
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
function getTableRows(eos, parameters) {
    return __awaiter(this, void 0, void 0, function* () {
        /** @type {?} */
        const result = yield getTableRowsResult(eos, parameters);
        return result ?
            result.rows :
            undefined;
    });
}
/**
 * @record
 * @template T
 */
function ITableRowQueryResult() { }
if (false) {
    /** @type {?} */
    ITableRowQueryResult.prototype.rows;
    /** @type {?} */
    ITableRowQueryResult.prototype.more;
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
function getTableRowsResult(eos, parameters) {
    return __awaiter(this, void 0, void 0, function* () {
        parameters.json = true;
        try {
            /** @type {?} */
            const result = yield eos.getTableRows(parameters);
            return result;
        }
        catch (error) {
            console.log('Cound not get table rows');
            console.log(error);
            return undefined;
        }
    });
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-txn-executor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEosTransactionListener() { }
if (false) {
    /**
     * @param {?} txnId
     * @return {?}
     */
    IEosTransactionListener.prototype.onEosTransactionConfirmed = function (txnId) { };
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEosTokenTransferData() { }
if (false) {
    /** @type {?} */
    IEosTokenTransferData.prototype.from;
    /** @type {?} */
    IEosTokenTransferData.prototype.to;
    /** @type {?} */
    IEosTokenTransferData.prototype.quantity;
    /** @type {?} */
    IEosTokenTransferData.prototype.memo;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IResolvedBet() { }
if (false) {
    /** @type {?} */
    IResolvedBet.prototype.id;
    /** @type {?} */
    IResolvedBet.prototype.isEasyAccount;
    /** @type {?} */
    IResolvedBet.prototype.bettor;
    /** @type {?} */
    IResolvedBet.prototype.amount;
    /** @type {?} */
    IResolvedBet.prototype.resolvedMilliseconds;
    /** @type {?} */
    IResolvedBet.prototype.referrer;
    /** @type {?} */
    IResolvedBet.prototype.jackpotSpin;
    /** @type {?} */
    IResolvedBet.prototype.stakeWeight;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/dice-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IResolvedDiceBet() { }
if (false) {
    /** @type {?} */
    IResolvedDiceBet.prototype.isRollOver;
    /** @type {?} */
    IResolvedDiceBet.prototype.number;
    /** @type {?} */
    IResolvedDiceBet.prototype.roll;
}
/**
 * @record
 */
function IDiceGameData() { }
if (false) {
    /** @type {?} */
    IDiceGameData.prototype.recentBets;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/chat-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const chatLoginMessagePrefix = 'My Chat Public Key is: ';
/**
 * @record
 */
function IChatAuthenticationData() { }
if (false) {
    /** @type {?} */
    IChatAuthenticationData.prototype.isEasyAccount;
    /** @type {?} */
    IChatAuthenticationData.prototype.chatPublicKey;
    /** @type {?} */
    IChatAuthenticationData.prototype.eosAccountName;
    /** @type {?} */
    IChatAuthenticationData.prototype.easyAccountId;
    /** @type {?} */
    IChatAuthenticationData.prototype.signature;
    /** @type {?} */
    IChatAuthenticationData.prototype.isSignedDataHased;
    /** @type {?} */
    IChatAuthenticationData.prototype.nonce;
}
/**
 * @record
 */
function IChatMessageData() { }
if (false) {
    /** @type {?} */
    IChatMessageData.prototype.chatPublicKey;
    /** @type {?} */
    IChatMessageData.prototype.message;
    /** @type {?} */
    IChatMessageData.prototype.nonce;
    /** @type {?} */
    IChatMessageData.prototype.signature;
}
/**
 * @record
 */
function IValidatedChatMessage() { }
if (false) {
    /** @type {?} */
    IValidatedChatMessage.prototype.from;
    /** @type {?} */
    IValidatedChatMessage.prototype.message;
    /** @type {?} */
    IValidatedChatMessage.prototype.time;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/card-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const CardSuit = {
    Hearts: 0,
    Diamonds: 1,
    Spades: 2,
    Clubs: 3,
};
CardSuit[CardSuit.Hearts] = 'Hearts';
CardSuit[CardSuit.Diamonds] = 'Diamonds';
CardSuit[CardSuit.Spades] = 'Spades';
CardSuit[CardSuit.Clubs] = 'Clubs';
/**
 * @record
 */
function ICardData() { }
if (false) {
    /** @type {?} */
    ICardData.prototype.suit;
    /** @type {?} */
    ICardData.prototype.rankName;
}
class CardRankNumber {
}
CardRankNumber.ACE = 1;
CardRankNumber.TEN = 10;
CardRankNumber.JACK = 11;
CardRankNumber.QUEEN = 12;
CardRankNumber.KING = 0;
if (false) {
    /** @type {?} */
    CardRankNumber.ACE;
    /** @type {?} */
    CardRankNumber.TEN;
    /** @type {?} */
    CardRankNumber.JACK;
    /** @type {?} */
    CardRankNumber.QUEEN;
    /** @type {?} */
    CardRankNumber.KING;
}
class CardRankName {
}
CardRankName.ACE = 'Ace';
CardRankName.JACK = 'Jack';
CardRankName.QUEEN = 'Queen';
CardRankName.KING = 'King';
if (false) {
    /** @type {?} */
    CardRankName.ACE;
    /** @type {?} */
    CardRankName.JACK;
    /** @type {?} */
    CardRankName.QUEEN;
    /** @type {?} */
    CardRankName.KING;
}
/**
 * @param {?} value
 * @return {?}
 */
function numberToSuit(value) {
    switch (value) {
        case 0:
            return CardSuit.Hearts;
        case 1:
            return CardSuit.Diamonds;
        case 2:
            return CardSuit.Spades;
        case 3:
            return CardSuit.Clubs;
    }
}
/**
 * @param {?} value
 * @return {?}
 */
function nameOfSuit(value) {
    switch (value) {
        case CardSuit.Hearts:
            return "Hearts";
        case CardSuit.Diamonds:
            return "Diamonds";
        case CardSuit.Spades:
            return "Spades";
        case CardSuit.Clubs:
            return "Clubs";
    }
}
/**
 * @param {?} value
 * @return {?}
 */
function nameOfRank(value) {
    switch (value) {
        case CardRankNumber.ACE:
            return CardRankName.ACE.substr(0, 1);
        case CardRankNumber.JACK:
            return CardRankName.JACK.substr(0, 1);
        case CardRankNumber.QUEEN:
            return CardRankName.QUEEN.substr(0, 1);
        case CardRankNumber.KING:
            return CardRankName.KING.substr(0, 1);
        default:
            return value.toString();
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/baccarat-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IHandData() { }
if (false) {
    /** @type {?} */
    IHandData.prototype.cards;
    /** @type {?} */
    IHandData.prototype.totalValue;
}
/**
 * @record
 */
function IBaccaratGameResultDataForMultiPlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.gameId;
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.winner;
    /** @type {?} */
    IBaccaratGameResultDataForMultiPlayer.prototype.hands;
}
/**
 * @record
 */
function IBaccaratGameResultDataForSinglePlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratGameResultDataForSinglePlayer.prototype.winner;
    /** @type {?} */
    IBaccaratGameResultDataForSinglePlayer.prototype.hands;
}
/**
 * @record
 */
function IBaccaratGameStartData() { }
if (false) {
    /** @type {?} */
    IBaccaratGameStartData.prototype.currentGameId;
    /** @type {?} */
    IBaccaratGameStartData.prototype.gameStartTime;
    /** @type {?} */
    IBaccaratGameStartData.prototype.currentServerTime;
}
/**
 * @record
 */
function IBaccaratBetDataForSinglePlayer() { }
if (false) {
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.id;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bettor;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.referral;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.referral_data;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.player_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.banker_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.tie_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.playerpair_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.bankerpair_bet_amt;
    /** @type {?} */
    IBaccaratBetDataForSinglePlayer.prototype.seed;
}
/**
 * @record
 */
function IResolvedBaccaratBet() { }
if (false) {
    /** @type {?} */
    IResolvedBaccaratBet.prototype.bet;
    /** @type {?} */
    IResolvedBaccaratBet.prototype.result;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/crash-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IActiveCrashBet() { }
if (false) {
    /** @type {?} */
    IActiveCrashBet.prototype.id;
    /** @type {?} */
    IActiveCrashBet.prototype.bettor;
    /** @type {?} */
    IActiveCrashBet.prototype.referral;
    /** @type {?} */
    IActiveCrashBet.prototype.referral_data;
    /** @type {?} */
    IActiveCrashBet.prototype.bet_amt;
    /** @type {?} */
    IActiveCrashBet.prototype.max_win;
    /** @type {?} */
    IActiveCrashBet.prototype.adv_tap_out_times100;
    /** @type {?} */
    IActiveCrashBet.prototype.seed;
    /** @type {?} */
    IActiveCrashBet.prototype.signing_key;
}
/**
 * @record
 */
function IResolvedCrashBet() { }
/**
 * @record
 */
function ITapOutRequestData() { }
if (false) {
    /** @type {?} */
    ITapOutRequestData.prototype.betId;
    /** @type {?} */
    ITapOutRequestData.prototype.signature;
}
/**
 * @record
 */
function ITappedOutMessageData() { }
if (false) {
    /** @type {?} */
    ITappedOutMessageData.prototype.betId;
    /** @type {?} */
    ITappedOutMessageData.prototype.value;
}
/**
 * @record
 */
function ICrashResultData() { }
if (false) {
    /** @type {?} */
    ICrashResultData.prototype.cashOutPoint;
    /**
     * isInsant = value == 0 *
     * @type {?}
     */
    ICrashResultData.prototype.crashPoint;
    /** @type {?} */
    ICrashResultData.prototype.isMaxLiabilitiesReached;
    /** @type {?} */
    ICrashResultData.prototype.isEndedEarly;
}
/**
 * @record
 */
function ICrashMessageData() { }
if (false) {
    /** @type {?} */
    ICrashMessageData.prototype.bet;
    /** @type {?} */
    ICrashMessageData.prototype.result;
}
/**
 * @record
 */
function ICrashGameStateData() { }
if (false) {
    /** @type {?} */
    ICrashGameStateData.prototype.currentGameId;
    /** @type {?} */
    ICrashGameStateData.prototype.isMoving;
    /** @type {?} */
    ICrashGameStateData.prototype.secondsBeforeStart;
    /** @type {?} */
    ICrashGameStateData.prototype.currentPoint;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/hilo-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IResolvedHiloBet() { }
if (false) {
    /** @type {?} */
    IResolvedHiloBet.prototype.betType;
    /** @type {?} */
    IResolvedHiloBet.prototype.startCard;
    /** @type {?} */
    IResolvedHiloBet.prototype.nextCard;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/blackjack-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IBlackjackInitialCards() { }
if (false) {
    /** @type {?} */
    IBlackjackInitialCards.prototype.dealerCards;
    /** @type {?} */
    IBlackjackInitialCards.prototype.playerCards;
}
/**
 * @record
 */
function IInitialBlackjackBetInfo() { }
if (false) {
    /** @type {?} */
    IInitialBlackjackBetInfo.prototype.amountAsInteger;
    /** @type {?} */
    IInitialBlackjackBetInfo.prototype.token;
}
/**
 * @record
 */
function IBlackjackGameState() { }
if (false) {
    /** @type {?} */
    IBlackjackGameState.prototype.id;
    /** @type {?} */
    IBlackjackGameState.prototype.initialBet;
    /** @type {?} */
    IBlackjackGameState.prototype.initialCards;
    /** @type {?} */
    IBlackjackGameState.prototype.isDealerBlackjack;
    /** @type {?} */
    IBlackjackGameState.prototype.commands;
    /** @type {?} */
    IBlackjackGameState.prototype.additionalPlayerCards;
    /** @type {?} */
    IBlackjackGameState.prototype.additionalDealerCards;
}
/**
 * @record
 */
function IBlackjackHandResult() { }
if (false) {
    /** @type {?} */
    IBlackjackHandResult.prototype.sum;
    /** @type {?} */
    IBlackjackHandResult.prototype.isBlackjack;
}
/**
 * @record
 */
function IBlackjackPlayerHandResult() { }
if (false) {
    /** @type {?} */
    IBlackjackPlayerHandResult.prototype.totalWagered;
    /** @type {?} */
    IBlackjackPlayerHandResult.prototype.payout;
}
/**
 * @record
 */
function IResolvedBlackjackBet() { }
if (false) {
    /** @type {?} */
    IResolvedBlackjackBet.prototype.dealerHandResult;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.playerResults;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.totalWagered;
    /** @type {?} */
    IResolvedBlackjackBet.prototype.totalPayout;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/messages.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class DiceMessage {
}
DiceMessage.PREFIX = 'DiceMessage';
/* Client to Server */
DiceMessage.GET_REFERRER = 'GET_REFERRER';
DiceMessage.SAVE_REFERRER = 'SAVE_REFERRER';
DiceMessage.SAVE_REFERRER_FOR_WAX = 'SAVE_REFERRER_FOR_WAX';
DiceMessage.RESOLVE_BET = 'RESOLVE_BET';
/* Server to Client */
DiceMessage.SYNC_GAME_DATA = 'SYNC_GAME_DATA';
DiceMessage.SYNC_BANKROLL = 'SYNC_BANKROLL';
DiceMessage.NEW_RESOLVED_BET = 'NEW_RESOLVED_BET';
if (false) {
    /** @type {?} */
    DiceMessage.PREFIX;
    /** @type {?} */
    DiceMessage.GET_REFERRER;
    /** @type {?} */
    DiceMessage.SAVE_REFERRER;
    /** @type {?} */
    DiceMessage.SAVE_REFERRER_FOR_WAX;
    /** @type {?} */
    DiceMessage.RESOLVE_BET;
    /** @type {?} */
    DiceMessage.SYNC_GAME_DATA;
    /** @type {?} */
    DiceMessage.SYNC_BANKROLL;
    /** @type {?} */
    DiceMessage.NEW_RESOLVED_BET;
}
class ChatMessage {
}
ChatMessage.PREFIX = 'ChatMessage';
/* Client to Server */
ChatMessage.AUTHENTICATE = 'AUTHENTICATE';
ChatMessage.GET_RECENT_MESSAGES = 'GET_RECENT_MESSAGES';
ChatMessage.CHAT = 'CHAT';
if (false) {
    /** @type {?} */
    ChatMessage.PREFIX;
    /** @type {?} */
    ChatMessage.AUTHENTICATE;
    /** @type {?} */
    ChatMessage.GET_RECENT_MESSAGES;
    /** @type {?} */
    ChatMessage.CHAT;
}
class BaccaratMessage {
}
BaccaratMessage.PREFIX = 'BaccaratMessage';
BaccaratMessage.BET_RESULT = "BET_RESULT";
if (false) {
    /** @type {?} */
    BaccaratMessage.PREFIX;
    /** @type {?} */
    BaccaratMessage.BET_RESULT;
}
class CrashMessage {
}
CrashMessage.PREFIX = 'CrashMessage';
/* Server to Client */
/* MULTI-PLAYER */
/*
    static START_BETTING = 'START_BETTING';
    static NEW_BETS = 'NEW_BETS';

    static SYNC_STATE = 'SYNC_STATE';
    */
/* */
/* Client to Server */
CrashMessage.TAP_OUT = 'TAP_OUT';
CrashMessage.START_GAME = 'START_GAME';
CrashMessage.TAPPED_OUT = 'TAPPED_OUT';
CrashMessage.CRASH = 'CRASH';
CrashMessage.RESOLVE_BET = 'RESOLVE_BET';
CrashMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
if (false) {
    /** @type {?} */
    CrashMessage.PREFIX;
    /** @type {?} */
    CrashMessage.TAP_OUT;
    /** @type {?} */
    CrashMessage.START_GAME;
    /** @type {?} */
    CrashMessage.TAPPED_OUT;
    /** @type {?} */
    CrashMessage.CRASH;
    /** @type {?} */
    CrashMessage.RESOLVE_BET;
    /** @type {?} */
    CrashMessage.RESOLVE_BET_RESULT;
}
class HiloMessage {
}
HiloMessage.PREFIX = 'HiloMessage';
HiloMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
if (false) {
    /** @type {?} */
    HiloMessage.PREFIX;
    /** @type {?} */
    HiloMessage.RESOLVE_BET_RESULT;
}
class BlackjackMessage {
}
BlackjackMessage.PREFIX = 'BlackjackMessage';
BlackjackMessage.GAME_COMMAND = 'GAME_COMMAND';
BlackjackMessage.RESOLVE_BET_RESULT = 'RESOLVE_BET_RESULT';
if (false) {
    /** @type {?} */
    BlackjackMessage.PREFIX;
    /** @type {?} */
    BlackjackMessage.GAME_COMMAND;
    /** @type {?} */
    BlackjackMessage.RESOLVE_BET_RESULT;
}
class BlackjackGameCommand {
}
BlackjackGameCommand.RESUME_GAME = 'RESUME_GAME';
BlackjackGameCommand.START_GAME = 'START_GAME';
BlackjackGameCommand.YES_TO_INSURANCE = 'YES_TO_INSURANCE';
BlackjackGameCommand.NO_TO_INSURANCE = 'NO_TO_INSURANCE';
BlackjackGameCommand.PEEK_FOR_BLACKJACK = 'PEEK_FOR_BLACKJACK';
BlackjackGameCommand.SPLIT = 'SPLIT';
BlackjackGameCommand.HIT = 'HIT';
BlackjackGameCommand.DOUBLE_DOWN = 'DOUBLE_DOWN';
BlackjackGameCommand.STAND = 'STAND';
BlackjackGameCommand.GET_DEALERS_CARDS = 'GET_DEALERS_CARDS';
if (false) {
    /** @type {?} */
    BlackjackGameCommand.RESUME_GAME;
    /** @type {?} */
    BlackjackGameCommand.START_GAME;
    /** @type {?} */
    BlackjackGameCommand.YES_TO_INSURANCE;
    /** @type {?} */
    BlackjackGameCommand.NO_TO_INSURANCE;
    /** @type {?} */
    BlackjackGameCommand.PEEK_FOR_BLACKJACK;
    /** @type {?} */
    BlackjackGameCommand.SPLIT;
    /** @type {?} */
    BlackjackGameCommand.HIT;
    /** @type {?} */
    BlackjackGameCommand.DOUBLE_DOWN;
    /** @type {?} */
    BlackjackGameCommand.STAND;
    /** @type {?} */
    BlackjackGameCommand.GET_DEALERS_CARDS;
}
/**
 * @param {?} string
 * @return {?}
 */
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
/**
 * @param {?} message
 * @return {?}
 */
function getMethodNameForMessage(message) {
    /** @type {?} */
    var parts = message.split('_');
    for (var i = 0; i < parts.length; i++) {
        parts[i] = capitalizeFirstLetter(parts[i]);
    }
    return 'on' + parts.join('');
}
/**
 * @param {?} args
 * @param {?} client
 * @return {?}
 */
function dispatchMessage(args, client) {
    /** @type {?} */
    var message = args[0];
    /** @type {?} */
    var methodName = getMethodNameForMessage(message);
    //console.log(methodName);
    //console.log(args);
    /** @type {?} */
    var argsArray = [];
    for (var i = 1; i < args.length; i++) {
        argsArray.push(args[i]);
    }
    /** @type {?} */
    var method = client[methodName];
    if (method == undefined) {
        console.log(methodName + ' DOES NOT EXIST!');
        return;
    }
    method.apply(client, argsArray);
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/request.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} messagePrefix
 * @param {?} messageName
 * @param {?} ars
 * @param {?} socket
 * @param {?=} promise
 * @return {?}
 */
function makeRequest(messagePrefix, messageName, ars, socket, promise = new CustomPromise()) {
    /** @type {?} */
    var args = [messagePrefix, messageName];
    for (var i = 0; i < ars.length; i++) {
        args.push(ars[i]);
    }
    args.push(ResponseManager.newHandler(promise));
    socket.emit.apply(socket, args);
    return promise;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MessageManager {
    /**
     * @param {?} emitter
     * @param {?} messagePrefix
     */
    constructor(emitter, messagePrefix) {
        this.emitter = emitter;
        this.messagePrefix = messagePrefix;
    }
    /**
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    sendMessage(messageName, ...args) {
        /** @type {?} */
        const start = [this.messagePrefix, messageName];
        /** @type {?} */
        const argsArray = start.concat(args);
        this.emitter.emit.apply(this.emitter, argsArray);
        return argsArray;
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.emitter;
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.messagePrefix;
}
class SocketMessageManager extends MessageManager {
    /**
     * @param {?} socket
     * @param {?} messagePrefix
     * @param {?} messageReceiver
     */
    constructor(socket, messagePrefix, messageReceiver) {
        super(socket, messagePrefix);
        this.socket = socket;
        this.messageReceiver = messageReceiver;
        this.setupMessageListener();
    }
    /**
     * @private
     * @return {?}
     */
    setupMessageListener() {
        // listen to all messages with message prefix
        /** @type {?} */
        const receiver = this.messageReceiver;
        this.socket.on(this.messagePrefix, (/**
         * @return {?}
         */
        function () {
            dispatchMessage(arguments, receiver);
        }));
    }
    /**
     * @protected
     * @template T
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    makeRequest(messageName, ...args) {
        return makeRequest(this.messagePrefix, messageName, args, this.socket);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.socket;
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.messageReceiver;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function IEmitter() { }
if (false) {
    /**
     * @param {?} event
     * @param {...?} args
     * @return {?}
     */
    IEmitter.prototype.emit = function (event, args) { };
}
/**
 * \@doNotMinify
 * @record
 */
function ISocket() { }
if (false) {
    /**
     * @param {?} event
     * @param {?} fn
     * @return {?}
     */
    ISocket.prototype.on = function (event, fn) { };
}
/**
 * @record
 */
function IServerConfig() { }
if (false) {
    /** @type {?} */
    IServerConfig.prototype.host;
    /** @type {?} */
    IServerConfig.prototype.port;
    /** @type {?} */
    IServerConfig.prototype.ssl;
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/dice-message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @abstract
 */
class DiceMessageManager extends SocketMessageManager {
    /**
     * @param {?} socket
     * @param {?} messageReceiver
     */
    constructor(socket, messageReceiver) {
        super(socket, DiceMessage.PREFIX, messageReceiver);
    }
    /**
     * @protected
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    sendDiceMessage(messageName, ...args) {
        /** @type {?} */
        var start = [messageName];
        this.sendMessage.apply(this, start.concat(args));
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: common-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: earnbet-common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { AssetMath, BaccaratMessage, BlackjackGameCommand, BlackjackMessage, CardRankName, CardRankNumber, CardSuit, ChatMessage, CrashMessage, CustomPromise, CustomTimer, DiceMessage, DiceMessageManager, EOSAssetAmount, FIFOList, GameId, HiloMessage, Hours, MessageManager, Milliseconds, Minutes, Month, NOW, QueueOfUniqueItems, ResponseDispatcher, ResponseManager, Seconds, SocketMessageManager, TimeFrame, chatLoginMessagePrefix, chooseRandomElement, debugMessage, dispatchMessage, executeTransaction, executeTransactionUntilSuccessful, getAccountIdForActiveEasyAccountBet, getAllTableRows, getBetIdFromTransactionId, getEndOfToday, getGlobalVariable, getRandomDateWithinRange, getRandomNumberBetwen0AndMax, getRandomNumberBetwen1AndMax, getRandomNumberWithinRange, getRandomUniqueElements, getStartOfToday, getTableRows, getTableRowsResult, getTokenBalance, getUniqueTableRow, getUrlParameterByName, logWithTime, nameOfRank, nameOfSuit, numberToSuit, removeItemFromArray, roundDownToNumber, roundDownToString, roundStringDownToString, shuffleArray, sleep, splitArrayIntoMany, splitArrayIntoTwo, toDateTimeFormat, trimString };
//# sourceMappingURL=earnbet-common.js.map
