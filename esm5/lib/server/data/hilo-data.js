/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/hilo-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IResolvedHiloBet() { }
if (false) {
    /** @type {?} */
    IResolvedHiloBet.prototype.betType;
    /** @type {?} */
    IResolvedHiloBet.prototype.startCard;
    /** @type {?} */
    IResolvedHiloBet.prototype.nextCard;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlsby1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvaGlsby1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0Esc0NBS0M7OztJQUhHLG1DQUFlOztJQUNmLHFDQUFpQjs7SUFDakIsb0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSVJlc29sdmVkQmV0IH0gZnJvbSBcIi4vY29tbW9uXCI7XG5cblxuZXhwb3J0IGludGVyZmFjZSBJUmVzb2x2ZWRIaWxvQmV0IGV4dGVuZHMgSVJlc29sdmVkQmV0XG57XG4gICAgYmV0VHlwZTpzdHJpbmc7XG4gICAgc3RhcnRDYXJkOm51bWJlcjtcbiAgICBuZXh0Q2FyZDpudW1iZXI7XG59Il19