/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/chat-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
export var chatLoginMessagePrefix = 'My Chat Public Key is: ';
/**
 * @record
 */
export function IChatAuthenticationData() { }
if (false) {
    /** @type {?} */
    IChatAuthenticationData.prototype.isEasyAccount;
    /** @type {?} */
    IChatAuthenticationData.prototype.chatPublicKey;
    /** @type {?} */
    IChatAuthenticationData.prototype.eosAccountName;
    /** @type {?} */
    IChatAuthenticationData.prototype.easyAccountId;
    /** @type {?} */
    IChatAuthenticationData.prototype.signature;
    /** @type {?} */
    IChatAuthenticationData.prototype.isSignedDataHased;
    /** @type {?} */
    IChatAuthenticationData.prototype.nonce;
}
/**
 * @record
 */
export function IChatMessageData() { }
if (false) {
    /** @type {?} */
    IChatMessageData.prototype.chatPublicKey;
    /** @type {?} */
    IChatMessageData.prototype.message;
    /** @type {?} */
    IChatMessageData.prototype.nonce;
    /** @type {?} */
    IChatMessageData.prototype.signature;
}
/**
 * @record
 */
export function IValidatedChatMessage() { }
if (false) {
    /** @type {?} */
    IValidatedChatMessage.prototype.from;
    /** @type {?} */
    IValidatedChatMessage.prototype.message;
    /** @type {?} */
    IValidatedChatMessage.prototype.time;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhdC1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvY2hhdC1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE1BQU0sS0FBTyxzQkFBc0IsR0FBRyx5QkFBeUI7Ozs7QUFHL0QsNkNBU0M7OztJQVBHLGdEQUFzQjs7SUFDdEIsZ0RBQXFCOztJQUNyQixpREFBc0I7O0lBQ3RCLGdEQUFxQjs7SUFDckIsNENBQWlCOztJQUNqQixvREFBMEI7O0lBQzFCLHdDQUFhOzs7OztBQUdqQixzQ0FNQzs7O0lBSkcseUNBQXFCOztJQUNyQixtQ0FBZTs7SUFDZixpQ0FBYTs7SUFDYixxQ0FBaUI7Ozs7O0FBR3JCLDJDQUtDOzs7SUFIRyxxQ0FBWTs7SUFDWix3Q0FBZTs7SUFDZixxQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBjb25zdCBjaGF0TG9naW5NZXNzYWdlUHJlZml4ID0gJ015IENoYXQgUHVibGljIEtleSBpczogJztcblxuXG5leHBvcnQgaW50ZXJmYWNlIElDaGF0QXV0aGVudGljYXRpb25EYXRhXG57XG4gICAgaXNFYXN5QWNjb3VudDpib29sZWFuO1xuICAgIGNoYXRQdWJsaWNLZXk6c3RyaW5nO1xuICAgIGVvc0FjY291bnROYW1lOnN0cmluZztcbiAgICBlYXN5QWNjb3VudElkOnN0cmluZztcbiAgICBzaWduYXR1cmU6c3RyaW5nO1xuICAgIGlzU2lnbmVkRGF0YUhhc2VkOmJvb2xlYW47XG4gICAgbm9uY2U6c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElDaGF0TWVzc2FnZURhdGFcbntcbiAgICBjaGF0UHVibGljS2V5OnN0cmluZztcbiAgICBtZXNzYWdlOnN0cmluZztcbiAgICBub25jZTpzdHJpbmc7XG4gICAgc2lnbmF0dXJlOnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJVmFsaWRhdGVkQ2hhdE1lc3NhZ2VcbntcbiAgICBmcm9tOnN0cmluZztcbiAgICBtZXNzYWdlOnN0cmluZztcbiAgICB0aW1lOm51bWJlcjtcbn0iXX0=