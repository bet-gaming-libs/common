/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/common.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IResolvedBet() { }
if (false) {
    /** @type {?} */
    IResolvedBet.prototype.id;
    /** @type {?} */
    IResolvedBet.prototype.isEasyAccount;
    /** @type {?} */
    IResolvedBet.prototype.bettor;
    /** @type {?} */
    IResolvedBet.prototype.amount;
    /** @type {?} */
    IResolvedBet.prototype.resolvedMilliseconds;
    /** @type {?} */
    IResolvedBet.prototype.referrer;
    /** @type {?} */
    IResolvedBet.prototype.jackpotSpin;
    /** @type {?} */
    IResolvedBet.prototype.stakeWeight;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvY29tbW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsa0NBVUM7OztJQVJHLDBCQUFVOztJQUNWLHFDQUFzQjs7SUFDdEIsOEJBQWM7O0lBQ2QsOEJBQWM7O0lBQ2QsNENBQTRCOztJQUM1QixnQ0FBZ0I7O0lBQ2hCLG1DQUFtQjs7SUFDbkIsbUNBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJUmVzb2x2ZWRCZXRcbntcbiAgICBpZDpzdHJpbmc7XG4gICAgaXNFYXN5QWNjb3VudDpib29sZWFuO1xuICAgIGJldHRvcjpzdHJpbmc7XG4gICAgYW1vdW50OnN0cmluZztcbiAgICByZXNvbHZlZE1pbGxpc2Vjb25kczpudW1iZXI7XG4gICAgcmVmZXJyZXI6c3RyaW5nO1xuICAgIGphY2twb3RTcGluOnN0cmluZztcbiAgICBzdGFrZVdlaWdodDpzdHJpbmc7XG59Il19