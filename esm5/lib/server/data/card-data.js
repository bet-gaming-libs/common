/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/card-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var CardSuit = {
    Hearts: 0,
    Diamonds: 1,
    Spades: 2,
    Clubs: 3,
};
export { CardSuit };
CardSuit[CardSuit.Hearts] = 'Hearts';
CardSuit[CardSuit.Diamonds] = 'Diamonds';
CardSuit[CardSuit.Spades] = 'Spades';
CardSuit[CardSuit.Clubs] = 'Clubs';
/**
 * @record
 */
export function ICardData() { }
if (false) {
    /** @type {?} */
    ICardData.prototype.suit;
    /** @type {?} */
    ICardData.prototype.rankName;
}
var CardRankNumber = /** @class */ (function () {
    function CardRankNumber() {
    }
    CardRankNumber.ACE = 1;
    CardRankNumber.TEN = 10;
    CardRankNumber.JACK = 11;
    CardRankNumber.QUEEN = 12;
    CardRankNumber.KING = 0;
    return CardRankNumber;
}());
export { CardRankNumber };
if (false) {
    /** @type {?} */
    CardRankNumber.ACE;
    /** @type {?} */
    CardRankNumber.TEN;
    /** @type {?} */
    CardRankNumber.JACK;
    /** @type {?} */
    CardRankNumber.QUEEN;
    /** @type {?} */
    CardRankNumber.KING;
}
var CardRankName = /** @class */ (function () {
    function CardRankName() {
    }
    CardRankName.ACE = 'Ace';
    CardRankName.JACK = 'Jack';
    CardRankName.QUEEN = 'Queen';
    CardRankName.KING = 'King';
    return CardRankName;
}());
export { CardRankName };
if (false) {
    /** @type {?} */
    CardRankName.ACE;
    /** @type {?} */
    CardRankName.JACK;
    /** @type {?} */
    CardRankName.QUEEN;
    /** @type {?} */
    CardRankName.KING;
}
/**
 * @param {?} value
 * @return {?}
 */
export function numberToSuit(value) {
    switch (value) {
        case 0:
            return CardSuit.Hearts;
        case 1:
            return CardSuit.Diamonds;
        case 2:
            return CardSuit.Spades;
        case 3:
            return CardSuit.Clubs;
    }
}
/**
 * @param {?} value
 * @return {?}
 */
export function nameOfSuit(value) {
    switch (value) {
        case CardSuit.Hearts:
            return "Hearts";
        case CardSuit.Diamonds:
            return "Diamonds";
        case CardSuit.Spades:
            return "Spades";
        case CardSuit.Clubs:
            return "Clubs";
    }
}
/**
 * @param {?} value
 * @return {?}
 */
export function nameOfRank(value) {
    switch (value) {
        case CardRankNumber.ACE:
            return CardRankName.ACE.substr(0, 1);
        case CardRankNumber.JACK:
            return CardRankName.JACK.substr(0, 1);
        case CardRankNumber.QUEEN:
            return CardRankName.QUEEN.substr(0, 1);
        case CardRankNumber.KING:
            return CardRankName.KING.substr(0, 1);
        default:
            return value.toString();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvY2FyZC1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLElBQVksUUFBUTtJQUVoQixNQUFNLEdBQUE7SUFDTixRQUFRLEdBQUE7SUFDUixNQUFNLEdBQUE7SUFDTixLQUFLLEdBQUE7RUFDUjs7Ozs7Ozs7O0FBRUQsK0JBSUM7OztJQUZHLHlCQUFjOztJQUNkLDZCQUFnQjs7QUFHcEI7SUFBQTtJQU9BLENBQUM7SUFMVSxrQkFBRyxHQUFLLENBQUMsQ0FBQztJQUNWLGtCQUFHLEdBQUssRUFBRSxDQUFDO0lBQ1gsbUJBQUksR0FBSSxFQUFFLENBQUM7SUFDWCxvQkFBSyxHQUFHLEVBQUUsQ0FBQztJQUNYLG1CQUFJLEdBQUksQ0FBQyxDQUFDO0lBQ3JCLHFCQUFDO0NBQUEsQUFQRCxJQU9DO1NBUFksY0FBYzs7O0lBRXZCLG1CQUFpQjs7SUFDakIsbUJBQWtCOztJQUNsQixvQkFBa0I7O0lBQ2xCLHFCQUFrQjs7SUFDbEIsb0JBQWlCOztBQUdyQjtJQUFBO0lBTUEsQ0FBQztJQUpVLGdCQUFHLEdBQUssS0FBSyxDQUFDO0lBQ2QsaUJBQUksR0FBSSxNQUFNLENBQUM7SUFDZixrQkFBSyxHQUFHLE9BQU8sQ0FBQztJQUNoQixpQkFBSSxHQUFJLE1BQU0sQ0FBQztJQUMxQixtQkFBQztDQUFBLEFBTkQsSUFNQztTQU5ZLFlBQVk7OztJQUVyQixpQkFBcUI7O0lBQ3JCLGtCQUFzQjs7SUFDdEIsbUJBQXVCOztJQUN2QixrQkFBc0I7Ozs7OztBQUcxQixNQUFNLFVBQVUsWUFBWSxDQUFDLEtBQVk7SUFDckMsUUFBUSxLQUFLLEVBQUU7UUFDWCxLQUFLLENBQUM7WUFDRixPQUFPLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFFM0IsS0FBSyxDQUFDO1lBQ0YsT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBRTdCLEtBQUssQ0FBQztZQUNGLE9BQU8sUUFBUSxDQUFDLE1BQU0sQ0FBQztRQUUzQixLQUFLLENBQUM7WUFDRixPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUM7S0FDN0I7QUFDTCxDQUFDOzs7OztBQUVELE1BQU0sVUFBVSxVQUFVLENBQUMsS0FBYztJQUNyQyxRQUFRLEtBQUssRUFBRTtRQUNYLEtBQUssUUFBUSxDQUFDLE1BQU07WUFDaEIsT0FBTyxRQUFRLENBQUM7UUFFcEIsS0FBSyxRQUFRLENBQUMsUUFBUTtZQUNsQixPQUFPLFVBQVUsQ0FBQztRQUV0QixLQUFLLFFBQVEsQ0FBQyxNQUFNO1lBQ2hCLE9BQU8sUUFBUSxDQUFDO1FBRXBCLEtBQUssUUFBUSxDQUFDLEtBQUs7WUFDZixPQUFPLE9BQU8sQ0FBQztLQUN0QjtBQUNMLENBQUM7Ozs7O0FBRUQsTUFBTSxVQUFVLFVBQVUsQ0FBQyxLQUFZO0lBQ25DLFFBQVEsS0FBSyxFQUFFO1FBQ1gsS0FBSyxjQUFjLENBQUMsR0FBRztZQUNuQixPQUFPLFlBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztRQUV4QyxLQUFLLGNBQWMsQ0FBQyxJQUFJO1lBQ3BCLE9BQU8sWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO1FBRXpDLEtBQUssY0FBYyxDQUFDLEtBQUs7WUFDckIsT0FBTyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7UUFFMUMsS0FBSyxjQUFjLENBQUMsSUFBSTtZQUNwQixPQUFPLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQztRQUd6QztZQUNJLE9BQU8sS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO0tBQy9CO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIENhcmRTdWl0XG57XG4gICAgSGVhcnRzLFxuICAgIERpYW1vbmRzLFxuICAgIFNwYWRlcyxcbiAgICBDbHVic1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElDYXJkRGF0YVxue1xuICAgIHN1aXQ6Q2FyZFN1aXQ7XG4gICAgcmFua05hbWU6c3RyaW5nO1xufVxuXG5leHBvcnQgY2xhc3MgQ2FyZFJhbmtOdW1iZXJcbntcbiAgICBzdGF0aWMgQUNFICAgPSAxO1xuICAgIHN0YXRpYyBURU4gICA9IDEwO1xuICAgIHN0YXRpYyBKQUNLICA9IDExO1xuICAgIHN0YXRpYyBRVUVFTiA9IDEyO1xuICAgIHN0YXRpYyBLSU5HICA9IDA7XG59XG5cbmV4cG9ydCBjbGFzcyBDYXJkUmFua05hbWVcbntcbiAgICBzdGF0aWMgQUNFICAgPSAnQWNlJztcbiAgICBzdGF0aWMgSkFDSyAgPSAnSmFjayc7XG4gICAgc3RhdGljIFFVRUVOID0gJ1F1ZWVuJztcbiAgICBzdGF0aWMgS0lORyAgPSAnS2luZyc7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBudW1iZXJUb1N1aXQodmFsdWU6bnVtYmVyKTpDYXJkU3VpdCB7XG4gICAgc3dpdGNoICh2YWx1ZSkge1xuICAgICAgICBjYXNlIDA6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFN1aXQuSGVhcnRzO1xuXG4gICAgICAgIGNhc2UgMTpcbiAgICAgICAgICAgIHJldHVybiBDYXJkU3VpdC5EaWFtb25kcztcblxuICAgICAgICBjYXNlIDI6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFN1aXQuU3BhZGVzO1xuXG4gICAgICAgIGNhc2UgMzpcbiAgICAgICAgICAgIHJldHVybiBDYXJkU3VpdC5DbHVicztcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBuYW1lT2ZTdWl0KHZhbHVlOkNhcmRTdWl0KTpzdHJpbmcge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgY2FzZSBDYXJkU3VpdC5IZWFydHM6XG4gICAgICAgICAgICByZXR1cm4gXCJIZWFydHNcIjtcblxuICAgICAgICBjYXNlIENhcmRTdWl0LkRpYW1vbmRzOlxuICAgICAgICAgICAgcmV0dXJuIFwiRGlhbW9uZHNcIjtcblxuICAgICAgICBjYXNlIENhcmRTdWl0LlNwYWRlczpcbiAgICAgICAgICAgIHJldHVybiBcIlNwYWRlc1wiO1xuXG4gICAgICAgIGNhc2UgQ2FyZFN1aXQuQ2x1YnM6XG4gICAgICAgICAgICByZXR1cm4gXCJDbHVic1wiO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIG5hbWVPZlJhbmsodmFsdWU6bnVtYmVyKTpzdHJpbmcge1xuICAgIHN3aXRjaCAodmFsdWUpIHtcbiAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5BQ0U6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFJhbmtOYW1lLkFDRS5zdWJzdHIoMCwxKTtcblxuICAgICAgICBjYXNlIENhcmRSYW5rTnVtYmVyLkpBQ0s6XG4gICAgICAgICAgICByZXR1cm4gQ2FyZFJhbmtOYW1lLkpBQ0suc3Vic3RyKDAsMSk7XG5cbiAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5RVUVFTjpcbiAgICAgICAgICAgIHJldHVybiBDYXJkUmFua05hbWUuUVVFRU4uc3Vic3RyKDAsMSk7XG5cbiAgICAgICAgY2FzZSBDYXJkUmFua051bWJlci5LSU5HOlxuICAgICAgICAgICAgcmV0dXJuIENhcmRSYW5rTmFtZS5LSU5HLnN1YnN0cigwLDEpO1xuXG5cbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZS50b1N0cmluZygpO1xuICAgIH1cbn0iXX0=