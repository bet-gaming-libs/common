/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/data/dice-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IResolvedDiceBet() { }
if (false) {
    /** @type {?} */
    IResolvedDiceBet.prototype.isRollOver;
    /** @type {?} */
    IResolvedDiceBet.prototype.number;
    /** @type {?} */
    IResolvedDiceBet.prototype.roll;
}
/**
 * @record
 */
export function IDiceGameData() { }
if (false) {
    /** @type {?} */
    IDiceGameData.prototype.recentBets;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGljZS1kYXRhLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL2RhdGEvZGljZS1kYXRhLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBR0Esc0NBS0M7OztJQUhHLHNDQUFtQjs7SUFDbkIsa0NBQWM7O0lBQ2QsZ0NBQVk7Ozs7O0FBR2hCLG1DQUdDOzs7SUFERyxtQ0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJUmVzb2x2ZWRCZXQgfSBmcm9tIFwiLi9jb21tb25cIjtcblxuXG5leHBvcnQgaW50ZXJmYWNlIElSZXNvbHZlZERpY2VCZXQgZXh0ZW5kcyBJUmVzb2x2ZWRCZXRcbntcbiAgICBpc1JvbGxPdmVyOmJvb2xlYW47XG4gICAgbnVtYmVyOm51bWJlcjtcbiAgICByb2xsOm51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJRGljZUdhbWVEYXRhXG57XG4gICAgcmVjZW50QmV0czpJUmVzb2x2ZWREaWNlQmV0W107XG59Il19