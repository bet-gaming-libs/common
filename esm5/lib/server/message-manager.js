/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { makeRequest } from './request';
import { dispatchMessage } from './messages';
var MessageManager = /** @class */ (function () {
    function MessageManager(emitter, messagePrefix) {
        this.emitter = emitter;
        this.messagePrefix = messagePrefix;
    }
    /**
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    MessageManager.prototype.sendMessage = /**
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    function (messageName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        /** @type {?} */
        var start = [this.messagePrefix, messageName];
        /** @type {?} */
        var argsArray = start.concat(args);
        this.emitter.emit.apply(this.emitter, argsArray);
        return argsArray;
    };
    return MessageManager;
}());
export { MessageManager };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.emitter;
    /**
     * @type {?}
     * @protected
     */
    MessageManager.prototype.messagePrefix;
}
var SocketMessageManager = /** @class */ (function (_super) {
    tslib_1.__extends(SocketMessageManager, _super);
    function SocketMessageManager(socket, messagePrefix, messageReceiver) {
        var _this = _super.call(this, socket, messagePrefix) || this;
        _this.socket = socket;
        _this.messageReceiver = messageReceiver;
        _this.setupMessageListener();
        return _this;
    }
    /**
     * @private
     * @return {?}
     */
    SocketMessageManager.prototype.setupMessageListener = /**
     * @private
     * @return {?}
     */
    function () {
        // listen to all messages with message prefix
        /** @type {?} */
        var receiver = this.messageReceiver;
        this.socket.on(this.messagePrefix, (/**
         * @return {?}
         */
        function () {
            dispatchMessage(arguments, receiver);
        }));
    };
    /**
     * @protected
     * @template T
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    SocketMessageManager.prototype.makeRequest = /**
     * @protected
     * @template T
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    function (messageName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        return makeRequest(this.messagePrefix, messageName, args, this.socket);
    };
    return SocketMessageManager;
}(MessageManager));
export { SocketMessageManager };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.socket;
    /**
     * @type {?}
     * @private
     */
    SocketMessageManager.prototype.messageReceiver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS1tYW5hZ2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvc2VydmVyL21lc3NhZ2UtbWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFDQSxPQUFPLEVBQUMsV0FBVyxFQUFDLE1BQU0sV0FBVyxDQUFDO0FBRXRDLE9BQU8sRUFBQyxlQUFlLEVBQUMsTUFBTSxZQUFZLENBQUM7QUFHM0M7SUFFSSx3QkFDYyxPQUFnQixFQUNoQixhQUFvQjtRQURwQixZQUFPLEdBQVAsT0FBTyxDQUFTO1FBQ2hCLGtCQUFhLEdBQWIsYUFBYSxDQUFPO0lBRWxDLENBQUM7Ozs7OztJQUVNLG9DQUFXOzs7OztJQUFsQixVQUFtQixXQUFrQjtRQUFFLGNBQWE7YUFBYixVQUFhLEVBQWIscUJBQWEsRUFBYixJQUFhO1lBQWIsNkJBQWE7OztZQUUxQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFDLFdBQVcsQ0FBQzs7WUFDeEMsU0FBUyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBRXBDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FDbkIsSUFBSSxDQUFDLE9BQU8sRUFDWixTQUFTLENBQ1osQ0FBQztRQUVGLE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7SUFDTCxxQkFBQztBQUFELENBQUMsQUFwQkQsSUFvQkM7Ozs7Ozs7SUFqQk8saUNBQTBCOzs7OztJQUMxQix1Q0FBOEI7O0FBa0J0QztJQUEwQyxnREFBYztJQUVwRCw4QkFDWSxNQUFjLEVBQ3RCLGFBQW9CLEVBQ1osZUFBbUI7UUFIL0IsWUFLSSxrQkFBTSxNQUFNLEVBQUMsYUFBYSxDQUFDLFNBRzlCO1FBUFcsWUFBTSxHQUFOLE1BQU0sQ0FBUTtRQUVkLHFCQUFlLEdBQWYsZUFBZSxDQUFJO1FBSTNCLEtBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDOztJQUNoQyxDQUFDOzs7OztJQUVPLG1EQUFvQjs7OztJQUE1Qjs7O1lBRVUsUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFlO1FBRXJDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUNWLElBQUksQ0FBQyxhQUFhOzs7UUFDbEI7WUFDSSxlQUFlLENBQUMsU0FBUyxFQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3hDLENBQUMsRUFDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7Ozs7SUFFUywwQ0FBVzs7Ozs7OztJQUFyQixVQUNJLFdBQWtCO1FBQ2xCLGNBQWE7YUFBYixVQUFhLEVBQWIscUJBQWEsRUFBYixJQUFhO1lBQWIsNkJBQWE7O1FBR2IsT0FBTyxXQUFXLENBQ2QsSUFBSSxDQUFDLGFBQWEsRUFDbEIsV0FBVyxFQUNYLElBQUksRUFDSixJQUFJLENBQUMsTUFBTSxDQUNkLENBQUM7SUFDTixDQUFDO0lBQ0wsMkJBQUM7QUFBRCxDQUFDLEFBcENELENBQTBDLGNBQWMsR0FvQ3ZEOzs7Ozs7O0lBakNPLHNDQUFzQjs7Ozs7SUFFdEIsK0NBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSVByb21pc2UgfSBmcm9tICcuLi91dGlsL3Byb21pc2UnO1xuaW1wb3J0IHttYWtlUmVxdWVzdH0gZnJvbSAnLi9yZXF1ZXN0JztcbmltcG9ydCB7IElTb2NrZXQsIElFbWl0dGVyIH0gZnJvbSAnLi9pbnRlcmZhY2VzJztcbmltcG9ydCB7ZGlzcGF0Y2hNZXNzYWdlfSBmcm9tICcuL21lc3NhZ2VzJztcblxuXG5leHBvcnQgY2xhc3MgTWVzc2FnZU1hbmFnZXJcbntcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJvdGVjdGVkIGVtaXR0ZXI6SUVtaXR0ZXIsXG4gICAgICAgIHByb3RlY3RlZCBtZXNzYWdlUHJlZml4OnN0cmluZ1xuICAgICkgeyAgICBcbiAgICB9XG5cbiAgICBwdWJsaWMgc2VuZE1lc3NhZ2UobWVzc2FnZU5hbWU6c3RyaW5nLCAuLi5hcmdzOmFueVtdKVxuICAgIHtcbiAgICAgICAgY29uc3Qgc3RhcnQgPSBbdGhpcy5tZXNzYWdlUHJlZml4LG1lc3NhZ2VOYW1lXTtcbiAgICAgICAgY29uc3QgYXJnc0FycmF5ID0gc3RhcnQuY29uY2F0KGFyZ3MpO1xuXG4gICAgICAgIHRoaXMuZW1pdHRlci5lbWl0LmFwcGx5KFxuICAgICAgICAgICAgdGhpcy5lbWl0dGVyLFxuICAgICAgICAgICAgYXJnc0FycmF5XG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIGFyZ3NBcnJheTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTb2NrZXRNZXNzYWdlTWFuYWdlciBleHRlbmRzIE1lc3NhZ2VNYW5hZ2VyXG57XG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgc29ja2V0OklTb2NrZXQsXG4gICAgICAgIG1lc3NhZ2VQcmVmaXg6c3RyaW5nLFxuICAgICAgICBwcml2YXRlIG1lc3NhZ2VSZWNlaXZlcjphbnlcbiAgICApIHtcbiAgICAgICAgc3VwZXIoc29ja2V0LG1lc3NhZ2VQcmVmaXgpO1xuXG4gICAgICAgIHRoaXMuc2V0dXBNZXNzYWdlTGlzdGVuZXIoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNldHVwTWVzc2FnZUxpc3RlbmVyKCkge1xuICAgICAgICAvLyBsaXN0ZW4gdG8gYWxsIG1lc3NhZ2VzIHdpdGggbWVzc2FnZSBwcmVmaXhcbiAgICAgICAgY29uc3QgcmVjZWl2ZXIgPSB0aGlzLm1lc3NhZ2VSZWNlaXZlcjtcblxuICAgICAgICB0aGlzLnNvY2tldC5vbihcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZVByZWZpeCxcbiAgICAgICAgICAgIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgIGRpc3BhdGNoTWVzc2FnZShhcmd1bWVudHMscmVjZWl2ZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICApO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBtYWtlUmVxdWVzdDxUPihcbiAgICAgICAgbWVzc2FnZU5hbWU6c3RyaW5nLFxuICAgICAgICAuLi5hcmdzOmFueVtdXG4gICAgKTpJUHJvbWlzZTxUPlxuICAgIHtcbiAgICAgICAgcmV0dXJuIG1ha2VSZXF1ZXN0PFQ+KFxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlUHJlZml4LFxuICAgICAgICAgICAgbWVzc2FnZU5hbWUsXG4gICAgICAgICAgICBhcmdzLFxuICAgICAgICAgICAgdGhpcy5zb2NrZXRcbiAgICAgICAgKTtcbiAgICB9XG59Il19