/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/dice-message-manager.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { DiceMessage } from './messages';
import { SocketMessageManager } from './message-manager';
/**
 * @abstract
 */
var /**
 * @abstract
 */
DiceMessageManager = /** @class */ (function (_super) {
    tslib_1.__extends(DiceMessageManager, _super);
    function DiceMessageManager(socket, messageReceiver) {
        return _super.call(this, socket, DiceMessage.PREFIX, messageReceiver) || this;
    }
    /**
     * @protected
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    DiceMessageManager.prototype.sendDiceMessage = /**
     * @protected
     * @param {?} messageName
     * @param {...?} args
     * @return {?}
     */
    function (messageName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        /** @type {?} */
        var start = [messageName];
        this.sendMessage.apply(this, start.concat(args));
    };
    return DiceMessageManager;
}(SocketMessageManager));
/**
 * @abstract
 */
export { DiceMessageManager };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGljZS1tZXNzYWdlLW1hbmFnZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2ZXIvZGljZS1tZXNzYWdlLW1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7OztBQUl6RDs7OztJQUFpRCw4Q0FBb0I7SUFFakUsNEJBQVksTUFBYyxFQUFDLGVBQW1CO2VBQzFDLGtCQUFNLE1BQU0sRUFBQyxXQUFXLENBQUMsTUFBTSxFQUFDLGVBQWUsQ0FBQztJQUNwRCxDQUFDOzs7Ozs7O0lBRVMsNENBQWU7Ozs7OztJQUF6QixVQUEwQixXQUFrQjtRQUFFLGNBQWE7YUFBYixVQUFhLEVBQWIscUJBQWEsRUFBYixJQUFhO1lBQWIsNkJBQWE7OztZQUNuRCxLQUFLLEdBQUcsQ0FBQyxXQUFXLENBQUM7UUFFekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQ2xCLElBQUksRUFDSixLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUNyQixDQUFDO0lBQ04sQ0FBQztJQUNMLHlCQUFDO0FBQUQsQ0FBQyxBQWRELENBQWlELG9CQUFvQixHQWNwRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpY2VNZXNzYWdlIH0gZnJvbSAnLi9tZXNzYWdlcyc7XG5pbXBvcnQgeyBTb2NrZXRNZXNzYWdlTWFuYWdlciB9IGZyb20gJy4vbWVzc2FnZS1tYW5hZ2VyJztcbmltcG9ydCB7IElTb2NrZXQgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xuXG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEaWNlTWVzc2FnZU1hbmFnZXIgZXh0ZW5kcyBTb2NrZXRNZXNzYWdlTWFuYWdlclxue1xuICAgIGNvbnN0cnVjdG9yKHNvY2tldDpJU29ja2V0LG1lc3NhZ2VSZWNlaXZlcjphbnkpIHtcbiAgICAgICAgc3VwZXIoc29ja2V0LERpY2VNZXNzYWdlLlBSRUZJWCxtZXNzYWdlUmVjZWl2ZXIpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBzZW5kRGljZU1lc3NhZ2UobWVzc2FnZU5hbWU6c3RyaW5nLCAuLi5hcmdzOmFueVtdKSB7XG4gICAgICAgIHZhciBzdGFydCA9IFttZXNzYWdlTmFtZV07XG5cbiAgICAgICAgdGhpcy5zZW5kTWVzc2FnZS5hcHBseShcbiAgICAgICAgICAgIHRoaXMsXG4gICAgICAgICAgICBzdGFydC5jb25jYXQoYXJncylcbiAgICAgICAgKTtcbiAgICB9XG59Il19