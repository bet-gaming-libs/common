/**
 * @fileoverview added by tsickle
 * Generated from: lib/server/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEmitter() { }
if (false) {
    /**
     * @param {?} event
     * @param {...?} args
     * @return {?}
     */
    IEmitter.prototype.emit = function (event, args) { };
}
/**
 * \@doNotMinify
 * @record
 */
export function ISocket() { }
if (false) {
    /**
     * @param {?} event
     * @param {?} fn
     * @return {?}
     */
    ISocket.prototype.on = function (event, fn) { };
}
/**
 * @record
 */
export function IServerConfig() { }
if (false) {
    /** @type {?} */
    IServerConfig.prototype.host;
    /** @type {?} */
    IServerConfig.prototype.port;
    /** @type {?} */
    IServerConfig.prototype.ssl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3NlcnZlci9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsOEJBRUM7Ozs7Ozs7SUFERyxxREFBb0M7Ozs7OztBQUl4Qyw2QkFFQzs7Ozs7OztJQURHLGdEQUF5Qzs7Ozs7QUFHN0MsbUNBS0M7OztJQUhHLDZCQUFZOztJQUNaLDZCQUFZOztJQUNaLDRCQUFZIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGludGVyZmFjZSBJRW1pdHRlciB7XG4gICAgZW1pdChldmVudDogc3RyaW5nLCAuLi5hcmdzOiBhbnlbXSk7XG59XG5cbi8qKiogQGRvTm90TWluaWZ5ICovXG5leHBvcnQgaW50ZXJmYWNlIElTb2NrZXQgZXh0ZW5kcyBJRW1pdHRlciB7XG4gICAgb24oZXZlbnQ6IHN0cmluZywgZm46IEZ1bmN0aW9uKTogSVNvY2tldDtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJU2VydmVyQ29uZmlnXG57XG4gICAgaG9zdDpzdHJpbmc7XG4gICAgcG9ydDpudW1iZXI7XG4gICAgc3NsOmJvb2xlYW47XG59Il19