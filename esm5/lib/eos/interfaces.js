/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/interfaces.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosTokenTransferData() { }
if (false) {
    /** @type {?} */
    IEosTokenTransferData.prototype.from;
    /** @type {?} */
    IEosTokenTransferData.prototype.to;
    /** @type {?} */
    IEosTokenTransferData.prototype.quantity;
    /** @type {?} */
    IEosTokenTransferData.prototype.memo;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJmYWNlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2Vvcy9pbnRlcmZhY2VzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsMkNBTUM7OztJQUpHLHFDQUFZOztJQUNaLG1DQUFVOztJQUNWLHlDQUFnQjs7SUFDaEIscUNBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFb3NUb2tlblRyYW5zZmVyRGF0YVxue1xuICAgIGZyb206c3RyaW5nO1xuICAgIHRvOnN0cmluZztcbiAgICBxdWFudGl0eTpzdHJpbmc7XG4gICAgbWVtbzpzdHJpbmc7XG59Il19