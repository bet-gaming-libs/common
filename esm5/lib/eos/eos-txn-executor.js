/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-txn-executor.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosTransactionListener() { }
if (false) {
    /**
     * @param {?} txnId
     * @return {?}
     */
    IEosTransactionListener.prototype.onEosTransactionConfirmed = function (txnId) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXR4bi1leGVjdXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2Vvcy9lb3MtdHhuLWV4ZWN1dG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7O0FBQUEsNkNBR0M7Ozs7OztJQURHLG1GQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBpbnRlcmZhY2UgSUVvc1RyYW5zYWN0aW9uTGlzdGVuZXJcbntcbiAgICBvbkVvc1RyYW5zYWN0aW9uQ29uZmlybWVkKHR4bklkOnN0cmluZyk6YW55O1xufVxuXG5cbmV4cG9ydCB0eXBlIFR4bkNvbmZpcm1lZENoZWNrZXIgPSAodHhuSWQ6c3RyaW5nKSA9PiBQcm9taXNlPGJvb2xlYW4+OyJdfQ==