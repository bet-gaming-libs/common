/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
var bigInt = require('big-integer');
/**
 * @param {?} txnId
 * @return {?}
 */
export function getBetIdFromTransactionId(txnId) {
    /** @type {?} */
    var shortStr = txnId.substring(0, 16);
    return bigInt(shortStr, 16).toString();
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
export function executeTransactionUntilSuccessful(eos, actions) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var txnId, error_1;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, executeTransaction(eos, actions)];
                case 1:
                    txnId = _a.sent();
                    if (txnId != undefined) {
                        return [2 /*return*/, txnId];
                    }
                    else {
                        return [2 /*return*/, executeTransactionUntilSuccessful(eos, actions)];
                    }
                    return [3 /*break*/, 3];
                case 2:
                    error_1 = _a.sent();
                    console.error(error_1);
                    return [2 /*return*/, executeTransactionUntilSuccessful(eos, actions)];
                case 3: return [2 /*return*/];
            }
        });
    });
}
/**
 * @record
 */
export function IActionAuthorization() { }
if (false) {
    /** @type {?} */
    IActionAuthorization.prototype.actor;
    /** @type {?} */
    IActionAuthorization.prototype.permission;
}
/**
 * @record
 */
export function ITransactionAction() { }
if (false) {
    /** @type {?} */
    ITransactionAction.prototype.account;
    /** @type {?} */
    ITransactionAction.prototype.name;
    /** @type {?} */
    ITransactionAction.prototype.authorization;
    /** @type {?} */
    ITransactionAction.prototype.data;
}
/**
 * @param {?} eos
 * @param {?} actions
 * @return {?}
 * @this {*}
 */
export function executeTransaction(eos, actions) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var result, error_2;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, eos.transaction({
                            actions: actions
                        })];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, result.transaction_id];
                case 2:
                    error_2 = _a.sent();
                    //error = JSON.parse(error);
                    //console.log( error );
                    throw error_2;
                case 3: return [2 /*return*/];
            }
        });
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} table_key
 * @return {?}
 * @this {*}
 */
export function getAllTableRows(eos, code, scope, table, table_key) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var allRows, limit, lower_bound, result, numOfRows, lastRow, indexOfLastRow;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    allRows = [];
                    limit = -1;
                    lower_bound = '0';
                    _a.label = 1;
                case 1:
                    if (!true) return [3 /*break*/, 3];
                    return [4 /*yield*/, getTableRowsResult(eos, {
                            code: code, scope: scope, table: table,
                            limit: limit, lower_bound: lower_bound
                        })];
                case 2:
                    result = _a.sent();
                    if (result == undefined ||
                        result.rows.length == 0) {
                        return [3 /*break*/, 3];
                    }
                    // add new rows
                    allRows = allRows.concat(result.rows);
                    //const indexOfFirstRow = bigInt( result.rows[0][table_key] );
                    numOfRows = result.rows.length;
                    lastRow = result.rows[numOfRows - 1];
                    indexOfLastRow = bigInt(lastRow[table_key]);
                    //console.log('Index of First Row: '+indexOfFirstRow.toString())
                    //console.log('Index of Last Row: '+indexOfLastRow.toString())
                    if (result.more === false) {
                        return [3 /*break*/, 3];
                    }
                    lower_bound = indexOfLastRow.add(1).toString();
                    return [3 /*break*/, 1];
                case 3: return [2 /*return*/, allRows];
            }
        });
    });
}
/*
export function getEosBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosio.token',
        accountName,
        'EOS'
    );
}

export function getBtcBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'eosbettokens',
        accountName,
        'BTC'
    );
}

export function getBetBalance(eos,accountName:string):Promise<string>
{
    return getTokenBalance(
        eos,
        'betdividends',
        accountName,
        'BET'
    );
}
*/
/**
 * @param {?} eos
 * @param {?} tokenContractAccount
 * @param {?} tokenHolderAccount
 * @param {?} tokenSymbol
 * @param {?=} table
 * @return {?}
 * @this {*}
 */
export function getTokenBalance(eos, tokenContractAccount, tokenHolderAccount, tokenSymbol, table) {
    if (table === void 0) { table = 'accounts'; }
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var rows, rows_1, rows_1_1, row, parts;
        var e_1, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, getTableRows(eos, {
                        code: tokenContractAccount,
                        scope: tokenHolderAccount,
                        table: table
                    })];
                case 1:
                    rows = _b.sent();
                    try {
                        for (rows_1 = tslib_1.__values(rows), rows_1_1 = rows_1.next(); !rows_1_1.done; rows_1_1 = rows_1.next()) {
                            row = rows_1_1.value;
                            parts = row.balance.split(' ');
                            if (parts[1] == tokenSymbol) {
                                return [2 /*return*/, parts[0]];
                            }
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (rows_1_1 && !rows_1_1.done && (_a = rows_1.return)) _a.call(rows_1);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                    return [2 /*return*/, '0'];
            }
        });
    });
}
/**
 * @record
 */
function IGlobalVarRow() { }
if (false) {
    /** @type {?} */
    IGlobalVarRow.prototype.id;
    /** @type {?} */
    IGlobalVarRow.prototype.val;
}
/**
 * @param {?} eos
 * @param {?} smartContractAccount
 * @param {?} variableId
 * @return {?}
 * @this {*}
 */
export function getGlobalVariable(eos, smartContractAccount, variableId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var rows, rows_2, rows_2_1, row;
        var e_2, _a;
        return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, getTableRows(eos, {
                        code: smartContractAccount,
                        scope: smartContractAccount,
                        table: 'globalvars'
                    })];
                case 1:
                    rows = _b.sent();
                    if (rows.length === 0) {
                        throw new Error("Contract not INIT");
                    }
                    try {
                        for (rows_2 = tslib_1.__values(rows), rows_2_1 = rows_2.next(); !rows_2_1.done; rows_2_1 = rows_2.next()) {
                            row = rows_2_1.value;
                            if (variableId == row.id) {
                                return [2 /*return*/, row.val];
                            }
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (rows_2_1 && !rows_2_1.done && (_a = rows_2.return)) _a.call(rows_2);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                    return [2 /*return*/, undefined];
            }
        });
    });
}
/**
 * @param {?} eos
 * @param {?} easyAccountContract
 * @param {?} betId
 * @return {?}
 * @this {*}
 */
export function getAccountIdForActiveEasyAccountBet(eos, easyAccountContract, betId) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var rows;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getUniqueTableRow(eos, easyAccountContract, easyAccountContract, 'activebets', betId)];
                case 1:
                    rows = _a.sent();
                    return [2 /*return*/, rows && rows.length > 0 ?
                            rows[0].key_id :
                            undefined];
            }
        });
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} code
 * @param {?} scope
 * @param {?} table
 * @param {?} rowId
 * @return {?}
 */
export function getUniqueTableRow(eos, code, scope, table, rowId) {
    return getTableRows(eos, {
        code: code,
        scope: scope,
        table: table,
        lower_bound: rowId,
        upper_bound: rowId
    });
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
export function getTableRows(eos, parameters) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var result;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getTableRowsResult(eos, parameters)];
                case 1:
                    result = _a.sent();
                    return [2 /*return*/, result ?
                            result.rows :
                            undefined];
            }
        });
    });
}
/**
 * @record
 * @template T
 */
export function ITableRowQueryResult() { }
if (false) {
    /** @type {?} */
    ITableRowQueryResult.prototype.rows;
    /** @type {?} */
    ITableRowQueryResult.prototype.more;
}
/**
 * @template T
 * @param {?} eos
 * @param {?} parameters
 * @return {?}
 * @this {*}
 */
export function getTableRowsResult(eos, parameters) {
    return tslib_1.__awaiter(this, void 0, void 0, function () {
        var result, error_3;
        return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    parameters.json = true;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, eos.getTableRows(parameters)];
                case 2:
                    result = _a.sent();
                    return [2 /*return*/, result];
                case 3:
                    error_3 = _a.sent();
                    console.log('Cound not get table rows');
                    console.log(error_3);
                    return [2 /*return*/, undefined];
                case 4: return [2 /*return*/];
            }
        });
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLXV0aWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDOzs7OztBQVFyQyxNQUFNLFVBQVUseUJBQXlCLENBQUMsS0FBWTs7UUFDOUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUVyQyxPQUFPLE1BQU0sQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7QUFDM0MsQ0FBQzs7Ozs7OztBQUdELE1BQU0sVUFBZ0IsaUNBQWlDLENBQ25ELEdBQUcsRUFDSCxPQUE0Qjs7Ozs7OztvQkFHVixxQkFBTSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUMsT0FBTyxDQUFDLEVBQUE7O29CQUE3QyxLQUFLLEdBQUcsU0FBcUM7b0JBRW5ELElBQUksS0FBSyxJQUFJLFNBQVMsRUFBRTt3QkFDcEIsc0JBQU8sS0FBSyxFQUFDO3FCQUNoQjt5QkFBTTt3QkFDSCxzQkFBTyxpQ0FBaUMsQ0FBQyxHQUFHLEVBQUMsT0FBTyxDQUFDLEVBQUM7cUJBQ3pEOzs7O29CQUVELE9BQU8sQ0FBQyxLQUFLLENBQUMsT0FBSyxDQUFDLENBQUM7b0JBRXJCLHNCQUFPLGlDQUFpQyxDQUFDLEdBQUcsRUFBQyxPQUFPLENBQUMsRUFBQzs7Ozs7Q0FFN0Q7Ozs7QUFHRCwwQ0FHQzs7O0lBRkcscUNBQWM7O0lBQ2QsMENBQW1COzs7OztBQUd2Qix3Q0FLQzs7O0lBSkcscUNBQWdCOztJQUNoQixrQ0FBYTs7SUFDYiwyQ0FBc0M7O0lBQ3RDLGtDQUFVOzs7Ozs7OztBQUdkLE1BQU0sVUFBZ0Isa0JBQWtCLENBQ3BDLEdBQUcsRUFDSCxPQUE2Qjs7Ozs7OztvQkFHVixxQkFBTSxHQUFHLENBQUMsV0FBVyxDQUFDOzRCQUNqQyxPQUFPLFNBQUE7eUJBQ1YsQ0FBQyxFQUFBOztvQkFGSSxNQUFNLEdBQUcsU0FFYjtvQkFFRixzQkFBTyxNQUFNLENBQUMsY0FBYyxFQUFDOzs7b0JBRTdCLDRCQUE0QjtvQkFDNUIsdUJBQXVCO29CQUV2QixNQUFNLE9BQUssQ0FBQzs7Ozs7Q0FJbkI7Ozs7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFnQixlQUFlLENBQ2pDLEdBQUcsRUFDSCxJQUFXLEVBQ1gsS0FBWSxFQUNaLEtBQVksRUFDWixTQUFnQjs7Ozs7O29CQUVaLE9BQU8sR0FBTyxFQUFFO29CQUVkLEtBQUssR0FBRyxDQUFDLENBQUM7b0JBQ1osV0FBVyxHQUFHLEdBQUc7Ozt5QkFFZCxJQUFJO29CQUNRLHFCQUFNLGtCQUFrQixDQUNuQyxHQUFHLEVBQ0g7NEJBQ0ksSUFBSSxNQUFBLEVBQUMsS0FBSyxPQUFBLEVBQUMsS0FBSyxPQUFBOzRCQUNoQixLQUFLLE9BQUEsRUFBQyxXQUFXLGFBQUE7eUJBQ3BCLENBQ0osRUFBQTs7b0JBTkssTUFBTSxHQUFHLFNBTWQ7b0JBRUQsSUFDSSxNQUFNLElBQUksU0FBUzt3QkFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUN6Qjt3QkFDRSx3QkFBTTtxQkFDVDtvQkFHRCxlQUFlO29CQUNmLE9BQU8sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQzs7b0JBS2hDLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU07b0JBQzlCLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBQyxDQUFDLENBQUM7b0JBRWxDLGNBQWMsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDO29CQUdqRCxnRUFBZ0U7b0JBQ2hFLDhEQUE4RDtvQkFHOUQsSUFBSSxNQUFNLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBRTt3QkFDdkIsd0JBQU07cUJBQ1Q7b0JBR0QsV0FBVyxHQUFHLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7O3dCQUduRCxzQkFBTyxPQUFPLEVBQUM7Ozs7Q0FDbEI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0NELE1BQU0sVUFBZ0IsZUFBZSxDQUNqQyxHQUFHLEVBQ0gsb0JBQTJCLEVBQzNCLGtCQUF5QixFQUN6QixXQUFrQixFQUNsQixLQUFrQjtJQUFsQixzQkFBQSxFQUFBLGtCQUFrQjs7Ozs7O3dCQUVMLHFCQUFNLFlBQVksQ0FDM0IsR0FBRyxFQUNIO3dCQUNJLElBQUksRUFBRSxvQkFBb0I7d0JBQzFCLEtBQUssRUFBRSxrQkFBa0I7d0JBQ3pCLEtBQUssT0FBQTtxQkFDUixDQUNKLEVBQUE7O29CQVBLLElBQUksR0FBRyxTQU9aOzt3QkFFRCxLQUFnQixTQUFBLGlCQUFBLElBQUksQ0FBQSxzRUFBRTs0QkFBYixHQUFHOzRCQUNKLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7NEJBRWxDLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLFdBQVcsRUFBRTtnQ0FDekIsc0JBQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFDOzZCQUNuQjt5QkFDSjs7Ozs7Ozs7O29CQUVELHNCQUFPLEdBQUcsRUFBQzs7OztDQUNkOzs7O0FBR0QsNEJBR0M7OztJQUZHLDJCQUFXOztJQUNYLDRCQUFZOzs7Ozs7Ozs7QUFHaEIsTUFBTSxVQUFnQixpQkFBaUIsQ0FDbkMsR0FBRyxFQUNILG9CQUEyQixFQUMzQixVQUFrQjs7Ozs7O3dCQUVMLHFCQUFNLFlBQVksQ0FDM0IsR0FBRyxFQUNIO3dCQUNJLElBQUksRUFBRSxvQkFBb0I7d0JBQzFCLEtBQUssRUFBRSxvQkFBb0I7d0JBQzNCLEtBQUssRUFBRSxZQUFZO3FCQUN0QixDQUNKLEVBQUE7O29CQVBLLElBQUksR0FBRyxTQU9aO29CQUdELElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7d0JBQ25CLE1BQU0sSUFBSSxLQUFLLENBQUMsbUJBQW1CLENBQUMsQ0FBQztxQkFDeEM7O3dCQUdELEtBQWdCLFNBQUEsaUJBQUEsSUFBSSxDQUFBLHNFQUFFOzRCQUFiLEdBQUc7NEJBQ1IsSUFBSSxVQUFVLElBQUksR0FBRyxDQUFDLEVBQUUsRUFBRTtnQ0FDdEIsc0JBQU8sR0FBRyxDQUFDLEdBQUcsRUFBQzs2QkFDbEI7eUJBQ0o7Ozs7Ozs7OztvQkFFRCxzQkFBTyxTQUFTLEVBQUM7Ozs7Q0FDcEI7Ozs7Ozs7O0FBR0QsTUFBTSxVQUFnQixtQ0FBbUMsQ0FDckQsR0FBRyxFQUNILG1CQUEwQixFQUMxQixLQUFZOzs7Ozt3QkFFQyxxQkFBTSxpQkFBaUIsQ0FDaEMsR0FBRyxFQUNILG1CQUFtQixFQUFDLG1CQUFtQixFQUN2QyxZQUFZLEVBQ1osS0FBSyxDQUNSLEVBQUE7O29CQUxLLElBQUksR0FBRyxTQUtaO29CQUVELHNCQUFPLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUN4QixJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ2hCLFNBQVMsRUFBQzs7OztDQUNyQjs7Ozs7Ozs7OztBQUVELE1BQU0sVUFBVSxpQkFBaUIsQ0FDN0IsR0FBRyxFQUNILElBQVcsRUFBQyxLQUFZLEVBQUMsS0FBWSxFQUNyQyxLQUFZO0lBRVosT0FBTyxZQUFZLENBQ2YsR0FBRyxFQUNIO1FBQ0ksSUFBSSxNQUFBO1FBQ0osS0FBSyxPQUFBO1FBQ0wsS0FBSyxPQUFBO1FBRUwsV0FBVyxFQUFFLEtBQUs7UUFDbEIsV0FBVyxFQUFFLEtBQUs7S0FDckIsQ0FDSixDQUFDO0FBQ04sQ0FBQzs7Ozs7Ozs7QUFHRCxNQUFNLFVBQWdCLFlBQVksQ0FDOUIsR0FBRyxFQUNILFVBQW9DOzs7Ozt3QkFFckIscUJBQU0sa0JBQWtCLENBQ25DLEdBQUcsRUFDSCxVQUFVLENBQ2IsRUFBQTs7b0JBSEssTUFBTSxHQUFHLFNBR2Q7b0JBRUQsc0JBQU8sTUFBTSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNiLFNBQVMsRUFBQzs7OztDQUNyQjs7Ozs7QUFFRCwwQ0FJQzs7O0lBRkcsb0NBQVM7O0lBQ1Qsb0NBQWE7Ozs7Ozs7OztBQUdqQixNQUFNLFVBQWdCLGtCQUFrQixDQUNwQyxHQUFHLEVBQ0gsVUFBbUM7Ozs7OztvQkFFbkMsVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Ozs7b0JBR0oscUJBQU0sR0FBRyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsRUFBQTs7b0JBQTNDLE1BQU0sR0FBRyxTQUFrQztvQkFDakQsc0JBQU8sTUFBTSxFQUFDOzs7b0JBRWQsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO29CQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQUssQ0FBQyxDQUFDO29CQUVuQixzQkFBTyxTQUFTLEVBQUM7Ozs7O0NBRXhCIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgYmlnSW50ID0gcmVxdWlyZSgnYmlnLWludGVnZXInKTtcblxuXG5pbXBvcnQgeyBJVGFibGVSb3dRdWVyeVBhcmFtZXRlcnMsIElUb2tlbkJhbGFuY2VSb3csIElFYXN5QWNjb3VudEFjdGl2ZUJldCB9IFxuICAgIGZyb20gXCIuL2Vvcy1kYXRhXCI7XG5cblxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0QmV0SWRGcm9tVHJhbnNhY3Rpb25JZCh0eG5JZDpzdHJpbmcpOnN0cmluZyB7XG4gICAgdmFyIHNob3J0U3RyID0gdHhuSWQuc3Vic3RyaW5nKDAsIDE2KTtcbiAgICBcbiAgICByZXR1cm4gYmlnSW50KHNob3J0U3RyLCAxNikudG9TdHJpbmcoKTtcbn1cblxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZXhlY3V0ZVRyYW5zYWN0aW9uVW50aWxTdWNjZXNzZnVsKFxuICAgIGVvcyxcbiAgICBhY3Rpb25zOklUcmFuc2FjdGlvbkFjdGlvbltdXG4pOlByb21pc2U8c3RyaW5nPiB7XG4gICAgdHJ5IHtcbiAgICAgICAgY29uc3QgdHhuSWQgPSBhd2FpdCBleGVjdXRlVHJhbnNhY3Rpb24oZW9zLGFjdGlvbnMpO1xuXG4gICAgICAgIGlmICh0eG5JZCAhPSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIHJldHVybiB0eG5JZDtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJldHVybiBleGVjdXRlVHJhbnNhY3Rpb25VbnRpbFN1Y2Nlc3NmdWwoZW9zLGFjdGlvbnMpO1xuICAgICAgICB9XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XG5cbiAgICAgICAgcmV0dXJuIGV4ZWN1dGVUcmFuc2FjdGlvblVudGlsU3VjY2Vzc2Z1bChlb3MsYWN0aW9ucyk7XG4gICAgfVxufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUFjdGlvbkF1dGhvcml6YXRpb24ge1xuICAgIGFjdG9yOiBzdHJpbmc7XG4gICAgcGVybWlzc2lvbjogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUcmFuc2FjdGlvbkFjdGlvbiB7XG4gICAgYWNjb3VudDogc3RyaW5nO1xuICAgIG5hbWU6IHN0cmluZztcbiAgICBhdXRob3JpemF0aW9uOiBJQWN0aW9uQXV0aG9yaXphdGlvbltdO1xuICAgIGRhdGE6IGFueTtcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGV4ZWN1dGVUcmFuc2FjdGlvbihcbiAgICBlb3MsXG4gICAgYWN0aW9uczogSVRyYW5zYWN0aW9uQWN0aW9uW11cbik6UHJvbWlzZTxzdHJpbmc+IHtcbiAgICB0cnkge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBlb3MudHJhbnNhY3Rpb24oe1xuICAgICAgICAgICAgYWN0aW9uc1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcmVzdWx0LnRyYW5zYWN0aW9uX2lkO1xuICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgIC8vZXJyb3IgPSBKU09OLnBhcnNlKGVycm9yKTtcbiAgICAgICAgLy9jb25zb2xlLmxvZyggZXJyb3IgKTtcbiAgICAgICAgXG4gICAgICAgIHRocm93IGVycm9yO1xuXG4gICAgICAgIC8vcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG59XG5cbmV4cG9ydCBhc3luYyBmdW5jdGlvbiBnZXRBbGxUYWJsZVJvd3M8VD4oXG4gICAgZW9zLFxuICAgIGNvZGU6c3RyaW5nLFxuICAgIHNjb3BlOnN0cmluZyxcbiAgICB0YWJsZTpzdHJpbmcsXG4gICAgdGFibGVfa2V5OnN0cmluZyxcbikge1xuICAgIHZhciBhbGxSb3dzOlRbXSA9IFtdO1xuXG4gICAgY29uc3QgbGltaXQgPSAtMTtcbiAgICB2YXIgbG93ZXJfYm91bmQgPSAnMCc7XG5cbiAgICB3aGlsZSAodHJ1ZSkge1xuICAgICAgICBjb25zdCByZXN1bHQgPSBhd2FpdCBnZXRUYWJsZVJvd3NSZXN1bHQ8VD4oXG4gICAgICAgICAgICBlb3MsXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgICAgY29kZSxzY29wZSx0YWJsZSxcbiAgICAgICAgICAgICAgICBsaW1pdCxsb3dlcl9ib3VuZFxuICAgICAgICAgICAgfVxuICAgICAgICApO1xuXG4gICAgICAgIGlmIChcbiAgICAgICAgICAgIHJlc3VsdCA9PSB1bmRlZmluZWQgfHxcbiAgICAgICAgICAgIHJlc3VsdC5yb3dzLmxlbmd0aCA9PSAwXG4gICAgICAgICkge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuXG4gICAgICAgIC8vIGFkZCBuZXcgcm93c1xuICAgICAgICBhbGxSb3dzID0gYWxsUm93cy5jb25jYXQocmVzdWx0LnJvd3MpO1xuXG5cbiAgICAgICAgLy9jb25zdCBpbmRleE9mRmlyc3RSb3cgPSBiaWdJbnQoIHJlc3VsdC5yb3dzWzBdW3RhYmxlX2tleV0gKTtcblxuICAgICAgICBjb25zdCBudW1PZlJvd3MgPSByZXN1bHQucm93cy5sZW5ndGg7XG4gICAgICAgIGNvbnN0IGxhc3RSb3cgPSByZXN1bHQucm93c1tudW1PZlJvd3MtMV07XG5cbiAgICAgICAgY29uc3QgaW5kZXhPZkxhc3RSb3cgPSBiaWdJbnQobGFzdFJvd1t0YWJsZV9rZXldKTtcblxuICAgICAgICBcbiAgICAgICAgLy9jb25zb2xlLmxvZygnSW5kZXggb2YgRmlyc3QgUm93OiAnK2luZGV4T2ZGaXJzdFJvdy50b1N0cmluZygpKVxuICAgICAgICAvL2NvbnNvbGUubG9nKCdJbmRleCBvZiBMYXN0IFJvdzogJytpbmRleE9mTGFzdFJvdy50b1N0cmluZygpKVxuXG5cbiAgICAgICAgaWYgKHJlc3VsdC5tb3JlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIH1cblxuXG4gICAgICAgIGxvd2VyX2JvdW5kID0gaW5kZXhPZkxhc3RSb3cuYWRkKDEpLnRvU3RyaW5nKCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIGFsbFJvd3M7XG59XG5cblxuLypcbmV4cG9ydCBmdW5jdGlvbiBnZXRFb3NCYWxhbmNlKGVvcyxhY2NvdW50TmFtZTpzdHJpbmcpOlByb21pc2U8c3RyaW5nPlxue1xuICAgIHJldHVybiBnZXRUb2tlbkJhbGFuY2UoXG4gICAgICAgIGVvcyxcbiAgICAgICAgJ2Vvc2lvLnRva2VuJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdFT1MnXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEJ0Y0JhbGFuY2UoZW9zLGFjY291bnROYW1lOnN0cmluZyk6UHJvbWlzZTxzdHJpbmc+XG57XG4gICAgcmV0dXJuIGdldFRva2VuQmFsYW5jZShcbiAgICAgICAgZW9zLFxuICAgICAgICAnZW9zYmV0dG9rZW5zJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdCVEMnXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldEJldEJhbGFuY2UoZW9zLGFjY291bnROYW1lOnN0cmluZyk6UHJvbWlzZTxzdHJpbmc+XG57XG4gICAgcmV0dXJuIGdldFRva2VuQmFsYW5jZShcbiAgICAgICAgZW9zLFxuICAgICAgICAnYmV0ZGl2aWRlbmRzJyxcbiAgICAgICAgYWNjb3VudE5hbWUsXG4gICAgICAgICdCRVQnXG4gICAgKTtcbn1cbiovXG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRva2VuQmFsYW5jZShcbiAgICBlb3MsXG4gICAgdG9rZW5Db250cmFjdEFjY291bnQ6c3RyaW5nLFxuICAgIHRva2VuSG9sZGVyQWNjb3VudDpzdHJpbmcsXG4gICAgdG9rZW5TeW1ib2w6c3RyaW5nLFxuICAgIHRhYmxlID0gJ2FjY291bnRzJ1xuKTpQcm9taXNlPHN0cmluZz4ge1xuICAgIGNvbnN0IHJvd3MgPSBhd2FpdCBnZXRUYWJsZVJvd3M8SVRva2VuQmFsYW5jZVJvdz4oXG4gICAgICAgIGVvcyxcbiAgICAgICAge1xuICAgICAgICAgICAgY29kZTogdG9rZW5Db250cmFjdEFjY291bnQsXG4gICAgICAgICAgICBzY29wZTogdG9rZW5Ib2xkZXJBY2NvdW50LFxuICAgICAgICAgICAgdGFibGVcbiAgICAgICAgfVxuICAgICk7XG5cbiAgICBmb3IgKHZhciByb3cgb2Ygcm93cykge1xuICAgICAgICB2YXIgcGFydHMgPSByb3cuYmFsYW5jZS5zcGxpdCgnICcpO1xuXG4gICAgICAgIGlmIChwYXJ0c1sxXSA9PSB0b2tlblN5bWJvbCkge1xuICAgICAgICAgICAgcmV0dXJuIHBhcnRzWzBdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuICcwJztcbn1cblxuXG5pbnRlcmZhY2UgSUdsb2JhbFZhclJvdyB7XG4gICAgaWQ6IG51bWJlcjtcbiAgICB2YWw6IHN0cmluZztcbn1cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldEdsb2JhbFZhcmlhYmxlKFxuICAgIGVvcyxcbiAgICBzbWFydENvbnRyYWN0QWNjb3VudDpzdHJpbmcsXG4gICAgdmFyaWFibGVJZDogbnVtYmVyXG4pIHtcbiAgICBjb25zdCByb3dzID0gYXdhaXQgZ2V0VGFibGVSb3dzPElHbG9iYWxWYXJSb3c+KFxuICAgICAgICBlb3MsXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvZGU6IHNtYXJ0Q29udHJhY3RBY2NvdW50LFxuICAgICAgICAgICAgc2NvcGU6IHNtYXJ0Q29udHJhY3RBY2NvdW50LFxuICAgICAgICAgICAgdGFibGU6ICdnbG9iYWx2YXJzJ1xuICAgICAgICB9XG4gICAgKTtcblxuICAgIFxuICAgIGlmIChyb3dzLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJDb250cmFjdCBub3QgSU5JVFwiKTtcbiAgICB9XG5cblxuICAgIGZvciAodmFyIHJvdyBvZiByb3dzKSB7XG4gICAgICAgIGlmICh2YXJpYWJsZUlkID09IHJvdy5pZCkge1xuICAgICAgICAgICAgcmV0dXJuIHJvdy52YWw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdW5kZWZpbmVkO1xufVxuXG4gXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0QWNjb3VudElkRm9yQWN0aXZlRWFzeUFjY291bnRCZXQoXG4gICAgZW9zLFxuICAgIGVhc3lBY2NvdW50Q29udHJhY3Q6c3RyaW5nLFxuICAgIGJldElkOnN0cmluZ1xuKTpQcm9taXNlPHN0cmluZz4ge1xuICAgIGNvbnN0IHJvd3MgPSBhd2FpdCBnZXRVbmlxdWVUYWJsZVJvdzxJRWFzeUFjY291bnRBY3RpdmVCZXQ+KFxuICAgICAgICBlb3MsXG4gICAgICAgIGVhc3lBY2NvdW50Q29udHJhY3QsZWFzeUFjY291bnRDb250cmFjdCxcbiAgICAgICAgJ2FjdGl2ZWJldHMnLFxuICAgICAgICBiZXRJZFxuICAgICk7XG5cbiAgICByZXR1cm4gcm93cyAmJiByb3dzLmxlbmd0aCA+IDAgP1xuICAgICAgICAgICAgcm93c1swXS5rZXlfaWQgOlxuICAgICAgICAgICAgdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2V0VW5pcXVlVGFibGVSb3c8VD4oXG4gICAgZW9zLFxuICAgIGNvZGU6c3RyaW5nLHNjb3BlOnN0cmluZyx0YWJsZTpzdHJpbmcsXG4gICAgcm93SWQ6c3RyaW5nXG4pIHtcbiAgICByZXR1cm4gZ2V0VGFibGVSb3dzPFQ+KFxuICAgICAgICBlb3MsXG4gICAgICAgIHtcbiAgICAgICAgICAgIGNvZGUsXG4gICAgICAgICAgICBzY29wZSxcbiAgICAgICAgICAgIHRhYmxlLFxuXG4gICAgICAgICAgICBsb3dlcl9ib3VuZDogcm93SWQsXG4gICAgICAgICAgICB1cHBlcl9ib3VuZDogcm93SWRcbiAgICAgICAgfVxuICAgICk7XG59XG5cblxuZXhwb3J0IGFzeW5jIGZ1bmN0aW9uIGdldFRhYmxlUm93czxUPihcbiAgICBlb3MsXG4gICAgcGFyYW1ldGVyczogSVRhYmxlUm93UXVlcnlQYXJhbWV0ZXJzXG4pOiBQcm9taXNlPFRbXT4ge1xuICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGdldFRhYmxlUm93c1Jlc3VsdDxUPihcbiAgICAgICAgZW9zLFxuICAgICAgICBwYXJhbWV0ZXJzXG4gICAgKTtcblxuICAgIHJldHVybiByZXN1bHQgP1xuICAgICAgICAgICAgcmVzdWx0LnJvd3MgOlxuICAgICAgICAgICAgdW5kZWZpbmVkO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUYWJsZVJvd1F1ZXJ5UmVzdWx0PFQ+XG57XG4gICAgcm93czpUW107XG4gICAgbW9yZTpib29sZWFuO1xufVxuXG5leHBvcnQgYXN5bmMgZnVuY3Rpb24gZ2V0VGFibGVSb3dzUmVzdWx0PFQ+KFxuICAgIGVvcyxcbiAgICBwYXJhbWV0ZXJzOklUYWJsZVJvd1F1ZXJ5UGFyYW1ldGVyc1xuKTpQcm9taXNlPElUYWJsZVJvd1F1ZXJ5UmVzdWx0PFQ+PiB7XG4gICAgcGFyYW1ldGVycy5qc29uID0gdHJ1ZTtcblxuICAgIHRyeSB7XG4gICAgICAgIGNvbnN0IHJlc3VsdCA9IGF3YWl0IGVvcy5nZXRUYWJsZVJvd3MocGFyYW1ldGVycyk7XG4gICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgY29uc29sZS5sb2coJ0NvdW5kIG5vdCBnZXQgdGFibGUgcm93cycpO1xuICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XG5cbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcbiAgICB9XG59Il19