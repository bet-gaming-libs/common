/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/int-math.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import Big from 'big.js';
var AssetMath = /** @class */ (function () {
    function AssetMath(precision, coin_name) {
        this._integer = 0;
        this.precision = precision;
        this._coin_name = coin_name;
    }
    /**
     * @param {?} intVal
     * @return {?}
     */
    AssetMath.prototype.initWithInteger = /**
     * @param {?} intVal
     * @return {?}
     */
    function (intVal) {
        Big.RM = 0;
        this._integer = Number(new Big(intVal).toFixed(0));
    };
    /**
     * @param {?} intDec
     * @return {?}
     */
    AssetMath.prototype.initWithDecimal = /**
     * @param {?} intDec
     * @return {?}
     */
    function (intDec) {
        Big.RM = 0;
        this._integer = Number(new Big(intDec).times("1e" + this.precision).toFixed(0));
    };
    /**
     * @param {?} valDecimal
     * @return {?}
     */
    AssetMath.prototype.decToInt = /**
     * @param {?} valDecimal
     * @return {?}
     */
    function (valDecimal) {
        Big.RM = 0;
        return Number(new Big(valDecimal).times("1e" + this.precision).toFixed(0));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    AssetMath.prototype.addInt = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    function (valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).plus(valInteger));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    AssetMath.prototype.addDec = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    function (valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).add((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    AssetMath.prototype.subInt = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    function (valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus(valInteger));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    AssetMath.prototype.subDec = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    function (valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).minus((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    AssetMath.prototype.divInt = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    function (valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div(valInteger));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    AssetMath.prototype.divDec = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    function (valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).div((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    AssetMath.prototype.multInt = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valInteger
     * @return {THIS}
     */
    function (valInteger) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times(valInteger));
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    AssetMath.prototype.multDec = /**
     * @template THIS
     * @this {THIS}
     * @param {?} valDecimal
     * @return {THIS}
     */
    function (valDecimal) {
        Big.RM = 0;
        (/** @type {?} */ (this))._integer = Number(new Big((/** @type {?} */ (this))._integer).times((/** @type {?} */ (this)).decToInt(valDecimal)));
        return (/** @type {?} */ (this));
    };
    Object.defineProperty(AssetMath.prototype, "assetString", {
        get: /**
         * @return {?}
         */
        function () {
            return this.toDecimalString() + " " + this.coinName;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetMath.prototype, "decimal", {
        get: /**
         * @return {?}
         */
        function () {
            return this.toDecimalString();
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    AssetMath.prototype.toDecimalString = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var decimal = new Big(this._integer).times("1e-" + this.precision);
        return decimal.toFixed(this.precision);
    };
    Object.defineProperty(AssetMath.prototype, "integer", {
        get: /**
         * @return {?}
         */
        function () {
            return this._integer;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AssetMath.prototype, "coinName", {
        get: /**
         * @return {?}
         */
        function () {
            return this._coin_name;
        },
        enumerable: true,
        configurable: true
    });
    return AssetMath;
}());
export { AssetMath };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._integer;
    /** @type {?} */
    AssetMath.prototype.precision;
    /**
     * @type {?}
     * @private
     */
    AssetMath.prototype._coin_name;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50LW1hdGguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvaW50LW1hdGgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEdBQUcsTUFBTSxRQUFRLENBQUM7QUFFekI7SUFLSSxtQkFBWSxTQUFpQixFQUFFLFNBQWlCO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsbUNBQWU7Ozs7SUFBZixVQUFnQixNQUF1QjtRQUNuQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7O0lBQ0QsbUNBQWU7Ozs7SUFBZixVQUFnQixNQUF1QjtRQUNuQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFLLElBQUksQ0FBQyxTQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7OztJQUVELDRCQUFROzs7O0lBQVIsVUFBUyxVQUEyQjtRQUNoQyxHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNYLE9BQU8sTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFLLElBQUksQ0FBQyxTQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMvRSxDQUFDOzs7Ozs7O0lBRUQsMEJBQU07Ozs7OztJQUFOLFVBQU8sVUFBMkI7UUFDOUIsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUNELDBCQUFNOzs7Ozs7SUFBTixVQUFPLFVBQTJCO1FBQzlCLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM5RSxPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFFRCwwQkFBTTs7Ozs7O0lBQU4sVUFBTyxVQUEyQjtRQUM5QixHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVYLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDakUsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBQ0QsMEJBQU07Ozs7OztJQUFOLFVBQU8sVUFBMkI7UUFDOUIsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2hGLE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUVELDBCQUFNOzs7Ozs7SUFBTixVQUFPLFVBQTJCO1FBQzlCLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUMvRCxPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFDRCwwQkFBTTs7Ozs7O0lBQU4sVUFBTyxVQUEyQjtRQUM5QixHQUFHLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUVYLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsSUFBSSxHQUFHLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUUsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBRUQsMkJBQU87Ozs7OztJQUFQLFVBQVEsVUFBMkI7UUFDL0IsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFFWCxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLElBQUksR0FBRyxDQUFDLG1CQUFBLElBQUksRUFBQSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLE9BQU8sbUJBQUEsSUFBSSxFQUFBLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUVELDJCQUFPOzs7Ozs7SUFBUCxVQUFRLFVBQTJCO1FBQy9CLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBRVgsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLEdBQUcsQ0FBQyxtQkFBQSxJQUFJLEVBQUEsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsbUJBQUEsSUFBSSxFQUFBLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNoRixPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxzQkFBSSxrQ0FBVzs7OztRQUFmO1lBQ0ksT0FBVSxJQUFJLENBQUMsZUFBZSxFQUFFLFNBQUksSUFBSSxDQUFDLFFBQVUsQ0FBQTtRQUN2RCxDQUFDOzs7T0FBQTtJQUNELHNCQUFJLDhCQUFPOzs7O1FBQVg7WUFDSSxPQUFPLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNsQyxDQUFDOzs7T0FBQTs7OztJQUVELG1DQUFlOzs7SUFBZjs7WUFDUSxPQUFPLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFNLElBQUksQ0FBQyxTQUFXLENBQUM7UUFDbEUsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQsc0JBQUksOEJBQU87Ozs7UUFBWDtZQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUN6QixDQUFDOzs7T0FBQTtJQUNELHNCQUFJLCtCQUFROzs7O1FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDM0IsQ0FBQzs7O09BQUE7SUFDTCxnQkFBQztBQUFELENBQUMsQUFoR0QsSUFnR0M7Ozs7Ozs7SUEvRkcsNkJBQXlCOztJQUN6Qiw4QkFBMkI7Ozs7O0lBQzNCLCtCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBCaWcgZnJvbSAnYmlnLmpzJztcblxuZXhwb3J0IGNsYXNzIEFzc2V0TWF0aCB7XG4gICAgcHJpdmF0ZSBfaW50ZWdlcjogbnVtYmVyO1xuICAgIHJlYWRvbmx5IHByZWNpc2lvbjogbnVtYmVyO1xuICAgIHByaXZhdGUgX2NvaW5fbmFtZTogc3RyaW5nO1xuXG4gICAgY29uc3RydWN0b3IocHJlY2lzaW9uOiBudW1iZXIsIGNvaW5fbmFtZTogc3RyaW5nKSB7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSAwO1xuICAgICAgICB0aGlzLnByZWNpc2lvbiA9IHByZWNpc2lvbjtcbiAgICAgICAgdGhpcy5fY29pbl9uYW1lID0gY29pbl9uYW1lO1xuICAgIH1cblxuICAgIGluaXRXaXRoSW50ZWdlcihpbnRWYWw6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuICAgICAgICB0aGlzLl9pbnRlZ2VyID0gTnVtYmVyKG5ldyBCaWcoaW50VmFsKS50b0ZpeGVkKDApKTtcbiAgICB9XG4gICAgaW5pdFdpdGhEZWNpbWFsKGludERlYzogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyhpbnREZWMpLnRpbWVzKGAxZSR7dGhpcy5wcmVjaXNpb259YCkudG9GaXhlZCgwKSk7XG4gICAgfVxuXG4gICAgZGVjVG9JbnQodmFsRGVjaW1hbDogc3RyaW5nIHwgbnVtYmVyKTogbnVtYmVyIHtcbiAgICAgICAgQmlnLlJNID0gMDtcbiAgICAgICAgcmV0dXJuIE51bWJlcihuZXcgQmlnKHZhbERlY2ltYWwpLnRpbWVzKGAxZSR7dGhpcy5wcmVjaXNpb259YCkudG9GaXhlZCgwKSk7XG4gICAgfVxuXG4gICAgYWRkSW50KHZhbEludGVnZXI6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS5wbHVzKHZhbEludGVnZXIpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuICAgIGFkZERlYyh2YWxEZWNpbWFsOiBzdHJpbmcgfCBudW1iZXIpIHtcbiAgICAgICAgQmlnLlJNID0gMDtcblxuICAgICAgICB0aGlzLl9pbnRlZ2VyID0gTnVtYmVyKG5ldyBCaWcodGhpcy5faW50ZWdlcikuYWRkKHRoaXMuZGVjVG9JbnQodmFsRGVjaW1hbCkpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgc3ViSW50KHZhbEludGVnZXI6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS5taW51cyh2YWxJbnRlZ2VyKSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgICBzdWJEZWModmFsRGVjaW1hbDogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLm1pbnVzKHRoaXMuZGVjVG9JbnQodmFsRGVjaW1hbCkpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgZGl2SW50KHZhbEludGVnZXI6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS5kaXYodmFsSW50ZWdlcikpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG4gICAgZGl2RGVjKHZhbERlY2ltYWw6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS5kaXYodGhpcy5kZWNUb0ludCh2YWxEZWNpbWFsKSkpO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBtdWx0SW50KHZhbEludGVnZXI6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBCaWcuUk0gPSAwO1xuXG4gICAgICAgIHRoaXMuX2ludGVnZXIgPSBOdW1iZXIobmV3IEJpZyh0aGlzLl9pbnRlZ2VyKS50aW1lcyh2YWxJbnRlZ2VyKSk7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cblxuICAgIG11bHREZWModmFsRGVjaW1hbDogc3RyaW5nIHwgbnVtYmVyKSB7XG4gICAgICAgIEJpZy5STSA9IDA7XG5cbiAgICAgICAgdGhpcy5faW50ZWdlciA9IE51bWJlcihuZXcgQmlnKHRoaXMuX2ludGVnZXIpLnRpbWVzKHRoaXMuZGVjVG9JbnQodmFsRGVjaW1hbCkpKTtcbiAgICAgICAgcmV0dXJuIHRoaXM7XG4gICAgfVxuXG4gICAgZ2V0IGFzc2V0U3RyaW5nKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiBgJHt0aGlzLnRvRGVjaW1hbFN0cmluZygpfSAke3RoaXMuY29pbk5hbWV9YFxuICAgIH1cbiAgICBnZXQgZGVjaW1hbCgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy50b0RlY2ltYWxTdHJpbmcoKTtcbiAgICB9XG5cbiAgICB0b0RlY2ltYWxTdHJpbmcoKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IGRlY2ltYWwgPSBuZXcgQmlnKHRoaXMuX2ludGVnZXIpLnRpbWVzKGAxZS0ke3RoaXMucHJlY2lzaW9ufWApO1xuICAgICAgICByZXR1cm4gZGVjaW1hbC50b0ZpeGVkKHRoaXMucHJlY2lzaW9uKTtcbiAgICB9XG5cbiAgICBnZXQgaW50ZWdlcigpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5faW50ZWdlcjtcbiAgICB9XG4gICAgZ2V0IGNvaW5OYW1lKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLl9jb2luX25hbWU7XG4gICAgfVxufVxuXG5cbiJdfQ==