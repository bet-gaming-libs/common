/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-data.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IEosConfigParameters() { }
if (false) {
    /** @type {?} */
    IEosConfigParameters.prototype.httpEndpoint;
    /** @type {?} */
    IEosConfigParameters.prototype.chainId;
    /** @type {?} */
    IEosConfigParameters.prototype.keyProvider;
    /** @type {?} */
    IEosConfigParameters.prototype.verbose;
}
/**
 * @record
 */
export function ITableRowQueryParameters() { }
if (false) {
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.json;
    /** @type {?} */
    ITableRowQueryParameters.prototype.code;
    /** @type {?} */
    ITableRowQueryParameters.prototype.scope;
    /** @type {?} */
    ITableRowQueryParameters.prototype.table;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.limit;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.table_key;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.key_type;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.index_position;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.lower_bound;
    /** @type {?|undefined} */
    ITableRowQueryParameters.prototype.upper_bound;
}
/**
 * @record
 */
export function IGlobalVariableRow() { }
if (false) {
    /** @type {?} */
    IGlobalVariableRow.prototype.id;
    /** @type {?} */
    IGlobalVariableRow.prototype.val;
}
/**
 * @record
 */
export function ITokenBalanceRow() { }
if (false) {
    /** @type {?} */
    ITokenBalanceRow.prototype.balance;
}
/**
 * @record
 */
export function ITransactionResult() { }
if (false) {
    /** @type {?} */
    ITransactionResult.prototype.transaction_id;
}
/**
 * @record
 */
export function IEasyAccountCurrencyBalanceRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountCurrencyBalanceRow.prototype.quantity;
    /** @type {?} */
    IEasyAccountCurrencyBalanceRow.prototype.amt_wagered;
}
/**
 * @record
 */
export function IEasyAccountBetBalanceRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.id;
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.quantity;
    /** @type {?} */
    IEasyAccountBetBalanceRow.prototype.claimed_divs;
}
/**
 * @record
 */
export function IEasyAccountInfoRow() { }
if (false) {
    /** @type {?} */
    IEasyAccountInfoRow.prototype.id;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.key;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.nonce;
    /** @type {?} */
    IEasyAccountInfoRow.prototype.email_backup;
}
/**
 * @record
 */
export function IEasyAccountActiveBet() { }
if (false) {
    /** @type {?} */
    IEasyAccountActiveBet.prototype.key_id;
}
/**
 * @record
 */
export function IEosActionData() { }
if (false) {
    /** @type {?} */
    IEosActionData.prototype.account;
    /** @type {?} */
    IEosActionData.prototype.name;
    /** @type {?} */
    IEosActionData.prototype.data;
}
/**
 * @record
 */
export function IEosActionAuthorization() { }
if (false) {
    /** @type {?} */
    IEosActionAuthorization.prototype.actor;
    /** @type {?} */
    IEosActionAuthorization.prototype.permission;
}
/**
 * @record
 */
export function IEosTransactionAction() { }
if (false) {
    /** @type {?} */
    IEosTransactionAction.prototype.authorization;
}
/**
 * @record
 */
export function ISignatureInfo() { }
if (false) {
    /** @type {?} */
    ISignatureInfo.prototype.nonce;
    /** @type {?} */
    ISignatureInfo.prototype.signature;
    /** @type {?} */
    ISignatureInfo.prototype.isSignedDataHashed;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLWRhdGEuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLWRhdGEudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSwwQ0FNQzs7O0lBSkcsNENBQW9COztJQUNwQix1Q0FBZTs7SUFDZiwyQ0FBbUI7O0lBQ25CLHVDQUFnQjs7Ozs7QUFJcEIsOENBY0M7OztJQVpHLHdDQUFlOztJQUVmLHdDQUFhOztJQUNiLHlDQUFjOztJQUNkLHlDQUFjOztJQUVkLHlDQUFjOztJQUNkLDZDQUFrQjs7SUFDbEIsNENBQWlCOztJQUNqQixrREFBdUI7O0lBQ3ZCLCtDQUEyQjs7SUFDM0IsK0NBQTJCOzs7OztBQUcvQix3Q0FJQzs7O0lBRkcsZ0NBQVU7O0lBQ1YsaUNBQVc7Ozs7O0FBR2Ysc0NBR0M7OztJQURHLG1DQUFlOzs7OztBQUduQix3Q0FHQzs7O0lBREcsNENBQXNCOzs7OztBQUcxQixvREFJQzs7O0lBRkcsa0RBQWdCOztJQUNoQixxREFBbUI7Ozs7O0FBR3ZCLCtDQUlDOzs7SUFIRyx1Q0FBcUI7O0lBQ3JCLDZDQUFxQjs7SUFDckIsaURBQXVCOzs7OztBQUczQix5Q0FNQzs7O0lBSkcsaUNBQVU7O0lBQ1Ysa0NBQVc7O0lBQ1gsb0NBQWE7O0lBQ2IsMkNBQW9COzs7OztBQUd4QiwyQ0FHQzs7O0lBREcsdUNBQWM7Ozs7O0FBR2xCLG9DQUtDOzs7SUFIRyxpQ0FBZTs7SUFDZiw4QkFBWTs7SUFDWiw4QkFBUzs7Ozs7QUFHYiw2Q0FJQzs7O0lBRkcsd0NBQWE7O0lBQ2IsNkNBQWtCOzs7OztBQUd0QiwyQ0FHQzs7O0lBREcsOENBQXdDOzs7OztBQUc1QyxvQ0FLQzs7O0lBSEcsK0JBQWE7O0lBQ2IsbUNBQWlCOztJQUNqQiw0Q0FBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFb3NDb25maWdQYXJhbWV0ZXJzXG57XG4gICAgaHR0cEVuZHBvaW50OnN0cmluZztcbiAgICBjaGFpbklkOnN0cmluZztcbiAgICBrZXlQcm92aWRlcjpzdHJpbmc7XG4gICAgdmVyYm9zZTpib29sZWFuO1xufVxuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVRhYmxlUm93UXVlcnlQYXJhbWV0ZXJzXG57XG4gICAganNvbj86IGJvb2xlYW47XG5cbiAgICBjb2RlOiBzdHJpbmc7XG4gICAgc2NvcGU6IHN0cmluZztcbiAgICB0YWJsZTogc3RyaW5nO1xuICAgIFxuICAgIGxpbWl0PzpudW1iZXI7XG4gICAgdGFibGVfa2V5PzpzdHJpbmc7XG4gICAga2V5X3R5cGU/OnN0cmluZztcbiAgICBpbmRleF9wb3NpdGlvbj86c3RyaW5nO1xuICAgIGxvd2VyX2JvdW5kPzpudW1iZXJ8c3RyaW5nO1xuICAgIHVwcGVyX2JvdW5kPzpudW1iZXJ8c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElHbG9iYWxWYXJpYWJsZVJvd1xue1xuICAgIGlkOm51bWJlcjtcbiAgICB2YWw6bnVtYmVyO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElUb2tlbkJhbGFuY2VSb3dcbntcbiAgICBiYWxhbmNlOnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJVHJhbnNhY3Rpb25SZXN1bHRcbntcbiAgICB0cmFuc2FjdGlvbl9pZDpzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVhc3lBY2NvdW50Q3VycmVuY3lCYWxhbmNlUm93XG57XG4gICAgcXVhbnRpdHk6c3RyaW5nO1xuICAgIGFtdF93YWdlcmVkOnN0cmluZztcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJRWFzeUFjY291bnRCZXRCYWxhbmNlUm93IHtcbiAgICBpZDogICAgICAgICAgIHN0cmluZztcbiAgICBxdWFudGl0eTogICAgIHN0cmluZztcbiAgICBjbGFpbWVkX2RpdnM6IHN0cmluZ1tdO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElFYXN5QWNjb3VudEluZm9Sb3dcbntcbiAgICBpZDpzdHJpbmc7XG4gICAga2V5OnN0cmluZztcbiAgICBub25jZTpudW1iZXI7XG4gICAgZW1haWxfYmFja3VwOm51bWJlcjtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJRWFzeUFjY291bnRBY3RpdmVCZXRcbntcbiAgICBrZXlfaWQ6c3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIElFb3NBY3Rpb25EYXRhXG57XG4gICAgYWNjb3VudDpzdHJpbmc7XG4gICAgbmFtZTpzdHJpbmc7XG4gICAgZGF0YTphbnk7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVvc0FjdGlvbkF1dGhvcml6YXRpb25cbntcbiAgICBhY3RvcjpzdHJpbmc7XG4gICAgcGVybWlzc2lvbjpzdHJpbmc7XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVvc1RyYW5zYWN0aW9uQWN0aW9uIGV4dGVuZHMgSUVvc0FjdGlvbkRhdGFcbntcbiAgICBhdXRob3JpemF0aW9uOklFb3NBY3Rpb25BdXRob3JpemF0aW9uW107XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgSVNpZ25hdHVyZUluZm9cbntcbiAgICBub25jZTpzdHJpbmc7XG4gICAgc2lnbmF0dXJlOnN0cmluZztcbiAgICBpc1NpZ25lZERhdGFIYXNoZWQ6Ym9vbGVhbjtcbn0iXX0=