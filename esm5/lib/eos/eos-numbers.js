/**
 * @fileoverview added by tsickle
 * Generated from: lib/eos/eos-numbers.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { AssetMath } from './int-math';
/**
 * @record
 */
export function IEOSAssetAmount() { }
if (false) {
    /** @type {?} */
    IEOSAssetAmount.prototype.symbol;
    /** @type {?} */
    IEOSAssetAmount.prototype.contract;
    /** @type {?} */
    IEOSAssetAmount.prototype.integer;
    /** @type {?} */
    IEOSAssetAmount.prototype.quantity;
}
var EOSAssetAmount = /** @class */ (function (_super) {
    tslib_1.__extends(EOSAssetAmount, _super);
    function EOSAssetAmount(precision, symbol, contract, decimalValue) {
        if (decimalValue === void 0) { decimalValue = '0'; }
        var _this = _super.call(this, precision, symbol) || this;
        _this.symbol = symbol;
        _this.contract = contract;
        _this.initWithDecimal(decimalValue);
        return _this;
    }
    Object.defineProperty(EOSAssetAmount.prototype, "quantity", {
        get: /**
         * @return {?}
         */
        function () {
            return this.assetString;
        },
        enumerable: true,
        configurable: true
    });
    return EOSAssetAmount;
}(AssetMath));
export { EOSAssetAmount };
if (false) {
    /** @type {?} */
    EOSAssetAmount.prototype.symbol;
    /** @type {?} */
    EOSAssetAmount.prototype.contract;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW9zLW51bWJlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9lb3MvZW9zLW51bWJlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLFlBQVksQ0FBQzs7OztBQUd2QyxxQ0FNQzs7O0lBSkcsaUNBQWM7O0lBQ2QsbUNBQWdCOztJQUNoQixrQ0FBZTs7SUFDZixtQ0FBZ0I7O0FBSXBCO0lBQW9DLDBDQUFTO0lBRXpDLHdCQUNJLFNBQWdCLEVBQ1AsTUFBYSxFQUNiLFFBQWUsRUFDeEIsWUFBeUI7UUFBekIsNkJBQUEsRUFBQSxrQkFBeUI7UUFKN0IsWUFNSSxrQkFBTSxTQUFTLEVBQUMsTUFBTSxDQUFDLFNBSTFCO1FBUlksWUFBTSxHQUFOLE1BQU0sQ0FBTztRQUNiLGNBQVEsR0FBUixRQUFRLENBQU87UUFNeEIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQzs7SUFDdkMsQ0FBQztJQUVELHNCQUFJLG9DQUFROzs7O1FBQVo7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDNUIsQ0FBQzs7O09BQUE7SUFDTCxxQkFBQztBQUFELENBQUMsQUFqQkQsQ0FBb0MsU0FBUyxHQWlCNUM7Ozs7SUFiTyxnQ0FBc0I7O0lBQ3RCLGtDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFzc2V0TWF0aCB9IGZyb20gJy4vaW50LW1hdGgnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSUVPU0Fzc2V0QW1vdW50XG57XG4gICAgc3ltYm9sOnN0cmluZztcbiAgICBjb250cmFjdDpzdHJpbmc7XG4gICAgaW50ZWdlcjpudW1iZXI7XG4gICAgcXVhbnRpdHk6c3RyaW5nO1xufVxuXG5cbmV4cG9ydCBjbGFzcyBFT1NBc3NldEFtb3VudCBleHRlbmRzIEFzc2V0TWF0aFxue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcmVjaXNpb246bnVtYmVyLFxuICAgICAgICByZWFkb25seSBzeW1ib2w6c3RyaW5nLFxuICAgICAgICByZWFkb25seSBjb250cmFjdDpzdHJpbmcsXG4gICAgICAgIGRlY2ltYWxWYWx1ZTpzdHJpbmcgPSAnMCdcbiAgICApIHtcbiAgICAgICAgc3VwZXIocHJlY2lzaW9uLHN5bWJvbCk7XG5cblxuICAgICAgICB0aGlzLmluaXRXaXRoRGVjaW1hbChkZWNpbWFsVmFsdWUpO1xuICAgIH1cblxuICAgIGdldCBxdWFudGl0eSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYXNzZXRTdHJpbmc7XG4gICAgfVxufVxuXG4vKlxuZXhwb3J0IGNsYXNzIEVvc0Ftb3VudCBleHRlbmRzIEVPU0Fzc2V0QW1vdW50XG57XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFBSRUNJU0lPTl9FT1MsXCJFT1NcIixFT1NfVE9LRU5fQ09OVFJBQ1QpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEJ0Y0Ftb3VudCBleHRlbmRzIEVPU0Fzc2V0QW1vdW50XG57XG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKFBSRUNJU0lPTl9CVEMsXCJCVENcIixFT1NCRVRfVE9LRU5TX0NPTlRSQUNUKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIEVvc0ludGVnZXIgZXh0ZW5kcyBFb3NBbW91bnQge1xuICAgIGNvbnN0cnVjdG9yKGludGVnZXI6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKCk7XG4gICAgICAgIHRoaXMuaW5pdFdpdGhJbnRlZ2VyKGludGVnZXIpO1xuICAgIH1cbn1cbmV4cG9ydCBjbGFzcyBFb3NEZWNpbWFsIGV4dGVuZHMgRW9zQW1vdW50IHtcbiAgICBjb25zdHJ1Y3RvcihkZWNpbWFsOiBzdHJpbmcgfCBudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgdGhpcy5pbml0V2l0aERlY2ltYWwoZGVjaW1hbCk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgQnRjSW50ZWdlciBleHRlbmRzIEJ0Y0Ftb3VudCB7XG4gICAgY29uc3RydWN0b3IoaW50ZWdlcjpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICAgICAgdGhpcy5pbml0V2l0aEludGVnZXIoaW50ZWdlcik7XG4gICAgfVxufVxuZXhwb3J0IGNsYXNzIEJ0Y0RlY2ltYWwgZXh0ZW5kcyBCdGNBbW91bnQge1xuICAgIGNvbnN0cnVjdG9yKGRlY2ltYWw6IHN0cmluZyB8IG51bWJlcikge1xuICAgICAgICBzdXBlcigpO1xuICAgICAgICB0aGlzLmluaXRXaXRoRGVjaW1hbChkZWNpbWFsKTtcbiAgICB9XG59XG4qLyJdfQ==