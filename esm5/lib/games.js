/**
 * @fileoverview added by tsickle
 * Generated from: lib/games.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var GameId = {
    DICE: 1,
    CRASH: 2,
    BACCARAT: 3,
    HILO: 4,
    BLACKJACK: 5,
};
export { GameId };
GameId[GameId.DICE] = 'DICE';
GameId[GameId.CRASH] = 'CRASH';
GameId[GameId.BACCARAT] = 'BACCARAT';
GameId[GameId.HILO] = 'HILO';
GameId[GameId.BLACKJACK] = 'BLACKJACK';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2FtZXMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9nYW1lcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxJQUFZLE1BQU07SUFFZCxJQUFJLEdBQU87SUFDWCxLQUFLLEdBQU07SUFDWCxRQUFRLEdBQUc7SUFDWCxJQUFJLEdBQU87SUFDWCxTQUFTLEdBQUU7RUFDZCIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBlbnVtIEdhbWVJZFxue1xuICAgIERJQ0UgICAgPSAxLFxuICAgIENSQVNIICAgPSAyLFxuICAgIEJBQ0NBUkFUPSAzLFxuICAgIEhJTE8gICAgPSA0LFxuICAgIEJMQUNLSkFDSz01XG59Il19