/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/time-frame.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var TimeFrame = /** @class */ (function () {
    function TimeFrame(start, duration) {
        this.start = start;
        this.end = new Date(start.getTime() + duration.milliseconds);
    }
    /**
     * @return {?}
     */
    TimeFrame.prototype.toString = /**
     * @return {?}
     */
    function () {
        return this.start.toString() + ' TO ' +
            this.end.toString();
    };
    return TimeFrame;
}());
export { TimeFrame };
if (false) {
    /** @type {?} */
    TimeFrame.prototype.end;
    /** @type {?} */
    TimeFrame.prototype.start;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1mcmFtZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL2RhdGUtdGltZS90aW1lLWZyYW1lLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBR0E7SUFJSSxtQkFDYSxLQUFVLEVBQ25CLFFBQXNCO1FBRGIsVUFBSyxHQUFMLEtBQUssQ0FBSztRQUduQixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksSUFBSSxDQUNmLEtBQUssQ0FBQyxPQUFPLEVBQUUsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUMxQyxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELDRCQUFROzs7SUFBUjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsR0FBRyxNQUFNO1lBQzdCLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQWpCRCxJQWlCQzs7OztJQWZHLHdCQUFrQjs7SUFHZCwwQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lUaW1lRHVyYXRpb259IGZyb20gJy4vaW50ZXJmYWNlcyc7XG5cblxuZXhwb3J0IGNsYXNzIFRpbWVGcmFtZVxue1xuICAgIHJlYWRvbmx5IGVuZDpEYXRlO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHJlYWRvbmx5IHN0YXJ0OkRhdGUsXG4gICAgICAgIGR1cmF0aW9uOklUaW1lRHVyYXRpb25cbiAgICApIHtcbiAgICAgICAgdGhpcy5lbmQgPSBuZXcgRGF0ZShcbiAgICAgICAgICAgIHN0YXJ0LmdldFRpbWUoKSArIGR1cmF0aW9uLm1pbGxpc2Vjb25kc1xuICAgICAgICApO1xuICAgIH1cblxuICAgIHRvU3RyaW5nKCk6c3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RhcnQudG9TdHJpbmcoKSArICcgVE8gJyArXG4gICAgICAgICAgICAgICAgdGhpcy5lbmQudG9TdHJpbmcoKTtcbiAgICB9XG59Il19