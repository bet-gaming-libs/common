/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/time-durations.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/** @type {?} */
var SECOND_IN_MS = 1000;
/** @type {?} */
var MINUTE_IN_MS = 60000;
/** @type {?} */
var DAY_IN_MS = 86400000;
var Milliseconds = /** @class */ (function () {
    function Milliseconds(milliseconds) {
        this.milliseconds = milliseconds;
    }
    /**
     * @param {?} other
     * @return {?}
     */
    Milliseconds.prototype.add = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return new Milliseconds(this.milliseconds +
            other.milliseconds);
    };
    /**
     * @param {?} other
     * @return {?}
     */
    Milliseconds.prototype.subtract = /**
     * @param {?} other
     * @return {?}
     */
    function (other) {
        return new Milliseconds(this.milliseconds -
            other.milliseconds);
    };
    return Milliseconds;
}());
export { Milliseconds };
if (false) {
    /** @type {?} */
    Milliseconds.prototype.milliseconds;
}
var Seconds = /** @class */ (function (_super) {
    tslib_1.__extends(Seconds, _super);
    function Seconds(seconds) {
        return _super.call(this, seconds * SECOND_IN_MS) || this;
    }
    return Seconds;
}(Milliseconds));
export { Seconds };
var Minutes = /** @class */ (function (_super) {
    tslib_1.__extends(Minutes, _super);
    function Minutes(minutes) {
        return _super.call(this, minutes * MINUTE_IN_MS) || this;
    }
    return Minutes;
}(Milliseconds));
export { Minutes };
var Hours = /** @class */ (function (_super) {
    tslib_1.__extends(Hours, _super);
    function Hours(hours) {
        return _super.call(this, hours * 60) || this;
    }
    return Hours;
}(Minutes));
export { Hours };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1kdXJhdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9kYXRlLXRpbWUvdGltZS1kdXJhdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztJQUlNLFlBQVksR0FBRyxJQUFJOztJQUNuQixZQUFZLEdBQUcsS0FBSzs7SUFDcEIsU0FBUyxHQUFHLFFBQVE7QUFJMUI7SUFFSSxzQkFDYSxZQUFtQjtRQUFuQixpQkFBWSxHQUFaLFlBQVksQ0FBTztJQUVoQyxDQUFDOzs7OztJQUVELDBCQUFHOzs7O0lBQUgsVUFBSSxLQUFtQjtRQUNuQixPQUFPLElBQUksWUFBWSxDQUNuQixJQUFJLENBQUMsWUFBWTtZQUNqQixLQUFLLENBQUMsWUFBWSxDQUNyQixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCwrQkFBUTs7OztJQUFSLFVBQVMsS0FBbUI7UUFDeEIsT0FBTyxJQUFJLFlBQVksQ0FDbkIsSUFBSSxDQUFDLFlBQVk7WUFDakIsS0FBSyxDQUFDLFlBQVksQ0FDckIsQ0FBQztJQUNOLENBQUM7SUFDTCxtQkFBQztBQUFELENBQUMsQUFwQkQsSUFvQkM7Ozs7SUFqQk8sb0NBQTRCOztBQW1CcEM7SUFBNkIsbUNBQVk7SUFFckMsaUJBQVksT0FBYztlQUN0QixrQkFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDO0lBQ2pDLENBQUM7SUFDTCxjQUFDO0FBQUQsQ0FBQyxBQUxELENBQTZCLFlBQVksR0FLeEM7O0FBRUQ7SUFBNkIsbUNBQVk7SUFFckMsaUJBQVksT0FBYztlQUN0QixrQkFBTSxPQUFPLEdBQUcsWUFBWSxDQUFDO0lBQ2pDLENBQUM7SUFDTCxjQUFDO0FBQUQsQ0FBQyxBQUxELENBQTZCLFlBQVksR0FLeEM7O0FBRUQ7SUFBMkIsaUNBQU87SUFFOUIsZUFBWSxLQUFZO2VBQ3BCLGtCQUFNLEtBQUssR0FBQyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUNMLFlBQUM7QUFBRCxDQUFDLEFBTEQsQ0FBMkIsT0FBTyxHQUtqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7SVRpbWVEdXJhdGlvbn0gZnJvbSAnLi9pbnRlcmZhY2VzJztcblxuXG5cbmNvbnN0IFNFQ09ORF9JTl9NUyA9IDEwMDA7XG5jb25zdCBNSU5VVEVfSU5fTVMgPSA2MDAwMDtcbmNvbnN0IERBWV9JTl9NUyA9IDg2NDAwMDAwO1xuXG5cblxuZXhwb3J0IGNsYXNzIE1pbGxpc2Vjb25kc1xue1xuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICByZWFkb25seSBtaWxsaXNlY29uZHM6bnVtYmVyXG4gICAgKSB7XG4gICAgfVxuXG4gICAgYWRkKG90aGVyOklUaW1lRHVyYXRpb24pOklUaW1lRHVyYXRpb24ge1xuICAgICAgICByZXR1cm4gbmV3IE1pbGxpc2Vjb25kcyhcbiAgICAgICAgICAgIHRoaXMubWlsbGlzZWNvbmRzICtcbiAgICAgICAgICAgIG90aGVyLm1pbGxpc2Vjb25kc1xuICAgICAgICApO1xuICAgIH1cbiAgICBcbiAgICBzdWJ0cmFjdChvdGhlcjpJVGltZUR1cmF0aW9uKTpJVGltZUR1cmF0aW9uIHtcbiAgICAgICAgcmV0dXJuIG5ldyBNaWxsaXNlY29uZHMoXG4gICAgICAgICAgICB0aGlzLm1pbGxpc2Vjb25kcyAtXG4gICAgICAgICAgICBvdGhlci5taWxsaXNlY29uZHNcbiAgICAgICAgKTtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTZWNvbmRzIGV4dGVuZHMgTWlsbGlzZWNvbmRzIGltcGxlbWVudHMgSVRpbWVEdXJhdGlvblxue1xuICAgIGNvbnN0cnVjdG9yKHNlY29uZHM6bnVtYmVyKSB7XG4gICAgICAgIHN1cGVyKHNlY29uZHMgKiBTRUNPTkRfSU5fTVMpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIE1pbnV0ZXMgZXh0ZW5kcyBNaWxsaXNlY29uZHMgaW1wbGVtZW50cyBJVGltZUR1cmF0aW9uXG57XG4gICAgY29uc3RydWN0b3IobWludXRlczpudW1iZXIpIHtcbiAgICAgICAgc3VwZXIobWludXRlcyAqIE1JTlVURV9JTl9NUyk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgSG91cnMgZXh0ZW5kcyBNaW51dGVzIGltcGxlbWVudHMgSVRpbWVEdXJhdGlvblxue1xuICAgIGNvbnN0cnVjdG9yKGhvdXJzOm51bWJlcikge1xuICAgICAgICBzdXBlcihob3Vycyo2MCk7XG4gICAgfVxufSJdfQ==