/**
 * @fileoverview added by tsickle
 * Generated from: lib/date-time/index.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var months = [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];
/**
 * @return {?}
 */
export function NOW() {
    return toDateTimeFormat(new Date());
}
/**
 * @param {?} date
 * @return {?}
 */
export function toDateTimeFormat(date) {
    /** @type {?} */
    var parts = date.toString().split(' ');
    /** @type {?} */
    var monthName = parts[1];
    /** @type {?} */
    var monthIndex = months.indexOf(monthName);
    if (monthIndex < 0) {
        throw new Error('Month NOT Found: ' + monthName);
    }
    /** @type {?} */
    var monthNum = monthIndex + 1;
    /** @type {?} */
    var month = (monthNum < 10 ?
        '0' :
        '') + monthNum;
    /** @type {?} */
    var day = parts[2];
    /** @type {?} */
    var year = parts[3];
    /** @type {?} */
    var time = parts[4];
    return year + '-' + month + '-' + day + ' ' + time;
}
/**
 * @param {?} start
 * @param {?} end
 * @return {?}
 */
export function getRandomDateWithinRange(start, end) {
    /** @type {?} */
    var startTime = start.getTime();
    /** @type {?} */
    var duration = end.getTime() - startTime;
    return new Date(startTime +
        (Math.random() * duration));
}
/**
 * @return {?}
 */
export function getStartOfToday() {
    /** @type {?} */
    var startOfDate = new Date();
    startOfDate.setHours(0, 0, 0, 0);
    return startOfDate;
}
/**
 * @return {?}
 */
export function getEndOfToday() {
    /** @type {?} */
    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);
    return endOfDay;
}
/** @enum {number} */
var Month = {
    JAN: 0,
    FEB: 1,
    MAR: 2,
    APR: 3,
    MAY: 4,
    JUN: 5,
    JUL: 6,
    AUG: 7,
    SEP: 8,
    OCT: 9,
    NOV: 10,
    DEC: 11,
};
export { Month };
Month[Month.JAN] = 'JAN';
Month[Month.FEB] = 'FEB';
Month[Month.MAR] = 'MAR';
Month[Month.APR] = 'APR';
Month[Month.MAY] = 'MAY';
Month[Month.JUN] = 'JUN';
Month[Month.JUL] = 'JUL';
Month[Month.AUG] = 'AUG';
Month[Month.SEP] = 'SEP';
Month[Month.OCT] = 'OCT';
Month[Month.NOV] = 'NOV';
Month[Month.DEC] = 'DEC';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi9kYXRlLXRpbWUvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0lBQU0sTUFBTSxHQUFHO0lBQ1gsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLO0lBQ3hDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSztDQUMzQzs7OztBQUlELE1BQU0sVUFBVSxHQUFHO0lBQ2YsT0FBTyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7QUFDeEMsQ0FBQzs7Ozs7QUFFRCxNQUFNLFVBQVUsZ0JBQWdCLENBQUMsSUFBUzs7UUFDaEMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOztRQUVsQyxTQUFTLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7UUFFcEIsVUFBVSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO0lBRTVDLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRTtRQUNoQixNQUFNLElBQUksS0FBSyxDQUFDLG1CQUFtQixHQUFDLFNBQVMsQ0FBQyxDQUFDO0tBQ2xEOztRQUVHLFFBQVEsR0FBRyxVQUFVLEdBQUcsQ0FBQzs7UUFFdkIsS0FBSyxHQUFVLENBQ2pCLFFBQVEsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNmLEdBQUcsQ0FBQyxDQUFDO1FBQ0wsRUFBRSxDQUNMLEdBQUcsUUFBUTs7UUFFTixHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7UUFDZCxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQzs7UUFDZixJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUVyQixPQUFPLElBQUksR0FBQyxHQUFHLEdBQUMsS0FBSyxHQUFDLEdBQUcsR0FBQyxHQUFHLEdBQUMsR0FBRyxHQUFDLElBQUksQ0FBQztBQUMzQyxDQUFDOzs7Ozs7QUFHRCxNQUFNLFVBQVUsd0JBQXdCLENBQUMsS0FBVyxFQUFFLEdBQVM7O1FBQ3JELFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxFQUFFOztRQUUzQixRQUFRLEdBQUcsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLFNBQVM7SUFFMUMsT0FBTyxJQUFJLElBQUksQ0FDWCxTQUFTO1FBQ1QsQ0FDSSxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsUUFBUSxDQUMzQixDQUNKLENBQUM7QUFDTixDQUFDOzs7O0FBRUQsTUFBTSxVQUFVLGVBQWU7O1FBQ3JCLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRTtJQUU5QixXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBRWpDLE9BQU8sV0FBVyxDQUFDO0FBQ3ZCLENBQUM7Ozs7QUFFRCxNQUFNLFVBQVUsYUFBYTs7UUFDbkIsUUFBUSxHQUFHLElBQUksSUFBSSxFQUFFO0lBRTNCLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFFbkMsT0FBTyxRQUFRLENBQUM7QUFDcEIsQ0FBQzs7QUFHRCxJQUFZLEtBQUs7SUFFYixHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLEdBQUE7SUFDSCxHQUFHLElBQUE7SUFDSCxHQUFHLElBQUE7RUFDTiIsInNvdXJjZXNDb250ZW50IjpbImNvbnN0IG1vbnRocyA9IFtcbiAgICAnSmFuJywgJ0ZlYicsICdNYXInLCAnQXByJywgJ01heScsICdKdW4nLCBcbiAgICAnSnVsJywgJ0F1ZycsICdTZXAnLCAnT2N0JywgJ05vdicsICdEZWMnXG5dO1xuXG5cblxuZXhwb3J0IGZ1bmN0aW9uIE5PVygpOnN0cmluZyB7XG4gICAgcmV0dXJuIHRvRGF0ZVRpbWVGb3JtYXQobmV3IERhdGUoKSk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB0b0RhdGVUaW1lRm9ybWF0KGRhdGU6RGF0ZSk6c3RyaW5nIHtcbiAgICBjb25zdCBwYXJ0cyA9IGRhdGUudG9TdHJpbmcoKS5zcGxpdCgnICcpO1xuXG4gICAgY29uc3QgbW9udGhOYW1lID0gcGFydHNbMV07XG5cbiAgICBjb25zdCBtb250aEluZGV4ID0gbW9udGhzLmluZGV4T2YobW9udGhOYW1lKTtcblxuICAgIGlmIChtb250aEluZGV4IDwgMCkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ01vbnRoIE5PVCBGb3VuZDogJyttb250aE5hbWUpO1xuICAgIH1cblxuICAgIGxldCBtb250aE51bSA9IG1vbnRoSW5kZXggKyAxO1xuXG4gICAgY29uc3QgbW9udGg6c3RyaW5nID0gKFxuICAgICAgICBtb250aE51bSA8IDEwID9cbiAgICAgICAgJzAnIDpcbiAgICAgICAgJydcbiAgICApICsgbW9udGhOdW07XG5cbiAgICBjb25zdCBkYXkgPSBwYXJ0c1syXTtcbiAgICBjb25zdCB5ZWFyID0gcGFydHNbM107XG4gICAgY29uc3QgdGltZSA9IHBhcnRzWzRdO1xuXG4gICAgcmV0dXJuIHllYXIrJy0nK21vbnRoKyctJytkYXkrJyAnK3RpbWU7XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFJhbmRvbURhdGVXaXRoaW5SYW5nZShzdGFydDogRGF0ZSwgZW5kOiBEYXRlKTogRGF0ZSB7XG4gICAgY29uc3Qgc3RhcnRUaW1lID0gc3RhcnQuZ2V0VGltZSgpO1xuXG4gICAgY29uc3QgZHVyYXRpb24gPSBlbmQuZ2V0VGltZSgpIC0gc3RhcnRUaW1lO1xuICAgIFxuICAgIHJldHVybiBuZXcgRGF0ZShcbiAgICAgICAgc3RhcnRUaW1lICsgXG4gICAgICAgIChcbiAgICAgICAgICAgIE1hdGgucmFuZG9tKCkgKiBkdXJhdGlvblxuICAgICAgICApXG4gICAgKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGdldFN0YXJ0T2ZUb2RheSgpOiBEYXRlIHtcbiAgICBjb25zdCBzdGFydE9mRGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICBzdGFydE9mRGF0ZS5zZXRIb3VycygwLCAwLCAwLCAwKTtcbiAgICBcbiAgICByZXR1cm4gc3RhcnRPZkRhdGU7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBnZXRFbmRPZlRvZGF5KCk6IERhdGUge1xuICAgIGNvbnN0IGVuZE9mRGF5ID0gbmV3IERhdGUoKTtcbiAgICBcbiAgICBlbmRPZkRheS5zZXRIb3VycygyMywgNTksIDU5LCA5OTkpO1xuICAgIFxuICAgIHJldHVybiBlbmRPZkRheTtcbn1cblxuXG5leHBvcnQgZW51bSBNb250aFxue1xuICAgIEpBTixcbiAgICBGRUIsXG4gICAgTUFSLFxuICAgIEFQUixcbiAgICBNQVksXG4gICAgSlVOLFxuICAgIEpVTCxcbiAgICBBVUcsXG4gICAgU0VQLFxuICAgIE9DVCxcbiAgICBOT1YsXG4gICAgREVDXG59Il19