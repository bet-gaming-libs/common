/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/timer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} forMilliseconds
 * @return {?}
 */
export function sleep(forMilliseconds) {
    return new Promise((/**
     * @param {?} resolve
     * @return {?}
     */
    function (resolve) { return setTimeout(resolve, forMilliseconds); }));
}
/**
 * @record
 */
export function ITimerListener() { }
if (false) {
    /**
     * @return {?}
     */
    ITimerListener.prototype.onTimerTick = function () { };
}
var CustomTimer = /** @class */ (function () {
    function CustomTimer(intervalInMilliseconds, listener) {
        var _this = this;
        this.intervalInMilliseconds = intervalInMilliseconds;
        this.listener = listener;
        this.intervalId = undefined;
        this.tick = (/**
         * @return {?}
         */
        function () {
            _this.listener.onTimerTick();
        });
    }
    /**
     * @return {?}
     */
    CustomTimer.prototype.start = /**
     * @return {?}
     */
    function () {
        if (this.isRunning) {
            return;
        }
        this.intervalId = setInterval(this.tick, this.intervalInMilliseconds);
    };
    /**
     * @return {?}
     */
    CustomTimer.prototype.stop = /**
     * @return {?}
     */
    function () {
        if (!this.isRunning) {
            return;
        }
        clearInterval(this.intervalId);
        this.intervalId = undefined;
    };
    Object.defineProperty(CustomTimer.prototype, "isRunning", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.intervalId != undefined;
        },
        enumerable: true,
        configurable: true
    });
    return CustomTimer;
}());
export { CustomTimer };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalId;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.tick;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.intervalInMilliseconds;
    /**
     * @type {?}
     * @private
     */
    CustomTimer.prototype.listener;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL3RpbWVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxLQUFLLENBQUMsZUFBc0I7SUFDeEMsT0FBTyxJQUFJLE9BQU87Ozs7SUFBTyxVQUFBLE9BQU8sSUFBSSxPQUFBLFVBQVUsQ0FBQyxPQUFPLEVBQUUsZUFBZSxDQUFDLEVBQXBDLENBQW9DLEVBQUMsQ0FBQztBQUM5RSxDQUFDOzs7O0FBRUQsb0NBR0M7Ozs7O0lBREcsdURBQW1COztBQUd2QjtJQUlJLHFCQUNZLHNCQUE2QixFQUM3QixRQUF1QjtRQUZuQyxpQkFJQztRQUhXLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBTztRQUM3QixhQUFRLEdBQVIsUUFBUSxDQUFlO1FBSjNCLGVBQVUsR0FBRyxTQUFTLENBQUM7UUFrQnZCLFNBQUk7OztRQUFHO1lBQ1gsS0FBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoQyxDQUFDLEVBQUE7SUFkRCxDQUFDOzs7O0lBRUQsMkJBQUs7OztJQUFMO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUN6QixJQUFJLENBQUMsSUFBSSxFQUNULElBQUksQ0FBQyxzQkFBc0IsQ0FDOUIsQ0FBQztJQUNOLENBQUM7Ozs7SUFLRCwwQkFBSTs7O0lBQUo7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNqQixPQUFPO1NBQ1Y7UUFFRCxhQUFhLENBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FDbEIsQ0FBQztRQUVGLElBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO0lBQ2hDLENBQUM7SUFFRCxzQkFBWSxrQ0FBUzs7Ozs7UUFBckI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksU0FBUyxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBQ0wsa0JBQUM7QUFBRCxDQUFDLEFBdkNELElBdUNDOzs7Ozs7O0lBckNHLGlDQUErQjs7Ozs7SUFrQi9CLDJCQUVDOzs7OztJQWpCRyw2Q0FBcUM7Ozs7O0lBQ3JDLCtCQUErQiIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiBzbGVlcChmb3JNaWxsaXNlY29uZHM6bnVtYmVyKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlPHZvaWQ+KHJlc29sdmUgPT4gc2V0VGltZW91dChyZXNvbHZlLCBmb3JNaWxsaXNlY29uZHMpKTtcbn1cblxuZXhwb3J0IGludGVyZmFjZSBJVGltZXJMaXN0ZW5lclxue1xuICAgIG9uVGltZXJUaWNrKCk6dm9pZDtcbn1cblxuZXhwb3J0IGNsYXNzIEN1c3RvbVRpbWVyXG57XG4gICAgcHJpdmF0ZSBpbnRlcnZhbElkID0gdW5kZWZpbmVkO1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgaW50ZXJ2YWxJbk1pbGxpc2Vjb25kczpudW1iZXIsXG4gICAgICAgIHByaXZhdGUgbGlzdGVuZXI6SVRpbWVyTGlzdGVuZXJcbiAgICApIHtcbiAgICB9XG5cbiAgICBzdGFydCgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNSdW5uaW5nKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmludGVydmFsSWQgPSBzZXRJbnRlcnZhbChcbiAgICAgICAgICAgIHRoaXMudGljayxcbiAgICAgICAgICAgIHRoaXMuaW50ZXJ2YWxJbk1pbGxpc2Vjb25kc1xuICAgICAgICApO1xuICAgIH1cbiAgICBwcml2YXRlIHRpY2sgPSAoKSA9PiB7XG4gICAgICAgIHRoaXMubGlzdGVuZXIub25UaW1lclRpY2soKTtcbiAgICB9XG5cbiAgICBzdG9wKCkge1xuICAgICAgICBpZiAoIXRoaXMuaXNSdW5uaW5nKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBjbGVhckludGVydmFsKFxuICAgICAgICAgICAgdGhpcy5pbnRlcnZhbElkXG4gICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5pbnRlcnZhbElkID0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IGlzUnVubmluZygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaW50ZXJ2YWxJZCAhPSB1bmRlZmluZWQ7XG4gICAgfVxufSJdfQ==