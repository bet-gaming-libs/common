/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/math-util.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownToNumber(amount, precision) {
    return Number(roundDownToString(amount, precision));
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundDownToString(amount, precision) {
    /** @type {?} */
    var amount_str = String(amount.toFixed(precision + 1));
    return roundStringDownToString(amount_str, precision);
}
/**
 * @param {?} amount
 * @param {?} precision
 * @return {?}
 */
export function roundStringDownToString(amount, precision) {
    /** @type {?} */
    var parts = amount.split('.');
    /** @type {?} */
    var isWholeNumber = parts.length == 1;
    /** @type {?} */
    var rounded = Number(parts[0] + '.' +
        (isWholeNumber ?
            '0' :
            parts[1].substr(0, precision)));
    /** @type {?} */
    var result = rounded.toFixed(precision);
    return result;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0aC11dGlsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvdXRpbC9tYXRoLXV0aWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxpQkFBaUIsQ0FBQyxNQUFhLEVBQUMsU0FBZ0I7SUFDNUQsT0FBTyxNQUFNLENBQ1QsaUJBQWlCLENBQUMsTUFBTSxFQUFDLFNBQVMsQ0FBQyxDQUN0QyxDQUFDO0FBQ04sQ0FBQzs7Ozs7O0FBRUQsTUFBTSxVQUFVLGlCQUFpQixDQUFDLE1BQWEsRUFBQyxTQUFnQjs7UUFDdEQsVUFBVSxHQUFHLE1BQU0sQ0FBRSxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsR0FBQyxDQUFDLENBQUMsQ0FBRTtJQUV4RCxPQUFPLHVCQUF1QixDQUFDLFVBQVUsRUFBQyxTQUFTLENBQUMsQ0FBQztBQUN6RCxDQUFDOzs7Ozs7QUFFRCxNQUFNLFVBQVUsdUJBQXVCLENBQUMsTUFBYSxFQUFDLFNBQWdCOztRQUM1RCxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7O1FBRXpCLGFBQWEsR0FDZixLQUFLLENBQUMsTUFBTSxJQUFJLENBQUM7O1FBRWYsT0FBTyxHQUFHLE1BQU0sQ0FDbEIsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUc7UUFDZCxDQUNJLGFBQWEsQ0FBQyxDQUFDO1lBQ1gsR0FBRyxDQUFDLENBQUM7WUFDTCxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBQyxTQUFTLENBQUMsQ0FDbkMsQ0FDSjs7UUFFSyxNQUFNLEdBQUcsT0FBTyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7SUFFekMsT0FBTyxNQUFNLENBQUM7QUFDbEIsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBmdW5jdGlvbiByb3VuZERvd25Ub051bWJlcihhbW91bnQ6bnVtYmVyLHByZWNpc2lvbjpudW1iZXIpOm51bWJlciB7XG4gICAgcmV0dXJuIE51bWJlcihcbiAgICAgICAgcm91bmREb3duVG9TdHJpbmcoYW1vdW50LHByZWNpc2lvbilcbiAgICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcm91bmREb3duVG9TdHJpbmcoYW1vdW50Om51bWJlcixwcmVjaXNpb246bnVtYmVyKTpzdHJpbmcge1xuICAgIGNvbnN0IGFtb3VudF9zdHIgPSBTdHJpbmcoIGFtb3VudC50b0ZpeGVkKHByZWNpc2lvbisxKSApO1xuICAgIFxuICAgIHJldHVybiByb3VuZFN0cmluZ0Rvd25Ub1N0cmluZyhhbW91bnRfc3RyLHByZWNpc2lvbik7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByb3VuZFN0cmluZ0Rvd25Ub1N0cmluZyhhbW91bnQ6c3RyaW5nLHByZWNpc2lvbjpudW1iZXIpOnN0cmluZyB7XG4gICAgY29uc3QgcGFydHMgPSBhbW91bnQuc3BsaXQoJy4nKTtcblxuICAgIGNvbnN0IGlzV2hvbGVOdW1iZXIgPVxuICAgICAgICBwYXJ0cy5sZW5ndGggPT0gMTtcblxuICAgIGNvbnN0IHJvdW5kZWQgPSBOdW1iZXIoIFxuICAgICAgICBwYXJ0c1swXSArICcuJyArIFxuICAgICAgICAoXG4gICAgICAgICAgICBpc1dob2xlTnVtYmVyID9cbiAgICAgICAgICAgICAgICAnMCcgOlxuICAgICAgICAgICAgICAgIHBhcnRzWzFdLnN1YnN0cigwLHByZWNpc2lvbikgXG4gICAgICAgIClcbiAgICApO1xuICAgIFxuICAgIGNvbnN0IHJlc3VsdCA9IHJvdW5kZWQudG9GaXhlZChwcmVjaXNpb24pO1xuICAgIFxuICAgIHJldHVybiByZXN1bHQ7XG59Il19