/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/log.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @param {?} message
 * @return {?}
 */
export function debugMessage(message) {
    /** @type {?} */
    var date = new Date();
    console.log((date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds()
        + ' - ' +
        message);
}
/**
 * @param {?=} message
 * @param {...?} optionalParams
 * @return {?}
 */
export function logWithTime(message) {
    var optionalParams = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        optionalParams[_i - 1] = arguments[_i];
    }
    /** @type {?} */
    var date = new Date();
    /** @type {?} */
    var dateString = (date.getMonth() + 1) + '-' +
        date.getDate() + ' ' +
        date.getHours() + ':' +
        date.getMinutes() + ':' +
        date.getSeconds() + '.' +
        date.getMilliseconds();
    console.log.apply(console, tslib_1.__spread([dateString
        //+ ' - ' + message
        ,
        message], optionalParams));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZWFybmJldC1jb21tb24vIiwic291cmNlcyI6WyJsaWIvdXRpbC9sb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFBLE1BQU0sVUFBVSxZQUFZLENBQUMsT0FBYzs7UUFDakMsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFO0lBR3ZCLE9BQU8sQ0FBQyxHQUFHLENBQ1AsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRztRQUMzQixJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRztRQUVwQixJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRztRQUNyQixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFO1VBRXBCLEtBQUs7UUFDUCxPQUFPLENBQ1YsQ0FBQztBQUNOLENBQUM7Ozs7OztBQUVELE1BQU0sVUFBVSxXQUFXLENBQUMsT0FBWTtJQUFDLHdCQUF1QjtTQUF2QixVQUF1QixFQUF2QixxQkFBdUIsRUFBdkIsSUFBdUI7UUFBdkIsdUNBQXVCOzs7UUFDdEQsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFOztRQUVqQixVQUFVLEdBQ1osQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsR0FBRztRQUMzQixJQUFJLENBQUMsT0FBTyxFQUFFLEdBQUcsR0FBRztRQUVwQixJQUFJLENBQUMsUUFBUSxFQUFFLEdBQUcsR0FBRztRQUNyQixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLEdBQUcsR0FBRztRQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFO0lBRzFCLE9BQU8sQ0FBQyxHQUFHLE9BQVgsT0FBTyxvQkFDSCxVQUFVO1FBQ1YsbUJBQW1COztRQUVuQixPQUFPLEdBQ0osY0FBYyxHQUNuQjtBQUNOLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gZGVidWdNZXNzYWdlKG1lc3NhZ2U6c3RyaW5nKSB7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKCk7XG5cblxuICAgIGNvbnNvbGUubG9nKFxuICAgICAgICAoZGF0ZS5nZXRNb250aCgpICsgMSkgKyAnLScgK1xuICAgICAgICBkYXRlLmdldERhdGUoKSArICcgJyArXG4gICAgICAgIFxuICAgICAgICBkYXRlLmdldEhvdXJzKCkgKyAnOicgK1xuICAgICAgICBkYXRlLmdldE1pbnV0ZXMoKSArICc6JyArXG4gICAgICAgIGRhdGUuZ2V0U2Vjb25kcygpICsgJy4nICtcbiAgICAgICAgZGF0ZS5nZXRNaWxsaXNlY29uZHMoKVxuICAgICAgICBcbiAgICAgICAgKyAnIC0gJyArXG4gICAgICAgIG1lc3NhZ2VcbiAgICApO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gbG9nV2l0aFRpbWUobWVzc2FnZT86YW55LC4uLm9wdGlvbmFsUGFyYW1zOmFueVtdKSB7XG4gICAgY29uc3QgZGF0ZSA9IG5ldyBEYXRlKCk7XG5cbiAgICBjb25zdCBkYXRlU3RyaW5nID0gXG4gICAgICAgIChkYXRlLmdldE1vbnRoKCkgKyAxKSArICctJyArXG4gICAgICAgIGRhdGUuZ2V0RGF0ZSgpICsgJyAnICtcbiAgICAgICAgXG4gICAgICAgIGRhdGUuZ2V0SG91cnMoKSArICc6JyArXG4gICAgICAgIGRhdGUuZ2V0TWludXRlcygpICsgJzonICtcbiAgICAgICAgZGF0ZS5nZXRTZWNvbmRzKCkgKyAnLicgK1xuICAgICAgICBkYXRlLmdldE1pbGxpc2Vjb25kcygpO1xuXG5cbiAgICBjb25zb2xlLmxvZyhcbiAgICAgICAgZGF0ZVN0cmluZ1xuICAgICAgICAvLysgJyAtICcgKyBtZXNzYWdlXG4gICAgICAgICxcbiAgICAgICAgbWVzc2FnZSxcbiAgICAgICAgLi4ub3B0aW9uYWxQYXJhbXNcbiAgICApO1xufSJdfQ==