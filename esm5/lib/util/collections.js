/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/collections.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/**
 * @record
 */
export function IUniqueItem() { }
if (false) {
    /** @type {?} */
    IUniqueItem.prototype.id;
}
/**
 * @template T
 */
var /**
 * @template T
 */
QueueOfUniqueItems = /** @class */ (function () {
    function QueueOfUniqueItems() {
        this.map = {};
        this.list = [];
    }
    /**
     * @param {?} items
     * @return {?}
     */
    QueueOfUniqueItems.prototype.addAll = /**
     * @param {?} items
     * @return {?}
     */
    function (items) {
        var e_1, _a;
        try {
            for (var items_1 = tslib_1.__values(items), items_1_1 = items_1.next(); !items_1_1.done; items_1_1 = items_1.next()) {
                var item = items_1_1.value;
                this.add(item);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (items_1_1 && !items_1_1.done && (_a = items_1.return)) _a.call(items_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * @param {?} item
     * @return {?}
     */
    QueueOfUniqueItems.prototype.add = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (!this.hasItemWithId(item.id)) {
            this.map[item.id] = item;
            this.list.push(item);
        }
    };
    /**
     * @param {?} itemId
     * @return {?}
     */
    QueueOfUniqueItems.prototype.removeItemWithId = /**
     * @param {?} itemId
     * @return {?}
     */
    function (itemId) {
        delete this.map[itemId];
        for (var i = 0; i < this.list.length; i++) {
            /** @type {?} */
            var item = this.list[i];
            if (itemId == item.id) {
                this.list.splice(i, 1);
                break;
            }
        }
    };
    /**
     * @param {?} itemId
     * @return {?}
     */
    QueueOfUniqueItems.prototype.hasItemWithId = /**
     * @param {?} itemId
     * @return {?}
     */
    function (itemId) {
        return this.map[itemId] != undefined;
    };
    /**
     * @param {?} itemId
     * @return {?}
     */
    QueueOfUniqueItems.prototype.getItemWithId = /**
     * @param {?} itemId
     * @return {?}
     */
    function (itemId) {
        return this.map[itemId];
    };
    Object.defineProperty(QueueOfUniqueItems.prototype, "items", {
        get: /**
         * @return {?}
         */
        function () {
            return this.list.slice();
        },
        enumerable: true,
        configurable: true
    });
    return QueueOfUniqueItems;
}());
/**
 * @template T
 */
export { QueueOfUniqueItems };
if (false) {
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.map;
    /**
     * @type {?}
     * @private
     */
    QueueOfUniqueItems.prototype.list;
}
/**
 * @template T
 */
var /**
 * @template T
 */
FIFOList = /** @class */ (function () {
    function FIFOList(maxNumOfItems) {
        this.maxNumOfItems = maxNumOfItems;
        this._items = [];
    }
    /**
     * @param {?} newItem
     * @return {?}
     */
    FIFOList.prototype.addAsFirst = /**
     * @param {?} newItem
     * @return {?}
     */
    function (newItem) {
        /** @type {?} */
        var end = this.items.slice(0, this.maxNumOfItems - 1);
        this._items = [newItem].concat(end);
    };
    /**
     * @param {?} newItem
     * @return {?}
     */
    FIFOList.prototype.addAsLast = /**
     * @param {?} newItem
     * @return {?}
     */
    function (newItem) {
        /** @type {?} */
        var start = [];
        if (this.items.length < this.maxNumOfItems) {
            start = this.items.slice();
        }
        else {
            start = this.items.slice(1, this.maxNumOfItems);
        }
        /** @type {?} */
        var merged = start.concat([newItem]);
        this._items = merged;
    };
    Object.defineProperty(FIFOList.prototype, "items", {
        get: /**
         * @return {?}
         */
        function () {
            return this._items;
        },
        enumerable: true,
        configurable: true
    });
    return FIFOList;
}());
/**
 * @template T
 */
export { FIFOList };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype._items;
    /**
     * @type {?}
     * @private
     */
    FIFOList.prototype.maxNumOfItems;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGVjdGlvbnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL2NvbGxlY3Rpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLGlDQUdDOzs7SUFERyx5QkFBVTs7Ozs7QUFHZDs7OztJQUtJO1FBSFEsUUFBRyxHQUF3QixFQUFFLENBQUM7UUFDOUIsU0FBSSxHQUFPLEVBQUUsQ0FBQztJQUd0QixDQUFDOzs7OztJQUVELG1DQUFNOzs7O0lBQU4sVUFBTyxLQUFTOzs7WUFDWixLQUFpQixJQUFBLFVBQUEsaUJBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO2dCQUFuQixJQUFJLElBQUksa0JBQUE7Z0JBQ1QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNsQjs7Ozs7Ozs7O0lBQ0wsQ0FBQzs7Ozs7SUFDRCxnQ0FBRzs7OztJQUFILFVBQUksSUFBTTtRQUNOLElBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRztZQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7WUFFekIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDeEI7SUFDTCxDQUFDOzs7OztJQUVELDZDQUFnQjs7OztJQUFoQixVQUFpQixNQUFhO1FBQzFCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV4QixLQUFLLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O2dCQUMvQixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFFekIsSUFBSSxNQUFNLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN0QixNQUFNO2FBQ1Q7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRUQsMENBQWE7Ozs7SUFBYixVQUFjLE1BQWE7UUFDdkIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLFNBQVMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVELDBDQUFhOzs7O0lBQWIsVUFBYyxNQUFhO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsc0JBQUkscUNBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUM3QixDQUFDOzs7T0FBQTtJQUNMLHlCQUFDO0FBQUQsQ0FBQyxBQTdDRCxJQTZDQzs7Ozs7Ozs7OztJQTNDRyxpQ0FBc0M7Ozs7O0lBQ3RDLGtDQUFzQjs7Ozs7QUE2QzFCOzs7O0lBSUksa0JBQW9CLGFBQW9CO1FBQXBCLGtCQUFhLEdBQWIsYUFBYSxDQUFPO1FBRmhDLFdBQU0sR0FBTyxFQUFFLENBQUM7SUFHeEIsQ0FBQzs7Ozs7SUFFRCw2QkFBVTs7OztJQUFWLFVBQVcsT0FBUzs7WUFDVixHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ3hCLENBQUMsRUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FDekI7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7O0lBQ0QsNEJBQVM7Ozs7SUFBVCxVQUFVLE9BQVM7O1lBQ1gsS0FBSyxHQUFPLEVBQUU7UUFFbEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3hDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQzlCO2FBQU07WUFDSCxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQ3BCLENBQUMsRUFDRCxJQUFJLENBQUMsYUFBYSxDQUNyQixDQUFDO1NBQ0w7O1lBRUssTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUV0QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBRUQsc0JBQUksMkJBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDOzs7T0FBQTtJQUNMLGVBQUM7QUFBRCxDQUFDLEFBbENELElBa0NDOzs7Ozs7Ozs7O0lBaENHLDBCQUF3Qjs7Ozs7SUFFWixpQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElVbmlxdWVJdGVtXG57XG4gICAgaWQ6c3RyaW5nO1xufVxuXG5leHBvcnQgY2xhc3MgUXVldWVPZlVuaXF1ZUl0ZW1zPFQgZXh0ZW5kcyBJVW5pcXVlSXRlbT5cbntcbiAgICBwcml2YXRlIG1hcDp7W2l0ZW1JZDpzdHJpbmddOiBUfSA9IHt9O1xuICAgIHByaXZhdGUgbGlzdDpUW10gPSBbXTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgIH1cblxuICAgIGFkZEFsbChpdGVtczpUW10pIHtcbiAgICAgICAgZm9yICh2YXIgaXRlbSBvZiBpdGVtcykge1xuICAgICAgICAgICAgdGhpcy5hZGQoaXRlbSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgYWRkKGl0ZW06VCkge1xuICAgICAgICBpZiAoICF0aGlzLmhhc0l0ZW1XaXRoSWQoaXRlbS5pZCkgKSB7XG4gICAgICAgICAgICB0aGlzLm1hcFtpdGVtLmlkXSA9IGl0ZW07XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIHRoaXMubGlzdC5wdXNoKGl0ZW0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmVtb3ZlSXRlbVdpdGhJZChpdGVtSWQ6c3RyaW5nKSB7XG4gICAgICAgIGRlbGV0ZSB0aGlzLm1hcFtpdGVtSWRdO1xuXG4gICAgICAgIGZvciAodmFyIGk9MDsgaSA8IHRoaXMubGlzdC5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgY29uc3QgaXRlbSA9IHRoaXMubGlzdFtpXTtcblxuICAgICAgICAgICAgaWYgKGl0ZW1JZCA9PSBpdGVtLmlkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0LnNwbGljZShpLDEpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgaGFzSXRlbVdpdGhJZChpdGVtSWQ6c3RyaW5nKTpib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWFwW2l0ZW1JZF0gIT0gdW5kZWZpbmVkO1xuICAgIH1cblxuICAgIGdldEl0ZW1XaXRoSWQoaXRlbUlkOnN0cmluZyk6VCB7XG4gICAgICAgIHJldHVybiB0aGlzLm1hcFtpdGVtSWRdO1xuICAgIH1cblxuICAgIGdldCBpdGVtcygpOlRbXSB7XG4gICAgICAgIHJldHVybiB0aGlzLmxpc3Quc2xpY2UoKTtcbiAgICB9XG59XG5cblxuZXhwb3J0IGNsYXNzIEZJRk9MaXN0PFQ+XG57XG4gICAgcHJpdmF0ZSBfaXRlbXM6VFtdID0gW107XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIG1heE51bU9mSXRlbXM6bnVtYmVyKSB7XG4gICAgfVxuXG4gICAgYWRkQXNGaXJzdChuZXdJdGVtOlQpIHtcbiAgICAgICAgY29uc3QgZW5kID0gdGhpcy5pdGVtcy5zbGljZShcbiAgICAgICAgICAgIDAsXG4gICAgICAgICAgICB0aGlzLm1heE51bU9mSXRlbXMgLSAxXG4gICAgICAgICk7XG4gICAgICAgIHRoaXMuX2l0ZW1zID0gW25ld0l0ZW1dLmNvbmNhdChlbmQpO1xuICAgIH1cbiAgICBhZGRBc0xhc3QobmV3SXRlbTpUKSB7XG4gICAgICAgIGxldCBzdGFydDpUW10gPSBbXTtcblxuICAgICAgICBpZiAodGhpcy5pdGVtcy5sZW5ndGggPCB0aGlzLm1heE51bU9mSXRlbXMpIHtcbiAgICAgICAgICAgIHN0YXJ0ID0gdGhpcy5pdGVtcy5zbGljZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc3RhcnQgPSB0aGlzLml0ZW1zLnNsaWNlKFxuICAgICAgICAgICAgICAgIDEsXG4gICAgICAgICAgICAgICAgdGhpcy5tYXhOdW1PZkl0ZW1zXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgbWVyZ2VkID0gc3RhcnQuY29uY2F0KFtuZXdJdGVtXSk7XG5cbiAgICAgICAgdGhpcy5faXRlbXMgPSBtZXJnZWQ7XG4gICAgfVxuXG4gICAgZ2V0IGl0ZW1zKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5faXRlbXM7XG4gICAgfVxufSJdfQ==