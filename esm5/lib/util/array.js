/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/array.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @template T
 * @param {?} array
 * @param {?} numOfElements
 * @return {?}
 */
export function getRandomUniqueElements(array, numOfElements) {
    /** @type {?} */
    var elements = [];
    /** @type {?} */
    var source = array.slice();
    for (var i = 0; i < numOfElements; i++) {
        /** @type {?} */
        var index = getRandomIndex(source);
        elements.push(source[index]);
        source.splice(index, 1);
    }
    return elements;
}
/**
 * @param {?} array
 * @return {?}
 */
function getRandomIndex(array) {
    return Math.round(Math.random() * (array.length - 1));
}
/**
 * @template T
 * @param {?} item
 * @param {?} array
 * @return {?}
 */
export function removeItemFromArray(item, array) {
    for (var i = 0; i < array.length; i++) {
        if (item == array[i]) {
            array.splice(i, 1);
            return;
        }
    }
}
/**
 * @template T
 * @param {?} items
 * @return {?}
 */
export function shuffleArray(items) {
    /** @type {?} */
    var clone = items.slice();
    clone.sort((/**
     * @return {?}
     */
    function () { return 0.5 - Math.random(); }));
    return clone;
}
/**
 * @template T
 * @param {?} items
 * @param {?} numOfItemsInFirstPart
 * @return {?}
 */
export function splitArrayIntoTwo(items, numOfItemsInFirstPart) {
    /** @type {?} */
    var part1 = items.slice(0, numOfItemsInFirstPart);
    /** @type {?} */
    var part2 = items.slice(numOfItemsInFirstPart);
    return [
        part1,
        part2
    ];
}
/**
 * @template T
 * @param {?} source
 * @param {?} maxNumOfItemsPerPart
 * @return {?}
 */
export function splitArrayIntoMany(source, maxNumOfItemsPerPart) {
    /** @type {?} */
    var items = source.slice();
    /** @type {?} */
    var parts = [];
    while (items.length > 0) {
        /** @type {?} */
        var part = items.splice(0, items.length > maxNumOfItemsPerPart ?
            maxNumOfItemsPerPart :
            items.length);
        parts.push(part);
    }
    return parts;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXJyYXkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL2FycmF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUEsTUFBTSxVQUFVLHVCQUF1QixDQUFJLEtBQVMsRUFBQyxhQUFvQjs7UUFFakUsUUFBUSxHQUFPLEVBQUU7O1FBRWpCLE1BQU0sR0FBTyxLQUFLLENBQUMsS0FBSyxFQUFFO0lBRTlCLEtBQUssSUFBSSxDQUFDLEdBQVEsQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O1lBQ3JDLEtBQUssR0FBVSxjQUFjLENBQUMsTUFBTSxDQUFDO1FBQ3pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDN0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUMsQ0FBQyxDQUFDLENBQUM7S0FDMUI7SUFFRCxPQUFPLFFBQVEsQ0FBQztBQUNwQixDQUFDOzs7OztBQUVELFNBQVMsY0FBYyxDQUFDLEtBQVc7SUFFL0IsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUUsQ0FBQztBQUM1RCxDQUFDOzs7Ozs7O0FBRUQsTUFBTSxVQUFVLG1CQUFtQixDQUEwQixJQUFNLEVBQUMsS0FBUztJQUN6RSxLQUFLLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtRQUNqQyxJQUFJLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDbEIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUM7WUFFbEIsT0FBTztTQUNWO0tBQ0o7QUFDTCxDQUFDOzs7Ozs7QUFJRCxNQUFNLFVBQVUsWUFBWSxDQUFJLEtBQVM7O1FBQy9CLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxFQUFFO0lBRTNCLEtBQUssQ0FBQyxJQUFJOzs7SUFBRSxjQUFNLE9BQUEsR0FBRyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBbkIsQ0FBbUIsRUFBRSxDQUFDO0lBRXhDLE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUM7Ozs7Ozs7QUFFRCxNQUFNLFVBQVUsaUJBQWlCLENBQUksS0FBUyxFQUFDLHFCQUE0Qjs7UUFDakUsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFDLHFCQUFxQixDQUFDOztRQUM1QyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQztJQUVoRCxPQUFPO1FBQ0gsS0FBSztRQUNMLEtBQUs7S0FDUixDQUFDO0FBQ04sQ0FBQzs7Ozs7OztBQUVELE1BQU0sVUFBVSxrQkFBa0IsQ0FBSSxNQUFVLEVBQUMsb0JBQTJCOztRQUNsRSxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRTs7UUFFdEIsS0FBSyxHQUFTLEVBQUU7SUFFdEIsT0FBTyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7WUFDZixJQUFJLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FDckIsQ0FBQyxFQUNELEtBQUssQ0FBQyxNQUFNLEdBQUcsb0JBQW9CLENBQUMsQ0FBQztZQUNqQyxvQkFBb0IsQ0FBQyxDQUFDO1lBQ3RCLEtBQUssQ0FBQyxNQUFNLENBQ25CO1FBRUQsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNwQjtJQUVELE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZnVuY3Rpb24gZ2V0UmFuZG9tVW5pcXVlRWxlbWVudHM8VD4oYXJyYXk6VFtdLG51bU9mRWxlbWVudHM6bnVtYmVyKTpUW11cbntcbiAgICB2YXIgZWxlbWVudHM6VFtdID0gW107XG4gICAgXG4gICAgdmFyIHNvdXJjZTpUW10gPSBhcnJheS5zbGljZSgpO1xuICAgIFxuICAgIGZvciAodmFyIGk6bnVtYmVyPTA7IGkgPCBudW1PZkVsZW1lbnRzOyBpKyspIHtcbiAgICAgICAgdmFyIGluZGV4Om51bWJlciA9IGdldFJhbmRvbUluZGV4KHNvdXJjZSk7XG4gICAgICAgIGVsZW1lbnRzLnB1c2goc291cmNlW2luZGV4XSk7XG4gICAgICAgIHNvdXJjZS5zcGxpY2UoaW5kZXgsMSk7XG4gICAgfVxuICAgIFxuICAgIHJldHVybiBlbGVtZW50cztcbn1cblxuZnVuY3Rpb24gZ2V0UmFuZG9tSW5kZXgoYXJyYXk6YW55W10pOm51bWJlclxue1xuICAgIHJldHVybiBNYXRoLnJvdW5kKCBNYXRoLnJhbmRvbSgpICogKGFycmF5Lmxlbmd0aCAtIDEpICk7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiByZW1vdmVJdGVtRnJvbUFycmF5PFQgZXh0ZW5kcyBudW1iZXJ8c3RyaW5nPihpdGVtOlQsYXJyYXk6VFtdKSB7XG4gICAgZm9yICh2YXIgaT0wOyBpIDwgYXJyYXkubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgaWYgKGl0ZW0gPT0gYXJyYXlbaV0pIHtcbiAgICAgICAgICAgIGFycmF5LnNwbGljZShpLDEpO1xuXG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICB9XG59XG5cblxuXG5leHBvcnQgZnVuY3Rpb24gc2h1ZmZsZUFycmF5PFQ+KGl0ZW1zOlRbXSk6VFtdIHtcbiAgICBjb25zdCBjbG9uZSA9IGl0ZW1zLnNsaWNlKCk7XG5cbiAgICBjbG9uZS5zb3J0KCAoKSA9PiAwLjUgLSBNYXRoLnJhbmRvbSgpICk7XG5cbiAgICByZXR1cm4gY2xvbmU7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzcGxpdEFycmF5SW50b1R3bzxUPihpdGVtczpUW10sbnVtT2ZJdGVtc0luRmlyc3RQYXJ0Om51bWJlcik6VFtdW10ge1xuICAgIGNvbnN0IHBhcnQxID0gaXRlbXMuc2xpY2UoMCxudW1PZkl0ZW1zSW5GaXJzdFBhcnQpO1xuICAgIGNvbnN0IHBhcnQyID0gaXRlbXMuc2xpY2UobnVtT2ZJdGVtc0luRmlyc3RQYXJ0KTtcblxuICAgIHJldHVybiBbXG4gICAgICAgIHBhcnQxLFxuICAgICAgICBwYXJ0MlxuICAgIF07XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzcGxpdEFycmF5SW50b01hbnk8VD4oc291cmNlOlRbXSxtYXhOdW1PZkl0ZW1zUGVyUGFydDpudW1iZXIpOlRbXVtdIHtcbiAgICBjb25zdCBpdGVtcyA9IHNvdXJjZS5zbGljZSgpO1xuXG4gICAgY29uc3QgcGFydHM6VFtdW10gPSBbXTtcblxuICAgIHdoaWxlIChpdGVtcy5sZW5ndGggPiAwKSB7XG4gICAgICAgIGNvbnN0IHBhcnQgPSBpdGVtcy5zcGxpY2UoXG4gICAgICAgICAgICAwLFxuICAgICAgICAgICAgaXRlbXMubGVuZ3RoID4gbWF4TnVtT2ZJdGVtc1BlclBhcnQgP1xuICAgICAgICAgICAgICAgIG1heE51bU9mSXRlbXNQZXJQYXJ0IDpcbiAgICAgICAgICAgICAgICBpdGVtcy5sZW5ndGhcbiAgICAgICAgKTtcbiAgICAgICAgXG4gICAgICAgIHBhcnRzLnB1c2gocGFydCk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHBhcnRzO1xufSJdfQ==