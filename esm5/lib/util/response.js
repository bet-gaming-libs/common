/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/response.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 * @template T
 */
export function IResponse() { }
if (false) {
    /** @type {?|undefined} */
    IResponse.prototype.error;
    /** @type {?|undefined} */
    IResponse.prototype.result;
}
/**
 * @template T
 */
var /**
 * @template T
 */
ResponseManager = /** @class */ (function () {
    // instance
    function ResponseManager(promise) {
        var _this = this;
        this.promise = promise;
        this.handler = (/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            if (response.error)
                _this.promise.fail(response.error);
            if (response.result)
                _this.promise.done(response.result);
        });
    }
    // class
    // class
    /**
     * @template T
     * @param {?} promise
     * @return {?}
     */
    ResponseManager.newHandler = 
    // class
    /**
     * @template T
     * @param {?} promise
     * @return {?}
     */
    function (promise) {
        return new ResponseManager(promise).handler;
    };
    return ResponseManager;
}());
/**
 * @template T
 */
export { ResponseManager };
if (false) {
    /** @type {?} */
    ResponseManager.prototype.handler;
    /**
     * @type {?}
     * @private
     */
    ResponseManager.prototype.promise;
}
/**
 * @template T
 */
var /**
 * @template T
 */
ResponseDispatcher = /** @class */ (function () {
    // instance
    function ResponseDispatcher(handler) {
        this.handler = handler;
    }
    // class
    // class
    /**
     * @template T
     * @param {?} handler
     * @return {?}
     */
    ResponseDispatcher.create = 
    // class
    /**
     * @template T
     * @param {?} handler
     * @return {?}
     */
    function (handler) {
        return new ResponseDispatcher(handler);
    };
    /**
     * @param {?} result
     * @return {?}
     */
    ResponseDispatcher.prototype.dispatchResult = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        this.handler({
            result: result
        });
    };
    /**
     * @param {?} code
     * @param {?=} data
     * @return {?}
     */
    ResponseDispatcher.prototype.dispatchError = /**
     * @param {?} code
     * @param {?=} data
     * @return {?}
     */
    function (code, data) {
        this.handler({
            error: {
                code: code,
                data: data
            }
        });
    };
    return ResponseDispatcher;
}());
/**
 * @template T
 */
export { ResponseDispatcher };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ResponseDispatcher.prototype.handler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVzcG9uc2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9lYXJuYmV0LWNvbW1vbi8iLCJzb3VyY2VzIjpbImxpYi91dGlsL3Jlc3BvbnNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUdBLCtCQUlDOzs7SUFGRywwQkFBYzs7SUFDZCwyQkFBVTs7Ozs7QUFLZDs7OztJQVNJLFdBQVc7SUFDWCx5QkFBNEIsT0FBd0I7UUFBcEQsaUJBQ0M7UUFEMkIsWUFBTyxHQUFQLE9BQU8sQ0FBaUI7UUFHN0MsWUFBTzs7OztRQUFHLFVBQUMsUUFBcUI7WUFFbkMsSUFBSSxRQUFRLENBQUMsS0FBSztnQkFDZCxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFFdEMsSUFBSSxRQUFRLENBQUMsTUFBTTtnQkFDZixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDM0MsQ0FBQyxFQUFBO0lBVEQsQ0FBQztJQVRELFFBQVE7Ozs7Ozs7SUFDRCwwQkFBVTs7Ozs7OztJQUFqQixVQUFxQixPQUF3QjtRQUV6QyxPQUFPLElBQUksZUFBZSxDQUFJLE9BQU8sQ0FBQyxDQUFDLE9BQU8sQ0FBQztJQUNuRCxDQUFDO0lBZUwsc0JBQUM7QUFBRCxDQUFDLEFBckJELElBcUJDOzs7Ozs7O0lBUkcsa0NBT0M7Ozs7O0lBVm1CLGtDQUFnQzs7Ozs7QUFheEQ7Ozs7SUFTSSxXQUFXO0lBQ1gsNEJBQTRCLE9BQTBCO1FBQTFCLFlBQU8sR0FBUCxPQUFPLENBQW1CO0lBQ3RELENBQUM7SUFURCxRQUFROzs7Ozs7O0lBQ0QseUJBQU07Ozs7Ozs7SUFBYixVQUFpQixPQUEwQjtRQUV2QyxPQUFPLElBQUksa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFPTSwyQ0FBYzs7OztJQUFyQixVQUFzQixNQUFRO1FBRTFCLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDVCxNQUFNLFFBQUE7U0FDVCxDQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTSwwQ0FBYTs7Ozs7SUFBcEIsVUFBcUIsSUFBVyxFQUFDLElBQUs7UUFFbEMsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUNULEtBQUssRUFBRTtnQkFDSCxJQUFJLE1BQUE7Z0JBQ0osSUFBSSxNQUFBO2FBQ1A7U0FDSixDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0wseUJBQUM7QUFBRCxDQUFDLEFBN0JELElBNkJDOzs7Ozs7Ozs7O0lBbkJ1QixxQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0lFcnJvcixDdXN0b21Qcm9taXNlfSBmcm9tICcuL3Byb21pc2UnO1xuXG5cbmV4cG9ydCBpbnRlcmZhY2UgSVJlc3BvbnNlPFQ+XG57XG4gICAgZXJyb3I/OklFcnJvcjtcbiAgICByZXN1bHQ/OlQ7XG59XG5cbmV4cG9ydCB0eXBlIFJlc3BvbnNlSGFuZGxlcjxUPiA9IChyZXNwb25zZTpJUmVzcG9uc2U8VD4pID0+IHZvaWQ7XG5cbmV4cG9ydCBjbGFzcyBSZXNwb25zZU1hbmFnZXI8VD5cbntcbiAgICAvLyBjbGFzc1xuICAgIHN0YXRpYyBuZXdIYW5kbGVyPFQ+KHByb21pc2U6Q3VzdG9tUHJvbWlzZTxUPik6UmVzcG9uc2VIYW5kbGVyPFQ+XG4gICAge1xuICAgICAgICByZXR1cm4gbmV3IFJlc3BvbnNlTWFuYWdlcjxUPihwcm9taXNlKS5oYW5kbGVyO1xuICAgIH1cblxuXG4gICAgLy8gaW5zdGFuY2VcbiAgICBwcml2YXRlIGNvbnN0cnVjdG9yKHByaXZhdGUgcHJvbWlzZTpDdXN0b21Qcm9taXNlPFQ+KSB7XG4gICAgfVxuXG4gICAgcHVibGljIGhhbmRsZXIgPSAocmVzcG9uc2U6SVJlc3BvbnNlPFQ+KSA9PlxuICAgIHtcbiAgICAgICAgaWYgKHJlc3BvbnNlLmVycm9yKVxuICAgICAgICAgICAgdGhpcy5wcm9taXNlLmZhaWwocmVzcG9uc2UuZXJyb3IpO1xuXG4gICAgICAgIGlmIChyZXNwb25zZS5yZXN1bHQpXG4gICAgICAgICAgICB0aGlzLnByb21pc2UuZG9uZShyZXNwb25zZS5yZXN1bHQpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFJlc3BvbnNlRGlzcGF0Y2hlcjxUPlxue1xuICAgIC8vIGNsYXNzXG4gICAgc3RhdGljIGNyZWF0ZTxUPihoYW5kbGVyOlJlc3BvbnNlSGFuZGxlcjxUPik6UmVzcG9uc2VEaXNwYXRjaGVyPFQ+XG4gICAge1xuICAgICAgICByZXR1cm4gbmV3IFJlc3BvbnNlRGlzcGF0Y2hlcihoYW5kbGVyKTtcbiAgICB9XG4gICAgXG4gICAgXG4gICAgLy8gaW5zdGFuY2VcbiAgICBwcml2YXRlIGNvbnN0cnVjdG9yKHByaXZhdGUgaGFuZGxlcjpSZXNwb25zZUhhbmRsZXI8VD4pIHtcbiAgICB9XG5cbiAgICBwdWJsaWMgZGlzcGF0Y2hSZXN1bHQocmVzdWx0OlQpXG4gICAge1xuICAgICAgICB0aGlzLmhhbmRsZXIoe1xuICAgICAgICAgICAgcmVzdWx0XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIHB1YmxpYyBkaXNwYXRjaEVycm9yKGNvZGU6c3RyaW5nLGRhdGE/KVxuICAgIHtcbiAgICAgICAgdGhpcy5oYW5kbGVyKHtcbiAgICAgICAgICAgIGVycm9yOiB7XG4gICAgICAgICAgICAgICAgY29kZSxcbiAgICAgICAgICAgICAgICBkYXRhXG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn0iXX0=