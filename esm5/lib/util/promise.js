/**
 * @fileoverview added by tsickle
 * Generated from: lib/util/promise.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function IError() { }
if (false) {
    /** @type {?} */
    IError.prototype.code;
    /** @type {?|undefined} */
    IError.prototype.data;
}
/**
 * @record
 * @template T
 */
export function IPromise() { }
if (false) {
    /**
     * @param {?} resultHandler
     * @return {?}
     */
    IPromise.prototype.then = function (resultHandler) { };
    /**
     * @param {?} errorHandler
     * @return {?}
     */
    IPromise.prototype.catch = function (errorHandler) { };
}
/**
 * @template T
 */
var /**
 * @template T
 */
CustomPromise = /** @class */ (function () {
    function CustomPromise() {
    }
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} resultHandler
     * @return {THIS}
     */
    CustomPromise.prototype.then = /**
     * @template THIS
     * @this {THIS}
     * @param {?} resultHandler
     * @return {THIS}
     */
    function (resultHandler) {
        (/** @type {?} */ (this)).resultHandler = resultHandler;
        return (/** @type {?} */ (this));
    };
    /**
     * @template THIS
     * @this {THIS}
     * @param {?} errorHandler
     * @return {THIS}
     */
    CustomPromise.prototype.catch = /**
     * @template THIS
     * @this {THIS}
     * @param {?} errorHandler
     * @return {THIS}
     */
    function (errorHandler) {
        (/** @type {?} */ (this)).errorHandler = errorHandler;
        return (/** @type {?} */ (this));
    };
    /**
     * @param {?} result
     * @return {?}
     */
    CustomPromise.prototype.done = /**
     * @param {?} result
     * @return {?}
     */
    function (result) {
        if (this.resultHandler)
            this.resultHandler(result);
    };
    /**
     * @param {?} error
     * @return {?}
     */
    CustomPromise.prototype.fail = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        if (this.errorHandler)
            this.errorHandler(error);
    };
    return CustomPromise;
}());
/**
 * @template T
 */
export { CustomPromise };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.resultHandler;
    /**
     * @type {?}
     * @private
     */
    CustomPromise.prototype.errorHandler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvbWlzZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2Vhcm5iZXQtY29tbW9uLyIsInNvdXJjZXMiOlsibGliL3V0aWwvcHJvbWlzZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUFBLDRCQUlDOzs7SUFGRyxzQkFBWTs7SUFDWixzQkFBTTs7Ozs7O0FBTVYsOEJBSUM7Ozs7OztJQUZHLHVEQUFpRDs7Ozs7SUFDakQsdURBQTZDOzs7OztBQUdqRDs7OztJQUtJO0lBQWUsQ0FBQzs7Ozs7OztJQUVULDRCQUFJOzs7Ozs7SUFBWCxVQUFZLGFBQThCO1FBRXRDLG1CQUFBLElBQUksRUFBQSxDQUFDLGFBQWEsR0FBRyxhQUFhLENBQUM7UUFDbkMsT0FBTyxtQkFBQSxJQUFJLEVBQUEsQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBQ00sNkJBQUs7Ozs7OztJQUFaLFVBQWEsWUFBeUI7UUFFbEMsbUJBQUEsSUFBSSxFQUFBLENBQUMsWUFBWSxHQUFHLFlBQVksQ0FBQztRQUNqQyxPQUFPLG1CQUFBLElBQUksRUFBQSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRU0sNEJBQUk7Ozs7SUFBWCxVQUFZLE1BQVE7UUFFaEIsSUFBSSxJQUFJLENBQUMsYUFBYTtZQUNsQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBQ00sNEJBQUk7Ozs7SUFBWCxVQUFZLEtBQVk7UUFFcEIsSUFBSSxJQUFJLENBQUMsWUFBWTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFDTCxvQkFBQztBQUFELENBQUMsQUE1QkQsSUE0QkM7Ozs7Ozs7Ozs7SUExQkcsc0NBQXVDOzs7OztJQUN2QyxxQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgaW50ZXJmYWNlIElFcnJvclxue1xuICAgIGNvZGU6c3RyaW5nO1xuICAgIGRhdGE/O1xufVxuXG5leHBvcnQgdHlwZSBSZXN1bHRIYW5kbGVyPFQ+ID0gKHJlc3VsdDpUKSA9PiB2b2lkO1xuZXhwb3J0IHR5cGUgRXJyb3JIYW5kbGVyID0gKGVycm9yOklFcnJvcikgPT4gdm9pZDtcblxuZXhwb3J0IGludGVyZmFjZSBJUHJvbWlzZTxUPlxue1xuICAgIHRoZW4ocmVzdWx0SGFuZGxlcjpSZXN1bHRIYW5kbGVyPFQ+KTpJUHJvbWlzZTxUPjtcbiAgICBjYXRjaChlcnJvckhhbmRsZXI6RXJyb3JIYW5kbGVyKTpJUHJvbWlzZTxUPjtcbn1cblxuZXhwb3J0IGNsYXNzIEN1c3RvbVByb21pc2U8VD4gaW1wbGVtZW50cyBJUHJvbWlzZTxUPlxue1xuICAgIHByaXZhdGUgcmVzdWx0SGFuZGxlcjpSZXN1bHRIYW5kbGVyPFQ+O1xuICAgIHByaXZhdGUgZXJyb3JIYW5kbGVyOkVycm9ySGFuZGxlcjtcblxuICAgIGNvbnN0cnVjdG9yKCkge31cblxuICAgIHB1YmxpYyB0aGVuKHJlc3VsdEhhbmRsZXI6UmVzdWx0SGFuZGxlcjxUPilcbiAgICB7XG4gICAgICAgIHRoaXMucmVzdWx0SGFuZGxlciA9IHJlc3VsdEhhbmRsZXI7XG4gICAgICAgIHJldHVybiB0aGlzO1xuICAgIH1cbiAgICBwdWJsaWMgY2F0Y2goZXJyb3JIYW5kbGVyOkVycm9ySGFuZGxlcilcbiAgICB7XG4gICAgICAgIHRoaXMuZXJyb3JIYW5kbGVyID0gZXJyb3JIYW5kbGVyO1xuICAgICAgICByZXR1cm4gdGhpcztcbiAgICB9XG5cbiAgICBwdWJsaWMgZG9uZShyZXN1bHQ6VClcbiAgICB7XG4gICAgICAgIGlmICh0aGlzLnJlc3VsdEhhbmRsZXIpXG4gICAgICAgICAgICB0aGlzLnJlc3VsdEhhbmRsZXIocmVzdWx0KTtcbiAgICB9XG4gICAgcHVibGljIGZhaWwoZXJyb3I6SUVycm9yKVxuICAgIHtcbiAgICAgICAgaWYgKHRoaXMuZXJyb3JIYW5kbGVyKVxuICAgICAgICAgICAgdGhpcy5lcnJvckhhbmRsZXIoZXJyb3IpO1xuICAgIH1cbn0iXX0=